Steuerung:
	Aufgeben -> Escape
	Charakterdetails -> Zoom (Mausrad)
	Benutzen/Abbrechen -> Linksklick

Serveradresse:
	data/app.config -> Server_Address = <IP des Servers>

Beispiel:	
#FilePreferences
#Tue Jan 26 22:55:19 CET 2016
Login_Name=Tim
Server_Address=7.4.17.226
Login_RememberName=true
