	drop table if exists CHARACTER_CLASS_IDS;

    create table CHARACTER_CLASS_IDS (
        LINEUP_CHARACTER_CLASS_IDS_PK bigint not null,
        MAP_KEY_COORD binary(255) not null,
        MAP_VALUE_CHARACTER_CLASS_ID integer,
        primary key (LINEUP_CHARACTER_CLASS_IDS_PK, MAP_KEY_COORD)
    );
	
	drop table if exists COORDINATE;

    create table COORDINATE (
        COORD_ID bigint not null,
        ROW integer,
        COLUMN integer,
        primary key (COORD_ID)
    );
	
	drop table if exists LINEUP;
	
    create table LINEUP (
        LINEUP_ID bigint not null,
        KINGPOSITION bigint,
        IMAGEKEY varchar(255),
        TITLE varchar(255),
        DESCRIPTION varchar(255),
        ACCOUNTID binary,
        primary key (LINEUP_ID)
    );
	
	alter table CHARACTER_CLASS_IDS 
	add constraint FKi6vbagxgaiuik564hvn81v0wu 
	foreign key (LINEUP_CHARACTER_CLASS_IDS_PK) 
	references LINEUP;

	alter table LINEUP 
	add constraint FKj5gsov5cc5o5ktvii9voi37al 
	foreign key (KINGPOSITION) 
	references COORDINATE;