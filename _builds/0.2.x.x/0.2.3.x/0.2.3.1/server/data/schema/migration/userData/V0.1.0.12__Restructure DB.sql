drop table if exists ACCOUNT;
drop table if exists ACCOUNT_SETTINGS;
drop table if exists COORDINATE;
drop table if exists enhancement_plans;
drop table if exists EnhancementPlan;
drop table if exists EnhancementSlot;
drop table if exists ITEM;
drop table if exists Lineup;
drop table if exists lineup_character_class_ids;
drop table if exists lineup_enhancements;
drop table if exists LOGINSESSION;
drop table if exists OWNERSHIP;
drop table if exists PURCHASE;
drop table if exists TRANSACTION;

create sequence hibernate_sequence start with 1 increment by 1;

    create table ACCOUNT (
        ACCOUNT_ID binary not null,
        NAME varchar(255),
        EMAIL varchar(255),
        PASSWORD_HASH varchar(255),
        STATUS integer,
        ROLE integer,
        BANNED_UNTIL timestamp,
        CASH_CURRENCY_BALANCE integer,
        INGAME_CURRENCY_BALANCE integer,
        primary key (ACCOUNT_ID)
    );

    create table ACCOUNT_SETTINGS (
        ACCOUNT_ID binary not null,
        SETTING_KEY varchar(255) not null,
        SETTING_VALUE varchar(255),
        primary key (ACCOUNT_ID, SETTING_KEY)
    );

    create table enhancement_plans (
        id binary not null,
        enhancementMap integer,
        enhancement_plan_id bigint not null,
        primary key (id, enhancement_plan_id)
    );

    create table EnhancementPlan (
        enhancement_plan_id binary not null,
        primary key (enhancement_plan_id)
    );

    create table EnhancementSlot (
        id bigint not null,
        rank integer not null,
        type integer,
        primary key (id)
    );

    create table ITEM (
        ITEM_ID bigint not null,
        NAME varchar(255),
        primary key (ITEM_ID)
    );

    create table Lineup (
        id bigint not null,
        accountId binary,
        description varchar(255),
        imageKey varchar(255),
        kingPosition binary(255),
        title varchar(255),
        primary key (id)
    );

    create table lineup_character_class_ids (
        join_table_id bigint not null,
        characterClassIds integer,
        coordinate binary(255) not null,
        primary key (join_table_id, coordinate)
    );

    create table lineup_enhancements (
        lineup_enhancements_id bigint not null,
        enhancements binary not null,
        enhancements_KEY binary(255) not null,
        primary key (lineup_enhancements_id, enhancements_KEY)
    );

    create table LOGINSESSION (
        SESSION_ID binary not null,
        ACCOUNTID binary,
        LOGGEDIN timestamp,
        LOGGEDOUT timestamp,
        LOGIN_METHOD integer,
        primary key (SESSION_ID)
    );

    create table OWNERSHIP (
        OWNERSHIP_ID binary not null,
        ACCOUNTID binary,
        ITEM bigint,
        PURCHASE binary,
        primary key (OWNERSHIP_ID)
    );

    create table PURCHASE (
        PURCHASE_ID binary not null,
        ACCOUNTID binary,
        AMOUNT float,
        ITEM bigint,
        CURRENCY integer,
        primary key (PURCHASE_ID)
    );

    create table TRANSACTION (
        TRANSACTION_ID binary not null,
        ACCOUNTID binary,
        PRICE float,
        CURRENCY varchar(255),
        DATE timestamp,
        primary key (TRANSACTION_ID)
    );

    alter table ACCOUNT 
        add constraint UKo00jv454sym2ewisgsolbap2y unique (NAME, EMAIL);

    alter table OWNERSHIP 
        add constraint UK1rhymu83wr4bomimv9w9pbbfn unique (ACCOUNTID, ITEM);

    alter table ACCOUNT_SETTINGS 
        add constraint FKn3m1m5pllqdh7in7348scl2yu 
        foreign key (ACCOUNT_ID) 
        references ACCOUNT;

    alter table enhancement_plans 
        add constraint FKi8mgmkcoch13gkrfl4oudbq3h 
        foreign key (enhancement_plan_id) 
        references EnhancementSlot;

    alter table enhancement_plans 
        add constraint FKrr95ok88h1ni2d09n0jvi3msf 
        foreign key (id) 
        references EnhancementPlan;

    alter table lineup_character_class_ids 
        add constraint FKp0ul8bbqyty2diowvd9hx3hdg 
        foreign key (join_table_id) 
        references Lineup;

    alter table lineup_enhancements 
        add constraint FK1x6ui0cnsl587cixu4niyij5i 
        foreign key (enhancements) 
        references EnhancementPlan;

    alter table lineup_enhancements 
        add constraint FKgks8r1wx3y9t384adwyd6kqo2 
        foreign key (lineup_enhancements_id) 
        references Lineup;

    alter table OWNERSHIP 
        add constraint FK7ju6creb61e0g5fgm0juj075w 
        foreign key (ITEM) 
        references ITEM;

    alter table OWNERSHIP 
        add constraint FK5be94jcnby456f8ov1qhhv5xt 
        foreign key (PURCHASE) 
        references PURCHASE;

    alter table PURCHASE 
        add constraint FKpelpn34t9iy5ex14jbkvhxt0r 
        foreign key (ITEM) 
        references ITEM;
