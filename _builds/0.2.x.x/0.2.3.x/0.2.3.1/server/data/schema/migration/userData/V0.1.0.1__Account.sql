create table ACCOUNT (
        ACCOUNT_ID varbinary not null,
        NAME varchar(255),
        EMAIL varchar(255),
        PASSWORD_HASH varchar(255),
        PASSWORD_SALT varchar(255),
        STATUS integer,
        ROLE integer,
        BANNED_UNTIL timestamp,
        CASH_CURRENCY_BALANCE integer,
        INGAME_CURRENCY_BALANCE integer,
        primary key (ACCOUNT_ID)
    );
	
alter table ACCOUNT 
	add constraint UKo00jv454sym2ewisgsolbap2y unique (NAME, EMAIL);