create table LOGINSESSION (
        SESSION_ID varbinary not null,
        ACCOUNTID binary,
        LOGGEDIN timestamp,
        LOGGEDOUT timestamp,
        primary key (SESSION_ID)
    );