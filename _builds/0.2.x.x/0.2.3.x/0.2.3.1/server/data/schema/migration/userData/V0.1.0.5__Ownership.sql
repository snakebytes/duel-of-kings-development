    create table OWNERSHIP (
        OWNERSHIP_ID binary not null,
        ACCOUNTID binary,
        ITEM bigint,
        PURCHASE binary,
        primary key (OWNERSHIP_ID)
    );