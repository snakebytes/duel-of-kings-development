    create table PURCHASE (
        PURCHASE_ID binary not null,
        ACCOUNTID binary,
        AMOUNT float,
        ITEM bigint,
        CURRENCY integer,
        primary key (PURCHASE_ID)
    );