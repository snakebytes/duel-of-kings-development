    create table TRANSACTION (
        TRANSACTION_ID binary not null,
        ACCOUNTID binary,
        PRICE float,
        CURRENCY varchar(255),
        DATE timestamp,
        primary key (TRANSACTION_ID)
    );