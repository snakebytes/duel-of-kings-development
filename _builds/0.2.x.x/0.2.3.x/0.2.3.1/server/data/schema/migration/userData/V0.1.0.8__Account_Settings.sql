    create table ACCOUNT_SETTINGS (
        ACCOUNT_ID binary not null,
        SETTING_KEY varchar(255) not null,
        SETTING_VALUE varchar(255),
        primary key (ACCOUNT_ID, SETTING_KEY)
    );
	
	alter table ACCOUNT_SETTINGS 
	add constraint FKn3m1m5pllqdh7in7348scl2yu 
	foreign key (ACCOUNT_ID) 
	references ACCOUNT;