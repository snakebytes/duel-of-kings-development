    create table LINEUP (
        LINEUP_ID bigint not null,
        KINGPOSITION bigint,
        IMAGEKEY varchar(255),
        TITLE varchar(255),
        DESCRIPTION varchar(255),
        ACCOUNTID binary,
        primary key (LINEUP_ID)
    );
	
	create table COORDINATE (
	LINEUP_ID bigint not null,
	ROW integer,
	COLUMN integer,
	idx binary(255),
	primary key (LINEUP_ID)
    );
	
	alter table COORDINATE 
		add constraint FKj00cc7e0jmp1me3rug3wpig5c 
		foreign key (LINEUP_ID) 
		references LINEUP;

	alter table LINEUP 
		add constraint FKj5gsov5cc5o5ktvii9voi37al 
		foreign key (KINGPOSITION) 
		references COORDINATE;