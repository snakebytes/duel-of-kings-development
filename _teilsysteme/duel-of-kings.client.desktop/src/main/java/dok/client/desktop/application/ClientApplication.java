package dok.client.desktop.application;

import java.awt.Dimension;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.ui.controllers.BotGameCreationView;
import dok.client.desktop.ui.controllers.EndScreen;
import dok.client.desktop.ui.controllers.GameView;
import dok.client.desktop.ui.controllers.LineupSelectionView;
import dok.client.desktop.ui.controllers.LoadingScreen;
import dok.client.desktop.ui.controllers.MainView;
import dok.client.desktop.ui.controllers.SplashView;
import dok.client.game.ClientGame;
import dok.client.game.impl.BotGame;
import dok.client.game.impl.DuelGame;
import dok.client.services.ApplicationImageService;
import dok.client.services.GameDataPersistenceService;
import dok.client.services.GameImageService;
import dok.client.services.GameService;
import dok.client.services.LineupService;
import dok.client.services.LoginService;
import dok.client.services.NetworkService;
import dok.client.services.PreferencesService;
import dok.client.services.QueuingService;
import dok.client.services.UserDataPersistenceService;
import dok.client.services.impl.ImageProvider;
import dok.client.services.impl.PrototypeLocalizationProvider;
import dok.commons.LocalizationService;
import dok.commons.impl.ObservableServiceRegistry;
import dok.commons.model.Lineup;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.events.ConnectionClosedEvent;
import dok.commons.network.api.events.ConnectionEstablishedEvent;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.ui.components.TextAreaLogAppender;
import dok.game.model.AILevel;
import dok.game.model.ComputerPlayer;
import dok.game.model.GameResult;
import dok.game.model.Player;
import dok.game.model.UserPlayer;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.network.model.WorkDonePacket;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Brings together all pieces of the client application,
 * containing services and user interfaces.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public final class ClientApplication extends Application implements StateMachineCallback, ConnectionListener, GameService {

	// Class Constants

	public static final String VERSION = "0.2.7.1";

	private static ClientApplication instance;
	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private ObservableServiceRegistry serviceRegistry;

	private SplashView splashView;
	private MainView mainView;
	private GameView gameView;
	private BotGameCreationView botGameCreationView;
	private LineupSelectionView lineupSelectionView;
	private LoadingScreen loadingScreen;

	// Constructor(s)

	public ClientApplication() throws Exception {

		//Initialize the "singleton" instance for testing purposes
		setInstance(this);

	}

	// Initialization

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {

			// Initialize Service Registry

			Set<Class<?>> services = new HashSet<>();
			services.add(Stage.class); // Main window
			services.add(UserDataPersistenceService.class); // Service responsible for persisting and managing persistent data
			services.add(LocalizationService.class); // Service responsible for translations
			services.add(LineupService.class); // Service responsible for managing the player's lineup(s)
			services.add(GameDataPersistenceService.class); // Service responsible for providing character & ability information
			services.add(ApplicationImageService.class); // Service responsible for providing general UI images
			services.add(GameImageService.class); // Service responsible for providing game-related images
			services.add(ClientGame.class); // Service which manages the currently active game
			services.add(LoginService.class); // Service which manages the active user and provides the functionality to send a login request to the server
			services.add(PreferencesService.class); // Service responsible for storing and providing user preference data
			services.add(Preferences.class); // Service responsible for storing and providing user preference data, soon to be deprecated
			services.add(QueuingService.class); // Service responsible for managing queue status and providing functionality to request leaving or joining a certain game queue
			services.add(NetworkService.class); // Connection to server

			serviceRegistry = new ObservableServiceRegistry(services);

			// Load localization
			try {

				getServiceRegistry().registerServiceProvider(new PrototypeLocalizationProvider(), LocalizationService.class);

				// Debug
				Enumeration<String> keys = getLocalizationProvider().getResourceBundle().getKeys();
				while (keys.hasMoreElements()) {
					logger.trace(keys.nextElement());
				}

			} catch (IOException e) {

				logger.log(Level.FATAL, "LocalizationProvider could not be initialized", e);
				throw e;

			}

			// Initialize Application Image Service
			getServiceRegistry().registerServiceProvider(new ImageProvider(), ApplicationImageService.class);

			// Create the debug display if needed
			if (getParameters().getRaw().contains("-debug")) {

				//JavaFX
//				// Initialize the textarea to display the log in
//				TextArea consoleDisplay = new TextArea();
//				consoleDisplay.setEditable(false);
//				consoleDisplay.setWrapText(true);
//				TextAreaLogAppender.setJavaFXLogDisplay(consoleDisplay);
//
//				// Change stanard output and errors streams
//				PrintStream outputStream = new PrintStream(new OutputStream() {
//
//					@Override
//					public void write(int b) throws IOException {
//						Platform.runLater(() -> consoleDisplay.appendText(String.valueOf((char) b)));
//					}
//
//				}, true);
//				System.setErr(outputStream);
//				System.setOut(outputStream);
//
//				// Initialize the log window
//				Stage logWindow = new Stage();
//				try {
//					logWindow.getIcons().add(getServiceRegistry().getServiceProvider(ApplicationImageService.class).getImage("Icon"));
//				} catch (NoSuchElementException e) {
//					throw e;
//				}
//				logWindow.initOwner(primaryStage);
//				logWindow.setScene(new Scene(consoleDisplay));
//				logWindow.setTitle("Log");
//				logWindow.setAlwaysOnTop(true);
//				logWindow.show();

				// Swing
				SwingUtilities.invokeLater(() -> {
					// Create window content
					JFrame frame = new JFrame();
					frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
					frame.setTitle("Log");
					frame.setMinimumSize(new Dimension(800, 600));
					try {
						frame.setIconImage(ImageIO.read(getClass().getClassLoader().getResourceAsStream("images/icon.png")));
					} catch (Exception e) {
						logger.warn("LoggerAppIcon could not be initialized", e);
					}

					JTextArea textArea = new JTextArea();
					JScrollPane scrollPane = new JScrollPane(textArea);

					// Define logging display
					TextAreaLogAppender.setSwingLogDisplay(textArea);

					// Change stanard output and errors streams
					PrintStream outputStream = new PrintStream(new OutputStream() {

						@Override
						public void write(int b) throws IOException {
							textArea.append(String.valueOf((char) b));
						}

					}, true);
					System.setErr(outputStream);
					System.setOut(outputStream);

					// Show the log window
					frame.setContentPane(scrollPane);
					frame.setVisible(true);
				});
			}

			// Initialize window parameters
			getServiceRegistry().registerServiceProvider(primaryStage, Stage.class);
			primaryStage.setTitle("Duel of Kings - Version " + VERSION);
			primaryStage.initStyle(StageStyle.UNDECORATED);
			try {
				primaryStage.getIcons().add(getServiceRegistry().getServiceProvider(ApplicationImageService.class).getImage("Icon"));
			} catch (NoSuchElementException e) {
				throw e;
			}

			// Define window size
			primaryStage.setWidth(800);
			primaryStage.setHeight(480);
			primaryStage.setMaximized(true);
			primaryStage.centerOnScreen();
			primaryStage.setResizable(false);

			// Initialize the starting view
			setSplashView(new SplashView());
			primaryStage.setScene(new Scene(getSplashView()));

			// Define global css
			primaryStage.getScene().getStylesheets().add(getClass().getResource("/ui/css/General.css").toExternalForm());

			// Show the main window
			primaryStage.show();

			// Update splash fonts
			getSplashView().updateFonts();

			// Update splash insets
			getSplashView().updateSpacing();

			// Play splash animation
			getSplashView().playInAnimation();

			// Create Initialization Provider
			Task<?> task = new ClientInitializationProvider(this);
			task.setOnSucceeded(onInitializationSucceeded);
			task.setOnCancelled(onInitializationFailed);
			task.setOnFailed(onInitializationFailed);

			// Start Initialization
			Thread t = new Thread(task);
			t.setDaemon(true);
			t.setName("InitializationThread");
			t.start();

		} catch (Exception e) {

			logger.log(Level.FATAL, "Application could not be started!", e);
			new Alert(AlertType.ERROR, "Anwendung konnte nicht gestartet werden (" + e.getLocalizedMessage() + ")").showAndWait();
			Platform.exit();

		}
	}

	@Override
	public void init() {
		// TODO
	}

	@Override
	public void stop() {
		logger.entry();
		// Deregister services
		getServiceRegistry().deregisterAll();
		logger.exit();
	}

	// Methods

	/**
	 * Indirectly invoked by the server to start the client-side version of the game.
	 */
	public void startDuelGame(List<Player> players, Player self) {
		if (getActiveGame() != null) throw new IllegalStateException();
		else {
			try {

				// Lookup main window
				Stage mainWindow;
				try {
					mainWindow = getServiceRegistry().getServiceProvider(Stage.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no main window registered", e);
					throw e;
				}

				// Display loading screen
				Platform.runLater(() -> mainWindow.getScene().setRoot(getLoadingScreen()));

				// Lookup server connection
				NetworkService networkService = null;
				try {
					networkService = getServiceRegistry().getServiceProvider(NetworkService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no active connection", e);
					throw e;
				}

				// Lookup localization service
				LocalizationService localizationService = null;
				try {
					localizationService = getServiceRegistry().getServiceProvider(LocalizationService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no localization service registered", e);
					throw e;
				}

				// Lookup game image service
				GameImageService gameImageService = null;
				try {
					gameImageService = getServiceRegistry().getServiceProvider(GameImageService.class);
					gameImageService.preloadGameGraphics();
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no game image service registered", e);
					throw e;
				}

				// Lookup character class service
				CharacterDataService characterClassService = null;
				try {
					characterClassService = getServiceRegistry().getServiceProvider(GameDataPersistenceService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no character class service registered", e);
					throw e;
				}

				// Lookup ability class service
				AbilityDataService abilityClassService = null;
				try {
					abilityClassService = getServiceRegistry().getServiceProvider(GameDataPersistenceService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no ability class service registered", e);
					throw e;
				}

				// Initialize Game and GameView
				setGameView(new GameView(self.getPlayerId(), localizationService, gameImageService, characterClassService, abilityClassService));
				setActiveGame(new DuelGame(networkService.getActiveConnection(), this, characterClassService, abilityClassService, getGameView(), self, players));
				getGameView().setGameService(getActiveGame());
				getGameView().setInputConsumer(getActiveGame().getInputProvider());
				getGameView().setPlayerNames(((UserPlayer) self).getDisplayName(), ((UserPlayer) getActiveGame().getFollowingPlayer(self.getPlayerId())).getDisplayName());
				getGameView().initializeFieldDisplays();

				// Display the GameView
				Platform.runLater(() -> mainWindow.getScene().setRoot(getGameView()));

				// Start the Game
				Thread t = new Thread(getActiveGame());
				t.setDaemon(true);
				t.setName("DuelGameThread");
				t.start();

				networkService.getActiveConnection().sendPacket(new WorkDonePacket());

			} catch (Exception e) {

				logger.log(Level.FATAL, "Game could not be created", e);
				// Lookup main window
				try {
					Stage mainWindow = getServiceRegistry().getServiceProvider(Stage.class);
					Platform.runLater(() -> mainWindow.getScene().setRoot(getLineupSelectionView()));
				} catch (NoSuchElementException e2) {
					logger.log(Level.ERROR, "There is no main window registered", e2);
					Platform.exit();
				}

			}

		}
	}

	/**
	 *
	 */
	public void startBotGame(AILevel aiLevel, List<Lineup> lineups) throws IllegalStateException {
		if (getActiveGame() != null) throw new IllegalStateException();
		else {
			try {
				Stage mainWindow;
				// Lookup main window
				try {
					mainWindow = getServiceRegistry().getServiceProvider(Stage.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no main window registered", e);
					throw e;
				}

				// Display loading screen
				Platform.runLater(() -> mainWindow.getScene().setRoot(getLoadingScreen()));

				// Lookup localization service
				LocalizationService localizationService = null;
				try {
					localizationService = getServiceRegistry().getServiceProvider(LocalizationService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no localization service registered", e);
					throw e;
				}

				// Lookup login service
				LoginService loginService = null;
				try {
					loginService = getServiceRegistry().getServiceProvider(LoginService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no login service registered", e);
					throw e;
				}

				// Lookup game image service
				GameImageService gameImageService = null;
				try {
					gameImageService = getServiceRegistry().getServiceProvider(GameImageService.class);
					gameImageService.preloadGameGraphics();
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no game image service registered", e);
					throw e;
				}

				// Preload needed character graphics
				for (Lineup lineup : lineups) {
					for (int row = 0; row < lineup.getRowCount(); row++) {
						for (int col = 0; col < lineup.getColCount(); col++) {
							gameImageService.preloadCharacterClassImage(lineup.getCharacterClassIdAt(new MutablePair<>(row,col)));
							// TODO: Abilities durch Verbesserungen ersetzen
							//gameImageService.preloadAbilityImage(lineup.getAbilityClassIdAt(new Pair<>(row,col)));
						}
					}
				}

				// Lookup game data persistence service
				GameDataPersistenceService gameDataPersistenceService = null;
				try {
					gameDataPersistenceService = getServiceRegistry().getServiceProvider(GameDataPersistenceService.class);
				} catch (NoSuchElementException e) {
					logger.log(Level.ERROR, "There is no character class service registered", e);
					throw e;
				}

				// Initialize Game and GameView
				setGameView(new GameView(localizationService, gameImageService, gameDataPersistenceService, gameDataPersistenceService));
				setActiveGame(new BotGame(this, getGameView(), gameDataPersistenceService, gameDataPersistenceService, aiLevel, new ArrayList<Lineup>(lineups), loginService.getActiveSession().getAccountId(), loginService.getActiveUserName()));
				getGameView().setPlayer(getActiveGame().getPlayers().get(0).getPlayerId());
				String self = ((UserPlayer) getActiveGame().getPlayers().get(0)).getDisplayName();
				String opponent = localizationService.getText("AILevel." + ((ComputerPlayer) getActiveGame().getPlayers().get(1)).getStrength());
				getGameView().setPlayerNames(self, opponent);
				getGameView().setGameService(getActiveGame());
				getGameView().setInputConsumer(getActiveGame().getInputProvider());
				getGameView().initializeFieldDisplays();

				// Show the GameView
				Platform.runLater(() -> mainWindow.getScene().setRoot(getGameView()));

				// Start the Game
				Thread t = new Thread(getActiveGame());
				t.setDaemon(true);
				t.setName("BotGameThread");
				t.start();

			} catch (Exception e) {

				logger.log(Level.FATAL, "Game could not be created", e);

				// Lookup main window
				try {
					Stage mainWindow = getServiceRegistry().getServiceProvider(Stage.class);
					Platform.runLater(() -> mainWindow.getScene().setRoot(getBotGameCreationView()));
				} catch (NoSuchElementException e2) {
					logger.log(Level.ERROR, "There is no main window registered", e2);
					Platform.exit();
				}

			}

		}
	}

	@Override
	public void onFinish(Object result) {

		// Deregister game
		try {
			setActiveGame(null);
			logger.info("Game has been terminated");
		} catch (Exception e) {
			logger.error("Game could not be terminated", e);
		}

		try {
			// Lookup main window
			Stage mainWindow = getServiceRegistry().getServiceProvider(Stage.class);

			// Switch to endscreen if possible, else go to main menu
			Platform.runLater(() -> {
				try {
					mainWindow.getScene().setRoot(new EndScreen((GameResult) result));
				} catch (IOException e) {
					Platform.runLater(() -> mainWindow.getScene().setRoot(getMainView()));
					logger.catching(e);
				}
			});
		} catch (Exception e) {
			logger.log(Level.ERROR, "Endscreen could not be displayed", e);
			Platform.exit();
		}
	}

	// <--- Connection Listener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		logger.debug(connectionEvent);
		// Manages what happens, if the active connection has been closed
		// This is most likely to happen when a network error has occurred (e.g. server became unreachable)
		if (connectionEvent instanceof ConnectionClosedEvent) {
			getServiceRegistry().deregisterServiceProvider(connectionEvent.getSource());
			//new Alert(AlertType.WARNING, getServiceRegistry().getServiceProvider(LocalizationService.class).getText("Application.Warnings.ConnectionLost")).showAndWait();
			Platform.exit();
		} else if (connectionEvent instanceof ConnectionEstablishedEvent) {
			logger.debug("Starting to read packets ...");
			Thread t = new Thread(connectionEvent.getSource());
			t.setDaemon(true);
			t.setName("ConnectionThread");
			t.start();
		}
	}

	// Event Handling

	private final EventHandler<WorkerStateEvent> onInitializationSucceeded = (WorkerStateEvent event) -> {
		getSplashView().setInitializationFinished(true);
	};

	private final EventHandler<WorkerStateEvent> onInitializationFailed = (WorkerStateEvent event) -> {
		Platform.exit();
	};

	// Getter & Setter

	public static ClientApplication getInstance() {
		return instance;
	}

	private static void setInstance(ClientApplication instance) {
		ClientApplication.instance = instance;
	}

	public ClientGame getActiveGame() {
		try {
			return getServiceRegistry().getServiceProvider(ClientGame.class);
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	private void setActiveGame(ClientGame activeGame) throws Exception {
		if (activeGame == null) {
			getServiceRegistry().deregisterServiceProvider(ClientGame.class);
		} else if (getServiceRegistry().getServiceProvider(ClientGame.class) != null) {
			throw new IllegalStateException("There already is an active game");
		} else {
			try {
				getServiceRegistry().registerServiceProvider(activeGame, ClientGame.class);
			} catch (Exception e) {
				logger.log(Level.WARN, "GameCould not be created", e);
				throw e;
			}
		}
	}

	public MainView getMainView() {
		return mainView;
	}

	public void setMainView(MainView mainView) {
		this.mainView = mainView;
	}

	public GameView getGameView() {
		return gameView;
	}

	public void setGameView(GameView gameView) {
		this.gameView = gameView;
	}

	public LoadingScreen getLoadingScreen() {
		return loadingScreen;
	}

	public void setLoadingScreen(LoadingScreen loadingScreen) {
		this.loadingScreen = loadingScreen;
	}

	public ObservableServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	public void setServiceRegistry(ObservableServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	public Stage getMainWindow() {
		try {
			return getServiceRegistry().getServiceProvider(Stage.class);
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public LocalizationService getLocalizationProvider() {
		try {
			return getServiceRegistry().getServiceProvider(LocalizationService.class);
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public SplashView getSplashView() {
		return splashView;
	}

	public void setSplashView(SplashView splashView) {
		this.splashView = splashView;
	}

	public BotGameCreationView getBotGameCreationView() {
		return botGameCreationView;
	}

	public void setBotGameCreationView(BotGameCreationView botGameCreationView) {
		this.botGameCreationView = botGameCreationView;
	}

	public LineupSelectionView getLineupSelectionView() {
		return lineupSelectionView;
	}

	public void setLineupSelectionView(LineupSelectionView lineupSelectionView) {
		this.lineupSelectionView = lineupSelectionView;
	}

}
