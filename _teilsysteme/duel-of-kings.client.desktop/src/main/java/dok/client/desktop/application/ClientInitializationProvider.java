package dok.client.desktop.application;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.ui.controllers.BotGameCreationView;
import dok.client.desktop.ui.controllers.LineupSelectionView;
import dok.client.desktop.ui.controllers.LoadingScreen;
import dok.client.desktop.ui.controllers.MainView;
import dok.client.services.GameDataPersistenceService;
import dok.client.services.GameImageService;
import dok.client.services.LineupService;
import dok.client.services.LoginService;
import dok.client.services.NetworkService;
import dok.client.services.PreferencesService;
import dok.client.services.QueuingService;
import dok.client.services.UserDataPersistenceService;
import dok.client.services.impl.GameDataPersistenceProvider;
import dok.client.services.impl.ImageProvider;
import dok.client.services.impl.LineupProvider;
import dok.client.services.impl.LoginProvider;
import dok.client.services.impl.NetworkProvider;
import dok.client.services.impl.PreferencesProvider;
import dok.client.services.impl.QueuingProvider;
import dok.client.services.impl.UserDataPersistenceProvider;
import dok.commons.impl.MultiTask;
import dok.commons.impl.SubTask;

public class ClientInitializationProvider extends MultiTask {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private final ClientApplication application;

	// Constructor(s)

	public ClientInitializationProvider(ClientApplication application) {
		super();
		logger.entry();

		// Define attributes
		this.application = application;

		// Add Tasks
		getRemainingTasks().add(userDataPersistenceInitialization);
		getRemainingTasks().add(gameDataPersistenceInitialization);
		getRemainingTasks().add(preferenceServiceInitialization);
		getRemainingTasks().add(networkServiceInitialization);
		getRemainingTasks().add(loginServiceInitialization);
		getRemainingTasks().add(queuingServiceInitialization);
		getRemainingTasks().add(gameImageServiceInitialization);
		getRemainingTasks().add(lineupServiceInitialization);
		getRemainingTasks().add(guiInitialization);

		logger.exit(this);
	}

	// Initializers

	private final SubTask<Object> userDataPersistenceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			// Register ability class service
			getApplication().getServiceRegistry().registerServiceProvider(new UserDataPersistenceProvider(), UserDataPersistenceService.class);

			logger.info("UserDataPersistenceService has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> gameDataPersistenceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			// Register ability class service
			getApplication().getServiceRegistry().registerServiceProvider(new GameDataPersistenceProvider(), GameDataPersistenceService.class);

			logger.info("GameDataPersistenceService has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> preferenceServiceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			PreferencesService prefService = new PreferencesProvider();
			getApplication().getServiceRegistry().registerServiceProvider(prefService, PreferencesService.class);
			getApplication().getServiceRegistry().registerServiceProvider(prefService.getPreferences(), Preferences.class);
			logger.info("Preference Service has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> networkServiceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			NetworkService networkService = new NetworkProvider(getApplication());
			getApplication().getServiceRegistry().registerServiceProvider(networkService, NetworkService.class);

			logger.info("Network Connection has been established");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> loginServiceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			getApplication().getServiceRegistry().registerServiceProvider(new LoginProvider(), LoginService.class);
			logger.info("Login Service has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> queuingServiceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			getApplication().getServiceRegistry().registerServiceProvider(new QueuingProvider(getApplication()), QueuingService.class);
			logger.info("Queuing Service has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> lineupServiceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			// Register the lineup service
			getApplication().getServiceRegistry().registerServiceProvider(new LineupProvider(), LineupService.class);

			logger.info("Lineup Service has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> gameImageServiceInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			// Register the application image service
			getApplication().getServiceRegistry().registerServiceProvider(new ImageProvider(), GameImageService.class);

			logger.info("Image Service has been initialized");

			return logger.exit(null);
		}

	};

	private final SubTask<Object> guiInitialization = new SubTask<Object>() {

		@Override
		protected Void call() throws Exception {
			logger.entry();

			// Initialize the GUI
			try {

				try {

					// Create the main view
					getApplication().setMainView(new MainView());
					getApplication().getServiceRegistry().getServiceProvider(LoginService.class).addListener(getApplication().getMainView());

					// Create the loading screen
					getApplication().setLoadingScreen(new LoadingScreen());

					// Create LineupSelectionView
					getApplication().setLineupSelectionView(new LineupSelectionView());
					getApplication().getServiceRegistry().getServiceProvider(LineupService.class).addListener(getApplication().getLineupSelectionView());

					// Create BotGameCreationView
					getApplication().setBotGameCreationView(new BotGameCreationView());
					getApplication().getServiceRegistry().getServiceProvider(LineupService.class).addListener(getApplication().getBotGameCreationView());


					logger.info("GUI has been initialized");

				} catch (NoSuchElementException e) {

					logger.log(Level.FATAL, "Requested Service has not been initialized");
					throw e;

				}

			} catch (IOException e) {

				logger.log(Level.FATAL, "GUI could not be initialized", e);
				throw e;

			}

			return logger.exit(null);
		}

	};

	// Getter & Setter

	public ClientApplication getApplication() {
		return application;
	}

}
