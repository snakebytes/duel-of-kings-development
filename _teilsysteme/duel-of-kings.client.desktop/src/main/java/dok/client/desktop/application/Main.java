package dok.client.desktop.application;

import javafx.application.Application;

public abstract class Main {

	public static void main(String[] args) {
		Application.launch(ClientApplication.class, args);
	}

}
