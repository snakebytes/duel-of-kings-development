package dok.client.desktop.ui;

import dok.client.desktop.application.ClientApplication;
import javafx.scene.layout.Region;

public abstract class UIConstants {

	public static final double TARGET_WINDOW_WIDTH = 1920;
	public static final double TARGET_WINDOW_HEIGHT = 1080;

	public static double getUIScalingFactor(Region region) {
		return ((region.getWidth() / TARGET_WINDOW_WIDTH) + (region.getHeight() / TARGET_WINDOW_HEIGHT)) / 2;
	}

	public static double getGlobalScalingFactor() {
		return ((ClientApplication.getInstance().getMainWindow().getWidth() / TARGET_WINDOW_WIDTH)
				+ (ClientApplication.getInstance().getMainWindow().getHeight() / TARGET_WINDOW_HEIGHT))
				/ 2;
	}

}
