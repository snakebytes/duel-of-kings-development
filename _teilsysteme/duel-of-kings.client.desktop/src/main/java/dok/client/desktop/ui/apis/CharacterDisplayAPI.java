package dok.client.desktop.ui.apis;

public interface CharacterDisplayAPI {

	public enum CharacterDisplayHighlightState {
		LEGAL_TARGET,
		PRIMARY_TARGET_ALLY,
		PRIMARY_TARGET_OPPONENT,
		SECONDARY_TARGET_ALLY,
		SECONDARY_TARGET_OPPONENT
	}

	public void addCharacterHighlightingLayer(CharacterDisplayHighlightState state);
	public void removeUpmostCharacterHighlightingLayer();
	public void clearCharacterHighlightingLayers();

	public void enableSourceHighlighting();
	public void disableSourceHighlighting();

	public void enableTargetHighlighting();
	public void disableTargetHighlighting();

}
