package dok.client.desktop.ui.apis;

import java.util.UUID;

import dok.client.desktop.ui.controllers.CharacterDisplay;

public interface GameViewAPI {

	public CharacterDisplay getCharacterDisplayByCharacterId(UUID characterId);

}
