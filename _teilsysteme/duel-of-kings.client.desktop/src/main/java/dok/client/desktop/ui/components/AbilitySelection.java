package dok.client.desktop.ui.components;

import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.ui.UIConstants;
import dok.client.services.AbilityClassImageService;
import dok.commons.LocalizationService;
import dok.game.model.Ability;
import dok.game.model.Character;
import dok.game.model.GameStateModel;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.util.CharacterStateUtils;
import javafx.scene.Node;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import jfxtras.scene.layout.CircularPane;

public final class AbilitySelection extends CircularPane {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();
	// Sizing Constants
	private static final double diameterModifier = 1.4; // Desired diameter / parent Size = Modifier
	private static final double inactiveSkillSizeModifier = 0.25; // Icon Size / Parent Size = Modifier
	private static final double activeSkillSizeModifier = 0.33; // Icon Size / Parent Size = Modifier

	// Attributes

	private final Consumer<Object> consumer;

	// Constructor(s)

	public AbilitySelection(
			Character character,
			double parentSize,
			GameStateModel model,
			AbilityClassImageService abilityClassImageService,
			LocalizationService localizationService,
			AbilityDataService abilityClassService,
			CharacterDataService characterClassService,
			Consumer<Object> consumer)
	{
		super();
		logger.debug("{} created for {} with parentSize={}", getClass().getSimpleName(), character, parentSize);

		// Initialize attributes with parameters
		this.consumer = consumer;

		// Initialize Styling & Layout
		setAnimationInterpolation(CircularPane::animateOverTheArcWithFade);
		setDiameter(parentSize*diameterModifier);
		setChildrenAreCircular(true);
		setClipAwayExcessWhitespace(false);

		// Debug & Testing
		//setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
		//setShowDebug(Color.GREENYELLOW);
		//setStartAngle(270.);



		// Add abilities
		for (Ability abil : character.getAbilities()) {
			logger.debug("Adding {} to {}", abil, this);

			// Create the icon
			ImageView abilityView = new ImageView(abilityClassImageService.getAbilityImage(abil.getAbilityClassId()));
			abilityView.setSmooth(true);

			// Create the text
			Label abilityLabel = new Label(abil.getTimeCounters() > 0 ? "" + abil.getTimeCounters(): null, abilityView);
			abilityLabel.setOnMouseClicked(this::onAbilitySelected);
			abilityLabel.setDisable(!(
            		!model.getCharacter(abil.getOwningCharacterId()).isStunned()
            		&& abil.getTimeCounters() == 0
            		&& !(model.getCharacter(abil.getOwningCharacterId()).isSilenced() && !abil.isBasic()) // Silence prevents characters from casting non-basic abilities
            		&& model.getCharacter(abil.getOwningCharacterId()).wasAvailableAtTurnStart()
            		&& ((abil.getAbilityClassId() == AbilityClassIds.SWAP_ABILITY_CLASS_ID)
            				|| !model.getCharacter(abil.getOwningCharacterId()).hasPerformedAction()))
            );
			abilityLabel.setUserData(abil);
			abilityLabel.getStyleClass().add("abilityView");
			abilityLabel.setContentDisplay(ContentDisplay.CENTER);
			abilityLabel.setTextFill(Color.WHITE);

			// Set font size
			abilityLabel.setFont(Font.font("Tahoma", 24 * UIConstants.getGlobalScalingFactor()));

			// Enabled effect

			if (abilityClassService.getAbilityClass(abil.getAbilityClassId()).isPassive()) {
				// TODO: Highlight Passive Abilities

				abilityView.setFitHeight(parentSize * activeSkillSizeModifier);
				abilityView.setFitWidth(parentSize * activeSkillSizeModifier);

			} else if (abilityLabel.isDisabled()) {

				InnerShadow effect = new InnerShadow();
				effect.setColor(Color.CRIMSON);
				effect.setRadius(12 * UIConstants.getGlobalScalingFactor());
				abilityLabel.setEffect(effect);

				abilityView.setFitHeight(parentSize * inactiveSkillSizeModifier);
				abilityView.setFitWidth(parentSize * inactiveSkillSizeModifier);

			} else if (abil.getAbilityClassId() == AbilityClassIds.SWAP_ABILITY_CLASS_ID && model.getRemainingFreeSwaps() > 0) {

				DropShadow effect = new DropShadow();
				effect.setColor(Color.GREENYELLOW);
				effect.setRadius(12 * UIConstants.getGlobalScalingFactor());
				abilityLabel.setEffect(effect);

				abilityView.setFitHeight(parentSize * activeSkillSizeModifier);
				abilityView.setFitWidth(parentSize * activeSkillSizeModifier);

				abilityLabel.setText(model.getRemainingFreeSwaps() + " " + localizationService.getText("Game.FreeSwap.Text"));
				abilityLabel.setFont(Font.font("Tahoma", 18 * UIConstants.getGlobalScalingFactor()));

			} else {

				DropShadow effect = new DropShadow();
				effect.setColor(Color.CORNSILK);
				effect.setRadius(12 * UIConstants.getGlobalScalingFactor());
				abilityLabel.setEffect(effect);

				abilityView.setFitHeight(parentSize * activeSkillSizeModifier);
				abilityView.setFitWidth(parentSize * activeSkillSizeModifier);

			}

			// Load Tooltip
			int charClassId = character.getCharacterClassId();
			Tooltip tooltip = new Tooltip(
					localizationService.getText("Game.Ability." + abil.getAbilityClassId() + ".Name")

					+

					"\n\n"

					+

					(abil.getAbilityClassId() == AbilityClassIds.ATTACK_ABILITY_CLASS_ID ?

					localizationService.getText("Game.Character." + charClassId + ".AttackTooltip",new Object[]{
							CharacterStateUtils.getCharacterAttackDamage(character, characterClassService),
							localizationService.getText("Game.Damage." + character.getDamageType())})

					: localizationService.getText("Game.Ability." + abil.getAbilityClassId() + ".Tooltip",
												abilityClassService.getAbilityClass(abil.getAbilityClassId()).getProperties().values().toArray())));

			Tooltip.install(abilityLabel, tooltip);

			// Add to View
			getChildren().add(abilityLabel);
		}

	}

	// Event Handlers

	public void onAbilitySelected(MouseEvent event) {
		if (event.getButton() == MouseButton.PRIMARY && !((Node)event.getSource()).isDisabled()) {
			getConsumer().accept(((Node)event.getSource()).getUserData());
			event.consume();
		}
	}

	// Getters & Setters

	public Consumer<Object> getConsumer() {
		return consumer;
	}

	public static double getDiametermodifier() {
		return diameterModifier;
	}

	public static double getInactiveskillsizemodifier() {
		return inactiveSkillSizeModifier;
	}

	public static double getActiveskillsizemodifier() {
		return activeSkillSizeModifier;
	}



}
