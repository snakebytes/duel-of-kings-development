package dok.client.desktop.ui.components;

import dok.client.desktop.ui.UIConstants;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.game.model.Buff;
import dok.game.model.api.AbilityDataService;
import dok.game.model.buffs.StunBuff;
import dok.game.model.buffs.StunCountersBuff;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class BuffCell extends Label {

	// Attributes

	private final GameImageService gameImageService;
	private final AbilityDataService abilityClassService;
	private final LocalizationService localizationService;
	private final ImageView iconDisplay;
	private final Buff buff;

	// Constructor(s)

	public BuffCell(Buff b, GameImageService gameImageService, AbilityDataService abilityClassService, LocalizationService localizationService) {
		super();

		// Initialize Variables with parameters

		this.gameImageService = gameImageService;
		this.abilityClassService = abilityClassService;
		this.localizationService = localizationService;
		this.iconDisplay = new ImageView();
		this.buff = b;

		// Initialize display values

		this.setBackground(null);
		this.setContentDisplay(ContentDisplay.CENTER);
		if (b instanceof StunCountersBuff)
			setTextFill(Color.BLACK);
		else if (b instanceof StunBuff)
			setTextFill(Color.WHITE);
		else
			setTextFill(Color.WHITE);

		// Define Style Class
		this.getStyleClass().add("buffCell");

		// Define Text
		if (b.getDuration() > 0) setText(String.valueOf(b.getDurationLeft()));
   	 	else if (b.getMaxStacks() > 0) setText(String.valueOf(b.getStacks()));
   	 	else setText(null);
		getIconDisplay().setImage(getGameImageService().getBuffImage(b.getClass()));
   	 	setTooltip(new Tooltip(getLocalizationService().getText("Game.Buff." + b.getClass().getSimpleName() + ".Name") + System.lineSeparator()
   	 							+ getLocalizationService().getText("Game.DurationLeft") + ": " + (b.getDuration() == 0 ?  getLocalizationService().getText("Game.Duration.Endless") : b.getDurationLeft())
   	 							+ System.lineSeparator() + System.lineSeparator()
   	 							+ getLocalizationService().getText("Game.Buff." + b.getClass().getSimpleName() + ".Tooltip",
   	 																b.getProperties().values().toArray())));
        setGraphic(getIconDisplay());

		// Update fonts and spacing
		updateFonts();
		updateSpacing();
	}

	// Methods

	public void updateSize(double value) {
		getIconDisplay().setFitHeight(value);
		getIconDisplay().setFitWidth(value);
	}

	public void updateSpacing() {
		//setPadding(new Insets(50. * UIConstants.getGlobalScalingFactor()));
	}

	public void updateFonts() {
		setFont(Font.font("Tahoma", 24. * UIConstants.getGlobalScalingFactor()));
	}

	// Getter & Setter

	public AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	public GameImageService getGameImageService() {
		return gameImageService;
	}

	public ImageView getIconDisplay() {
		return iconDisplay;
	}

	public LocalizationService getLocalizationService() {
		return localizationService;
	}

	public Buff getItem() {
		return buff;
	}

}
