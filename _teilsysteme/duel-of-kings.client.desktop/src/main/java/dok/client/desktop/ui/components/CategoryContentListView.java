package dok.client.desktop.ui.components;

import dok.client.desktop.ui.controllers.LineupEditorCategoryView;
import dok.client.model.EnhancementSlotCategory;
import dok.commons.model.EnhancementPlan;
import dok.commons.model.EnhancementSlot;
import dok.game.model.CharacterClass;
import dok.game.model.CharacterEnhancementClass;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.control.ListView;

/**
 * A ListView displaying the contents of the currently selected category.
 * Intended for use with and controlled by {@link CategoryTabListView}.
 * Updates the {@link CategoryTabListView} and the containing {@link LineupEditorCategoryView}'s lineup.
 *
 * @author Steffen
 */
public class CategoryContentListView extends ListView<Object> {

	// Constructors

	public CategoryContentListView() {
		super();
		initialize();
	}

	// Attributes

	/**
	 * The view containing this component as well as the controlling {@link CategoryTabListView}.	 *
	 */
	private LineupEditorCategoryView catView;

	// Methods

	public void initialize() {
		setOrientation(Orientation.VERTICAL);
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		this.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			/**
			 * This handler will use the {@link LineupEditorCategoryView}'s methods to acess the lineup and refresh the {@link CategoryTabListView}.
			 */
			@Override
			public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
				if(newValue instanceof CharacterClass) {
					CharacterClass ccnv = (CharacterClass) newValue;
					if(ccnv.getId() != catView.getTemporaryChanges().getCharacterClassIdAt(catView.getSelectedPosition().toPair())) {
						catView.getTemporaryChanges().getCharacterClassIds().put(catView.getSelectedPosition(), ccnv.getId());
						catView.getTemporaryChanges().getEnhancementPlans().put(catView.getSelectedPosition(), null);
						catView.refreshTabList();
					}

				} else if(newValue instanceof CharacterEnhancementClass) {
					CharacterEnhancementClass enhnv = (CharacterEnhancementClass) newValue;
					EnhancementSlot selectedSlot = ((EnhancementSlotCategory) catView.getCategoryTabListView().getSelectionModel().getSelectedItem()).getSlot();
					catView.getTemporaryChanges().getEnhancementPlans().putIfAbsent(catView.getSelectedPosition(), new EnhancementPlan());
					EnhancementPlan plan = catView.getTemporaryChanges().getEnhancementPlans().get(catView.getSelectedPosition());
					Integer id = plan.getEnhancementMap().get(selectedSlot);
					if(id == null || enhnv.getId() != id) {
						catView.getTemporaryChanges().getEnhancementPlans().get(catView.getSelectedPosition()).getEnhancementMap().put(selectedSlot, enhnv.getId());
						catView.refreshTabList();
					}
				} else {
					// error
				}
			}
		});
	}

	// Getters & Setters

	public LineupEditorCategoryView getCatView() {
		return catView;
	}

	public void setCatView(LineupEditorCategoryView catView) {
		this.catView = catView;
	}
}
