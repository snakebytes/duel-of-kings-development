package dok.client.desktop.ui.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.controllers.LineupEditorCategoryView;
import dok.client.model.Category;
import dok.client.model.CharacterClassCategory;
import dok.client.model.EnhancementSlotCategory;
import dok.commons.model.EnhancementPlan;
import dok.commons.model.Lineup;
import dok.game.model.CharacterClass;
import dok.game.model.CharacterEnhancementClass;
import dok.game.model.api.CharacterDataService;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Part of a {@link LineupEditorCategoryView}, displaying all configurable properties of the selected Position as different categories.
 * Intended for use together with a {@link CategoryContentListView}, which will be updated when a selection in this component is changed.
 *
 * @author Steffen
 */
public class CategoryTabListView extends ListView<Category> {

	// Class Constants

	Logger logger = LogManager.getLogger();

	// Inner Classes

	public class CategoryTabListCell extends ListCell<Category> {

		// Attributes

		private final StackPane mainContainer = new StackPane();
		private final Label contentLabel = new Label();
		private ImageView icon = new ImageView();

		// Constructor

		public CategoryTabListCell() {
			super();
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			contentLabel.setContentDisplay(ContentDisplay.LEFT);
			contentLabel.setGraphic(icon);
			icon.setSmooth(true);
			icon.setPreserveRatio(true);
			getContentLabel().setFont(Font.font("Papyrus", 20 * UIConstants.getGlobalScalingFactor()));
			setAlignment(Pos.CENTER);
			StackPane.setAlignment(getContentLabel(), Pos.CENTER_LEFT);
			StackPane.setMargin(getContentLabel(), new Insets(5));
			getMainContainer().getChildren().add(getContentLabel());
			setGraphic(getMainContainer());
		}

		// Methods

		@Override
		protected void updateItem(Category item, boolean empty) {
		     super.updateItem(item, empty);
		     if (empty || item == null) {
		         contentLabel.setText(null);
		         icon.setImage(null);
		     } else {
		    	 if(item instanceof CharacterClassCategory) {
		    		 getContentLabel().setText(ClientApplication.getInstance().getLocalizationProvider().getText("Category.CharacterClasses.Name"));
		    	 } else if(item instanceof EnhancementSlotCategory) {
		    		 EnhancementSlotCategory cat = (EnhancementSlotCategory) item;
		    		 getContentLabel().setText(ClientApplication.getInstance().getLocalizationProvider().getText("Category.EnhancementSlot." + cat.getSlot().getType() + "." + cat.getSlot().getRank() + ".Name"));
		    	 }
		    	 icon.setImage(item.getIcon());


		    	// Rebind size
	    		 setMaxWidth(getListView().getMaxWidth() / Math.min(getListView().getItems().size(), 3));
	    		 setMaxHeight(getListView().getMaxHeight() - 30);
	    		 icon.setFitHeight(getMaxHeight());

		    	 if (item.getIcon() != null) {
		    		 // Change Image
		    		 icon.setImage(item.getIcon());
		    	 } else {
		    		 icon.setImage(null);
		    	 }
		     }
		}

		// Getter & Setter

		public StackPane getMainContainer() {
			return mainContainer;
		}

		public Label getContentLabel() {
			return contentLabel;
		}

	}

	// Attibutes

	private CategoryContentListView contentView;
	private CharacterClass targetClass;
	private CharacterDataService characterDataService;

	// Constructors

	public CategoryTabListView() {
		super();
	}

	/**
	 *
	 * @param items Categories to display
	 * @param contentView {@link CategoryContentListView} which displays the selected category's content.
	 * @param targetClass Character Class selected on which the categories are based.
	 * @param characterClassService
	 */
	public CategoryTabListView(ObservableList<Category> items, CategoryContentListView contentView, CharacterClass targetClass, CharacterDataService characterClassService) {
		super(items);
		this.contentView = contentView;
		this.targetClass = targetClass;
		this.characterDataService = characterClassService;
		initialize();
	}

	// Methods

	public void initialize() {
		setOrientation(Orientation.HORIZONTAL);
		getStyleClass().add("tabListView");
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		setCellFactory((listView) -> {
			return new CategoryTabListCell();
		});
		setBackground(new Background(new BackgroundFill(Color.STEELBLUE, null, null)));
		getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Category>() {
			@SuppressWarnings("unchecked")
			@Override
			public void changed(ObservableValue<? extends Category> observable, Category oldValue, Category newValue) {
				contentView.getSelectionModel().clearSelection();
				if(newValue instanceof CharacterClassCategory) {
					ObservableList<CharacterClass> obsList = FXCollections.observableList(contentView.getCatView().getCharacterClasses());
					if(obsList != null) {
						contentView.setItems((ObservableList<Object>)(ObservableList<?>) obsList);
					}
					contentView.setCellFactory((listView) -> {
						return new CharacterClassCategoryCell();
					});
					if(targetClass != null) {
						contentView.getSelectionModel().select(targetClass);
					}
				} else if (newValue instanceof EnhancementSlotCategory) {
					EnhancementSlotCategory enhNv = (EnhancementSlotCategory) newValue;
					List<Integer> validIds = characterDataService.getValidEnhancementClassIds(enhNv.getSlot(), targetClass.getId());
					EnhancementPlan ep = contentView.getCatView().getTemporaryChanges().getEnhancementPlans().get(
							contentView.getCatView().getSelectedPosition());

					List<CharacterEnhancementClass> validEnhancements= new ArrayList<>();
					for(int i : validIds) {
						CharacterEnhancementClass cec = characterDataService.getCharacterEnhancementClass(i);
						if(ep == null || !ep.getEnhancementMap().containsValue(i) || (ep.getEnhancementMap().get(enhNv.getSlot()) != null && ep.getEnhancementMap().get(enhNv.getSlot()) == i)) {
							validEnhancements.add(cec);
						}
					}
					contentView.setItems((ObservableList<Object>)(ObservableList<?>) FXCollections.observableList(validEnhancements));
					contentView.setCellFactory((listView) -> {
						return new EnhancementSlotCategoryCell();
					});
					Lineup lineup = contentView.getCatView().getTemporaryChanges();
					EnhancementPlan plan = lineup.getEnhancementPlans().get(contentView.getCatView().getSelectedPosition());
					if(plan != null) {
						if(plan.getEnhancementMap().get(enhNv.getSlot()) != null) {
							CharacterEnhancementClass enhancement = characterDataService.getCharacterEnhancementClass(plan.getEnhancementMap().get(enhNv.getSlot()));
							if(enhancement != null) contentView.getSelectionModel().select(enhancement);
						}
					}
				}
			}
		});
	}

	// Getters & Setters

	public CategoryContentListView getContentView() {
		return contentView;
	}

	public void setContentView(CategoryContentListView contentView) {
		this.contentView = contentView;
	}

	public CharacterClass getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(CharacterClass targetClass) {
		this.targetClass = targetClass;
	}

	public CharacterDataService getCharacterDataService() {
		return characterDataService;
	}

	public void setCharacterDataService(CharacterDataService characterDataService) {
		this.characterDataService = characterDataService;
	}
}
