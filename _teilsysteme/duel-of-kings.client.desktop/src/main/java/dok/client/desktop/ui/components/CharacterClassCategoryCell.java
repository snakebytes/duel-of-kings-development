package dok.client.desktop.ui.components;

import java.util.ArrayList;
import java.util.List;

import dok.client.desktop.application.ClientApplication;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.commons.model.EnhancementSlot;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterClass;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Displays a single Character Class, intended for use in a [@link CategoryContentListView}
 *
 * @author Steffen
 */

public class CharacterClassCategoryCell extends ListCell<Object> {

    private final HBox mainContainer = new HBox();
    private final ImageView portrait = new ImageView();
    private final VBox centerContainer = new VBox();
    private final Label nameLabel = new Label();
    private final HBox statsContainer = new HBox();
    private final Label hpLabel = new Label();
    private final Label atkLabel = new Label();
    private final Label arLabel = new Label();
    private final Label mrLabel = new Label();
    private final Label descLabel = new Label();
    private final VBox enhancementsContainer = new VBox();
    private final HBox rank1 = new HBox();
    private final HBox rank2 = new HBox();
    private final HBox rank3 = new HBox();
    private final List<HBox> ranks = new ArrayList<>();


    public CharacterClassCategoryCell() {
        super();
        // build hirarchy
        ranks.add(rank1);
        rank1.setAlignment(Pos.CENTER);
        ranks.add(rank2);
        rank2.setAlignment(Pos.CENTER);
        ranks.add(rank3);
        rank3.setAlignment(Pos.CENTER);
        enhancementsContainer.getChildren().addAll(rank3, rank2, rank1);
        statsContainer.getChildren().addAll(hpLabel, atkLabel, arLabel, mrLabel);
        centerContainer.getChildren().addAll(nameLabel, statsContainer, descLabel);
        mainContainer.getChildren().addAll(portrait, centerContainer, enhancementsContainer);

        // general setup
        hpLabel.setContentDisplay(ContentDisplay.LEFT);
        atkLabel.setContentDisplay(ContentDisplay.LEFT);
        arLabel.setContentDisplay(ContentDisplay.LEFT);
        mrLabel.setContentDisplay(ContentDisplay.LEFT);
        nameLabel.setAlignment(Pos.CENTER);
        descLabel.setAlignment(Pos.CENTER);
        descLabel.setWrapText(true);

        portrait.setPreserveRatio(true);
        portrait.fitHeightProperty().bind(maxHeightProperty());

        GameImageService imageS = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameImageService.class);
        ImageView ivhp = new ImageView(imageS.getIcon("health"));
        ivhp.fitHeightProperty().bind(maxHeightProperty().divide(3).subtract(5));
        ivhp.setPreserveRatio(true);
        hpLabel.setGraphic(ivhp);
        ImageView ivatk = new ImageView(imageS.getIcon("attack_damage"));
        ivatk.setPreserveRatio(true);
        ivatk.fitHeightProperty().bind(maxHeightProperty().divide(3).subtract(5));
        atkLabel.setGraphic(ivatk);
        ImageView ivar = new ImageView(imageS.getIcon("armor"));
        ivar.setPreserveRatio(true);
        ivar.fitHeightProperty().bind(maxHeightProperty().divide(3).subtract(5));
        arLabel.setGraphic(ivar);
        ImageView ivmr = new ImageView(imageS.getIcon("magic_resistance"));
        ivmr.setPreserveRatio(true);
        ivmr.fitHeightProperty().bind(maxHeightProperty().divide(3).subtract(5));
        mrLabel.setGraphic(ivmr);
    }

    @Override
    protected void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item == null) {
            setText(null);
            setGraphic(null);
        } else if(item instanceof CharacterClass) {
            CharacterClass ccItem = (CharacterClass) item;
            GameImageService imageS = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameImageService.class);
            portrait.setImage(imageS.getCharacterClassImage(ccItem.getId()));
            setGraphic(mainContainer);
            LocalizationService locale = ClientApplication.getInstance().getLocalizationProvider();
            nameLabel.setText(locale.getText("Game.Character." + ccItem.getId() + ".Name"));
            hpLabel.setText(""+ccItem.getBaseAttributeValue(CharacterAttribute.HEALTH));
            atkLabel.setText(""+ccItem.getBaseAttributeValue(CharacterAttribute.ATTACK_DAMAGE));
            arLabel.setText(""+ccItem.getBaseAttributeValue(CharacterAttribute.ARMOR));
            mrLabel.setText(""+ccItem.getBaseAttributeValue(CharacterAttribute.MAGIC_RESISTANCE));
            descLabel.setText(locale.getText("Game.Character." + ccItem.getId() + ".Beschreibung"));

            for(HBox h : ranks) {
            	h.getChildren().clear();
            }
            for(EnhancementSlot e : ccItem.getEnhancementSlots()) {
                ImageView temp = new ImageView(imageS.getEnhancementSlotImage(e.getType()));
                temp.setPreserveRatio(true);
                temp.fitHeightProperty().bind(maxHeightProperty().divide(3));
                ranks.get(e.getRank()-1).getChildren().add(temp);
            }

            setMaxHeight(getListView().getHeight() / Math.min(Math.max(4, getListView().getItems().size()), 6));
            nameLabel.setMaxWidth(getListView().getPrefWidth() - portrait.getFitHeight() - enhancementsContainer.getMaxWidth() - 200);
            descLabel.setMaxWidth(getListView().getPrefWidth() - portrait.getFitHeight() - enhancementsContainer.getMaxWidth() - 200);

        } else {
            setText(null);
            setGraphic(null);
        }
    }
}
