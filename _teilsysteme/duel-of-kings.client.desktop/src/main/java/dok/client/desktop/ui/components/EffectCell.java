package dok.client.desktop.ui.components;

import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.controllers.CharacterDisplay;
import dok.client.desktop.ui.controllers.FieldDisplay;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.game.model.Effect;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

public class EffectCell extends Label {

	// Class Constants

	@SuppressWarnings("unused")
	private static final Logger LOGGER = LogManager.getLogger();

	// Attributes

	private final Map<UUID, FieldDisplay> fieldDisplays;
	private final GameImageService gameImageService;
	private final LocalizationService localizationService;
	private final AbilityDataService abilityClassService;
	private final ObjectProperty<GameStateModel> model = new SimpleObjectProperty<>();
	private final ImageView iconImageView;
	private final Effect item;

	// Constructor(s)

	public EffectCell(Effect item, ObjectProperty<GameStateModel> model, Map<UUID, FieldDisplay> fieldDisplays, GameImageService gameImageService, AbilityDataService abilityClassService, LocalizationService localizationService) {

		// Initialize fields with parameter values
		this.item = item;
		this.fieldDisplays = fieldDisplays;
		this.gameImageService = gameImageService;
		this.localizationService = localizationService;
		this.abilityClassService = abilityClassService;
		this.model.bind(model);
		iconImageView = new ImageView();

		// Initialize image view
		getIconImageView().fitWidthProperty().bind(prefWidthProperty());
		getIconImageView().fitHeightProperty().bind(prefHeightProperty());
		this.setBackground(null);
		this.setContentDisplay(ContentDisplay.CENTER);
		//this.setOnMouseEntered(this::onMouseEntered);
		//this.setOnMouseExited(this::onMouseExited);
		this.getStyleClass().add("effectCell");

		//
		setText(null);
   	 	getIconImageView().setImage(getGameImageService().getAbilityImage(item.getSourceAbility().getAbilityClassId()));
   	 	setTooltip(new Tooltip(localizationService.getText("Game.Effect." + item.getClass().getSimpleName() + ".Name")
				  + System.lineSeparator() + System.lineSeparator()
   	 			  + getLocalizationService().getText("Game.Effect." + item.getClass().getSimpleName() + ".Tooltip", item.getProperties().values().toArray())));
        setGraphic(getIconImageView());

		// Update fonts and spacing
		updateFonts();
		updateSpacing();
		updateSize();
	}

	// Methods

	public void onMouseEntered(MouseEvent event) {

		// Highlight source
		Position pos = getModel().getCharacter(getItem().getSourceAbility().getOwningCharacterId()).getPosition();
		getFieldDisplays().get(pos.getPlayer()).getDisplays().get(new MutablePair<Integer, Integer>(pos.getRow(), pos.getCol())).enableSourceHighlighting();

		// Highlight targets
		for(Position p : getItem().getTargetedPositions().keySet()) {
			if (p != null)
				getFieldDisplays()
				.get(p.getPlayer())
				.getDisplays()
				.get(new MutablePair<Integer, Integer>(p.getRow(), p.getCol()))
				.enableTargetHighlighting();
		}
	}

	public void onMouseExited(MouseEvent event) {
		if (getItem() == null) return;

		// Unhighlight source
		Position pos = getModel().getCharacter(getItem().getSourceAbility().getOwningCharacterId()).getPosition();
		getFieldDisplays().get(pos.getPlayer()).getDisplays().get(new MutablePair<Integer, Integer>(pos.getRow(), pos.getCol())).disableSourceHighlighting();

		// Unhighlight targets
		for (FieldDisplay fieldDisplay : getFieldDisplays().values())
			for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
				characterDisplay.disableTargetHighlighting();
	}

	public void updateSize() {
	}

	public void updateSpacing() {
		//setPadding(new Insets(50. * UIConstants.getGlobalScalingFactor()));
	}

	public void updateFonts() {
		setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
	}

	// Getter & Setter

	public ImageView getIconImageView() {
		return iconImageView;
	}

	public LocalizationService getLocalizationService() {
		return localizationService;
	}

	public GameImageService getGameImageService() {
		return gameImageService;
	}

	public AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	public Map<UUID, FieldDisplay> getFieldDisplays() {
		return fieldDisplays;
	}

	public final ObjectProperty<GameStateModel> modelProperty() {
		return this.model;
	}


	public final GameStateModel getModel() {
		return this.modelProperty().get();
	}


	public final void setModel(final GameStateModel model) {
		this.modelProperty().set(model);
	}

	public Effect getItem() {
		return item;
	}


}
