package dok.client.desktop.ui.components;

import dok.client.desktop.application.ClientApplication;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.game.model.CharacterEnhancementClass;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

/**
 * ListViewCell for a CharacterEnhancement. Will be empty for every other object.
 *
 * @author Steffen
 */
public class EnhancementSlotCategoryCell extends ListCell<Object> {

	private ImageView portrait = new ImageView();

	public EnhancementSlotCategoryCell() {
		super();
		setGraphic(portrait);
		portrait.setPreserveRatio(true);
		portrait.fitHeightProperty().bind(maxHeightProperty());
	}

	@Override
	protected void updateItem(Object item, boolean empty) {
		super.updateItem(item, empty);
		if(empty || item == null) {
			setText(null);
	        portrait.setImage(null);
		} else if(item instanceof CharacterEnhancementClass) {
			CharacterEnhancementClass enhItem = (CharacterEnhancementClass) item;
			portrait.setImage(ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameImageService.class).getEnhancementImage(enhItem.getId()));
			LocalizationService locale = ClientApplication.getInstance().getLocalizationProvider();
			setText(locale.getText("Game.Enhancement." + enhItem.getId() + ".Name") + "\n" + locale.getText("Game.Enhancement." + enhItem.getId() + ".Desc"));
			setMaxHeight(getListView().getHeight() / Math.min(Math.max(4, getListView().getItems().size()), 6));
			System.out.println(getMaxHeight());
		} else {
			setText(null);
			portrait.setImage(null);
		}
	}
}
