package dok.client.desktop.ui.components;

import java.io.IOException;
import java.util.List;

import dok.client.desktop.application.ClientApplication;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * A dialog to display a certain set of messages and a single dismiss-button.
 * Used to display the results of an attempt to save the lineup.
 *
 * @author Steffen
 */
public class LineupEditorSaveDialog extends StackPane {

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

    // Injected Nodes

    @FXML VBox messagesContainer;
    @FXML Button okButton;

    // Attributes

    /**
     * All messages to be displayed, each in its own label.
     */
    private List<String> messages;

    //Constructor(s)

    public LineupEditorSaveDialog() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
    }

    // Methods

    @FXML
    public void initialize() {
    	reloadMessages();
    	okButton.setOnAction(this::okButtonOnAction);
    }

    /**
     * Reloads the Labels displaying the messages from the locally stored list.
     */
    public void reloadMessages() {
    	messagesContainer.getChildren().clear();
    	if(messages == null) return;
    	for(String s : messages) {
    		Label label = new Label(s);
    		label.setWrapText(true);
    		label.getStyleClass().add("messageLabel");
    		messagesContainer.getChildren().add(label);
    	}
    }

    private void okButtonOnAction(ActionEvent event) {
    	setVisible(false);
    }

    // Getter & Setter

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }


    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> message) {
		this.messages = message;
	}

}
