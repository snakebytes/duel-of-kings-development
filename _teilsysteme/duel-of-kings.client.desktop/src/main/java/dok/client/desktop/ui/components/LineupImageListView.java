package dok.client.desktop.ui.components;

import dok.client.desktop.application.ClientApplication;
import dok.client.model.LineupImageFileNames;
import dok.client.services.ApplicationImageService;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Displays all images registered for use as a Lineup-ImageKey in {@link LineupImageFileNames}
 *
 * @author Steffen
 *
 */

public class LineupImageListView extends ListView<LineupImageFileNames> {

    // Inner Classes

    public class LineupImageCell extends ListCell<LineupImageFileNames> {

        // Attributes

        private ImageView portrait = new ImageView();

        // Constructors

        public LineupImageCell() {
            super();
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            portrait.setSmooth(true);
            portrait.setPreserveRatio(true);
            getStyleClass().add("lineupImageCell");
            setAlignment(Pos.CENTER);
        }

        // Methods

        @Override
        protected void updateItem(LineupImageFileNames item, boolean empty) {
            super.updateItem(item, empty);
            if (empty || item == null) {
                setText(null);
                setGraphic(null);
             } else {
                setGraphic(portrait);
                Image test = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(ApplicationImageService.class)
                         .getLineupImage(item.getImageKey());
                portrait.setImage(test);
                portrait.setFitHeight(getListView().getHeight() - 20);
             }
        }

        // Getters & Setters

    }

    // Attributes

    // Constructors

    public LineupImageListView() {
        super();
        setup();
    }

    public LineupImageListView(ObservableList<LineupImageFileNames> items) {
        super(items);
        setup();
    }

    // Methods

    private void setup() {
        setOrientation(Orientation.HORIZONTAL);
        setCellFactory((listView) -> {
            return new LineupImageCell();
        });
    }

    @FXML
    public void initialize() {
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add("LineupImageListView");
    }

    // Getters & Setters

}
