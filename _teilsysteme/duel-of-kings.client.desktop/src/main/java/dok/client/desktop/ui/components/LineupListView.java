package dok.client.desktop.ui.components;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.services.ApplicationImageService;
import dok.commons.model.Lineup;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class LineupListView extends ListView<Lineup> {

	// Inner Classes

	public class LineupListCell extends ListCell<Lineup> {

		// Attributes

		private final StackPane mainContainer = new StackPane();
		private final ImageView portraitImageView = new ImageView();
		private final Label titleLabel = new Label();

		// Constructor

		public LineupListCell() {
			super();
			setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
			getPortraitImageView().setSmooth(true);
			getPortraitImageView().setPreserveRatio(true);
			getPortraitImageView().getStyleClass().add("portraitImageView");
			getTitleLabel().setFont(Font.font("Papyrus", 32 * UIConstants.getGlobalScalingFactor()));
			setAlignment(Pos.CENTER);
			StackPane.setAlignment(getTitleLabel(), Pos.BOTTOM_CENTER);
			StackPane.setMargin(getTitleLabel(), new Insets(15));
			getMainContainer().getChildren().addAll(getPortraitImageView(), getTitleLabel());
		}

		// Methods

		@Override
		protected void updateItem(Lineup item, boolean empty) {
		     super.updateItem(item, empty);

		     if (empty || item == null) {

		         setText(null);
		         setGraphic(null);

		     } else {

		    	 getTitleLabel().setText(item.getTitle());

		    	// Rebind size
	    		 getPortraitImageView().fitHeightProperty().unbind();
	    		 getPortraitImageView().fitHeightProperty().bind(getListView().heightProperty().subtract(30));
	    		 prefWidthProperty().unbind();
	    		 prefWidthProperty().bind(getListView().widthProperty().divide(3).subtract(5));

		    	 if (item.getImageKey() != null) {
		    		 // Change Image
		    		 getPortraitImageView()
		    	 		.setImage(ClientApplication.getInstance().getServiceRegistry().getServiceProvider(ApplicationImageService.class)
		    	 			.getLineupImage(item.getImageKey()));
		    	 } else {
		    		 getPortraitImageView().setImage(null);
		    	 }

		    	 setGraphic(getMainContainer());

		     }
		}

		// Getter & Setter

		public StackPane getMainContainer() {
			return mainContainer;
		}

		public Label getTitleLabel() {
			return titleLabel;
		}

		public ImageView getPortraitImageView() {
			return portraitImageView;
		}

	}

	// Constructor(s)

	public LineupListView() {
		super();
		initialize();
	}

	public LineupListView(ObservableList<Lineup> items) {
		super(items);
		initialize();
	}

	// Methods

	private void initialize() {
		setOrientation(Orientation.HORIZONTAL);
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		getStyleClass().add("LineupListView");
		setCellFactory((listView) -> {
			return new LineupListCell();
		});
	}

}
