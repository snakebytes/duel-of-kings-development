package dok.client.desktop.ui.components;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import dok.client.desktop.ui.UIConstants;
import dok.client.services.TriggerClassImageService;
import dok.commons.LocalizationService;
import dok.game.model.Character;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import jfxtras.scene.layout.CircularPane;

public class TriggerSelection extends CircularPane {

	// Sizing Constants
	private static final double diameterModifier = 1.4; // Desired diameter / parent Size = Modifier
	private static final double activeSkillSizeModifier = 0.33; // Icon Size / Parent Size = Modifier

	// Attributes

	private final List<ImageView> triggerDisplays = new ArrayList<>();
	private final Consumer<Object> triggerConsumer;

	// Constructor(s)

	public TriggerSelection(Character character, double parentSize, List<Trigger> triggers, Consumer<Object> triggerConsumer,
			LocalizationService localizationService, AbilityDataService abilityClassService, TriggerClassImageService triggerClassImageService) {
		super();

		// Initialize Variables
		this.triggerConsumer = triggerConsumer;

		// Initialize Styling & Layout
		setAnimationInterpolation(CircularPane::animateOverTheArcWithFade);
		setDiameter(parentSize * diameterModifier);
		setChildrenAreCircular(true);
		setClipAwayExcessWhitespace(false);

		// Initialize Trigger displays

		for (Trigger trigger : new ArrayList<>(triggers)) {
			// Create the icon
			ImageView triggerView = new ImageView(triggerClassImageService.getTriggerClassImage(trigger.getTriggerClassId()));
			triggerView.setSmooth(true);
			triggerView.setFitHeight(parentSize * activeSkillSizeModifier);
			triggerView.setFitWidth(parentSize * activeSkillSizeModifier);
			triggerView.setUserData(trigger);
			triggerView.setOnMouseClicked(this::onAbilitySelected);

			DropShadow effect = new DropShadow();
			effect.setColor(Color.CORNSILK);
			effect.setRadius(12 * UIConstants.getGlobalScalingFactor());
			triggerView.setEffect(effect);

			// Load Tooltip
			Tooltip tooltip = new Tooltip(localizationService.getText("Game.Trigger." + trigger.getTriggerClassId() + ".Name")
										  + System.lineSeparator() + System.lineSeparator()
										  + localizationService.getText("Game.Trigger." + trigger.getTriggerClassId() + ".Tooltip",
																		abilityClassService.getTriggerClass(trigger.getTriggerClassId()).getProperties().values().toArray()));
			Tooltip.install(triggerView, tooltip);

			// Add to View
			getChildren().add(triggerView);

			// Add to model
			getTriggerDisplays().add(triggerView);
		}
		// DEBUG
		//setShowDebug(Color.GREENYELLOW);
	}

	// Event Handlers

	public void onAbilitySelected(MouseEvent event) {
		if (event.getButton() == MouseButton.PRIMARY) {
			getTriggerConsumer().accept(((Node)event.getSource()).getUserData());
			event.consume();
		}
	}

	// Getter & Setter

	public Consumer<Object> getTriggerConsumer() {
		return triggerConsumer;
	}

	public List<ImageView> getTriggerDisplays() {
		return triggerDisplays;
	}

	public static double getDiametermodifier() {
		return diameterModifier;
	}

	public static double getActiveskillsizemodifier() {
		return activeSkillSizeModifier;
	}



}
