package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.components.LineupListView;
import dok.client.services.LineupService.LineupServiceListener;
import dok.commons.model.Lineup;
import dok.game.model.AILevel;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Callback;

public final class BotGameCreationView extends StackPane implements LineupServiceListener {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	//Attributes

	//TODO ...

	//Properties

	private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

	//Injected Nodes

	@FXML Button startBotGameButton;
	@FXML Button cancelButton;

	@FXML VBox aiStrengthContainer;
	@FXML Label aiStrengthLabel;
	@FXML ComboBox<AILevel> computerAILevelComboBox;

	@FXML VBox playerLineupContainer;
	@FXML Label playerLineupLabel;
	@FXML LineupListView playerLineupSelectionView;

	@FXML VBox computerLineupContainer;
	@FXML Label computerLineupLabel;
	@FXML LineupListView computerLineupSelectionView;

	//Constructor
	public BotGameCreationView() throws IOException {
		super();
		logger.entry();

		//Apply CSS
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		setId(getClass().getSimpleName());

		//Setup FXMLLoader
		setFXMLLoader(new FXMLLoader());
		getFXMLLoader().setRoot(this);
		getFXMLLoader().setController(this);
		getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
		getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

		//Load the View
		getFXMLLoader().load();

		logger.exit(this);
	}

	//Initialization

	@FXML
	private void initialize() {

		// AILevel selection
		computerAILevelComboBox.getItems().addAll(Arrays.asList(AILevel.values()).subList(0, 2));
		computerAILevelComboBox.getSelectionModel().select(AILevel.BEGINNER);
		computerAILevelComboBox.setCellFactory(new Callback<ListView<AILevel>, ListCell<AILevel>>() {

			@Override
			public ListCell<AILevel> call(ListView<AILevel> param) {
				return new ListCell<AILevel>() {

					@Override
					protected void updateItem(AILevel item, boolean empty) {
						super.updateItem(item, empty);

					     if (empty || item == null) {
					         setText(null);
					         setGraphic(null);
					     } else {
					         setText(ClientApplication.getInstance().getLocalizationProvider().getText("AILevel." + item));
					     }
					}

				};
			}

		});
		computerAILevelComboBox.setButtonCell(computerAILevelComboBox.getCellFactory().call(null));

		// Start Button
		startBotGameButton.setOnAction(this::onStartBotGameButtonAction);
		startBotGameButton.disableProperty().bind(playerLineupSelectionView.getSelectionModel().selectedItemProperty().isNotNull()
													.and(computerLineupSelectionView.getSelectionModel().selectedItemProperty().isNotNull())
													.and(computerAILevelComboBox.getSelectionModel().selectedItemProperty().isNotNull()).not());

		// Cancel Button
		cancelButton.setOnAction(this::onCancelButtonAction);

		// Initial Update Call
		updateSpacing();
		updateFonts();

	}

	public void updateSpacing() {
		StackPane.setMargin(startBotGameButton, new Insets(50. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(cancelButton, new Insets(50. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(playerLineupContainer, new Insets(50. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(computerLineupContainer, new Insets(50. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(aiStrengthContainer, new Insets(50. * UIConstants.getGlobalScalingFactor()));

		playerLineupContainer.maxWidthProperty().bind(widthProperty().multiply(0.45));
		playerLineupContainer.setSpacing(10 * UIConstants.getGlobalScalingFactor());
		playerLineupContainer.maxHeightProperty().bind(heightProperty().multiply(0.50));

		computerLineupContainer.maxWidthProperty().bind(widthProperty().multiply(0.45));
		computerLineupContainer.setSpacing(10 * UIConstants.getGlobalScalingFactor());
		computerLineupContainer.maxHeightProperty().bind(heightProperty().multiply(0.50));

		aiStrengthContainer.maxWidthProperty().bind(widthProperty().multiply(0.30));
		aiStrengthContainer.maxHeightProperty().bind(heightProperty().multiply(0.10));
	}

	public void updateFonts() {
		playerLineupLabel.setFont(Font.font("Papyrus", 32 * UIConstants.getGlobalScalingFactor()));
		computerLineupLabel.setFont(Font.font("Papyrus", 32 * UIConstants.getGlobalScalingFactor()));
		startBotGameButton.setFont(Font.font("Papyrus", 32 * UIConstants.getGlobalScalingFactor()));
		cancelButton.setFont(Font.font("Papyrus", 24 * UIConstants.getGlobalScalingFactor()));
		aiStrengthLabel.setFont(Font.font("Papyrus", 24 * UIConstants.getGlobalScalingFactor()));
	}

	// <--- LineupServiceListener --->

	@Override
	public void onLineupSaved(Lineup lineup) {
		Platform.runLater(() -> {
			playerLineupSelectionView.getItems().remove(lineup);
			playerLineupSelectionView.getItems().add(lineup);
			playerLineupSelectionView.getSelectionModel().selectFirst();
			playerLineupSelectionView.scrollTo(0);
			computerLineupSelectionView.getItems().remove(lineup);
			computerLineupSelectionView.getItems().add(lineup);
			computerLineupSelectionView.getSelectionModel().selectFirst();
			computerLineupSelectionView.scrollTo(0);
		});
	}

	@Override
	public void onLineupsUpdated(List<Lineup> lineups) {
		Platform.runLater(() -> {
			playerLineupSelectionView.getItems().clear();
			playerLineupSelectionView.getItems().addAll(lineups);
			playerLineupSelectionView.getSelectionModel().selectFirst();
			playerLineupSelectionView.scrollTo(0);
			computerLineupSelectionView.getItems().clear();
			computerLineupSelectionView.getItems().addAll(lineups);
			computerLineupSelectionView.getSelectionModel().selectFirst();
			computerLineupSelectionView.scrollTo(0);
		});
	}

	//Event Handler

	public void onCancelButtonAction(ActionEvent event) {
		Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getMainView()));
	}

	public void onStartBotGameButtonAction(ActionEvent event) {
		Thread t = new Thread(() ->
		ClientApplication.getInstance().startBotGame(computerAILevelComboBox.getSelectionModel().getSelectedItem(),
				FXCollections.observableArrayList(playerLineupSelectionView.getSelectionModel().getSelectedItem(),
				computerLineupSelectionView.getSelectionModel().getSelectedItem())));
		t.setDaemon(true);
		t.setName("BotGameCreationThread");
		t.start();
	}

	//Getter & Setter

	public final ObjectProperty<FXMLLoader> FXMLLoaderProperty() {
		return this.fxmlLoader;
	}

	public final javafx.fxml.FXMLLoader getFXMLLoader() {
		return this.FXMLLoaderProperty().get();
	}

	private final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
		this.FXMLLoaderProperty().set(fxmlLoader);
	}

}
