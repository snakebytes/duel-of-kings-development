package dok.client.desktop.ui.controllers;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.components.BuffCell;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.game.model.Ability;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.buffs.StunCountersBuff;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.util.CharacterStateUtils;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public final class CharacterDetailDisplay extends StackPane {

    // Class Constants

    private static final Logger LOGGER = LogManager.getLogger();

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();
    private final ObjectProperty<Character> character = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalizationService> localizationService = new SimpleObjectProperty<>();
    private final ObjectProperty<GameImageService> gameImageService = new SimpleObjectProperty<>();
    private final ObjectProperty<AbilityDataService> abilityClassService = new SimpleObjectProperty<>();
    private final ObjectProperty<CharacterDataService> characterClassService = new SimpleObjectProperty<>();

    //Injected Nodes

    @FXML StackPane innerContainer;

    @FXML VBox innerCenterContainer;
    @FXML ImageView characterImageView;
    @FXML Label classNameLabel;
    @FXML TilePane buffTilePane;
    @FXML ImageView kingImageView;

    @FXML GridPane statsGridPane;
    @FXML Label magicResistanceImageLabel;
    @FXML Label magicResistanceValueLabel;
    @FXML Label magicResistanceNameLabel;
    @FXML Label armorImageLabel;
    @FXML Label armorValueLabel;
    @FXML Label armorNameLabel;
    @FXML Label healthImageLabel;
    @FXML Label healthValueLabel;
    @FXML Label healthNameLabel;
    @FXML Label attackDamageImageLabel;
    @FXML Label attackDamageValueLabel;
    @FXML Label attackDamageNameLabel;
    @FXML Label timerCountersImageLabel;
    @FXML Label timerCountersValueLabel;
    @FXML Label timerCountersNameLabel;

    @FXML Accordion detailSkillView;

    //Constructor

    public CharacterDetailDisplay() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));

        //Load the View
        getFXMLLoader().load();
    }

    public CharacterDetailDisplay(LocalizationService localizationService, GameImageService gameImageService, CharacterDataService characterClassService, AbilityDataService abilityClassService) throws IOException {
    	super();

        setLocalizationService(localizationService);
        setGameImageService(gameImageService);
        setCharacterClassService(characterClassService);
        setAbilityClassService(abilityClassService);

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));

        //Load the View
        getFXMLLoader().load();
    }

    //Initialization

    @FXML
    private void initialize() {

        // Tooltips
        timerCountersImageLabel.setTooltip(new Tooltip());
        attackDamageImageLabel.setTooltip(new Tooltip());
        armorImageLabel.setTooltip(new Tooltip());
        magicResistanceImageLabel.setTooltip(new Tooltip());
        healthImageLabel.setTooltip(new Tooltip());

    	// Inner Container
    	innerContainer.maxWidthProperty().bind(widthProperty().multiply(0.80));
    	innerContainer.maxHeightProperty().bind(heightProperty().multiply(0.80));
    	innerContainer.setPadding(new Insets(20 * UIConstants.getGlobalScalingFactor()));

    	// Right Display
        detailSkillView.maxWidthProperty().bind(innerContainer.widthProperty().multiply(0.30));
        detailSkillView.maxHeightProperty().bind(innerContainer.heightProperty().multiply(1));

        // Center Container
        innerCenterContainer.maxWidthProperty().bind(innerContainer.widthProperty().multiply(0.30));
        innerCenterContainer.maxHeightProperty().bind(innerContainer.heightProperty().multiply(1));

        // Character Image
        characterImageView.fitWidthProperty().bind(characterImageView.fitHeightProperty());
        characterImageView.fitHeightProperty().bind(innerCenterContainer.heightProperty().multiply(0.60));
        characterImageView.setSmooth(true);

        // King Image View
        kingImageView.fitHeightProperty().bind(characterImageView.fitHeightProperty().multiply(0.15));
        kingImageView.fitWidthProperty().bind(characterImageView.fitWidthProperty().multiply(0.15));
        kingImageView.setSmooth(true);

        // Buff View
        buffTilePane.prefWidthProperty().bind(innerCenterContainer.widthProperty().multiply(0.9));
        buffTilePane.setBackground(null);
        buffTilePane.prefTileWidthProperty().bind(buffTilePane.widthProperty().divide(7));
        buffTilePane.prefTileHeightProperty().bind(buffTilePane.prefTileWidthProperty());

        classNameLabel.setText("?");

        // Stats Grid Pane
        statsGridPane.maxWidthProperty().bind(innerContainer.widthProperty().multiply(0.30));
        statsGridPane.maxHeightProperty().bind(innerContainer.heightProperty().multiply(0.50));
        statsGridPane.setAlignment(Pos.CENTER);
        for(Node n : statsGridPane.getChildren()) {
        	if(n instanceof Label) {
        		Label l = (Label) n;
        		l.maxWidthProperty().bind(statsGridPane.widthProperty());
        	}
        }

        // Time Counters Labels
        ImageView timeCountersImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/time_counters.png")));
        timerCountersImageLabel.setGraphic(timeCountersImageView);
        timerCountersImageLabel.maxWidthProperty().bind(statsGridPane.widthProperty().multiply(0.09));
        timerCountersImageLabel.maxHeightProperty().bind(statsGridPane.heightProperty().multiply(0.09));
        timeCountersImageView.fitHeightProperty().bind(timerCountersImageLabel.maxHeightProperty());
        timeCountersImageView.fitWidthProperty().bind(timeCountersImageView.fitHeightProperty());

        // Armor Labels
        ImageView armorImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/armor.png")));
        armorImageView.fitWidthProperty().bind(armorImageView.fitHeightProperty());
        armorImageView.fitHeightProperty().bind(statsGridPane.heightProperty().multiply(0.09));
        armorImageView.setSmooth(true);
        armorImageLabel.setGraphic(armorImageView);


        // MagicResistance Labels
        ImageView magicResistanceImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/magic_resistance.png")));
        magicResistanceImageView.fitWidthProperty().bind(magicResistanceImageView.fitHeightProperty());
        magicResistanceImageView.fitHeightProperty().bind(statsGridPane.heightProperty().multiply(0.09));
        magicResistanceImageView.setSmooth(true);
        magicResistanceImageLabel.setGraphic(magicResistanceImageView);

        // Health Labels
        ImageView healthImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/health.png")));
        healthImageView.fitWidthProperty().bind(healthImageView.fitHeightProperty());
        healthImageView.fitHeightProperty().bind(statsGridPane.heightProperty().multiply(0.09));
        healthImageLabel.setGraphic(healthImageView);

        // Attack Damage Labels
        ImageView attackDamageImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/attack_damage.png")));
        attackDamageImageView.fitWidthProperty().bind(attackDamageImageView.fitHeightProperty());
        attackDamageImageView.fitHeightProperty().bind(statsGridPane.heightProperty().multiply(0.09));
        attackDamageImageLabel.setGraphic(attackDamageImageView);

        updateFonts();
        updateSpacing();
    }

    public void updateTooltips() {
        try {
            String t1 = getLocalizationService().getText("Game.TimeCounters.Name")
                      + System.lineSeparator() + System.lineSeparator()
                      + getLocalizationService().getText("Game.TimeCounters.Description");
            Platform.runLater(() -> timerCountersImageLabel.getTooltip().setText(t1));

            String t3 =
                    getLocalizationService().getText("Game.CharacterAttributes.AttackDamage.Name") + ": " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.ATTACK_DAMAGE, getCharacterClassService())
                  + System.lineSeparator()
                  + getLocalizationService().getText("Game.CharacterAttributes.DamageType.Name") + ": " + getLocalizationService().getText("Game.DamageType." + getCharacter().getDamageType())
                  + System.lineSeparator() + System.lineSeparator()
                  + getLocalizationService().getText("Game.CharacterAttributes.AttackDamage.Description");
            Platform.runLater(() -> attackDamageImageLabel.getTooltip().setText(t3));

            String t4 =
                    getLocalizationService().getText("Game.CharacterAttributes.Armor.Name") + ": " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.ARMOR, getCharacterClassService())
                  + System.lineSeparator() + System.lineSeparator()
                  + getLocalizationService().getText("Game.CharacterAttributes.Armor.Description");
            Platform.runLater(() -> armorImageLabel.getTooltip().setText(t4));

            String t5 =
                    getLocalizationService().getText("Game.CharacterAttributes.MagicResistance.Name") + ": " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.MAGIC_RESISTANCE, getCharacterClassService())
                  + System.lineSeparator() + System.lineSeparator()
                  + getLocalizationService().getText("Game.CharacterAttributes.MagicResistance.Description");
            Platform.runLater(() -> magicResistanceImageLabel.getTooltip().setText(t5));

            String t6 =
                    getLocalizationService().getText("Game.CharacterAttributes.Health.Name") + ": "
                    + (CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService()) - getCharacter().getDamageTaken())
                    + " / " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService())
                    + System.lineSeparator() + System.lineSeparator()
                    + getLocalizationService().getText("Game.CharacterAttributes.Health.Description");
            Platform.runLater(() -> healthImageLabel.getTooltip().setText(t6));

            Platform.runLater(() -> Tooltip.install(kingImageView, new Tooltip(getLocalizationService().getText("Game.King.Name")
                    + System.lineSeparator() + System.lineSeparator()
                    + getLocalizationService().getText("Game.King.Description"))));
        } catch (Exception e) {
            LOGGER.warn("Tooltips could not be updated", e);
        }
    }

    //Event Handler

    public void onCharacterChanged(Character oldValue, Character newValue) {
    	if (newValue == null) return;
        int id = newValue.getCharacterClassId();
        LOGGER.trace("Character " + oldValue + " changed to " + newValue);

        // Show Character Attribute Images
        if (CharacterStateUtils.isCharacterAlive(newValue, getCharacterClassService())) {

            String newArmor = "" + CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.ARMOR, getCharacterClassService());
            String newMr = "" + CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.MAGIC_RESISTANCE, getCharacterClassService());
            String newAd = "" + CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.ATTACK_DAMAGE, getCharacterClassService());
            String newHealth = "" + (CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.HEALTH, getCharacterClassService())
            		- Math.min(newValue.getDamageTaken(), CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.HEALTH, getCharacterClassService())))
                    + "/" + CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.HEALTH, getCharacterClassService());

            String armorName = "" + getLocalizationService().getText("Game.CharacterAttributes.Armor.Name");
            String mrName = "" + getLocalizationService().getText("Game.CharacterAttributes.MagicResistance.Name");
            String adName = "" + getLocalizationService().getText("Game.CharacterAttributes.AttackDamage.Name");
            String healthName = "" + getLocalizationService().getText("Game.CharacterAttributes.Health.Name");
            String tcName = "" + getLocalizationService().getText("Game.TimeCounters.Name");

            Platform.runLater(() -> {

                armorImageLabel.setVisible(true);
                magicResistanceImageLabel.setVisible(true);
                attackDamageImageLabel.setVisible(true);
                healthImageLabel.setVisible(true);

                // Character Attributes

                armorValueLabel.setText(newArmor);
                magicResistanceValueLabel.setText(newMr);
                attackDamageValueLabel.setText(newAd);
                healthValueLabel.setText(newHealth);

                armorNameLabel.setText(armorName);
                magicResistanceNameLabel.setText(mrName);
                attackDamageNameLabel.setText(adName);
                healthNameLabel.setText(healthName);


            });

            // TimeCounters

            if (oldValue == null || newValue.getTimeCounters() != oldValue.getTimeCounters()) {

                if (newValue.getTimeCounters() > 0) {
                    String newTimeCounters = "" + newValue.getTimeCounters();
                    Platform.runLater(() -> {
                    	timerCountersImageLabel.setVisible(true);
                        timerCountersValueLabel.setVisible(true);
                        timerCountersValueLabel.setText(newTimeCounters);
                        timerCountersNameLabel.setVisible(true);
                        timerCountersNameLabel.setText(tcName);
                    });
                } else {
                    Platform.runLater(() -> {
                    	timerCountersValueLabel.setVisible(false);
                    	timerCountersNameLabel.setVisible(false);
                    	timerCountersImageLabel.setVisible(false);
                    });
                }

            }

            // Buffs

            Platform.runLater(() -> buffTilePane.getChildren().clear());

            if (newValue.getBuffs().size() > 0) {
                Platform.runLater(() -> buffTilePane.setVisible(true));
                for (Buff b : newValue.getBuffs()) {
                    BuffCell cell = new BuffCell(b, getGameImageService(), getAbilityClassService(), getLocalizationService());
                    if(b.getClass().equals(StunCountersBuff.class)) {
                    	cell.setStyle("-fx-text-fill: black");
                    }
                    cell.updateSize(buffTilePane.getPrefTileWidth());
                    Platform.runLater(() -> buffTilePane.getChildren().add(cell));
                }
            } else
                Platform.runLater(() -> buffTilePane.setVisible(false));

        } else {

            Platform.runLater(() -> {
                timerCountersImageLabel.setVisible(false);
                buffTilePane.setVisible(false);
            });

        }

        // Character Image

        boolean alive = CharacterStateUtils.isCharacterAlive(newValue, getCharacterClassService());
        if (alive && (oldValue == null || oldValue.getCharacterClassId() != newValue.getCharacterClassId())) {
            Image newCharacterImage = getGameImageService().getCharacterClassImage(id);
            Platform.runLater(() -> characterImageView.setImage(newCharacterImage));
        } else if (!alive) {
            Image newCharacterImage = getGameImageService().getCharacterClassImage(-1);
            Platform.runLater(() -> characterImageView.setImage(newCharacterImage));
        }

        // Class Name

        Platform.runLater(() -> {
               classNameLabel.setVisible(true);
               classNameLabel.setText(getLocalizationService().getText("Game.Character." + id + ".Name"));
        });

        // King Image

        if (newValue.isKing() && (oldValue == null || !oldValue.isKing()))
            Platform.runLater(() -> kingImageView.setImage(getGameImageService().getIcon("king")));
        else if (!newValue.isKing() && (oldValue == null || oldValue.isKing()))
            Platform.runLater(() -> kingImageView.setImage(null));

        // Skill Accordion

        Platform.runLater(() -> detailSkillView.getPanes().clear());
        for(Ability ab : newValue.getAbilities()) {
        	int abcid = ab.getAbilityClassId();
        	TitledPane tp = new TitledPane();
        	ImageView titleGraphic = new ImageView(getGameImageService().getAbilityImage(abcid));
        	titleGraphic.setFitHeight(64 * UIConstants.getGlobalScalingFactor());
        	titleGraphic.setFitWidth(64 * UIConstants.getGlobalScalingFactor());
        	tp.setGraphic(titleGraphic);
        	tp.setGraphicTextGap(5);
        	tp.setText(localizationServiceProperty().get().getText("Game.Ability." + abcid + ".Name"));
        	Label l = new Label(ab.getAbilityClassId() == AbilityClassIds.ATTACK_ABILITY_CLASS_ID ?

					getLocalizationService().getText("Game.Character." + getCharacter().getCharacterClassId() + ".AttackTooltip",new Object[]{
							CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.ATTACK_DAMAGE, getCharacterClassService()),
							getLocalizationService().getText("Game.Damage." + getCharacter().getDamageType())})

					: getLocalizationService().getText("Game.Ability." + ab.getAbilityClassId() + ".Tooltip",
												getAbilityClassService().getAbilityClass(ab.getAbilityClassId()).getProperties().values().toArray()));
        	l.setWrapText(true);
        	tp.setContent(l);

        	 Platform.runLater(() -> detailSkillView.getPanes().add(tp));
        }
        Platform.runLater(() -> detailSkillView.getPanes().get(0).setExpanded(true));
        updateTooltips();

    }

    //Methods

    public void updateFonts() {
        classNameLabel.setFont(Font.font("Papyrus", 48. * UIConstants.getGlobalScalingFactor()));
        magicResistanceImageLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        magicResistanceValueLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        magicResistanceNameLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        armorImageLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        armorValueLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        armorNameLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        healthImageLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        healthValueLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        healthNameLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        attackDamageImageLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        attackDamageValueLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        attackDamageNameLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        attackDamageImageLabel.setTextFill(Color.BLANCHEDALMOND);
        timerCountersImageLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        timerCountersValueLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        timerCountersNameLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
    }

    public void updateSpacing() {
        statsGridPane.setPadding(new Insets(5 * UIConstants.getGlobalScalingFactor(), 5 * UIConstants.getGlobalScalingFactor(), 5 * UIConstants.getGlobalScalingFactor(), 0));
        statsGridPane.setVgap(10 * UIConstants.getGlobalScalingFactor());
        statsGridPane.setHgap(10 * UIConstants.getGlobalScalingFactor());
        buffTilePane.setPadding(new Insets(5  * UIConstants.getGlobalScalingFactor()));
        buffTilePane.setMaxHeight(buffTilePane.getWidth() / 6 * (1 + Math.floor(buffTilePane.getChildren().size() / 6)));
    }

    //Getter & Setter

    public final ObjectProperty<FXMLLoader> FXMLLoaderProperty() {
        return this.fxmlLoader;
    }


    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.FXMLLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.FXMLLoaderProperty().set(fxmlLoader);
    }

    public final ObjectProperty<Character> characterProperty() {
        return this.character;
    }


    public final Character getCharacter() {
        return this.characterProperty().get();
    }


    public final void setCharacter(final Character character) {
        Character oldValue = getCharacter();
        this.characterProperty().set(character);
        onCharacterChanged(oldValue, character);
    }

    public final ObjectProperty<LocalizationService> localizationServiceProperty() {
        return this.localizationService;
    }


    public final LocalizationService getLocalizationService() {
        return this.localizationServiceProperty().get();
    }


    public final void setLocalizationService(final LocalizationService localizationService) {
        this.localizationServiceProperty().set(localizationService);
    }

    public final ObjectProperty<GameImageService> gameImageServiceProperty() {
        return this.gameImageService;
    }


    public final GameImageService getGameImageService() {
        return this.gameImageServiceProperty().get();
    }


    public final void setGameImageService(final GameImageService gameImageService) {
        this.gameImageServiceProperty().set(gameImageService);
    }

    public final ObjectProperty<AbilityDataService> abilityClassServiceProperty() {
        return this.abilityClassService;
    }


    public final AbilityDataService getAbilityClassService() {
        return this.abilityClassServiceProperty().get();
    }


    public final void setAbilityClassService(final AbilityDataService abilityClassService) {
        this.abilityClassServiceProperty().set(abilityClassService);
    }

    public final ObjectProperty<CharacterDataService> characterClassServiceProperty() {
        return this.characterClassService;
    }


    public final CharacterDataService getCharacterClassService() {
        return this.characterClassServiceProperty().get();
    }


    public final void setCharacterClassService(final CharacterDataService characterClassService) {
        this.characterClassServiceProperty().set(characterClassService);
    }

}
