package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.apis.CharacterDisplayAPI;
import dok.client.desktop.ui.components.BuffCell;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.game.model.AbilityClass;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.DamageType;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.util.CharacterStateUtils;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public final class CharacterDisplay extends StackPane implements CharacterDisplayAPI {

    // Class Constants

    private static final Logger LOGGER = LogManager.getLogger();

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();
    private final ObjectProperty<Position> position = new SimpleObjectProperty<>();
    private final ObjectProperty<Character> character = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalizationService> localizationService = new SimpleObjectProperty<>();
    private final ObjectProperty<Consumer<Object>> inputConsumer = new SimpleObjectProperty<>();
    private final ObjectProperty<GameImageService> gameImageService = new SimpleObjectProperty<>();
    private final ObjectProperty<AbilityDataService> abilityClassService = new SimpleObjectProperty<>();
    private final ObjectProperty<CharacterDataService> characterClassService = new SimpleObjectProperty<>();
    private final ObjectProperty<GameView> responsibleGameView = new SimpleObjectProperty<>();
    private final Queue<Animation> animationQueue = new LinkedBlockingQueue<>();
    private final Stack<CharacterDisplayHighlightState> highlightingStack = new Stack<>();
    private volatile ObjectProperty<Animation> currentAnimation = new SimpleObjectProperty<>();

    //Injected Nodes

    @FXML ImageView characterImageView;
    @FXML Label classNameLabel;
    @FXML TilePane buffTilePane;
    @FXML Rectangle damageTakenRectangle;
    @FXML ImageView kingImageView;
    @FXML ImageView rankImageView;
    @FXML ImageView sourceTargetHighlightImageView;
    @FXML Rectangle selectionHighlightRectangle;
    @FXML VBox statsVBox;

    @FXML Label magicResistanceLabel;
    @FXML Label armorLabel;
    @FXML Label healthLabel;
    @FXML Label attackDamageLabel;
    @FXML Label timerCountersLabel;

    @FXML Label healthChangeLabel;
    @FXML Separator statsSeparator;

    //Constructor

    public CharacterDisplay() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));

        //Load the View
        getFXMLLoader().load();
    }

    public CharacterDisplay(GameView gameView, LocalizationService localizationService, GameImageService gameImageService, CharacterDataService characterClassService, AbilityDataService abilityClassService, Consumer<Object> inputConsumer, Position position) throws IOException {
        super();
        setPosition(position);
        setLocalizationService(localizationService);
        setGameImageService(gameImageService);
        setInputConsumer(inputConsumer);
        setCharacterClassService(characterClassService);
        setAbilityClassService(abilityClassService);
        setResponsibleGameView(gameView);

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));

        //Load the View
        getFXMLLoader().load();
    }

    //Initialization

    @FXML
    private void initialize() {
        // Register click handler
        this.setOnMouseClicked(this::onMouseClicked);
        this.setOnMouseEntered(this::onMouseEnter);
        this.setOnMouseExited(this::onMouseExit);
        this.setOnScroll(getResponsibleGameView()::onCharacterDisplayScroll);

        // Tooltips
        timerCountersLabel.setTooltip(new Tooltip());
        attackDamageLabel.setTooltip(new Tooltip());
        armorLabel.setTooltip(new Tooltip());
        magicResistanceLabel.setTooltip(new Tooltip());
        healthLabel.setTooltip(new Tooltip());

        // HealthChangeLabel
        healthChangeLabel.prefHeightProperty().bind(widthProperty());
        healthChangeLabel.prefWidthProperty().bind(heightProperty());
        healthChangeLabel.setVisible(false);
        healthChangeLabel.setAlignment(Pos.CENTER);
        healthChangeLabel.setTextAlignment(TextAlignment.CENTER);

        // Character Image
        characterImageView.fitWidthProperty().bind(widthProperty().multiply(0.96));
        characterImageView.fitHeightProperty().bind(heightProperty().multiply(0.96));
        characterImageView.setSmooth(true);

        // Stats Separator
        statsSeparator.maxWidthProperty().bind(widthProperty().multiply(0.6));

        // Armor Label
        ImageView armorImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/armor.png")));
        armorImageView.fitWidthProperty().bind(widthProperty().multiply(0.18));
        armorImageView.fitHeightProperty().bind(heightProperty().multiply(0.18));
        armorImageView.setSmooth(true);
        armorLabel.setGraphic(armorImageView);
        statsVBox.getChildren().remove(armorLabel);

        // MagicResistance Label
        ImageView magicResistanceImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/magic_resistance.png")));
        magicResistanceImageView.fitWidthProperty().bind(widthProperty().multiply(0.18));
        magicResistanceImageView.fitHeightProperty().bind(heightProperty().multiply(0.18));
        magicResistanceImageView.setSmooth(true);
        magicResistanceLabel.setGraphic(magicResistanceImageView);
        statsVBox.getChildren().remove(magicResistanceLabel);
        statsVBox.setVisible(false);

        // Health Label
        ImageView healthImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/health.png")));
        healthImageView.fitWidthProperty().bind(widthProperty().multiply(0.18));
        healthImageView.fitHeightProperty().bind(heightProperty().multiply(0.18));
        healthLabel.setGraphic(healthImageView);

        // Attack Damage Label
        ImageView attackDamageImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/damage.png")));
        attackDamageImageView.fitWidthProperty().bind(widthProperty().multiply(0.18));
        attackDamageImageView.fitHeightProperty().bind(heightProperty().multiply(0.18));
        attackDamageLabel.setGraphic(attackDamageImageView);

        // Time Counters Label
        ImageView timeCountersImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/game/time_counters.png")));
        timerCountersLabel.setGraphic(timeCountersImageView);
        timerCountersLabel.maxWidthProperty().bind(widthProperty().multiply(0.35));
        timerCountersLabel.maxHeightProperty().bind(heightProperty().multiply(0.35));
        timeCountersImageView.fitHeightProperty().bind(timerCountersLabel.maxHeightProperty());
        timeCountersImageView.fitWidthProperty().bind(timerCountersLabel.maxWidthProperty());
        timerCountersLabel.setVisible(false);

        // King Image View
        kingImageView.fitHeightProperty().bind(heightProperty().multiply(0.15));
        kingImageView.fitWidthProperty().bind(widthProperty().multiply(0.15));
        kingImageView.setSmooth(true);

        // Rank Image View
        rankImageView.fitHeightProperty().bind(heightProperty().multiply(0.10));
        rankImageView.fitWidthProperty().bind(widthProperty().multiply(0.10));
        rankImageView.setVisible(false);

        // Buff View
        buffTilePane.prefWidthProperty().bind(widthProperty().multiply(0.80));
        buffTilePane.setBackground(null);
        buffTilePane.prefTileWidthProperty().bind(buffTilePane.widthProperty().divide(7));
        buffTilePane.prefTileHeightProperty().bind(buffTilePane.prefTileWidthProperty());

        // Health Display
        damageTakenRectangle.widthProperty().bind(widthProperty());
        damageTakenRectangle.setMouseTransparent(true);

        // Selection Highlight Image View
        sourceTargetHighlightImageView.fitWidthProperty().bind(widthProperty());
        sourceTargetHighlightImageView.fitHeightProperty().bind(heightProperty());
        sourceTargetHighlightImageView.setSmooth(true);
        sourceTargetHighlightImageView.setMouseTransparent(true);
        sourceTargetHighlightImageView.setPickOnBounds(true);

        classNameLabel.setText("?");

        updateFonts();
        updateSpacing();
    }

    // <API>

    public void processAnimations() {
        if (getCurrentAnimation() == null && getAnimationQueue().peek() != null) {
            setCurrentAnimation(getAnimationQueue().poll());
            getCurrentAnimation().setOnFinished(this::resetAnimation);
            Platform.runLater(() -> getCurrentAnimation().playFromStart());
        }
    }

    private void resetAnimation(ActionEvent event) {
        setCurrentAnimation(null);
        processAnimations();
    }

    public void queueAnimation(Animation animation) {
        getAnimationQueue().add(animation);
    }

    public void playHealthChangeAnimation(double damageTaken) {
        LOGGER.debug("Attempting to play health change animation ...");
        FadeTransition fadeTransitionIn = new FadeTransition(Duration.seconds(0.25), healthChangeLabel);
        fadeTransitionIn.setFromValue(0);
        fadeTransitionIn.setToValue(1);
        fadeTransitionIn.setInterpolator(Interpolator.EASE_IN);

        double toScale = Math.min(1, Math.max(0.50 + damageTaken * 0.10, 0));
        LOGGER.debug("Scaling health change to: " + toScale);
        ScaleTransition scaleTransitionIn = new ScaleTransition(Duration.seconds(0.25), healthChangeLabel);
        scaleTransitionIn.setFromX(0);
        scaleTransitionIn.setFromY(0);
        scaleTransitionIn.setToX(toScale);
        scaleTransitionIn.setToY(toScale);
        scaleTransitionIn.setInterpolator(Interpolator.EASE_IN);

        ParallelTransition inTransition = new ParallelTransition(fadeTransitionIn, scaleTransitionIn);

        FadeTransition fadeTransitionOut = new FadeTransition(Duration.seconds(0.25), healthChangeLabel);
        fadeTransitionOut.setFromValue(1);
        fadeTransitionOut.setToValue(0);
        fadeTransitionOut.setInterpolator(Interpolator.EASE_IN);

        ScaleTransition scaleTransitionOut = new ScaleTransition(Duration.seconds(0.25), healthChangeLabel);
        scaleTransitionOut.setToX(0);
        scaleTransitionOut.setToY(0);
        scaleTransitionOut.setInterpolator(Interpolator.EASE_IN);

        ParallelTransition outTransition = new ParallelTransition(fadeTransitionOut, scaleTransitionOut);
        outTransition.setDelay(Duration.seconds(1));
        outTransition.setOnFinished((event2) -> healthChangeLabel.setVisible(false));
        outTransition.playFromStart();

        Platform.runLater(() -> healthChangeLabel.setVisible(true));

        queueAnimation(inTransition);
        queueAnimation(outTransition);
        processAnimations();
    }

    public void displayHealthChange(int damageTaken) {
        if (damageTaken == 0) return;
        else if (damageTaken < 0){
            LOGGER.debug("{} has been healed by {}.", this, Math.abs(damageTaken));
            Platform.runLater(() -> {
                healthChangeLabel.setEffect(new DropShadow(10 * UIConstants.getGlobalScalingFactor(), Color.DARKOLIVEGREEN));
                healthChangeLabel.setTextFill(Color.CHARTREUSE);
                healthChangeLabel.setText("+" + Math.abs(damageTaken));
            });
        } else if (damageTaken > 0) {
            LOGGER.debug("{} has been dealt {} damage.", this, damageTaken);
            Platform.runLater(() -> {
                healthChangeLabel.setEffect(new DropShadow(10 * UIConstants.getGlobalScalingFactor(), Color.CORAL));
                healthChangeLabel.setTextFill(Color.CRIMSON);
                healthChangeLabel.setText("-" + damageTaken);
            });
        }
        playHealthChangeAnimation(Math.abs(damageTaken));
    }

    public void updateTooltips() {

        try {
            String t1 = getLocalizationService().getText("Game.TimeCounters.Name")
                      + System.lineSeparator() + System.lineSeparator()
                      + getLocalizationService().getText("Game.TimeCounters.Description");
            Platform.runLater(() -> timerCountersLabel.getTooltip().setText(t1));

            String t4 =
                    getLocalizationService().getText("Game.CharacterAttributes.Armor.Name") + ": " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.ARMOR, getCharacterClassService())
                  + System.lineSeparator() + System.lineSeparator()
                  + getLocalizationService().getText("Game.CharacterAttributes.Armor.Description");
            Platform.runLater(() -> armorLabel.getTooltip().setText(t4));

            String t5 =
                    getLocalizationService().getText("Game.CharacterAttributes.MagicResistance.Name") + ": " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.MAGIC_RESISTANCE, getCharacterClassService())
                  + System.lineSeparator() + System.lineSeparator()
                  + getLocalizationService().getText("Game.CharacterAttributes.MagicResistance.Description");
            Platform.runLater(() -> magicResistanceLabel.getTooltip().setText(t5));

            String t6 =
                    getLocalizationService().getText("Game.CharacterAttributes.Health.Name") + ": "
                    + (CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService()) - getCharacter().getDamageTaken())
                    + " / " + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService())
                    + System.lineSeparator() + System.lineSeparator()
                    + getLocalizationService().getText("Game.CharacterAttributes.Health.Description");
            Platform.runLater(() -> healthLabel.getTooltip().setText(t6));

            Platform.runLater(() -> Tooltip.install(kingImageView, new Tooltip(getLocalizationService().getText("Game.King.Name")
                    + System.lineSeparator() + System.lineSeparator()
                    + getLocalizationService().getText("Game.King.Description"))));
        } catch (Exception e) {
            LOGGER.warn("Tooltips could not be updated", e);
        }
    }

    public CharacterDisplayHighlightState getCurrentHighlightState() {
        return getHighlightingStack().isEmpty() ? null : getHighlightingStack().peek();
    }

    public void clearHighlightState() {
    	LOGGER.debug("Clearing highlight state values ...");
    	Platform.runLater(() -> {
	    	setEffect(null);
	        statsVBox.setVisible(false);
	        statsVBox.getChildren().remove(armorLabel);
	        statsVBox.getChildren().remove(magicResistanceLabel);
	        setCursor(Cursor.DEFAULT);
	        if (getCharacter() != null && CharacterStateUtils.isCharacterAlive(getCharacter(), getCharacterClassService())) {
	        	buffTilePane.setVisible(true);
	        	if (getCharacter().getTimeCounters() > 0) timerCountersLabel.setVisible(true);
	        }
    	});
    }

    public void updateHighlightState() {
        // Clear previous highlighting
        clearHighlightState();

        // Logging
        LOGGER.debug("Attempting to update highlight state of " + getCharacter() + " to " + getCurrentHighlightState());

        // Null is not a legal highlighting state
        if (getCurrentHighlightState() == null || (getCharacter() != null && CharacterStateUtils.isCharacterDead(getCharacter(), getCharacterClassService()))) return;

        // Update effect
        InnerShadow effect = new InnerShadow();
        effect.setRadius(30 * UIConstants.getGlobalScalingFactor());
        effect.setBlurType(BlurType.GAUSSIAN);
        switch (getCurrentHighlightState()) {
            case LEGAL_TARGET:
                effect.setColor(Color.CORNSILK);
                break;
            case PRIMARY_TARGET_OPPONENT:
                effect.setColor(Color.RED);
                break;
            case SECONDARY_TARGET_OPPONENT:
                effect.setColor(Color.CORAL);
                break;
            case PRIMARY_TARGET_ALLY:
                effect.setColor(Color.GREENYELLOW);
                break;
            case SECONDARY_TARGET_ALLY:
                 effect.setColor(Color.GOLD); //effect.setColor(Color.CORAL);
                break;
        }
        LOGGER.debug("Overriding effect ...");
        Platform.runLater(() -> setEffect(effect));

        // Display info depending on ability, if there is an active selection going on
        if (getResponsibleGameView() == null || getResponsibleGameView().getActiveAbility() == null
        		|| getCurrentHighlightState() == CharacterDisplayHighlightState.LEGAL_TARGET) return;
        switch (getResponsibleGameView().getActiveAbility().getAbilityClassId()) {
            case AbilityClassIds.ATTACK_ABILITY_CLASS_ID:
            	Platform.runLater(() -> {
	                statsVBox.setVisible(true);
	                timerCountersLabel.setVisible(false);
	                buffTilePane.setVisible(false);
            	});

                Character caster = getResponsibleGameView().getCurrentModel().getCharacter(getResponsibleGameView().getActiveAbility().getOwningCharacterId());
                DamageType dt = caster.getDamageType();

                if (dt == DamageType.PHYSICAL) {
                    Platform.runLater(() -> {
                        statsVBox.getChildren().add(armorLabel);
                        String newArmor = String.valueOf(CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.ARMOR, getCharacterClassService()));
                        armorLabel.setText(newArmor);
                        attackDamageLabel.setTextFill(Color.WHITE);
                    });
                } else if (dt == DamageType.MAGICAL) {
                	Platform.runLater(() -> {
                		statsVBox.getChildren().add(magicResistanceLabel);
                        String newMr = String.valueOf(CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.MAGIC_RESISTANCE, getCharacterClassService()));
                        magicResistanceLabel.setText(newMr);
                        attackDamageLabel.setTextFill(Color.DEEPSKYBLUE);
                    });
                }

                int newAd = CharacterStateUtils.getCharacterAttributeValue(caster, CharacterAttribute.ATTACK_DAMAGE, getCharacterClassService());

                TargetType targetType = getResponsibleGameView().getActiveTargets().get(getPosition());
                AbilityClass abilityClass = getAbilityClassService().getAbilityClass(getResponsibleGameView().getActiveAbility().getAbilityClassId());

                if (targetType == TargetType.SECONDARY) {
                	newAd = (int) Math.round(newAd * abilityClass.getPropertyValue("secondaryDamageMultiplier").doubleValue());
                }

                String newAdString = String.valueOf(newAd);

                String newHealth = String.valueOf(CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService())
                        - Math.min(getCharacter().getDamageTaken(), CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService()))
                      + "/" + CharacterStateUtils.getCharacterAttributeValue(getCharacter(), CharacterAttribute.HEALTH, getCharacterClassService()));

                String t3 =
                        getLocalizationService().getText("Game.CharacterAttributes.Damage.Name") + ": " + CharacterStateUtils.getCharacterAttributeValue(caster, CharacterAttribute.ATTACK_DAMAGE, getCharacterClassService())
                      + System.lineSeparator()
                      + getLocalizationService().getText("Game.CharacterAttributes.DamageType.Name") + ": " + getLocalizationService().getText("Game.DamageType." + caster.getDamageType())
                      + System.lineSeparator() + System.lineSeparator()
                      + getLocalizationService().getText("Game.CharacterAttributes.Damage.Description");

                Platform.runLater(() -> {

                    attackDamageLabel.getTooltip().setText(t3);
                	attackDamageLabel.setText(newAdString);
                    healthLabel.setText(newHealth);

                });
            break;
        }
    }

    @Override
    public void addCharacterHighlightingLayer(CharacterDisplayHighlightState state) {
        getHighlightingStack().add(state);
        updateHighlightState();
    }

    @Override
    public void removeUpmostCharacterHighlightingLayer() {
        if (!getHighlightingStack().isEmpty()) {
            getHighlightingStack().pop();
            updateHighlightState();
        }
    }

    @Override
    public void clearCharacterHighlightingLayers() {
        getHighlightingStack().clear();
        clearHighlightState();
    }

    @Override
    public void enableSourceHighlighting() {
        sourceTargetHighlightImageView.setImage(getGameImageService().getIcon("source"));
    }

    @Override
    public void disableSourceHighlighting() {
        sourceTargetHighlightImageView.setImage(null);
    }

    @Override
    public void enableTargetHighlighting() {
        sourceTargetHighlightImageView.setImage(getGameImageService().getIcon("target"));
    }

    @Override
    public void disableTargetHighlighting() {
        sourceTargetHighlightImageView.setImage(null);
    }

    // </API>

    public BuffCell getBuffCell(Buff buff) {
    	for (Node child : new ArrayList<>(buffTilePane.getChildren())) {
    		BuffCell cell = (BuffCell) child;
    		if (cell.getItem().hasSameValuesAs(buff)) return cell;
    	}
    	return null;
    }

    public void addBuffCell(Buff buff) {
    	BuffCell buffCell = getBuffCell(buff);
    	if (buffCell != null) return;
    	else {
			BuffCell cell = new BuffCell(buff, getGameImageService(), getAbilityClassService(), getLocalizationService());
			buffTilePane.getChildren().add(cell);
			cell.updateSize(buffTilePane.getPrefTileWidth());
    	}
    }

    public void removeBuffCell(Buff buff) {
    	BuffCell buffCell = getBuffCell(buff);
    	if (buffCell == null) return;
    	else {
    		buffTilePane.getChildren().remove(buffCell);
    	}
    }

    public List<Buff> getDisplayedBuffCellItems() {
    	List<Buff> result = new ArrayList<>();
    	//
    	for (Node child : new ArrayList<>(buffTilePane.getChildren())) {
    		if (child instanceof BuffCell) result.add(((BuffCell) child).getItem());
    	}
    	//
    	return result;
    }

    public boolean containsExactBuff(Collection<Buff> buffs, Buff toSearch) {
    	for (Buff buff : buffs)
    		if (buff.hasSameValuesAs(toSearch)) return true;

    	return false;
    }

    //Event Handler

    public void onMouseEnter(MouseEvent event) {
        getResponsibleGameView().onCharacterDisplayHover(this);
    }

    public void onMouseExit(MouseEvent event) {
        getResponsibleGameView().removeHoverHighlighting();
    }

    public void onMouseClicked(MouseEvent event) {
        if (event.getButton() != MouseButton.PRIMARY) return;
        if (getInputConsumer() != null && getCurrentHighlightState() != null) {
            getInputConsumer().accept(getPosition());
            event.consume();
        } else LOGGER.warn("Cannot send Position because there is no consumer registered");
    }

    public void onCharacterChanged(Character oldValue, Character newValue) {
        int id = newValue.getCharacterClassId();
        LOGGER.debug("Character at " + getPosition() + " changed to " + newValue);

        // Show Character Attribute Images
        if (CharacterStateUtils.isCharacterAlive(newValue, getCharacterClassService())) {

            // TimeCounters

            if (oldValue == null || newValue.getTimeCounters() != oldValue.getTimeCounters()) {

                if (newValue.getTimeCounters() > 0) {
                    String newTimeCounters = "" + newValue.getTimeCounters();
                    Platform.runLater(() -> {
                        timerCountersLabel.setVisible(true);
                        timerCountersLabel.setText(newTimeCounters);
                    });
                } else {
                    Platform.runLater(() -> timerCountersLabel.setVisible(false));
                }

            }

            // Damage Taken
            if (oldValue == null || newValue.getDamageTaken() != oldValue.getDamageTaken()) {
                if (oldValue != null && oldValue.getId().equals(newValue.getId())) displayHealthChange(newValue.getDamageTaken() - oldValue.getDamageTaken());
                double newHealthPercentage = Math.min(1, (double) newValue.getDamageTaken() / (double) CharacterStateUtils.getCharacterAttributeValue(newValue, CharacterAttribute.HEALTH, getCharacterClassService()));
                Platform.runLater(() -> {
                    damageTakenRectangle.heightProperty().unbind();
                    damageTakenRectangle.heightProperty().bind(heightProperty().multiply(newHealthPercentage));
                });
            }

            // Rank Image

            if (oldValue == null || newValue.getRank() != oldValue.getRank()) {

                Image rankImage;

                if (newValue.getRank() == 1) {
                    rankImage = getGameImageService().getIcon("rank_1");
                } else if (newValue.getRank() == 2) {
                    rankImage = getGameImageService().getIcon("rank_2");
                } else {
                    rankImage = null;
                }

                Platform.runLater(() -> rankImageView.setImage(rankImage));

            }

            // Buffs

            Platform.runLater(() -> {

	            // remove buffs
	            for (Buff buff : getDisplayedBuffCellItems()) {
	            	if (!containsExactBuff(newValue.getBuffs(), buff))
	            		removeBuffCell(buff);
	            }

	            // add buffs
	            for (Buff buff : new ArrayList<>(newValue.getBuffs())) {

	            	if (getBuffCell(buff) == null) {
	            		addBuffCell(buff);
	            	}

	            }

            });

        } else {

            Platform.runLater(() -> {
                damageTakenRectangle.setVisible(false);
                timerCountersLabel.setVisible(false);
                rankImageView.setVisible(false);
                buffTilePane.setVisible(false);
            });

        }

        // Character Image

        boolean alive = CharacterStateUtils.isCharacterAlive(newValue, getCharacterClassService());
        if (alive && (oldValue == null || oldValue.getCharacterClassId() != newValue.getCharacterClassId())) {
            Image newCharacterImage = getGameImageService().getCharacterClassImage(id);
            Platform.runLater(() -> characterImageView.setImage(newCharacterImage));
        } else if (!alive) {
            Image newCharacterImage = getGameImageService().getCharacterClassImage(-1);
            Platform.runLater(() -> characterImageView.setImage(newCharacterImage));
        }

        // Class Name if Image is not visible

        Platform.runLater(() -> {
	        if (characterImageView.getImage() == null) {
	               classNameLabel.setVisible(true);
	               classNameLabel.setText(getLocalizationService().getText("Game.Character." + id + ".Name"));
	        } else {
	            classNameLabel.setVisible(false);
	        }
        });

        // King Image

        if (newValue.isKing() && (oldValue == null || !oldValue.isKing()))
            Platform.runLater(() -> kingImageView.setImage(getGameImageService().getIcon("king")));
        else if (!newValue.isKing() && (oldValue == null || oldValue.isKing()))
            Platform.runLater(() -> kingImageView.setImage(null));

        updateTooltips();

    }

    //Methods

    public void updateFonts() {
        classNameLabel.setFont(Font.font("Papyrus", 96. * UIConstants.getGlobalScalingFactor()));
        magicResistanceLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        armorLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        healthLabel.setFont(Font.font("Tahoma", 18. * UIConstants.getGlobalScalingFactor()));
        attackDamageLabel.setFont(Font.font("Tahoma", 20. * UIConstants.getGlobalScalingFactor()));
        attackDamageLabel.setTextFill(Color.BLANCHEDALMOND);
        timerCountersLabel.setFont(Font.font("Tahoma", 48. * UIConstants.getGlobalScalingFactor()));
        healthChangeLabel.setFont(Font.font("Tahoma", 172. * UIConstants.getGlobalScalingFactor()));
    }

    public void updateSpacing() {
        statsVBox.setPadding(new Insets(5 * UIConstants.getGlobalScalingFactor(), 5 * UIConstants.getGlobalScalingFactor(), 5 * UIConstants.getGlobalScalingFactor(), 0));
        buffTilePane.setPadding(new Insets(5  * UIConstants.getGlobalScalingFactor()));
        StackPane.setMargin(rankImageView, new Insets(10 * UIConstants.getGlobalScalingFactor()));
        buffTilePane.setMaxHeight(buffTilePane.getWidth() / 6 * (1 + Math.floor(buffTilePane.getChildren().size() / 6)));
        buffTilePane.setHgap(5 * UIConstants.getGlobalScalingFactor());
    }

    //Getter & Setter

    public final ObjectProperty<FXMLLoader> FXMLLoaderProperty() {
        return this.fxmlLoader;
    }


    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.FXMLLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.FXMLLoaderProperty().set(fxmlLoader);
    }

    public final ObjectProperty<Position> positionProperty() {
        return this.position;
    }


    public final Position getPosition() {
        return this.positionProperty().get();
    }


    public final void setPosition(final Position position) {
        this.positionProperty().set(position);
    }

    public final ObjectProperty<Character> characterProperty() {
        return this.character;
    }


    public final Character getCharacter() {
        return this.characterProperty().get();
    }


    public final void setCharacter(final Character character) {
        Character oldValue = getCharacter();
        this.characterProperty().set(character);
        onCharacterChanged(oldValue, character);
    }

    public final ObjectProperty<LocalizationService> localizationServiceProperty() {
        return this.localizationService;
    }


    public final dok.commons.LocalizationService getLocalizationService() {
        return this.localizationServiceProperty().get();
    }


    public final void setLocalizationService(final dok.commons.LocalizationService localizationService) {
        this.localizationServiceProperty().set(localizationService);
    }

    public final ObjectProperty<Consumer<Object>> inputConsumerProperty() {
        return this.inputConsumer;
    }


    public final Consumer<java.lang.Object> getInputConsumer() {
        return this.inputConsumerProperty().get();
    }


    public final void setInputConsumer(final Consumer<java.lang.Object> inputConsumer) {
        this.inputConsumerProperty().set(inputConsumer);
    }

    public final ObjectProperty<GameImageService> gameImageServiceProperty() {
        return this.gameImageService;
    }


    public final dok.client.services.GameImageService getGameImageService() {
        return this.gameImageServiceProperty().get();
    }


    public final void setGameImageService(final dok.client.services.GameImageService gameImageService) {
        this.gameImageServiceProperty().set(gameImageService);
    }

    public final ObjectProperty<AbilityDataService> abilityClassServiceProperty() {
        return this.abilityClassService;
    }


    public final AbilityDataService getAbilityClassService() {
        return this.abilityClassServiceProperty().get();
    }


    public final void setAbilityClassService(final AbilityDataService abilityClassService) {
        this.abilityClassServiceProperty().set(abilityClassService);
    }

    public final ObjectProperty<CharacterDataService> characterClassServiceProperty() {
        return this.characterClassService;
    }


    public final CharacterDataService getCharacterClassService() {
        return this.characterClassServiceProperty().get();
    }


    public final void setCharacterClassService(final CharacterDataService characterClassService) {
        this.characterClassServiceProperty().set(characterClassService);
    }

    public Queue<Animation> getAnimationQueue() {
        return animationQueue;
    }

    public final ReadOnlyObjectProperty<Animation> currentAnimationProperty() {
        return this.currentAnimation;
    }


    public final javafx.animation.Animation getCurrentAnimation() {
        return this.currentAnimationProperty().get();
    }


    private final void setCurrentAnimation(final javafx.animation.Animation currentAnimation) {
        this.currentAnimation.set(currentAnimation);
    }

    public Stack<CharacterDisplayHighlightState> getHighlightingStack() {
        return highlightingStack;
    }

    public final ObjectProperty<GameView> responsibleGameViewProperty() {
        return this.responsibleGameView;
    }


    public final dok.client.desktop.ui.controllers.GameView getResponsibleGameView() {
        return this.responsibleGameViewProperty().get();
    }


    public final void setResponsibleGameView(final dok.client.desktop.ui.controllers.GameView responsibleGameView) {
        this.responsibleGameViewProperty().set(responsibleGameView);
    }



}
