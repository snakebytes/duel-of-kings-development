package dok.client.desktop.ui.controllers;

import java.io.IOException;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.game.model.GameResult;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class EndScreen extends StackPane {

	//Properties

	private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

	//Injected Nodes

	@FXML Label resultLabel;
	@FXML Button continueButton;

	//Constructor(s)

	public EndScreen() throws IOException {
		super();

		//Apply CSS
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		getStyleClass().add(getClass().getSimpleName());

		//Setup FXMLLoader
		setFXMLLoader(new FXMLLoader());
		getFXMLLoader().setRoot(this);
		getFXMLLoader().setController(this);
		getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
		getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

		//Load the View
		getFXMLLoader().load();
	}

	public EndScreen(GameResult result) throws IOException {
		this();
		switch (result.getResultType()) {
			case WIN:
				resultLabel.setTextFill(Color.CHARTREUSE);
				break;
			case LOSS:
				resultLabel.setTextFill(Color.CRIMSON);
				break;
			case DRAW:
				resultLabel.setTextFill(Color.GOLD);
				break;
		}
		resultLabel.setText(ClientApplication.getInstance().getLocalizationProvider().getText("Game.ResultType." + result.getResultType()));
	}

	// Methods

	@FXML
	public void initialize() {
		continueButton.setOnAction(this::continueButtonOnAction);
		updateSpacing();
		updateFonts();
	}

	public void updateSpacing() {
		setPadding(new Insets(50. * UIConstants.getGlobalScalingFactor()));
	}

	public void updateFonts() {
		continueButton.setFont(Font.font("Papyrus", 40. * UIConstants.getGlobalScalingFactor()));
		resultLabel.setFont(Font.font("Papyrus", 96. * UIConstants.getGlobalScalingFactor()));
	}

	// Events

	public void continueButtonOnAction(ActionEvent event) {
		ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getMainView());
	}

	// Getter & Setter

	public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
		return this.fxmlLoader;
	}


	public final javafx.fxml.FXMLLoader getFXMLLoader() {
		return this.fxmlLoaderProperty().get();
	}


	public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
		this.fxmlLoaderProperty().set(fxmlLoader);
	}

}
