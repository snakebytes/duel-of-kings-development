package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.game.model.Character;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public final class FieldDisplay extends GridPane {

	// Class Constants

	private static final Logger LOGGER = LogManager.getLogger();

	// Attributes

	private int rows;
	private int cols;
	private boolean flipped;

	// Properties

	private final ObjectProperty<LocalizationService> localizationService = new SimpleObjectProperty<>();
	private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();
	private final ObjectProperty<Map<Pair<Integer, Integer>, Character>> field = new SimpleObjectProperty<>();
	private final ObjectProperty<Map<Pair<Integer, Integer>, CharacterDisplay>> displays = new SimpleObjectProperty<>(new HashMap<Pair<Integer, Integer>, CharacterDisplay>());
	private final ObjectProperty<UUID> player = new SimpleObjectProperty<>();
	private final ObjectProperty<Consumer<Object>> inputConsumer = new SimpleObjectProperty<>();
	private final ObjectProperty<GameImageService> gameImageService = new SimpleObjectProperty<>();
	private final ObjectProperty<AbilityDataService> abilityClassService = new SimpleObjectProperty<>();
	private final ObjectProperty<CharacterDataService> characterClassService = new SimpleObjectProperty<>();
	private final ObjectProperty<GameView> gameView = new SimpleObjectProperty<GameView>();

	//Injected Nodes

	//TODO ...

	//Constructor
	public FieldDisplay() throws IOException {
		super();

		//Apply CSS
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		getStyleClass().add(getClass().getSimpleName());

		//Setup FXMLLoader
		setFXMLLoader(new FXMLLoader());
		getFXMLLoader().setRoot(this);
		getFXMLLoader().setController(this);
		getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));

		//Load the View
		getFXMLLoader().load();
	}

	public FieldDisplay(GameView gameView, LocalizationService localizationService, GameImageService gameImageService, CharacterDataService characterClassService, AbilityDataService abilityClassService, Consumer<Object> consumer, UUID player, int rows, int cols, boolean flip) throws IOException {
		this();
		setPlayer(player);
		this.setRows(rows);
		this.setCols(cols);
		this.setLocalizationService(localizationService);
		this.setInputConsumer(consumer);
		this.setGameImageService(gameImageService);
		this.setCharacterClassService(characterClassService);
		setGameView(gameView);
		setAbilityClassService(abilityClassService);
		this.flipped = flip;
	}

	//Initialization

	@FXML
	private void initialize() {
		updateSpacing();
		updateFonts();
	}

	//Event Handler

	public void onFieldChanged(Map<Pair<Integer, Integer>, Character> oldValue, Map<Pair<Integer, Integer>, Character> newValue) {
		LOGGER.debug("Attempting to update field to " + newValue);
		for (int row = 0; row < this.getRows(); row++) {
			for (int col = 0; col < this.getCols(); col++) {
				final int c = col;
				final int r = row;
				List<Node> nodes = getChildren().stream().filter((node) -> GridPane.getColumnIndex(node) == c && GridPane.getRowIndex(node) == r).collect(Collectors.toList());
				if (nodes.size() > 0) {
					CharacterDisplay characterDisplay = (CharacterDisplay) nodes.get(0);
					Character chr = getField().get(characterDisplay.getPosition().getCoordinates());
					if (characterDisplay.getCharacter() == null || !chr.hasEqualValues(characterDisplay.getCharacter()))
						characterDisplay.setCharacter(chr);
				} else LOGGER.error("There is no CharacterDisplay for Position (r=" + row + ", c=" + col + ")");
			}
		}
	}

	//Methods

	public void initializeCharacterDisplays() {
		LOGGER.entry();
		LOGGER.debug("Initializing CharacterDisplays (flip=" + isFlipped() + ")");

		// Initialize Field
		for (int row = 0; row < getRows(); row++) {
			for (int col = 0; col < getCols(); col++) {
				Position pos = null;
				if (isFlipped()) pos = new Position(getPlayer(), getCols() - col - 1, row);
				else pos = new Position(getPlayer(), col, row);
				LOGGER.debug("Initializing CharacterDisplay at position " + pos + " at grid-coordinates (r: " + row + ", c:" + col + ")");
				try {
					// Initialize display
					CharacterDisplay characterDisplay = new CharacterDisplay(getGameView(), getLocalizationService(), getGameImageService(), getCharacterClassService(), getAbilityClassService(), getInputConsumer(), pos);
					GridPane.setHgrow(characterDisplay, Priority.ALWAYS);
					GridPane.setVgrow(characterDisplay, Priority.ALWAYS);

					// Add the display to the grid
					add(characterDisplay, col, row);

					getDisplays().put(pos.getCoordinates(), characterDisplay);
				} catch (IOException e) {
					LOGGER.fatal("CharacterDisplay at position " + pos + " could not be created", e);
				}
			}
		}
		LOGGER.exit();
	}

	public void updateFonts() {
		getDisplays().values().forEach((characterDisplay) -> characterDisplay.updateFonts());
	}

	public void updateSpacing() {
		setPrefWidth(ClientApplication.getInstance().getMainWindow().getWidth() * 0.375);
		setMaxHeight(getPrefWidth());

		// Update Character Displays
		getDisplays().values().forEach((characterDisplay) -> characterDisplay.updateSpacing());
	}

	//Getter & Setter

	public final ObjectProperty<FXMLLoader> FXMLLoaderProperty() {
		return this.fxmlLoader;
	}


	public final javafx.fxml.FXMLLoader getFXMLLoader() {
		return this.FXMLLoaderProperty().get();
	}


	public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
		this.FXMLLoaderProperty().set(fxmlLoader);
	}

	public final ObjectProperty<Map<Pair<Integer, Integer>, Character>> fieldProperty() {
		return this.field;
	}


	public final java.util.Map<Pair<java.lang.Integer, java.lang.Integer>, dok.game.model.Character> getField() {
		return this.fieldProperty().get();
	}


	public final void setField(final Map<Pair<Integer, Integer>, Character> newValue) {
		Map<Pair<Integer, Integer>, Character> oldValue = getField();
		this.fieldProperty().set(newValue);
		onFieldChanged(oldValue, newValue);
	}

	public final ObjectProperty<UUID> playerProperty() {
		return this.player;
	}


	public final java.util.UUID getPlayer() {
		return this.playerProperty().get();
	}

	public final void setPlayer(final java.util.UUID player) {
		this.playerProperty().set(player);
	}


	protected final int getRows() {
		return rows;
	}

	protected final void setRows(int rows) {
		this.rows = rows;
	}

	protected final int getCols() {
		return cols;
	}

	protected final void setCols(int cols) {
		this.cols = cols;
	}

	public final ObjectProperty<LocalizationService> localizationServiceProperty() {
		return this.localizationService;
	}


	public final dok.commons.LocalizationService getLocalizationService() {
		return this.localizationServiceProperty().get();
	}


	public final void setLocalizationService(final dok.commons.LocalizationService localizationService) {
		this.localizationServiceProperty().set(localizationService);
	}

	public boolean isFlipped() {
		return flipped;
	}

	public final ObjectProperty<Consumer<Object>> inputConsumerProperty() {
		return this.inputConsumer;
	}


	public final Consumer<java.lang.Object> getInputConsumer() {
		return this.inputConsumerProperty().get();
	}


	public final void setInputConsumer(final Consumer<java.lang.Object> inputConsumer) {
		this.inputConsumerProperty().set(inputConsumer);
	}

	public final ObjectProperty<Map<Pair<Integer, Integer>, CharacterDisplay>> displaysProperty() {
		return this.displays;
	}


	public final java.util.Map<Pair<java.lang.Integer, Integer>, CharacterDisplay> getDisplays() {
		return this.displaysProperty().get();
	}


	public final void setDisplays(
			final Map<Pair<java.lang.Integer, Integer>, CharacterDisplay> displays) {
		this.displaysProperty().set(displays);
	}

	public final ObjectProperty<GameImageService> gameImageServiceProperty() {
		return this.gameImageService;
	}


	public final dok.client.services.GameImageService getGameImageService() {
		return this.gameImageServiceProperty().get();
	}


	public final void setGameImageService(final dok.client.services.GameImageService gameImageService) {
		this.gameImageServiceProperty().set(gameImageService);
	}

	public final ObjectProperty<AbilityDataService> abilityClassServiceProperty() {
		return this.abilityClassService;
	}


	public final AbilityDataService getAbilityClassService() {
		return this.abilityClassServiceProperty().get();
	}


	public final void setAbilityClassService(final AbilityDataService abilityClassService) {
		this.abilityClassServiceProperty().set(abilityClassService);
	}

	public final ObjectProperty<CharacterDataService> characterClassServiceProperty() {
		return this.characterClassService;
	}


	public final CharacterDataService getCharacterClassService() {
		return this.characterClassServiceProperty().get();
	}


	public final void setCharacterClassService(final CharacterDataService characterClassService) {
		this.characterClassServiceProperty().set(characterClassService);
	}

	public final ObjectProperty<GameView> gameViewProperty() {
		return this.gameView;
	}


	public final dok.client.desktop.ui.controllers.GameView getGameView() {
		return this.gameViewProperty().get();
	}


	public final void setGameView(final dok.client.desktop.ui.controllers.GameView gameView) {
		this.gameViewProperty().set(gameView);
	}


}
