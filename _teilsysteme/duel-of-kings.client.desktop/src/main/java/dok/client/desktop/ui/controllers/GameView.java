package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.apis.CharacterDisplayAPI.CharacterDisplayHighlightState;
import dok.client.desktop.ui.apis.GameViewAPI;
import dok.client.desktop.ui.components.AbilitySelection;
import dok.client.desktop.ui.components.EffectCell;
import dok.client.desktop.ui.components.TriggerSelection;
import dok.client.services.GameImageService;
import dok.client.services.GameUIService;
import dok.commons.LocalizationService;
import dok.commons.impl.CustomTimer;
import dok.commons.model.constants.ModelConstants;
import dok.game.logic.Game;
import dok.game.logic.GameLogicConstants;
import dok.game.model.Ability;
import dok.game.model.AbilityClass;
import dok.game.model.Character;
import dok.game.model.Effect;
import dok.game.model.GamePhase;
import dok.game.model.GameStateModel;
import dok.game.model.Player;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.util.Duration;

public final class GameView extends BorderPane implements GameUIService, GameViewAPI {

    // Class Constants

    private static final Logger LOGGER = LogManager.getLogger(GameView.class.getName());

    //Attributes

    private Thread timerLabelUpdateThread;
    private Thread interfaceUpdateThread;
    private final List<TriggerSelection> triggerSelections = new ArrayList<>();
    private final Queue<GameStateModel> modelQueue = new ConcurrentLinkedQueue<>();
    private Transition notificationAnimation;

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();
    private final ObjectProperty<UUID> player = new SimpleObjectProperty<>();
    private final ObjectProperty<Game> gameService = new SimpleObjectProperty<>();
    private final ObservableMap<UUID, FieldDisplay> fieldDisplays = FXCollections.observableMap(new HashMap<>());
    private final ObjectProperty<LocalizationService> localizationService = new SimpleObjectProperty<>();
    private final ObjectProperty<Consumer<Object>> inputConsumer = new SimpleObjectProperty<>();
    private final ObjectProperty<AbilitySelection> abilitySelection = new SimpleObjectProperty<>();
    private final ObjectProperty<CustomTimer> turnTimer = new SimpleObjectProperty<>();
    private final ObjectProperty<CustomTimer> timer = new SimpleObjectProperty<>();
    private final ObjectProperty<GameImageService> gameImageService = new SimpleObjectProperty<>();
    private final ObjectProperty<CharacterDataService> characterClassService = new SimpleObjectProperty<>();
    private final ObjectProperty<AbilityDataService> abilityClassService = new SimpleObjectProperty<>();
    private final ObjectProperty<GameStateModel> currentModel = new SimpleObjectProperty<>();
    private final ObjectProperty<Ability> activeAbility = new SimpleObjectProperty<>();
    private final ObjectProperty<Map<Position, TargetType>> activeTargets = new SimpleObjectProperty<Map<Position,TargetType>>();
    private final ObservableList<Animation> effectStackAnimations = FXCollections.observableArrayList();
    private Animation timerBlinkAnimation;
    private Animation characterDetailDisplayAnimationIn;
    private Animation characterDetailDisplayAnimationOut;

    //Injected Nodes

    @FXML HBox fieldsHBox;
    @FXML Label timerLabel;
    @FXML Label taskLabel;
    @FXML Label roundCountLabel;
    @FXML Label actionsLeftLabel;
    @FXML Label actionsLeftLabel2;
    @FXML Button cancelButton;
    @FXML StackPane centerStackPane;
    @FXML VBox effectStackVBox;
    //@FXML Pane drawingPane;
    @FXML StackPane topStackPane;
    @FXML Label notificationBanner;
    //@FXML Label effectStackLabel;
    @FXML Label selfDisplay;
    @FXML Label opponentDisplay;
    CharacterDetailDisplay characterDetailDisplay;
    Label effectStackDummyLabel;

    //Constructor

    public GameView() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
    }

    public GameView(LocalizationService localizationService, GameImageService gameImageService, Game gameService, CharacterDataService characterClassService, AbilityDataService abilityClassService, Consumer<Object> inputConsumer, UUID player) throws IOException {
        super();

        setLocalizationService(localizationService);
        setGameService(gameService);
        setPlayer(player);
        setInputConsumer(inputConsumer);
        setGameImageService(gameImageService);
        setAbilityClassService(abilityClassService);

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();

    }

    public GameView(UUID player, LocalizationService localizationService, GameImageService gameImageService,
    		CharacterDataService characterClassService, AbilityDataService abilityClassService) throws IOException
    {
        super();

        setPlayer(player);
        setLocalizationService(localizationService);
        setGameImageService(gameImageService);
        setAbilityClassService(abilityClassService);
        setCharacterClassService(characterClassService);

        // Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        // Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        // Load the View
        getFXMLLoader().load();

        // Additional initialization
        initializeCharacterDetailDisplay();
	}

    public GameView(LocalizationService localizationService, GameImageService gameImageService,
    		CharacterDataService characterClassService, AbilityDataService abilityClassService) throws IOException
    {
        super();

        setLocalizationService(localizationService);
        setGameImageService(gameImageService);
        setAbilityClassService(abilityClassService);
        setCharacterClassService(characterClassService);

        // Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        // Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        // Load the View
        getFXMLLoader().load();

        // Additional initialization
        initializeCharacterDetailDisplay();
	}

    //Initialization

	@FXML
    private void initialize() throws IOException {
        this.setOnKeyPressed(this::onKeyPressed);
        this.setOnMouseClicked(this::onMouseClicked);
        this.cancelButton.setOnAction(this::onCancelButtonAction);

        // Timer Blink Animation
        ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(0.50), timerLabel);
        scaleTransition.setFromX(1);
        scaleTransition.setFromY(1);
        scaleTransition.setToX(1.75);
        scaleTransition.setToY(1.75);
        setTimerBlinkAnimation(scaleTransition);
        getTimerBlinkAnimation().setAutoReverse(true);
        getTimerBlinkAnimation().setCycleCount(Timeline.INDEFINITE);

        effectStackDummyLabel = new Label("...");
        effectStackDummyLabel.setAlignment(Pos.CENTER);
        effectStackDummyLabel.prefWidthProperty().bind(effectStackVBox.widthProperty().multiply(0.85));
        effectStackDummyLabel.prefHeightProperty().bind(effectStackDummyLabel.prefWidthProperty());
        effectStackDummyLabel.setTextFill(Color.WHITE);

        cancelButton.prefHeightProperty().bind(heightProperty().multiply(0.06));
        cancelButton.prefWidthProperty().bind(cancelButton.prefHeightProperty().multiply(1.7));

        //ImageView roundCounterImageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/round_counter.png")));
        roundCountLabel.setAlignment(Pos.CENTER);
        roundCountLabel.prefHeightProperty().bind(taskLabel.heightProperty());
        //roundCountLabel.prefWidthProperty().bind(roundCountLabel.prefHeightProperty());
        //roundCounterImageView.fitHeightProperty().bind(roundCountLabel.prefHeightProperty());
        //roundCounterImageView.fitWidthProperty().bind(roundCountLabel.prefWidthProperty());
        //roundCounterImageView.setSmooth(true);
        //roundCountLabel.setGraphic(roundCounterImageView);

        // effectStackVBox
        effectStackVBox.setAlignment(Pos.BOTTOM_CENTER);
        effectStackVBox.setBackground(null);
        effectStackVBox.maxWidthProperty().bind(widthProperty().multiply(0.04));
        effectStackVBox.setMaxHeight(ClientApplication.getInstance().getMainWindow().getWidth() * 0.375);
        //effectStackVBox.setVisible(false);

        Rectangle effectStackClip = new Rectangle();
        effectStackClip.widthProperty().bind(effectStackVBox.widthProperty());
        effectStackClip.heightProperty().bind(effectStackVBox.heightProperty());
        effectStackVBox.setClip(effectStackClip);

        //ImageView timerLabelImage = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/timer.png")));
        //timerLabelImage.fitHeightProperty().bind(timerLabel.heightProperty());
        //timerLabelImage.fitWidthProperty().bind(timerLabelImage.fitHeightProperty());
        timerLabel.prefHeightProperty().bind(heightProperty().multiply(0.05));
        timerLabel.prefWidthProperty().bind(timerLabel.heightProperty());
        //timerLabel.setGraphic(timerLabelImage);
        timerLabel.setContentDisplay(ContentDisplay.CENTER);
        timerLabel.setAlignment(Pos.CENTER);

        notificationBanner.setAlignment(Pos.CENTER);
        notificationBanner.prefWidthProperty().bind(centerStackPane.widthProperty());
        notificationBanner.prefHeightProperty().bind(centerStackPane.heightProperty().multiply(0.20));
        notificationBanner.setVisible(false);
        notificationBanner.setMouseTransparent(true);

        // Update stuff
        updateFonts();
        updateSpacing();
        updateSize();

    }

    public void initializeCharacterDetailDisplay() throws IOException {
		characterDetailDisplay = new CharacterDetailDisplay(getLocalizationService(), getGameImageService(), getCharacterClassService(), getAbilityClassService());
        characterDetailDisplay.setOnScroll(this::onCharacterDetailDisplayScroll);
        centerStackPane.getChildren().add(characterDetailDisplay);
        characterDetailDisplay.setVisible(false);

        // Character Detail Display Animations
        ScaleTransition cddScaleAnimationIn = new ScaleTransition(Duration.seconds(0.25), characterDetailDisplay.innerContainer);
        cddScaleAnimationIn.setToX(1);
        cddScaleAnimationIn.setToY(1);

        FadeTransition cddFadeTransitionIn = new FadeTransition(Duration.seconds(0.25), characterDetailDisplay);
        cddFadeTransitionIn.setToValue(1);

        setCharacterDetailDisplayAnimationIn(new ParallelTransition(cddScaleAnimationIn, cddFadeTransitionIn));

        ScaleTransition cddScaleAnimationOut = new ScaleTransition(Duration.seconds(0.25), characterDetailDisplay.innerContainer);
        cddScaleAnimationOut.setToX(0);
        cddScaleAnimationOut.setToY(0);

        FadeTransition cddFadeTransitionOut = new FadeTransition(Duration.seconds(0.25), characterDetailDisplay);
        cddFadeTransitionOut.setToValue(0);

        setCharacterDetailDisplayAnimationOut(new ParallelTransition(cddScaleAnimationOut, cddFadeTransitionOut));
        getCharacterDetailDisplayAnimationOut().setOnFinished((event) -> characterDetailDisplay.setVisible(false));
    }

    private void updateTimer() {
    	do {
			// Stop updating if the execution of this thread has been interrupted
			if (Thread.interrupted()) break;

			int timeLeft = -1;

		    if (getTimer() != null && !getTimer().isStopped()) {

		    	timeLeft = (int) (getTimer().getTimeLeft().toMillis() / 1000);
		    	String newValue = String.valueOf(timeLeft);
		    	Platform.runLater(() -> {
		    		this.timerLabel.setVisible(true);
		        	this.timerLabel.setText(newValue);
		    	});

		    } else if (getTurnTimer() != null && !getTurnTimer().isStopped()) {

		    	timeLeft = (int) (getTurnTimer().getTimeLeft().toMillis() / 1000);
		    	String newValue = String.valueOf(timeLeft);
		    	Platform.runLater(() -> {
		    		this.timerLabel.setVisible(true);
		        	this.timerLabel.setText(newValue);
		    	});

		    } else Platform.runLater(() -> this.timerLabel.setVisible(false));

	    	if (timeLeft < 10 && timeLeft >= 0 && getTimerBlinkAnimation().getStatus() == Status.STOPPED) {
	    		timerLabel.setTextFill(Color.RED);
	    		getTimerBlinkAnimation().playFromStart();
	    	} else if (timeLeft > 10 && getTimerBlinkAnimation().getStatus() == Status.RUNNING) {
	    		timerLabel.setTextFill(Color.WHITE);
	    		getTimerBlinkAnimation().stop();
	    	}

			try {
			    Thread.sleep(1000);
			} catch (InterruptedException e) {
			    break;
			}

       } while(isVisible());
    }

    //Event Handler

    public void onCharacterDisplayScroll(ScrollEvent event) {
    	LOGGER.debug(event);
    	if (event.getDeltaY() > 0) {
    		CharacterDisplay characterDisplay = (CharacterDisplay) event.getSource();
    		characterDetailDisplay.setCharacter(characterDisplay.getCharacter());
    		characterDetailDisplay.setVisible(true);
	        if (getCharacterDetailDisplayAnimationOut().getStatus() == Status.RUNNING) getCharacterDetailDisplayAnimationOut().stop();
	        getCharacterDetailDisplayAnimationIn().play();
    	}
    }

    public void onCharacterDetailDisplayScroll(ScrollEvent event) {
    	if (event.getDeltaY() < 0) {
    		if (getCharacterDetailDisplayAnimationIn().getStatus() == Status.RUNNING) getCharacterDetailDisplayAnimationIn().stop();
	        getCharacterDetailDisplayAnimationOut().play();
    	}
    }

    public void onKeyPressed(KeyEvent event) {
        LOGGER.entry(event);
        if (event.getCode() == KeyCode.ESCAPE)
            getInputConsumer().accept(GameLogicConstants.SURRENDER_CODE);
        LOGGER.exit();
    }

    public void onCancelButtonAction(ActionEvent event) {
        getInputConsumer().accept(null);
        event.consume();
    }

    public void onMouseClicked(MouseEvent event) {
        if (event.getButton() == MouseButton.PRIMARY) {
            getInputConsumer().accept(false);
            event.consume();
        }
    }

    //Methods

    public void setPlayerNames(String self, String opponent) {
    	selfDisplay.setText(self);
    	opponentDisplay.setText(opponent);
    }

    public void initializeFieldDisplay(UUID player) {
        try {
            FieldDisplay fieldDisplay = new FieldDisplay(this, getLocalizationService(), getGameImageService(), getCharacterClassService(), getAbilityClassService(), getInputConsumer(), player, getGameService().getLineup(player).getRowCount(), getGameService().getLineup(player).getColCount(), player.equals(getPlayer()));
            fieldDisplay.initializeCharacterDisplays();
            getFieldDisplays().put(player, fieldDisplay);
            fieldsHBox.getChildren().add(fieldDisplay);
        } catch (IOException e) {
            LOGGER.warn("FieldDisplay for player(" + player + ") could not be created", e);
        }
    }

    public void initializeFieldDisplays() {
        LOGGER.debug("Initializing GameViewFields");
        if (getGameService() != null) {
            // Sort owning player to the front
            List<UUID> tmpPlayerids = new ArrayList<>(getGameService().getPlayerIds());
            int tries = 0;
            LOGGER.debug("Attempting to pull " + getPlayer() + " to the front...");
            while (tries++ < tmpPlayerids.size() && !tmpPlayerids.get(0).equals(getPlayer())) {
                UUID tmpPlayerid = tmpPlayerids.remove(0);
                LOGGER.debug("Moving " + tmpPlayerid + " to the back");
                tmpPlayerids.add(tmpPlayerid);
            }

            // Initialize the fields
            for (UUID playerid : tmpPlayerids) {
                initializeFieldDisplay(playerid);
            }
        } else LOGGER.warn("Fields of GameView could not be initialized because there is no PlayerListService registered");

        // IMPORTANT: Update Layout and CSS of both the view itself, but also all children!
        applyCss();
        layout();
    }

    public void updateFonts() {

        timerLabel.setFont(Font.font("Papyrus", 36. * UIConstants.getGlobalScalingFactor()));
        roundCountLabel.setFont(Font.font("Papyrus", 32. * UIConstants.getGlobalScalingFactor()));
        actionsLeftLabel.setFont(Font.font("Papyrus", 24. * UIConstants.getGlobalScalingFactor()));
        actionsLeftLabel2.setFont(Font.font("Papyrus", 24. * UIConstants.getGlobalScalingFactor()));
        cancelButton.setFont(Font.font("Papyrus", 24. * UIConstants.getGlobalScalingFactor()));
        taskLabel.setFont(Font.font("Papyrus", 24. * UIConstants.getGlobalScalingFactor()));
        notificationBanner.setFont(Font.font("Papyrus", 48. * UIConstants.getGlobalScalingFactor()));
        effectStackDummyLabel.setFont(Font.font("Tahoma", 16 * UIConstants.getGlobalScalingFactor()));
        selfDisplay.setFont(Font.font("Papyrus", 36. * UIConstants.getGlobalScalingFactor()));
        opponentDisplay.setFont(Font.font("Papyrus", 36. * UIConstants.getGlobalScalingFactor()));
        //effectStackLabel.setFont(Font.font("Papyrus", 32. * UIConstants.getGlobalScalingFactor()));

        // Update field display fonts
        getFieldDisplays().values().forEach((display) -> display.updateFonts());
    }

    public void updateSpacing() {
        StackPane.setMargin(fieldsHBox, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.05, ClientApplication.getInstance().getMainWindow().getWidth() * 0.10, 0, ClientApplication.getInstance().getMainWindow().getWidth() * 0.10));
        StackPane.setMargin(effectStackVBox, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.05, 0, 0, 0));
        //StackPane.setMargin(effectStackLabel, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.775, 0, 0, 0));
        StackPane.setMargin(cancelButton, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.08, 0, 0, 0));
        //StackPane.setMargin(timerLabel, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.01, 0, 0, 0));
        StackPane.setMargin(actionsLeftLabel, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.10, 0, 0, ClientApplication.getInstance().getMainWindow().getWidth() * 0.10));
        StackPane.setMargin(actionsLeftLabel2, new Insets(ClientApplication.getInstance().getMainWindow().getHeight() * 0.10, ClientApplication.getInstance().getMainWindow().getWidth() * 0.10 - actionsLeftLabel2.getWidth(), 0, 0));
        StackPane.setMargin(selfDisplay, new Insets(850 * UIConstants.getGlobalScalingFactor(), 800 * UIConstants.getGlobalScalingFactor(), 0, 0));
        StackPane.setMargin(opponentDisplay, new Insets(850 * UIConstants.getGlobalScalingFactor(), 0, 0, 800 * UIConstants.getGlobalScalingFactor()));

        fieldsHBox.setSpacing(ClientApplication.getInstance().getMainWindow().getWidth() * 0.05);
        effectStackVBox.setSpacing(5 * UIConstants.getGlobalScalingFactor());
        effectStackVBox.setPadding(new Insets(5 * UIConstants.getGlobalScalingFactor()));

        // Update field display spacings
        getFieldDisplays().values().forEach((display) -> display.updateSpacing());
    }

    public void updateSize() {

    }

    // <GameUIService>

    @Override
    public void hideReactions() {
        for (TriggerSelection trs : new ArrayList<>(getTriggerSelections())) {
            Platform.runLater(() -> centerStackPane.getChildren().remove(trs));
            getTriggerSelections().remove(trs);
        }
        Platform.runLater(() -> requestFocus());
    }

    @Override
    public void showReactions(List<Trigger> possibleReactions) {
        LOGGER.debug("Reactions to choose from: " + possibleReactions);
        // Sort triggers by character
        Map<UUID, List<Trigger>> triggersByCharacterId = new HashMap<>();
        for (Trigger trigger : possibleReactions) {
            UUID characterId = trigger.getSourceAbility().getOwningCharacterId();
            if (!triggersByCharacterId.containsKey(characterId)) triggersByCharacterId.put(characterId, new ArrayList<>());
            triggersByCharacterId.get(characterId).add(trigger);
        }
        // Show the triggers next to their character
        for (UUID characterId : triggersByCharacterId.keySet()) {

            Platform.runLater(() -> {

                CharacterDisplay display = getCharacterDisplayByCharacterId(characterId);
            	TriggerSelection triggerSelection = new TriggerSelection(
                        getCurrentModel().getCharacter(characterId),
                        display.getHeight(),
                        triggersByCharacterId.get(characterId),
                        getInputConsumer(),
                        getLocalizationService(),
                        getAbilityClassService(),
                        getGameImageService());

    	        // Add to view
    	        StackPane.setAlignment(triggerSelection, Pos.TOP_LEFT);
            	getTriggerSelections().add(triggerSelection);
    	        centerStackPane.getChildren().add(triggerSelection);

    	        // Define Position
    	        Bounds boundsInTarget = centerStackPane.sceneToLocal(display.localToScene(display.getLayoutBounds()));
    	        double offsetX = boundsInTarget.getMinX() + ((boundsInTarget.getWidth() * (1-TriggerSelection.getDiametermodifier()))/2);
    	        double offsetY = boundsInTarget.getMinY() + ((boundsInTarget.getHeight() * (1-TriggerSelection.getDiametermodifier()))/2);
    			if(triggersByCharacterId.keySet().size() <= 1) {
    				// Make the one icon that is oriented downards from the circular view center appear centered
    				offsetY -= (boundsInTarget.getHeight() * (TriggerSelection.getDiametermodifier())) / 2; // shift upwards by radius of the menu
    				offsetY += triggerSelection.getChildren().get(0).getLayoutBounds().getHeight()/2; // shift down again by radius of one icon
    			}
    	        StackPane.setMargin(triggerSelection, new Insets(offsetY, 0, 0, offsetX));

    	         // Display debug message
    	         LOGGER.debug("{} added at position({} | {}) with size({} | {})", triggerSelection, offsetX, offsetY,
    	        		 triggerSelection.getWidth(), triggerSelection.getHeight());

            });
        }
    }

    @Override
    public void setTurnTimer(CustomTimer timer) {
        LOGGER.entry(timer);
        turnTimerProperty().set(timer);

        // Update timer display
        if (getTimerLabelUpdateThread() == null || !getTimerLabelUpdateThread().isAlive()) {
            setTimerLabelUpdateThread(new Thread(this::updateTimer));
            getTimerLabelUpdateThread().setName("TimerLabelUpdateThread");
            getTimerLabelUpdateThread().setDaemon(true);
            getTimerLabelUpdateThread().start();
        }
        LOGGER.exit();
    }

    @Override
    public void setChooseTimer(CustomTimer timer) {
        LOGGER.entry(timer);
        setTimer(timer);

        // Update timer display
        if (getTimerLabelUpdateThread() == null || !getTimerLabelUpdateThread().isAlive()) {
            setTimerLabelUpdateThread(new Thread(this::updateTimer));
            getTimerLabelUpdateThread().setName("TimerLabelUpdateThread");
            getTimerLabelUpdateThread().setDaemon(true);
            getTimerLabelUpdateThread().start();
        }
        LOGGER.exit();
    }

    @Override
    public void showCharacterAbilities(Character character) {
        if (getAbilitySelection() != null) return; // TODO do hideCharacterAbilityDisplay(); instead ?
        LOGGER.debug("showing abilities of " + character);

        // Get Character Display
        CharacterDisplay charDisplay = getFieldDisplays().get(character.getPosition().getPlayer()).getDisplays().get(new MutablePair<Integer,Integer>(character.getPosition().getRow(), character.getPosition().getCol()));

        Platform.runLater(() -> {

	        // Create circular selection menu
	        setAbilitySelection(new AbilitySelection(character, charDisplay.getWidth(), getCurrentModel(), getGameImageService(), getLocalizationService(), getAbilityClassService(), getCharacterClassService(), getInputConsumer()));

	        StackPane.setAlignment(getAbilitySelection(), Pos.TOP_LEFT);

	        // Add to view
	        centerStackPane.getChildren().add(getAbilitySelection());

	        // Define Position
	        Bounds boundsInTarget = centerStackPane.sceneToLocal(charDisplay.localToScene(charDisplay.getLayoutBounds()));
	        double boundsWidth = boundsInTarget.getWidth();
	        double boundsHeight = boundsInTarget.getHeight();
	        double offsetX = boundsInTarget.getMinX() + ((boundsWidth * (1-AbilitySelection.getDiametermodifier()))/2);
	        double offsetY = boundsInTarget.getMinY() + ((boundsHeight * (1-AbilitySelection.getDiametermodifier()))/2);

	        StackPane.setMargin(getAbilitySelection(), new Insets(offsetY, 0, 0, offsetX));

	         // Display debug message
	         LOGGER.debug("{} added at position({} | {}) with size({} | {})", getAbilitySelection(), offsetX, offsetY,
	        		 getAbilitySelection().getWidth(), getAbilitySelection().getHeight());

        });
    }

    @Override
    public void hideCharacterAbilityDisplay() {
        if (getAbilitySelection() == null) return;
        AbilitySelection tmp = getAbilitySelection();
        getAbilitySelection().setOnAnimateOutFinished((event) -> {
            centerStackPane.getChildren().remove(tmp);
            requestFocus();
        });
        getAbilitySelection().animateOut();
        setAbilitySelection(null);
    }

    public List<Effect> getDisplayedEffectStackItems() {
    	List<Effect> result = new ArrayList<>();
    	for (Node child : new LinkedList<>(effectStackVBox.getChildren())) {
    		if (child instanceof EffectCell)
    			result.add(((EffectCell) child).getItem());
    	}
    	return result;
    }

    public void addEffectToStack(Effect effect) {
    	// Instantiate the effect cell
    	EffectCell effectCell = new EffectCell(effect, currentModelProperty(), getFieldDisplays(), getGameImageService(), getAbilityClassService(), getLocalizationService());

    	// Define cell parameters
    	effectCell.prefWidthProperty().bind(effectStackVBox.widthProperty().multiply(0.85));
    	effectCell.prefHeightProperty().bind(effectCell.prefWidthProperty());
    	effectCell.setEffect(new DropShadow(25 * UIConstants.getGlobalScalingFactor(), Color.CORNSILK));

    	// Limit displayed stack size
        if (effectStackVBox.getChildren().size() >= 8) {
        	Platform.runLater(() -> effectStackVBox.getChildren().remove(effectStackVBox.getChildren().size() - 1));
        }

        // Add the cell to the Stack
        Platform.runLater(() -> {
        	try {
        		effectStackVBox.getChildren().add(effectCell);
        	} catch (Exception e) {

        	}
        });

        // Play animation
        FadeTransition fadeTransitionIn = new FadeTransition(Duration.seconds(0.25), effectCell);
        fadeTransitionIn.setFromValue(0);
        fadeTransitionIn.setToValue(1);
        fadeTransitionIn.setInterpolator(Interpolator.EASE_IN);

        ScaleTransition scaleTransitionIn = new ScaleTransition(Duration.seconds(0.25), effectCell);
        scaleTransitionIn.setFromX(0);
        scaleTransitionIn.setFromY(0);
        scaleTransitionIn.setToX(1);
        scaleTransitionIn.setToY(1);
        scaleTransitionIn.setInterpolator(Interpolator.EASE_IN);

        ParallelTransition inTransition = new ParallelTransition(fadeTransitionIn, scaleTransitionIn);
        getEffectStackAnimations().add(inTransition);
        inTransition.playFromStart();
    }

    public void removeEffectFromStack(Effect effect) {
    	EffectCell effectCell = getEffectCell(effect);
    	if (effectCell != null) {
    		// Calculate source and target location
    		Bounds sourceBounds = centerStackPane.sceneToLocal(effectCell.localToScene(effectCell.getLayoutBounds()));
    		double sourceX = sourceBounds.getMinX();
    		double sourceY = sourceBounds.getMinY();
    		LOGGER.debug("SourceX: {} | SourceY: {}", sourceX, sourceY);

    		Bounds targetBounds = centerStackPane.getLayoutBounds();
    		double targetX = (targetBounds.getWidth() / 2) - (sourceBounds.getWidth() / 2);
    		double targetY = (targetBounds.getHeight() / 2) - (sourceBounds.getHeight() / 2);
    		LOGGER.debug("TargetX: {} | TargetY: {}", targetX, targetY);

    		Platform.runLater(() -> {
    			effectCell.setMouseTransparent(true);
    			StackPane.setAlignment(effectCell, Pos.TOP_LEFT);
	    		effectStackVBox.getChildren().remove(effectCell);
	    		centerStackPane.getChildren().add(effectCell);
    		});

    		// Highlight source
    		Position pos = getCurrentModel().getCharacter(effect.getSourceAbility().getOwningCharacterId()).getPosition();
    		getFieldDisplays().get(pos.getPlayer()).getDisplays().get(new MutablePair<Integer, Integer>(pos.getRow(), pos.getCol())).enableSourceHighlighting();

    		// Highlight targets
    		for(Position p : effect.getTargetedPositions().keySet()) {
    			if (p != null)
    				getFieldDisplays()
    				.get(p.getPlayer())
    				.getDisplays()
    				.get(new MutablePair<Integer, Integer>(p.getRow(), p.getCol()))
    				.enableTargetHighlighting();
    		}

            // Play animation
            ScaleTransition scaleTransitionIn = new ScaleTransition(Duration.seconds(0.65), effectCell);
            scaleTransitionIn.setToX(5);
            scaleTransitionIn.setToY(5);
            scaleTransitionIn.setInterpolator(Interpolator.EASE_IN);

            TranslateTransition translateTransitionIn = new TranslateTransition(Duration.seconds(0.65), effectCell);
            translateTransitionIn.setFromX(sourceX);
            translateTransitionIn.setFromY(sourceY);
            translateTransitionIn.setToX(targetX);
            translateTransitionIn.setToY(targetY);

            ParallelTransition inTransition = new ParallelTransition(scaleTransitionIn, translateTransitionIn);

            inTransition.setOnFinished((event) -> {
            	getEffectStackAnimations().remove(inTransition);

                FadeTransition fadeTransitionOut = new FadeTransition(Duration.seconds(0.70), effectCell);
                fadeTransitionOut.setToValue(0);
                fadeTransitionOut.setInterpolator(Interpolator.EASE_IN);
                fadeTransitionOut.setDelay(Duration.seconds(0.15));

                getEffectStackAnimations().add(fadeTransitionOut);
                fadeTransitionOut.setOnFinished((event2) -> {
                	centerStackPane.getChildren().remove(effectCell);
                	getEffectStackAnimations().remove(fadeTransitionOut);

            		// Unhighlight source
            		Position position = getCurrentModel().getCharacter(effect.getSourceAbility().getOwningCharacterId()).getPosition();
            		getFieldDisplays().get(position.getPlayer()).getDisplays().get(new MutablePair<Integer, Integer>(position.getRow(), position.getCol())).disableSourceHighlighting();

            		// Unhighlight targets
            		for (FieldDisplay fieldDisplay : getFieldDisplays().values())
            			for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
            				characterDisplay.disableTargetHighlighting();
                });
                fadeTransitionOut.playFromStart();
            });
            getEffectStackAnimations().add(inTransition);
            inTransition.playFromStart();
    	}
    }

    public EffectCell getEffectCell(Effect effect) {
    	for (Node child : new LinkedList<>(effectStackVBox.getChildren())) {
    		EffectCell cell = (EffectCell) child;
    		if (cell.getItem().equals(effect)) return cell;
    	}
    	return null;
    }

    public boolean isAnimating() {
    	// Effect stack animations
    	Iterator<Animation> effectStackAnimationIt = getEffectStackAnimations().iterator();
    	while (effectStackAnimationIt.hasNext()) {
    		try {
    			Animation next = effectStackAnimationIt.next();
    			if (next.getStatus() == Status.RUNNING) return true;
    		} catch (Throwable e) {
    			break;
    		}
    	}

    	// Character Animations
    	Iterator<FieldDisplay> fieldDisplayIt = getFieldDisplays().values().iterator();
    	while (fieldDisplayIt.hasNext()) {
    		try {
    			FieldDisplay fieldDisplay = fieldDisplayIt.next();
    			Iterator<CharacterDisplay> characterDisplayIt = fieldDisplay.getDisplays().values().iterator();
    			while (characterDisplayIt.hasNext()) {
    				CharacterDisplay characterDisplay = characterDisplayIt.next();
    				Iterator<Animation> animationIt = characterDisplay.getAnimationQueue().iterator();
    				while (animationIt.hasNext()) {
    					Animation anim = animationIt.next();
    					if (anim.getStatus() == Status.RUNNING) return true;
    				}
    			}
    		} catch (Throwable e) {
    			break;
    		}
    	}

        // Notification Animation
        if (getNotificationAnimation() != null && getNotificationAnimation().getStatus() == Status.RUNNING) return true;

    	// None
    	return false;
    }

    public void displayNotification(String text) {
        // Stop previous animation
        if (getNotificationAnimation() != null) getNotificationAnimation().stop();

        // Change notification message
        Platform.runLater(() -> notificationBanner.setText(text));

        // Initialize animation
        FadeTransition ft1 = new FadeTransition(Duration.seconds(0.25), notificationBanner);
        ft1.setFromValue(0.00);
        ft1.setToValue(1);
        ft1.setInterpolator(Interpolator.EASE_OUT);
        ft1.setOnFinished((event) -> {
            FadeTransition ft2 = new FadeTransition(Duration.seconds(0.25), notificationBanner);
            ft2.setDelay(Duration.seconds(1));
            ft2.setFromValue(1.00);
            ft2.setToValue(0);
            ft2.setInterpolator(Interpolator.EASE_IN);
            ft2.setOnFinished((innerEvent) -> {
                notificationBanner.setVisible(false);
                setNotificationAnimation(null);
            });
            ft2.play();
            setNotificationAnimation(ft2);
        });

        // Start animation
        ft1.play();

        // Display banner
        notificationBanner.setVisible(true);

        // Save notification animation to be able to stop it later if a new notification event occurs
        setNotificationAnimation(ft1);
    }

    @SuppressWarnings("unchecked")
    public synchronized void updateDisplay() {
        while (getModelQueue().peek() != null) {

        	// Stop updating if the execution of this thread has been interrupted
        	if (Thread.interrupted()) break;

        	// Poll the model to update to
            GameStateModel newModel = getModelQueue().poll();
            GameStateModel oldModel = getCurrentModel();
            LOGGER.debug("Updating model display to " + newModel);

            // Notifications
            if (getCurrentModel() != null) {
                if (getCurrentModel().getCurrentGamePhase() == GamePhase.PLACE_CHARACTERS
                        && newModel.getCurrentGamePhase() == GamePhase.PLACE_TIME_COUNTERS)
                {
                    displayNotification(getLocalizationService().getText("Game.Notification.PlaceTimeCounters"));
                } else if ((newModel.getCurrentGamePhase() == GamePhase.START_OF_TURN
                            && newModel.getActivePlayer().equals(getPlayer())
                            && !newModel.getActivePlayer().equals(getCurrentModel().getActivePlayer())
                            && getCurrentModel().getRoundCount() != newModel.getRoundCount())
                            || (getCurrentModel().getCurrentGamePhase() == GamePhase.PLACE_TIME_COUNTERS
                                    && newModel.getCurrentGamePhase() == GamePhase.START_OF_TURN)) {
                    if (newModel.getActivePlayer().equals(getPlayer()))
                        displayNotification(getLocalizationService().getText("Game.Notification.YourTurn"));
                } else if (getCurrentModel().getCurrentGamePhase() != GamePhase.REACTION && newModel.getCurrentGamePhase() == GamePhase.REACTION
                        && newModel.getPriorityPlayer().equals(getPlayer())) {
                    displayNotification(getLocalizationService().getText("Game.Notification.React"));
                } else if (getCurrentModel().getCurrentGamePhase() == GamePhase.UPGRADE) {
                	displayNotification(getLocalizationService().getText("Game.Notification.Upgrade"));
                }
            } else if (getCurrentModel() == null && newModel.getCurrentGamePhase() == GamePhase.PLACE_CHARACTERS) {
                displayNotification(getLocalizationService().getText("Game.Notification.PlaceCharacters"));
            }

            // Update Effect Stack
            if (oldModel == null || !newModel.getEffectStack().equals(oldModel.getEffectStack())
            					|| !newModel.getEffectHistory().equals(oldModel.getEffectHistory())) {

//                // effect history
//                for (Effect effect : new ArrayList<>(newModel.getEffectHistory())) {
//                	 EffectCell cell = new EffectCell(effect, currentModel, getFieldDisplays(), drawingPane, getGameImageService(), getAbilityClassService(), getLocalizationService());
//                     cell.prefWidthProperty().bind(effectStackVBox.widthProperty().multiply(0.85));
//                     cell.prefHeightProperty().bind(cell.prefWidthProperty());
//                     if (toAdd.size() >= 8) {
//                         toAdd.remove(toAdd.size() - 1);
//                     }
//                     toAdd.add(cell);
//                }

                // remove from effect stack
                for (Effect effect : getDisplayedEffectStackItems()) {
                	if (!newModel.getEffectStack().contains(effect))
                		removeEffectFromStack(effect);
                }

                // add to effect stack
                for (Effect effect : (Stack<Effect>) newModel.getEffectStack().clone()) {

                	if (getEffectCell(effect) == null) {
                		addEffectToStack(effect);
                	}

                }

                // Add dummy label if stack size is too large
                if ((newModel.getEffectStack().size()) > 8) {
                	if (!effectStackVBox.getChildren().contains(effectStackDummyLabel)) {
                		Platform.runLater(() -> effectStackVBox.getChildren().add(effectStackDummyLabel));
                	}
                } else Platform.runLater(() -> effectStackVBox.getChildren().remove(effectStackDummyLabel));

                // Wait for animations to finish
                while (isAnimating())
    				try {
    					Thread.sleep(1);
    				} catch (InterruptedException e) {
    					LOGGER.catching(e);
    				}

            }

            // Update Round Count Label
            Platform.runLater(() -> roundCountLabel.setText(getLocalizationService().getText("GameView.RoundCountLabel.Text") + ": " + newModel.getRoundCount()));

            // Update active player
            if (newModel.getActivePlayer().equals(getPlayer()) || (newModel.getCurrentGamePhase() == GamePhase.REACTION
            		&& newModel.getPriorityPlayer() != null && newModel.getPriorityPlayer().equals(getPlayer()))) {
                Platform.runLater(() -> cancelButton.setDisable(false));
            } else {
                Platform.runLater(() -> {
                    hideCharacterAbilityDisplay();
                    cancelButton.setDisable(true);
                });
            }

            // Update available actions
            if (newModel.getCurrentGamePhase() == GamePhase.ACTION) {

                if (newModel.getActivePlayer().equals(getPlayer())) {
                    String newText = getLocalizationService().getText("GameView.ActionsLeftLabel.Text") + ": " + (GameLogicConstants.AVAILABLE_ACTIONS_PER_TURN - newModel.getPerformedActionsCount());
                    Platform.runLater(() -> {
                        actionsLeftLabel2.setVisible(false);
                        actionsLeftLabel.setVisible(true);
                        actionsLeftLabel.setText(newText);
                    });
                } else {
                    String newText = getLocalizationService().getText("GameView.ActionsLeftLabel.Text") + ": " + (GameLogicConstants.AVAILABLE_ACTIONS_PER_TURN - newModel.getPerformedActionsCount());
                    Platform.runLater(() -> {
                        actionsLeftLabel.setVisible(false);
                        actionsLeftLabel2.setVisible(true);
                        actionsLeftLabel2.setText(newText);
                    });
                }

            } else {

                Platform.runLater(() -> {
                    actionsLeftLabel.setVisible(false);
                    actionsLeftLabel2.setVisible(false);
                });

            }

            // Update Fields
            if (getGameService() != null)
                for (Player player : getGameService().getPlayers()) {
                    FieldDisplay fd = getFieldDisplays().get(player.getPlayerId());
                    if (fd != null) {
                        Map<Pair<Integer, Integer>, Character> field = newModel.getField(player.getPlayerId());
                        LOGGER.debug("Attempting to set field of " + player + " to " + field);
                        if (field != null) {
                            fd.setField(field);
                        } else LOGGER.warn("The field of player(" + player + ") could not be overriden because there is no appropriate field in the given model");
                    } else LOGGER.warn("The field of player(" + player + ") could not be display because there is no FieldDisplay for this player");
                }
            else LOGGER.warn("The view model could not be updated because there is no PlayerListService registered");

            // Wait for animations to finish
            while (isAnimating())
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					LOGGER.catching(e);
				}

            // Mark the new model as current
            setCurrentModel(newModel);

            // Run garbage collection to free up memory space
            System.gc();
        }
    }

    @Override
    public void updateModelDisplay(GameStateModel newModel) {
        getModelQueue().add(newModel);
//        if (getInterfaceUpdateThread() == null || !getInterfaceUpdateThread().isAlive()) {
//        	// Update interface
//            setInterfaceUpdateThread(new Thread(this::updateDisplay));
//            getInterfaceUpdateThread().setName("InterfaceUpdateThread");
//            getInterfaceUpdateThread().setDaemon(true);
//            getInterfaceUpdateThread().start();
//        }
        updateDisplay();
    }

    @Override
    public void showTaskMessage(final String localizedMessageKey, final Object[] parameters) {
        LOGGER.debug("showing task message " + localizedMessageKey);
        Platform.runLater(() -> taskLabel.setText(getLocalizationService().getText(localizedMessageKey, parameters)));
    }

    @Override
    public void enableSelectionHighlighting(UUID chooser, Ability ability, Position source, GameStateModel model, ITargetFilter filter) {
    	LOGGER.debug("attempting to enable selection highlighting ...");
        disableSelectionHighlighting();
        setActiveAbility(ability);
        for (FieldDisplay fieldDisplay : getFieldDisplays().values())
            for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
                if (filter.test(chooser, ability == null ? ModelConstants.UNDEFINED_ID : ability.getAbilityClassId(), source, characterDisplay.getPosition(), 0, model, getCharacterClassService(), getAbilityClassService()))
                    characterDisplay.addCharacterHighlightingLayer(CharacterDisplayHighlightState.LEGAL_TARGET);
    }

    public void onCharacterDisplayHover(CharacterDisplay characterDisplay) {
    	if (getActiveAbility() != null) {
    		AbilityClass abilityClass = getAbilityClassService().getAbilityClass(getActiveAbility().getAbilityClassId());
    		Character caster = getCurrentModel().getCharacter(getActiveAbility().getOwningCharacterId());
    		Set<Position> initialTargets = new HashSet<>();
    		initialTargets.add(characterDisplay.getCharacter().getPosition());
    		if (abilityClass.getTargetFilter().test(getPlayer(), abilityClass.getId(),
    				caster.getPosition(), characterDisplay.getCharacter().getPosition(), 1, getCurrentModel(), getCharacterClassService(), getAbilityClassService())) {
	    		setActiveTargets(abilityClass.getHitFilter().generateTargets(getActiveAbility(), initialTargets, getCurrentModel(),
	    				getCharacterClassService(), getAbilityClassService()));

	    		for (Entry<Position, TargetType> entry : getActiveTargets().entrySet()) {
	    			boolean isOpponent = !getCurrentModel().getCharacterAt(entry.getKey()).getOwningPlayerId().equals(caster.getOwningPlayerId());
	    			Character chr = getCurrentModel().getCharacterAt(entry.getKey());
	    			getCharacterDisplayByCharacterId(chr.getId()).addCharacterHighlightingLayer(entry.getValue() == TargetType.PRIMARY
	    			? (isOpponent ? CharacterDisplayHighlightState.PRIMARY_TARGET_OPPONENT : CharacterDisplayHighlightState.PRIMARY_TARGET_ALLY)
	    					: (isOpponent ? CharacterDisplayHighlightState.SECONDARY_TARGET_OPPONENT : CharacterDisplayHighlightState.SECONDARY_TARGET_ALLY));
	    		}
    		}
    	}
    }

    @Override
    public void disableSelectionHighlighting() {
    	setActiveAbility(null);
        for (FieldDisplay fieldDisplay : getFieldDisplays().values())
            for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
                characterDisplay.clearCharacterHighlightingLayers();
    }

    public void removeHoverHighlighting() {
    	if (getActiveTargets() == null) return;
    	for (Entry<Position, TargetType> entry : getActiveTargets().entrySet()) {
			Character chr = getCurrentModel().getCharacterAt(entry.getKey());
			getCharacterDisplayByCharacterId(chr.getId()).removeUpmostCharacterHighlightingLayer();
		}
        setActiveTargets(null);
    }

    public void removeUpmostSelectionHighlightingLayer() {
    	 for (FieldDisplay fieldDisplay : getFieldDisplays().values())
             for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
                 characterDisplay.removeUpmostCharacterHighlightingLayer();
    }

    @Override
    public void enableSourceHighlightning(Position pos) {
        disableSourceHighlightning();
        getFieldDisplays()
        .get(pos.getPlayer())
        .getDisplays()
        .get(new MutablePair<Integer,Integer>(pos.getRow(), pos.getCol()))
        .enableSourceHighlighting();
    }

    @Override
    public void enableTargetHighlightning(Position pos) {
        getFieldDisplays()
        .get(pos.getPlayer())
        .getDisplays()
        .get(new MutablePair<Integer,Integer>(pos.getRow(), pos.getCol()))
        .enableTargetHighlighting();
    }

    @Override
    public void disableSourceHighlightning() {
        for (FieldDisplay fieldDisplay : getFieldDisplays().values())
            for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
                characterDisplay.disableSourceHighlighting();
    }

    @Override
    public void disableTargetHighlightning() {
        for (FieldDisplay fieldDisplay : getFieldDisplays().values())
            for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
                characterDisplay.disableTargetHighlighting();
    }

    // </GameUIService>

    // <GameViewAPI>

    @Override
    public CharacterDisplay getCharacterDisplayByCharacterId(UUID characterId) {
        for (FieldDisplay fieldDisplay : getFieldDisplays().values())
            for (CharacterDisplay characterDisplay : fieldDisplay.getDisplays().values())
                if (characterDisplay.getCharacter().getId().equals(characterId)) return characterDisplay;
        return null;
    }

    // </GameViewAPI>

    //Getter & Setter

    public final ObjectProperty<FXMLLoader> FXMLLoaderProperty() {
        return this.fxmlLoader;
    }

    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.FXMLLoaderProperty().get();
    }

    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.FXMLLoaderProperty().set(fxmlLoader);
    }

    public ObservableMap<UUID, FieldDisplay> getFieldDisplays() {
        return fieldDisplays;
    }

    public final ObjectProperty<UUID> playerProperty() {
        return this.player;
    }

    public final java.util.UUID getPlayer() {
        return this.playerProperty().get();
    }


    public final void setPlayer(final java.util.UUID player) {
        this.playerProperty().set(player);
    }

    public final ObjectProperty<Game> gameServiceProperty() {
        return this.gameService;
    }


    public final dok.game.logic.Game getGameService() {
        return this.gameServiceProperty().get();
    }


    public final void setGameService(final dok.game.logic.Game gameService) {
        this.gameServiceProperty().set(gameService);
    }

    public final ObjectProperty<LocalizationService> localizationServiceProperty() {
        return this.localizationService;
    }


    public final dok.commons.LocalizationService getLocalizationService() {
        return this.localizationServiceProperty().get();
    }


    public final void setLocalizationService(final dok.commons.LocalizationService localizationService) {
        this.localizationServiceProperty().set(localizationService);
    }

    public final ObjectProperty<Consumer<Object>> inputConsumerProperty() {
        return this.inputConsumer;
    }


    public final Consumer<Object> getInputConsumer() {
        return this.inputConsumerProperty().get();
    }


    public final void setInputConsumer(final Consumer<Object> inputConsumer) {
        this.inputConsumerProperty().set(inputConsumer);
    }

    public final ObjectProperty<AbilitySelection> abilitySelectionProperty() {
        return this.abilitySelection;
    }


    public final AbilitySelection getAbilitySelection() {
        return this.abilitySelectionProperty().get();
    }


    public final void setAbilitySelection(AbilitySelection abilitySelection) {
        this.abilitySelectionProperty().set(abilitySelection);
    }

    public final ObjectProperty<CustomTimer> turnTimerProperty() {
        return this.turnTimer;
    }


    public final dok.commons.impl.CustomTimer getTurnTimer() {
        return this.turnTimerProperty().get();
    }

    public final ObjectProperty<GameImageService> gameImageServiceProperty() {
        return this.gameImageService;
    }


    public final dok.client.services.GameImageService getGameImageService() {
        return this.gameImageServiceProperty().get();
    }


    public final void setGameImageService(final GameImageService gameImageService) {
        this.gameImageServiceProperty().set(gameImageService);
    }

    public final ObjectProperty<CharacterDataService> characterClassServiceProperty() {
        return this.characterClassService;
    }


    public final CharacterDataService getCharacterClassService() {
        return this.characterClassServiceProperty().get();
    }


    public final void setCharacterClassService(final CharacterDataService characterClassService) {
        this.characterClassServiceProperty().set(characterClassService);
    }


    public final ObjectProperty<AbilityDataService> abilityClassServiceProperty() {
        return this.abilityClassService;
    }


    public final AbilityDataService getAbilityClassService() {
        return this.abilityClassServiceProperty().get();
    }


    public final void setAbilityClassService(final AbilityDataService abilityClassService) {
        this.abilityClassServiceProperty().set(abilityClassService);
    }

    public final ObjectProperty<CustomTimer> timerProperty() {
        return this.timer;
    }


    public final dok.commons.impl.CustomTimer getTimer() {
        return this.timerProperty().get();
    }


    public final void setTimer(final CustomTimer timer) {
        this.timerProperty().set(timer);
    }

    protected final Label getTimerLabel() {
        return timerLabel;
    }

    protected final void setTimerLabel(Label timerLabel) {
        this.timerLabel = timerLabel;
    }

    public Thread getTimerLabelUpdateThread() {
        return timerLabelUpdateThread;
    }

    public void setTimerLabelUpdateThread(Thread timerLabelUpdateThread) {
        this.timerLabelUpdateThread = timerLabelUpdateThread;
    }

    public List<TriggerSelection> getTriggerSelections() {
        return triggerSelections;
    }

    public final ObjectProperty<GameStateModel> currentModelProperty() {
        return this.currentModel;
    }


    public final GameStateModel getCurrentModel() {
        return this.currentModelProperty().get();
    }


    public final void setCurrentModel(final GameStateModel currentModel) {
        this.currentModelProperty().set(currentModel);
    }

    public Queue<GameStateModel> getModelQueue() {
        return modelQueue;
    }

    public Thread getInterfaceUpdateThread() {
        return interfaceUpdateThread;
    }

    public void setInterfaceUpdateThread(Thread interfaceUpdateThread) {
        this.interfaceUpdateThread = interfaceUpdateThread;
    }

    public Transition getNotificationAnimation() {
        return notificationAnimation;
    }

    public void setNotificationAnimation(Transition notificationAnimation) {
        this.notificationAnimation = notificationAnimation;
    }

	public final ObjectProperty<Ability> activeAbilityProperty() {
		return this.activeAbility;
	}


	public final Ability getActiveAbility() {
		return this.activeAbilityProperty().get();
	}


	public final void setActiveAbility(final Ability activeAbility) {
		this.activeAbilityProperty().set(activeAbility);
	}

	public final ObjectProperty<Map<Position, TargetType>> activeTargetsProperty() {
		return this.activeTargets;
	}


	public final java.util.Map<dok.game.model.Position, dok.game.model.TargetType> getActiveTargets() {
		return this.activeTargetsProperty().get();
	}


	public final void setActiveTargets(
			final java.util.Map<dok.game.model.Position, dok.game.model.TargetType> activeTargets) {
		this.activeTargetsProperty().set(activeTargets);
	}

	public ObservableList<Animation> getEffectStackAnimations() {
		return effectStackAnimations;
	}

	public Animation getTimerBlinkAnimation() {
		return timerBlinkAnimation;
	}

	public void setTimerBlinkAnimation(Animation timerBlinkAnimation) {
		this.timerBlinkAnimation = timerBlinkAnimation;
	}

	public Animation getCharacterDetailDisplayAnimationIn() {
		return characterDetailDisplayAnimationIn;
	}

	public void setCharacterDetailDisplayAnimationIn(Animation characterDetailDisplayAnimationIn) {
		this.characterDetailDisplayAnimationIn = characterDetailDisplayAnimationIn;
	}

	public Animation getCharacterDetailDisplayAnimationOut() {
		return characterDetailDisplayAnimationOut;
	}

	public void setCharacterDetailDisplayAnimationOut(Animation characterDetailDisplayAnimationOut) {
		this.characterDetailDisplayAnimationOut = characterDetailDisplayAnimationOut;
	}

}
