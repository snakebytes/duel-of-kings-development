package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.components.CategoryContentListView;
import dok.client.desktop.ui.components.CategoryTabListView;
import dok.client.model.Category;
import dok.client.model.CharacterClassCategory;
import dok.client.model.EnhancementSlotCategory;
import dok.client.services.GameDataPersistenceService;
import dok.client.services.GameImageService;
import dok.commons.LocalizationService;
import dok.commons.model.Coordinate;
import dok.commons.model.EnhancementPlan;
import dok.commons.model.EnhancementSlot;
import dok.commons.model.Lineup;
import dok.game.model.CharacterClass;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * Displays one specific character's configurations as different categories.
 *
 * @author Steffen
 */
public class LineupEditorCategoryView extends StackPane {

	// Attributes

	/**
	 * Position of the selected character in the lineup.
	 */
	private Coordinate selectedPosition;

	/**
	 * Editor Main View which preceded this view.
	 */
	private LineupEditorMainView parent;

	/**
	 * Storage for changes made.
	 */
	private Lineup temporaryChanges;

	/**
	 * Character Classes which can be selected for the current position. Cached to minimize service access.
	 */
    private List<CharacterClass> characterClasses;

    private LocalizationService localizationService;
    private GameImageService gameImageService;
    private AbilityDataService abilityClassService;
    private CharacterDataService characterClassService;

    // Injected Nodes

    @FXML CheckBox kingselectCheckBox;
    @FXML CategoryTabListView categoryTabListView;
	@FXML ImageView positionDisplay;
    @FXML HBox centerContainer;
    @FXML Label switchCategoryLeft;
    @FXML Label switchCategoryRight;
    @FXML CategoryContentListView categoryContentListView;
    @FXML Button backButton;
    @FXML Button saveButton;

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

    // Constructors

    public LineupEditorCategoryView() throws IOException {
    	super();

    	//Apply CSS
        //getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        //getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
    }

    public LineupEditorCategoryView(Coordinate position, LineupEditorMainView parent) throws IOException {
    	super();

    	setSelectedPosition(position);
    	setParent(parent);
    	setLocalizationService(ClientApplication.getInstance().getLocalizationProvider());
        setGameImageService(ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameImageService.class));
        setAbilityClassService(ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameDataPersistenceService.class));
        setCharacterClassService(ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameDataPersistenceService.class));

    	//Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();

        initialize();
    }

    // Methods

    public void initialize() {
    	temporaryChanges = new Lineup(parent.getLineupEdited());

    	kingselectCheckBox.setSelected(getSelectedPosition().equals(parent.getLineupEdited().getKingPosition()));
    	positionDisplay.setImage(gameImageService.getLineupEditorIcon("position-" + getSelectedPosition().getRow() + "-" + getSelectedPosition().getColumn()));
    	positionDisplay.setPreserveRatio(true);
    	switchCategoryLeft.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    	ImageView slIV = new ImageView(gameImageService.getLineupEditorIcon("switchCategoryLeft"));
    	slIV.setPreserveRatio(true);
    	switchCategoryLeft.setGraphic(slIV);
    	switchCategoryRight.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    	ImageView srIV = new ImageView(gameImageService.getLineupEditorIcon("switchCategoryRight"));
    	srIV.setPreserveRatio(true);
    	switchCategoryRight.setGraphic(srIV);

        ImageView backImgV = new ImageView(gameImageService.getLineupEditorIcon("back"));
        backImgV.setFitHeight(100);
        backImgV.setPreserveRatio(true);
        backButton.setGraphic(backImgV);

        ImageView saveImgV = new ImageView(gameImageService.getLineupEditorIcon("save"));
        saveImgV.setFitHeight(100);
        saveImgV.setPreserveRatio(true);
        saveButton.setGraphic(saveImgV);

        kingselectCheckBox.setFont(Font.font("Papyrus", 24. * UIConstants.getGlobalScalingFactor()));
        kingselectCheckBox.setTextFill(Color.WHITE);

    	backButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(parent));
			}
    	});
    	saveButton.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle(ActionEvent event) {
    			parent.setLineupEdited(temporaryChanges);
    			Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(parent));
    		}
    	});
    	kingselectCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				temporaryChanges.setKingPosition(selectedPosition);
			}
    	});
    	switchCategoryLeft.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				int newIndex = (categoryTabListView.getSelectionModel().getSelectedIndex()-1);
				if(newIndex < 0) newIndex = categoryTabListView.getItems().size()+newIndex;
				categoryTabListView.getSelectionModel().select(newIndex);
			}
    	});
    	switchCategoryRight.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				categoryTabListView.getSelectionModel().select((categoryTabListView.getSelectionModel().getSelectedIndex()+1)%categoryTabListView.getItems().size());
			}
    	});
    	categoryContentListView.setCatView(this);

    	refreshTabList();

    	categoryTabListView.getSelectionModel().select(0);

    	updateSizing();
    	updateSpacing();
    }

    /**
     * Refreshes this view's {@link CategoryTabListView} depending on the current states. Called by this view's {@link CategoryContentListView} when selection changes.
     */
    public void refreshTabList() {

    	CharacterClass targetClass = characterClassService.getCharacterClass(temporaryChanges.getCharacterClassIdAt(getSelectedPosition().toPair()));

    	// Set up Categories for Tab view
    	ArrayList<Category> categories = new ArrayList<>();
    	Image img;
    	if(targetClass == null) {
    		img = gameImageService.getCharacterClassImage(-2);
    	} else img = gameImageService.getCharacterClassImage(targetClass.getId());
    	categories.add(new CharacterClassCategory(img));
    	if(targetClass != null) {
    		for(EnhancementSlot slot : targetClass.getEnhancementSlots()) {
    			EnhancementPlan plan;
    			if((plan = temporaryChanges.getEnhancementPlans().get(getSelectedPosition())) != null) {
    				Integer enhancementid = plan.getEnhancementMap().get(slot);
    				if(enhancementid != null) {
    					categories.add(new EnhancementSlotCategory(slot, gameImageService.getEnhancementImage(enhancementid)));
    				} else {
    					categories.add(new EnhancementSlotCategory(slot, gameImageService.getEnhancementSlotImage(slot.getType())));
    				}
    			} else {
    				categories.add(new EnhancementSlotCategory(slot, gameImageService.getEnhancementSlotImage(slot.getType())));
    			}
    		}
    	}
    	ObservableList<Category> obv = FXCollections.observableArrayList(categories);
    	obv.sort(null);
    	categoryTabListView.setItems(obv);
    	categoryTabListView.setContentView(categoryContentListView);
    	categoryTabListView.setTargetClass(targetClass);
    	categoryTabListView.setCharacterDataService(characterClassService);
    	categoryTabListView.initialize();
    }

    private void updateSizing() {
    	centerContainer.setMaxHeight(600);
    	categoryContentListView.setPrefWidth(1000);
    	categoryContentListView.setMaxHeight(800);
    	switchCategoryLeft.setMaxWidth(200);
    	switchCategoryRight.setMaxWidth(200);
    	positionDisplay.setFitHeight(100);
    	categoryTabListView.setMaxHeight(100);
    	categoryTabListView.setMaxWidth(1000);
    }

    private void updateSpacing() {

    }

    // Getters & Setters

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }


    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

	public Coordinate getSelectedPosition() {
		return selectedPosition;
	}

	public void setSelectedPosition(Coordinate selectedPosition) {
		this.selectedPosition = selectedPosition;
	}

	public LineupEditorMainView getParentView() {
		return parent;
	}

	public void setParent(LineupEditorMainView parent) {
		this.parent = parent;
	}

	public LocalizationService getLocalizationService() {
		return localizationService;
	}

	public void setLocalizationService(LocalizationService localizationService) {
		this.localizationService = localizationService;
	}

	public GameImageService getGameImageService() {
		return gameImageService;
	}

	public void setGameImageService(GameImageService gameImageService) {
		this.gameImageService = gameImageService;
	}

	public AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	public void setAbilityClassService(AbilityDataService abilityClassService) {
		this.abilityClassService = abilityClassService;
	}

	public CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	public void setCharacterClassService(CharacterDataService characterClassService) {
		this.characterClassService = characterClassService;
	}

	public Lineup getTemporaryChanges() {
		return temporaryChanges;
	}

    public CategoryTabListView getCategoryTabListView() {
		return categoryTabListView;
	}

	public List<CharacterClass> getCharacterClasses() {
		if(this.characterClasses == null) {
			Map<Integer, Integer> idCounts = new HashMap<>();
			List<CharacterClass> allClasses = getCharacterClassService().getAllCharacterClasses();
			for(CharacterClass cc : allClasses) {
	        	idCounts.put(cc.getId(), 0);
	        }
			for(Integer i : getTemporaryChanges().getCharacterClassIds().values()) {
				idCounts.put(i, idCounts.get(i) + 1);
			}

			for(int i = allClasses.size() -1; i >= 0; i--) {
				if(idCounts.get(allClasses.get(i).getId()) >= 3 && temporaryChanges.getCharacterClassIdAt(selectedPosition.toPair()) != allClasses.get(i).getId()) {
					allClasses.remove(i);
				}
			}
			setCharacterClasses(allClasses);
		}
		return characterClasses;
	}

	public void setCharacterClasses(List<CharacterClass> characterClasses) {
		this.characterClasses = characterClasses;
	}

}
