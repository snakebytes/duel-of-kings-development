package dok.client.desktop.ui.controllers;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.components.LineupImageListView;
import dok.client.model.LineupImageFileNames;
import dok.commons.model.Lineup;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * Displays properties of a lineup which are directly configurable: Name, Description and its image.
 *
 * @author Steffen
 */
public class LineupEditorConfigDisplay extends StackPane {

	// Class Constants

	Logger logger = LogManager.getLogger();

	// Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

    // Attributes

    /**
     * The lineup which's attributes are displayed.
     */
    private Lineup lineup;

    // Injected Nodes

    @FXML VBox mainContainer;
    @FXML GridPane centerContainer;
	@FXML Label titleLabel;
	@FXML Label nameLabel;
	@FXML TextField nameField;
	@FXML Label descLabel;
	@FXML TextArea descArea;
	@FXML Label imageLabel;
	@FXML LineupImageListView imageListView;
	@FXML HBox buttonContainer;
	@FXML Button backButton;
	@FXML Button saveButton;

    // Constructors

	public LineupEditorConfigDisplay() throws IOException {
		super();

		//Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
	}

    // Methods

    @FXML
    public void initialize() {
    	backButton.setOnAction(this::backButtonOnAction);
    	saveButton.setOnAction(this::saveButtonOnAction);
    	imageListView.setItems(FXCollections.observableArrayList(LineupImageFileNames.values()));
    	descArea.setWrapText(true);

    	refreshOptionContent();

    	centerContainer.setHgap(20);
    	centerContainer.setVgap(10);
    	imageListView.setPrefWidth(400);
    }

    /**
     * Refreshes the display's content from the lineup
     */
    private void refreshOptionContent() {
    	if(getLineup() != null) {
    		nameField.setText(getLineup().getTitle());
    		descArea.setText(getLineup().getDescription());
    		if(getLineup().getImageKey() != null) {
    			imageListView.getSelectionModel().select(LineupImageFileNames.getByString(getLineup().getImageKey()));
    		}
    	}
    }

    // Event Handlers

    public void backButtonOnAction(ActionEvent event) {
    	setVisible(false);
    }

    public void saveButtonOnAction(ActionEvent event) {
    	getLineup().setTitle(nameField.getText());
    	getLineup().setDescription(descArea.getText());
    	logger.debug("SELECTED ITEM = " + imageListView.getSelectionModel().getSelectedItem());
    	if(imageListView.getSelectionModel().getSelectedItem() != null) {
    		getLineup().setImageKey(imageListView.getSelectionModel().getSelectedItem().getImageKey());
    	}
    	setVisible(false);
    }

    // Getters & Setters

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }

    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

	public Lineup getLineup() {
		return lineup;
	}

	public void setLineup(Lineup lineup) {
		this.lineup = lineup;
		refreshOptionContent();
	}
}
