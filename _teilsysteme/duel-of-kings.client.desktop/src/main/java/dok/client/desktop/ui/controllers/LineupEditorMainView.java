package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.components.LineupEditorSaveDialog;
import dok.client.services.GameDataPersistenceService;
import dok.client.services.GameImageService;
import dok.client.services.LineupService;
import dok.client.services.LineupService.LineupServiceListener;
import dok.client.services.LoginService;
import dok.commons.LocalizationService;
import dok.commons.model.Coordinate;
import dok.commons.model.EnhancementPlan;
import dok.commons.model.EnhancementSlot;
import dok.commons.model.Lineup;
import dok.commons.model.StatusCode;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * The first, general view of the Lineup Editor. Provides a general overview over the Lineup, as well as means to access more detailed views for editing.
 *
 * @author Steffen
 *
 */

public class LineupEditorMainView extends StackPane {

    // Inner Classes

    /**
     * Displays one single character slot of the Lineup, including enhancements in a toggleable overlay and an icon marking the leader.
     *
     * @author Steffen
     *
     */

    public class LineupEditorCharacterCell extends StackPane {

        // Attributes

        /**
         * State of the enhancement overlay.
         * 1 = invisible
         * 2 = visible
         */
        private int state = 1;

        /**
         * Display for the enhancements of this character
         * Only visible if state = true
         */
        private VBox enhancementOverlay;

        /**
         * Seperate containers for each rank of enhancements. Index + 1 = rank of contained enhancement
         */
        private List<HBox> enhancementLevels;

        /**
         * Position of this character in the lineup
         */
        private Coordinate position;

        /**
         * Icon that will be shown if this character is the lineup's leader
         */
        private ImageView kingIcon = new ImageView();

        /**
         * Container for the character's class-image
         */
        private ImageView portrait;

        // Constructors

        /**
         * @param pos Position of this character in the lineup
         * @param characterClassId ID of the character's class. -2 = no class chosen yet
         * @param lineup The complete lineup that is being edited
         * @param gameImageService {@link GameImageService} for current application
         * @param characterDataService {@link CharacterDataService} for current application
         */

        public LineupEditorCharacterCell(Coordinate pos, int characterClassId, Lineup lineup,
                GameImageService gameImageService, CharacterDataService characterDataService) {
            position = pos;
            portrait = new ImageView(gameImageService.getCharacterClassImage(characterClassId));
            portrait.setPreserveRatio(true);
            portrait.fitHeightProperty().bind(maxHeightProperty());
            getChildren().add(portrait);
            if(pos.equals(lineup.getKingPosition())) {
                kingIcon.setImage(gameImageService.getIcon("king"));
            }
            StackPane.setAlignment(kingIcon, Pos.TOP_CENTER);
            kingIcon.setPreserveRatio(true);
            kingIcon.fitHeightProperty().bind(maxHeightProperty().divide(3));
            getChildren().add(kingIcon);
            enhancementOverlay = new VBox();
            enhancementLevels = new ArrayList<HBox>();
            HBox hbox = new HBox();
            hbox.setAlignment(Pos.CENTER);
            enhancementLevels.add(hbox);
            hbox = new HBox();
            hbox.setAlignment(Pos.CENTER);
            enhancementLevels.add(hbox);
            hbox = new HBox();
            hbox.setAlignment(Pos.CENTER);
            enhancementLevels.add(hbox);
            enhancementOverlay.getChildren().addAll(enhancementLevels);
            enhancementOverlay.getStyleClass().add("enhancementOverlay");
            enhancementOverlay.setVisible(false);
            getChildren().add(enhancementOverlay);
            if (characterClassId < 0) {
            } else if (lineup.getEnhancementPlans().get(pos) == null) {
                for (EnhancementSlot slot : characterDataService.getCharacterClass(characterClassId).getEnhancementSlots()) {
                    ImageView temp = new ImageView(gameImageService.getEnhancementSlotImage(slot.getType()));
                    temp.setPreserveRatio(true);
                    temp.fitHeightProperty().bind(maxHeightProperty().divide(3));
                    enhancementLevels.get(3 - slot.getRank()).getChildren().add(temp);
                }
            } else {
                EnhancementPlan plan = lineup.getEnhancementPlans().get(pos);
                for (EnhancementSlot slot : characterDataService.getCharacterClass(characterClassId).getEnhancementSlots()) {
                    Integer enhId = plan.getEnhancementMap().get(slot);
                    if (enhId == null) {
                        ImageView temp = new ImageView(gameImageService.getEnhancementSlotImage(slot.getType()));
                        temp.setPreserveRatio(true);
                        temp.fitHeightProperty().bind(maxHeightProperty().divide(3));
                        enhancementLevels.get(3 - slot.getRank()).getChildren().add(temp);
                    } else {
                        ImageView temp = new ImageView(gameImageService.getEnhancementImage(enhId));
                        temp.setPreserveRatio(true);
                        temp.fitHeightProperty().bind(maxHeightProperty().divide(3));
                        enhancementLevels.get(3 - slot.getRank()).getChildren().add(temp);
                    }
                }
            }
            setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {

                    LineupEditorCharacterCell cell = (LineupEditorCharacterCell) event.getSource();
                    LineupEditorMainView parent = (LineupEditorMainView) ClientApplication.getInstance()
                            .getMainWindow().getScene().getRoot();
                    Platform.runLater(() -> {
                        try {
                            ClientApplication.getInstance().getMainWindow().getScene()
                                    .setRoot(ClientApplication.getInstance().getLoadingScreen());
                            LineupEditorCategoryView temp = new LineupEditorCategoryView(cell.position, parent);
                            ClientApplication.getInstance().getMainWindow().getScene().setRoot(temp);
                        } catch (Exception e) {
                            logger.catching(e);
                        }
                    });

                }
            });
            getStyleClass().add(getClass().getSimpleName());
        }

        // Methods

        public void toggleEnhancementView() {
            if (state == 1) {
                enhancementOverlay.setVisible(true);
                state = 2;
            } else if (state == 2) {
                enhancementOverlay.setVisible(false);
                state = 1;
            }
        }

        // Getters & Setters

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public VBox getEnhancementOverlay() {
            return enhancementOverlay;
        }

        public void setEnhancementOverlay(VBox enhancementOverlay) {
            this.enhancementOverlay = enhancementOverlay;
        }

        public ImageView getPortrait() {
            return portrait;
        }

    }

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    // Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

    // Injected Nodes

    @FXML
    Button toggleButton;
    @FXML
    HBox revertConfigContainer;
    @FXML
    Button revertButton;
    @FXML
    Button configButton;
    @FXML
    Button backButton;
    @FXML
    Button saveButton;
    @FXML
    GridPane lineupView;
    @FXML
    LineupEditorConfigDisplay configDisplay;
    @FXML
    LineupEditorSaveDialog saveDialog;

    // Attributes

    /**
     * The lineup that was given to the editor to edit.
     * This is the lineup the editor will revert to if using the "revert" or "cancel" buttons.
     */
    private Lineup lineup;

    /**
     * A copy of the lineup where all unsaved editing is taking place.
     */
    private Lineup lineupEdited;

    /**
     * {@link LocalizationService} for current application
     */
    private LocalizationService localizationService;

    /**
     * {@link GameImageService} for current application
     */
    private GameImageService gameImageService;

    /**
     * {@link AbilityDataService} for current application
     */
    private AbilityDataService abilityClassService;

    /**
     * {@link CharacterDataService} for current application
     */
    private CharacterDataService characterClassService;

    private LineupService lineupService;

    private LineupServiceListener lineupServiceListener;

    // Constructors

    public LineupEditorMainView() throws IOException {
        super();

        // Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        // Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        // Load the View
        getFXMLLoader().load();

        initialize();
    }

    public LineupEditorMainView(Lineup lineup) throws IOException {
        this();
        if (lineup == null) {
            this.lineup = new Lineup();
        } else
            this.lineup = lineup;
        if (this.lineup.getAccountId() == null) {
            this.lineup.setAccountId(ClientApplication.getInstance().getServiceRegistry()
                    .getServiceProvider(LoginService.class).getActiveSession().getAccountId());
        }
        this.lineupEdited = this.lineup;

        generateLineupView();
        initialize();

    }

    // Methods

    public void initialize() {

        setLocalizationService(ClientApplication.getInstance().getLocalizationProvider());
        setGameImageService(
                ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameImageService.class));
        setAbilityClassService(
                ClientApplication.getInstance().getServiceRegistry().getServiceProvider(AbilityDataService.class));
        setCharacterClassService(
                ClientApplication.getInstance().getServiceRegistry().getServiceProvider(GameDataPersistenceService.class));

        toggleButton.setOnAction(this::toggleButtonOnAction);
        revertButton.setOnAction(this::revertButtonOnAction);
        configButton.setOnAction(this::configButtonOnAction);
        backButton.setOnAction(this::backButtonOnAction);
        saveButton.setOnAction(this::saveButtonOnAction);

        ImageView toggleImgV = new ImageView(gameImageService.getLineupEditorIcon("toggle"));
        toggleImgV.setFitHeight(100);
        toggleImgV.setPreserveRatio(true);
        toggleButton.setGraphic(toggleImgV);

        ImageView revertImgV = new ImageView(gameImageService.getLineupEditorIcon("revert"));
        revertImgV.setFitHeight(100);
        revertImgV.setPreserveRatio(true);
        revertButton.setGraphic(revertImgV);

        ImageView configImgV = new ImageView(gameImageService.getLineupEditorIcon("config"));
        configImgV.setFitHeight(100);
        configImgV.setPreserveRatio(true);
        configButton.setGraphic(configImgV);

        ImageView backImgV = new ImageView(gameImageService.getLineupEditorIcon("back"));
        backImgV.setFitHeight(100);
        backImgV.setPreserveRatio(true);
        backButton.setGraphic(backImgV);

        ImageView saveImgV = new ImageView(gameImageService.getLineupEditorIcon("save"));
        saveImgV.setFitHeight(100);
        saveImgV.setPreserveRatio(true);
        saveButton.setGraphic(saveImgV);

        configDisplay.setVisible(false);
        saveDialog.setVisible(false);

        updateSizing();
        updateSpacing();
    }

    /**
     * Clears and generates the display of the actual lineup grid.
     * This function will not fetch required data and rely on them being already present in the View's attributes.
     */
    private void generateLineupView() {
        lineupView.getChildren().clear();
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                Coordinate location = new Coordinate(r, c);
                if (lineupEdited.getCharacterClassIds().containsKey(location)) {
                    int characterClass = lineupEdited.getCharacterClassIdAt(location.toPair());
                    LineupEditorCharacterCell cell = new LineupEditorCharacterCell(location, characterClass,
                            lineupEdited, getGameImageService(), characterClassService);
                    lineupView.add(cell, 2-r, c);
                    GridPane.setHgrow(cell, Priority.ALWAYS);
                    GridPane.setVgrow(cell, Priority.ALWAYS);
                    cell.setMaxHeight(300);
                } else {
                    LineupEditorCharacterCell cell = new LineupEditorCharacterCell(location, -2, lineupEdited,
                            getGameImageService(), characterClassService);
                    lineupView.add(cell, 2-r, c);
                    GridPane.setHgrow(cell, Priority.ALWAYS);
                    GridPane.setVgrow(cell, Priority.ALWAYS);
                    cell.setMaxHeight(300);
                }
            }
        }

        ImageView enemyDirection = new ImageView(getGameImageService().getLineupEditorIcon("enemyDirection"));
        enemyDirection.setPreserveRatio(true);
        enemyDirection.setFitHeight(900);
        lineupView.add(enemyDirection, 3, 0);
        GridPane.setRowSpan(enemyDirection, 3);
    }

    private void updateSizing() {
        for (Node n : lineupView.getChildren()) {
            if (n instanceof LineupEditorCharacterCell) {
                ((LineupEditorCharacterCell) n).setMaxWidth(300);
            }
        }
        lineupView.setMaxSize(1200, 900);
        revertConfigContainer.setMaxSize(500, 250);
    }

    private void updateSpacing() {

    }

    /**
     * Toggles each cell's enhancement overlay
     * @param event The ActionEvent fired. Will not be used.
     */
    public void toggleButtonOnAction(ActionEvent event) {
        for (Node n : lineupView.getChildren()) {
            if (n instanceof LineupEditorCharacterCell) {
                ((LineupEditorCharacterCell) n).toggleEnhancementView();
            }
        }
    }

    /**
     * Sets the working copy of the lineup to be equal to the last saved version and triggers a redraw of the lineup grid.
     * @param event The ActionEvent fired. Will not be used.
     */
    public void revertButtonOnAction(ActionEvent event) {
        if (this.lineup == null) {
            this.lineupEdited = new Lineup();
        } else
            setLineupEdited(lineup);
        generateLineupView();
    }

    /**
     * Displays the configuration-window for the lineup currently being edited
     * @param event The ActionEvent fired. Will not be used.
     */
    public void configButtonOnAction(ActionEvent event) {
        configDisplay.setLineup(lineupEdited);
        configDisplay.setVisible(true);
    }

    /**
     * Displays the Main menu of the application without saving any changes made.
     * @param event The ActionEvent fired. Will not be used.
     */

    public void backButtonOnAction(ActionEvent event) {
    	if(lineupService != null && lineupServiceListener != null) {
    		lineupService.removeListener(lineupServiceListener);
    	}
        Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene()
                .setRoot(ClientApplication.getInstance().getMainView()));
    }

    /**
     * This function contains the logic by which a lineup is considered valid for saving, as well as triggering the display of success- of failure-messages.
     * @param event The ActionEvent fired. Will not be used.
     */
    public void saveButtonOnAction(ActionEvent event) {
        Map<String, Boolean> checks = new HashMap<>();
        // Check if Name of Lineup is set
        checks.put("Name", lineupEdited.getTitle() != null && lineupEdited.getTitle().trim() != "");

        // Check if Description of Lineup is set
        checks.put("Desc", lineupEdited.getDescription() != null && lineupEdited.getDescription().trim() != "");

        // Check if Image of Lineup is set
        checks.put("Image", lineupEdited.getImageKey() != null);

        // Check if all positions have character-classes selected
        boolean temp = true;
        for (Integer i : lineupEdited.getCharacterClassIds().values()) {
            temp = temp && i != null;
        }
        temp = temp && lineupEdited.getCharacterClassIds().values().size() == 9;
        checks.put("Classes", temp);

        // Check if a king-position is set
        checks.put("King", lineupEdited.getKingPosition() != null);

        // Count occurences of all classes in the lineup
        checks.put("ClassCount", true);
        Map<Integer, Integer> idCounts = new HashMap<>();
        for(Integer i : lineupEdited.getCharacterClassIds().values()) {
            idCounts.put(i, 0);
        }
        for(Integer i : lineupEdited.getCharacterClassIds().values()) {
            idCounts.put(i, idCounts.get(i) + 1);
        }
        // Check if one class is placed in the lineup more than 3 times
        for(Entry<Integer, Integer> e : idCounts.entrySet()) {
            if(e.getValue() > 3) {
                checks.put("ClassCount", false);
                break;
            }
        }

        // check if errors occured and print error messages
        List<String> messages = new ArrayList<>();
        for (Entry<String, Boolean> entry : checks.entrySet()) {
            if (!entry.getValue()) {
                temp = false;
                messages.add(ClientApplication.getInstance().getLocalizationProvider()
                        .getText("LineupEditor.SaveErrors." + entry.getKey()));
            }
        }

        // DEBUG - SAVE RESTRICTION OVERRIDE
        // temp = true;


        if (temp) {
            lineupService = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(LineupService.class);
            lineupService.addListener(lineupServiceListener = new LineupServiceListener() {

				@Override
				public void onLineupSaved(Lineup savedLineup) {
					logger.debug("LineupMainView registered correct save");
					Platform.runLater(new Runnable() {
						public void run() {
							messages.clear();
							messages.add(ClientApplication.getInstance().getLocalizationProvider()
			                        .getText("LineupEditor.SaveDialog.Success"));
							saveDialog.reloadMessages();
						}
					});
					lineup = savedLineup;
					lineupEdited = savedLineup;
				}

				@Override
				public void onLineupsUpdated(List<Lineup> lineups) {
					// do nothing
				}

				@Override
				public void onLineupSaveError(StatusCode errorCode) {
					logger.debug("LineupMainView got ErrorCode " + errorCode);
					Platform.runLater(new Runnable() {
						public void run() {
							messages.clear();
							messages.add(ClientApplication.getInstance().getLocalizationProvider()
			                        .getText("LineupEditor.SaveDialog.Failure") + ": " + errorCode);
							saveDialog.reloadMessages();
						}
					});
				}
            });
            messages.add("Speichern...");
            lineupService.saveLineup(lineupEdited);
        }
        saveDialog.setMessages(messages);
        saveDialog.reloadMessages();
        saveDialog.setVisible(true);
    }

    // Getters & Setters

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }

    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }

    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

    public LocalizationService getLocalizationService() {
        return localizationService;
    }

    public void setLocalizationService(LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    public GameImageService getGameImageService() {
        return gameImageService;
    }

    public void setGameImageService(GameImageService gameImageService) {
        this.gameImageService = gameImageService;
    }

    public AbilityDataService getAbilityClassService() {
        return abilityClassService;
    }

    public void setAbilityClassService(AbilityDataService abilityClassService) {
        this.abilityClassService = abilityClassService;
    }

    public CharacterDataService getCharacterClassService() {
        return characterClassService;
    }

    public void setCharacterClassService(CharacterDataService characterClassService) {
        this.characterClassService = characterClassService;
    }

    public Lineup getLineupEdited() {
        return lineupEdited;
    }

    public void setLineupEdited(Lineup lineupEdited) {
        this.lineupEdited = lineupEdited;
        generateLineupView();
    }

}
