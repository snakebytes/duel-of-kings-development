package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.services.GameImageService;
import dok.client.services.LineupService;
import dok.client.services.LineupService.LineupServiceListener;
import dok.commons.LocalizationService;
import dok.commons.model.Lineup;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * A window displaying all lineups registered with the currently logged in account for selection,
 * as well as buttons to open the {@link LineupEditorMainView} for the selected lineup or a newly created one
 * @author Steffen
 */
public class LineupEditorSelectionDisplay extends StackPane implements LineupServiceListener {

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

    // Attributes

    private LocalizationService localizationService;
    private GameImageService gameImageService;
    private AbilityDataService abilityClassService;
    private CharacterDataService characterClassService;

    // Injected Nodes

    @FXML VBox innerContainer;
    @FXML Label titleLabel;
    @FXML ComboBox<Lineup> lineupSelector;
    @FXML HBox buttonContainer;
    @FXML Button acceptButton;
    @FXML Button createButton;
    @FXML Button cancelButton;

    // Constructor

    public LineupEditorSelectionDisplay() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
    }

    // Methods

    @FXML
    public void initialize() {

        createButton.setOnAction(this::createButtonOnAction);
        acceptButton.setOnAction(this::acceptButtonOnAction);
        cancelButton.setOnAction(this::cancelButtonOnAction);

        ClientApplication.getInstance().getServiceRegistry().getServiceProvider(LineupService.class).addListener(this);
        lineupSelector.setCellFactory(new Callback<ListView<Lineup>, ListCell<Lineup>>() {
            @Override
            public ListCell<Lineup> call(ListView<Lineup> p) {
                ListCell<Lineup> cell = new ListCell<Lineup>() {
                    @Override
                    protected void updateItem(Lineup item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText("");
                        } else {
                            setText(item.getTitle());
                        }
                    }
                };
                return cell;
            }
        });
        lineupSelector.setButtonCell(new ListCell<Lineup>() {
            @Override
            protected void updateItem(Lineup item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(item.getTitle());
                }

            }
        });
    }

    // <--- LineupServiceListener --->

	@Override
	public void onLineupSaved(Lineup lineup) {
		lineupSelector.getItems().remove(lineup);
		lineupSelector.getItems().add(lineup);
	}

	@Override
	public void onLineupsUpdated(List<Lineup> lineups) {
        lineupSelector.getItems().clear();
        lineupSelector.getItems().addAll(lineups);
	}

    // Event Handlers

    public void createButtonOnAction(ActionEvent event) {
        try {
            LineupEditorMainView p = new LineupEditorMainView(null);
            Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(p));
            this.setVisible(false);
        } catch(IOException e) {
            logger.catching(e);
        }
    }

    public void acceptButtonOnAction(ActionEvent event) {
        try {
        	LineupEditorMainView p = new LineupEditorMainView(lineupSelector.getSelectionModel().getSelectedItem());
        	Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(p));
        	this.setVisible(false);
        } catch(IOException e) {
        	logger.catching(e);
        }

    }

    public void cancelButtonOnAction(ActionEvent event) {
        lineupSelector.getSelectionModel().clearSelection();
        this.setVisible(false);
    }

    // Getters & Setters

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }

    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

    public LocalizationService getLocalizationService() {
        return localizationService;
    }

    public void setLocalizationService(LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    public GameImageService getGameImageService() {
        return gameImageService;
    }

    public void setGameImageService(GameImageService gameImageService) {
        this.gameImageService = gameImageService;
    }

    public AbilityDataService getAbilityClassService() {
        return abilityClassService;
    }

    public void setAbilityClassService(AbilityDataService abilityClassService) {
        this.abilityClassService = abilityClassService;
    }

    public CharacterDataService getCharacterClassService() {
        return characterClassService;
    }

    public void setCharacterClassService(CharacterDataService characterClassService) {
        this.characterClassService = characterClassService;
    }
}
