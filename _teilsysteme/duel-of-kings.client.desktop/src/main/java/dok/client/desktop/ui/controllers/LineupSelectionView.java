package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.desktop.ui.components.LineupListView;
import dok.client.services.LineupService.LineupServiceListener;
import dok.client.services.QueuingService;
import dok.client.services.QueuingService.QueuingListener;
import dok.client.services.QueuingService.QueuingStatus;
import dok.commons.model.Lineup;
import dok.game.model.GameType;
import dok.game.network.model.QueuingPacket.QueuingResult;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class LineupSelectionView extends StackPane implements QueuingListener, LineupServiceListener {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	//Properties

	private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

	//Injected Nodes

	@FXML LineupListView lineupSelectionView;
	@FXML Button cancelButton;
	@FXML Button startButton;
	@FXML QueueDisplay queueDisplay;
	@FXML Label titleLabel;

	//Constructor(s)

	public LineupSelectionView() throws IOException {
		super();

		//Apply CSS
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		getStyleClass().add(getClass().getSimpleName());

		//Setup FXMLLoader
		setFXMLLoader(new FXMLLoader());
		getFXMLLoader().setRoot(this);
		getFXMLLoader().setController(this);
		getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
		getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

		//Load the View
		getFXMLLoader().load();
	}

	// Methods

	@FXML
	public void initialize() {

		queueDisplay.setVisible(false);
		cancelButton.setOnAction(this::onCancelButtonAction);
		startButton.setOnAction(this::startButtonOnAction);
		queueDisplay.getCancelButton().setOnAction(this::queuingDisplayCancelButtonOnAction);

		//
		updateSpacing();
		updateFonts();
	}

	public void updateSpacing() {
		StackPane.setMargin(lineupSelectionView, new Insets(150. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(startButton, new Insets(50. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(cancelButton, new Insets(50. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(titleLabel, new Insets(50. * UIConstants.getGlobalScalingFactor()));
	}

	public void updateFonts() {
		titleLabel.setFont(Font.font("Papyrus", 32 * UIConstants.getGlobalScalingFactor()));
		startButton.setFont(Font.font("Papyrus", 24 * UIConstants.getGlobalScalingFactor()));
		cancelButton.setFont(Font.font("Papyrus", 24 * UIConstants.getGlobalScalingFactor()));
	}

	public void updateQueueDisplayTimer() {
		while(queueDisplay.isVisible() && !Thread.interrupted()) {
			try {
				QueuingService service = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(QueuingService.class);
				Duration dur = service.getTimeInQueue();
				int seconds = (int) ((dur.toMillis() / 1000) % 60);
				int minutes = (int) Math.floor(dur.toMillis() / 1000 / 60);
				Platform.runLater(() -> queueDisplay.timerLabel.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds)));
				Thread.sleep(1000);
			} catch (NoSuchElementException e) {
				// TODO
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	// <--- QueuingListener --->

	@Override
	public void handleQueuingResult(QueuingResult result) {}

	@Override
	public void handleQueueStatusChange(QueuingStatus oldStatus, QueuingStatus newStatus) {
		logger.entry(oldStatus, newStatus);
		if (oldStatus == QueuingStatus.ATTEMPTING && newStatus == QueuingStatus.ACTIVE) {
			queueDisplay.setVisible(true);
			Thread t = new Thread(this::updateQueueDisplayTimer);
			t.setName("QueueTimerDisplayUpdateThread");
			t.setDaemon(true);
			t.start();
		} else if (oldStatus == QueuingStatus.ACTIVE && newStatus == QueuingStatus.IDLE) {
			try {
				QueuingService service = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(QueuingService.class);
				service.removeListener(this);
				queueDisplay.setVisible(false);
			} catch (NoSuchElementException e) {
				// TODO
			}
		}
		logger.exit();
	}

	// <--- LineupServiceListener --->
	@Override
	public void onLineupSaved(Lineup lineup) {
		Platform.runLater(() -> {
			lineupSelectionView.getItems().remove(lineup);
			lineupSelectionView.getItems().add(lineup);
			lineupSelectionView.getSelectionModel().selectFirst();
			lineupSelectionView.scrollTo(0);
		});
	}

	@Override
	public void onLineupsUpdated(List<Lineup> lineups) {
		Platform.runLater(() -> {
			lineupSelectionView.getItems().clear();
			lineupSelectionView.getItems().addAll(lineups);
			lineupSelectionView.getSelectionModel().selectFirst();
			lineupSelectionView.scrollTo(0);
		});
	}

	// Events

	public void startButtonOnAction(ActionEvent event) {
		try {
			QueuingService service = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(QueuingService.class);
			service.addListener(this);
			try {
				service.queueUp(GameType.Duel, lineupSelectionView.getSelectionModel().getSelectedItem().getId());
			} catch (IllegalStateException e) {
				// TODO
				logger.catching(e);
			}
		} catch (NoSuchElementException e) {
			// TODO
			logger.catching(e);
		}
	}

	public void queuingDisplayCancelButtonOnAction(ActionEvent event) {
		try {
			QueuingService service = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(QueuingService.class);
			service.leaveQueue();
		} catch (NoSuchElementException e) {
			// TODO
		}
	}

	public void cancelButtonOnAction(ActionEvent event) {
		ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getMainView());
	}



	// Events

	public void onCancelButtonAction(ActionEvent event) {
		ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getMainView());
	}

	// Getter & Setter

	public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
		return this.fxmlLoader;
	}


	public final javafx.fxml.FXMLLoader getFXMLLoader() {
		return this.fxmlLoaderProperty().get();
	}


	public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
		this.fxmlLoaderProperty().set(fxmlLoader);
	}

}
