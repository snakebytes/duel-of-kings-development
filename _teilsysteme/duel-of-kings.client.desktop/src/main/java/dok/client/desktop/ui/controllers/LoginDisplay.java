package dok.client.desktop.ui.controllers;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.services.LoginService;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class LoginDisplay extends StackPane {

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

    // Injected Nodes

    @FXML TextField requestedNameTextField;
    @FXML PasswordField passwordField;
    @FXML CheckBox rememberNameCheckBox;
    @FXML Button loginButton;
    @FXML Button cancelButton;
    @FXML VBox innerContainer;
    @FXML VBox innerInnerContainer;
    @FXML HBox buttonContainer;
    @FXML Label titleLabel;
    @FXML Label requestedNameLabel;

    //Constructor(s)

    public LoginDisplay() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
    }

    // Methods

    @FXML
    public void initialize() {
        try {
            Preferences prefs = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(Preferences.class);
            rememberNameCheckBox.setSelected(prefs.getBoolean("Login_RememberName", false));
            requestedNameTextField.setText(prefs.get("Login_Name", null));
        } catch (NoSuchElementException e) {
            logger.catching(e);
            // TODO Preferences Service could not be found
        }
        rememberNameCheckBox.setOnAction(this::rememberNameCheckBoxOnAction);
        requestedNameTextField.setOnAction(this::rememberNameCheckBoxOnAction);
        loginButton.setOnAction(this::loginButtonOnAction);
        cancelButton.setOnAction(this::cancelButtonOnAction);
        requestedNameTextField.requestFocus();
    }

    // Event Handlers

    public void rememberNameCheckBoxOnAction(ActionEvent event) {
        try {
            Preferences prefs = ClientApplication.getInstance().getServiceRegistry().getServiceProvider(Preferences.class);
            prefs.putBoolean("Login_RememberName", rememberNameCheckBox.isSelected());
        } catch (NoSuchElementException e) {
            logger.catching(e);
            // TODO Preferences Service could not be found
        }
    }

    public void loginButtonOnAction(ActionEvent event) {
    	if (requestedNameTextField.getText() == null || passwordField.getText() == null) return;
        String requestedName = requestedNameTextField.getText().trim().toLowerCase();
        String password = passwordField.getText().trim();
        if (requestedName.equals("")) return;
        try {
            ClientApplication.getInstance().getServiceRegistry().getServiceProvider(LoginService.class).attemptLogin(requestedName, password);
        } catch (NoSuchElementException e) {
            logger.catching(e);
            // TODO Display Login Service Missing Error
        }
    }

    public void cancelButtonOnAction(ActionEvent event) {
        Platform.exit();
    }

    // Getter & Setter

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }


    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

}
