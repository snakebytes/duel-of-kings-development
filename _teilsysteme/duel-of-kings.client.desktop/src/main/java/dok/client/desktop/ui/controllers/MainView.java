package dok.client.desktop.ui.controllers;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import dok.client.services.LoginService.LoginServiceListener;
import dok.commons.model.LoginSession;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class MainView extends StackPane implements LoginServiceListener {

	// Class Constants

	@SuppressWarnings("unused")
	private static final Logger logger = LogManager.getLogger();

	//Properties

	private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

	//Injected Nodes

	@FXML LoginDisplay loginOverlay;
	@FXML LineupEditorSelectionDisplay lineupEditorSelectionDisplay;
	@FXML Label exitButton;
	@FXML Label fightPlayerButton;
	@FXML Label fightAIButton;
	@FXML Label lineupEditorButton;
//	@FXML Label settingsButton;
//	@FXML Label friendListButton;
//	@FXML Label profileButton;
//	@FXML Label shopButton;
	@FXML ImageView fightPlayerButtonGraphic;
	@FXML ImageView fightAIButtonGraphic;
	@FXML ImageView lineupEditorButtonGraphic;
//	@FXML ImageView settingsButtonGraphic;
//	@FXML ImageView friendListButtonGraphic;
//	@FXML ImageView profileButtonGraphic;
//	@FXML ImageView shopButtonGraphic;

	//Constructor(s)

	public MainView() throws IOException {
		super();

		//Apply CSS
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		getStyleClass().add(getClass().getSimpleName());

		//Setup FXMLLoader
		setFXMLLoader(new FXMLLoader());
		getFXMLLoader().setRoot(this);
		getFXMLLoader().setController(this);
		getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
		getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

		//Load the View
		getFXMLLoader().load();
	}

	// Methods

	@FXML
	public void initialize() {
		exitButton.setOnMouseClicked(this::onExitButtonAction);
		fightPlayerButton.setOnMouseClicked(this::onFightPlayerButtonAction);
		fightAIButton.setOnMouseClicked(this::onFightAIButtonAction);
		lineupEditorButton.setOnMouseClicked(this::onLineupEditorButtonAction);
//		settingsButton.setOnMouseClicked(this::onSettingsButtonAction);
//		friendListButton.setOnMouseClicked(this::onFriendListButtonAction);
//		profileButton.setOnMouseClicked(this::onProfileButtonAction);
//		shopButton.setOnMouseClicked(this::onShopButtonAction);

		lineupEditorSelectionDisplay.setVisible(false);

		updateSizing();
		updateSpacing();
		updateFonts();
	}

	public void updateSizing() {
		double labelWidth = 400 * UIConstants.getGlobalScalingFactor();
		double labelHeight = 100 * UIConstants.getGlobalScalingFactor();
		Font labelFont = Font.font("Papyrus", 48.0  * UIConstants.getGlobalScalingFactor());

		fightPlayerButton.setPrefWidth(labelWidth);
		fightPlayerButton.setPrefHeight(labelHeight);
		fightPlayerButton.setFont(labelFont);

		fightPlayerButtonGraphic.setFitHeight(fightPlayerButton.getPrefHeight());

		fightAIButton.setPrefWidth(labelWidth);
		fightAIButton.setPrefHeight(labelHeight);
		fightAIButton.setFont(labelFont);

		fightAIButtonGraphic.setFitHeight(fightAIButton.getPrefHeight());

		exitButton.setFont(labelFont);

		lineupEditorButton.setPrefWidth(labelWidth);
		lineupEditorButton.setPrefHeight(labelHeight);
		lineupEditorButton.setFont(labelFont);

		lineupEditorButtonGraphic.setFitHeight(lineupEditorButton.getPrefHeight());

//		settingsButton.setPrefWidth(labelWidth);
//		settingsButton.setPrefHeight(labelHeight);
//		settingsButton.setFont(labelFont);
//
//		settingsButtonGraphic.setFitHeight(settingsButton.getPrefHeight());
//
//		friendListButton.setPrefWidth(labelWidth);
//		friendListButton.setPrefHeight(labelHeight);
//		friendListButton.setFont(labelFont);
//
//		friendListButtonGraphic.setFitHeight(friendListButton.getPrefHeight());
//
//		profileButton.setPrefWidth(labelWidth);
//		profileButton.setPrefHeight(labelHeight);
//		profileButton.setFont(labelFont);
//
//		profileButtonGraphic.setFitHeight(profileButton.getPrefHeight());
//
//		shopButton.setPrefWidth(labelWidth);
//		shopButton.setPrefHeight(labelHeight);
//		shopButton.setFont(labelFont);
//
//		shopButtonGraphic.setFitHeight(shopButton.getPrefHeight());

	}

	public void updateSpacing() {
		StackPane.setMargin(fightAIButton, new Insets(0, 0, 600. * UIConstants.getGlobalScalingFactor(), 450. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(fightPlayerButton, new Insets(300. * UIConstants.getGlobalScalingFactor(), 1000. * UIConstants.getGlobalScalingFactor(), 0, 0));
		StackPane.setMargin(exitButton, new Insets(850. * UIConstants.getGlobalScalingFactor(), 0, 0, 675. * UIConstants.getGlobalScalingFactor()));
		StackPane.setMargin(lineupEditorButton, new Insets(0, 800. * UIConstants.getGlobalScalingFactor(), 500. * UIConstants.getGlobalScalingFactor(), 0));
//		StackPane.setMargin(settingsButton, new Insets(200. * UIConstants.getGlobalScalingFactor(), 400. * UIConstants.getGlobalScalingFactor(), 200. * UIConstants.getGlobalScalingFactor(), 0));
//		StackPane.setMargin(friendListButton, new Insets(400. * UIConstants.getGlobalScalingFactor(), 0, 0, 250. * UIConstants.getGlobalScalingFactor()));
//		StackPane.setMargin(profileButton, new Insets(800. * UIConstants.getGlobalScalingFactor(), 0, 0, 0));
//		StackPane.setMargin(shopButton, new Insets(500. * UIConstants.getGlobalScalingFactor(), 0, 600. * UIConstants.getGlobalScalingFactor(), 500. * UIConstants.getGlobalScalingFactor()));
	}

	public void updateFonts() {

	}

	// <--- LoginListener --->

	@Override
	public void onSuccessfulLogin(LoginSession session) {
		loginOverlay.setVisible(false);
	}

	// Events

	public void onExitButtonAction(MouseEvent event) {
		Platform.exit();
	}

	public void onFightPlayerButtonAction(MouseEvent event) {
		Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getLineupSelectionView()));
	}

	public void onFightAIButtonAction(MouseEvent event) {
		Platform.runLater(() -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getBotGameCreationView()));
	}

	public void onLineupEditorButtonAction(MouseEvent event) {
		lineupEditorSelectionDisplay.setVisible(true);
	}

	public void onSettingsButtonAction(MouseEvent event) {
		// TODO
	}

	public void onFriendListButtonAction(MouseEvent event) {
		// TODO
	}

	public void onProfileButtonAction(MouseEvent event) {
		// TODO
	}

	public void onShopButtonAction(MouseEvent event) {
		// TODO
	}

	// Getter & Setter

	public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
		return this.fxmlLoader;
	}


	public final javafx.fxml.FXMLLoader getFXMLLoader() {
		return this.fxmlLoaderProperty().get();
	}


	public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
		this.fxmlLoaderProperty().set(fxmlLoader);
	}

}
