package dok.client.desktop.ui.controllers;

import java.io.IOException;

import dok.client.desktop.application.ClientApplication;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class QueueDisplay extends StackPane {

	//Properties

	private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();

	//Injected Nodes

	@FXML Label timerLabel;
	@FXML Button cancelButton;
	@FXML VBox innerContainer;
	@FXML ImageView loadingGraphicImageView;
	@FXML Label loadingTitleLabel;

	//Constructor(s)

	public QueueDisplay() throws IOException {
		super();

		//Apply CSS
		getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
		getStyleClass().add(getClass().getSimpleName());

		//Setup FXMLLoader
		setFXMLLoader(new FXMLLoader());
		getFXMLLoader().setRoot(this);
		getFXMLLoader().setController(this);
		getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
		getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

		//Load the View
		getFXMLLoader().load();
	}

	// Methods

	@FXML
	public void initialize() {
		loadingGraphicImageView.setFitHeight(75);
	}

	// Getter & Setter

	public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
		return this.fxmlLoader;
	}


	public final javafx.fxml.FXMLLoader getFXMLLoader() {
		return this.fxmlLoaderProperty().get();
	}


	public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
		this.fxmlLoaderProperty().set(fxmlLoader);
	}

	public Button getCancelButton() {
		return cancelButton;
	}

}
