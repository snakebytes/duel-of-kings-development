package dok.client.desktop.ui.controllers;

import java.io.IOException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.desktop.application.ClientApplication;
import dok.client.desktop.ui.UIConstants;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

public class SplashView extends StackPane {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

    //Properties

    private final ObjectProperty<FXMLLoader> fxmlLoader = new SimpleObjectProperty<>();
    private final BooleanProperty initializationFinished = new SimpleBooleanProperty(false);
    private final BooleanProperty animationFinished = new SimpleBooleanProperty(false);

    // Injected Nodes

    @FXML ImageView logoImageView;
    @FXML ImageView baruschkaBrothersImageView;
    @FXML ImageView snakeBytesImageView;
    @FXML ImageView loadingImageView;
    @FXML Label continueLabel;

    // Constructor(s)

    public SplashView() throws IOException {
        super();

        //Apply CSS
        getStylesheets().add(getClass().getResource("/ui/css/" + getClass().getSimpleName() + ".css").toExternalForm());
        getStyleClass().add(getClass().getSimpleName());

        //Setup FXMLLoader
        setFXMLLoader(new FXMLLoader());
        getFXMLLoader().setRoot(this);
        getFXMLLoader().setController(this);
        getFXMLLoader().setLocation(getClass().getResource("/ui/views/" + getClass().getSimpleName() + ".fxml"));
        getFXMLLoader().setResources(ClientApplication.getInstance().getLocalizationProvider().getResourceBundle());

        //Load the View
        getFXMLLoader().load();
    }

    // Methods

    @FXML
    public void initialize() {
        // Initialize the logo image view
        logoImageView.setSmooth(true);
        logoImageView.setImage(new Image(getClass().getResourceAsStream("/images/Logo.png")));
        logoImageView.fitWidthProperty().bind(widthProperty().multiply(0.5));
        logoImageView.fitHeightProperty().bind(heightProperty().multiply(0.5));

        logoImageView.setScaleX(0.75);
        logoImageView.setScaleY(0.75);
        logoImageView.setOpacity(0.25);

        // Initialize baruschka brothers logo image view
        baruschkaBrothersImageView.setSmooth(true);
        baruschkaBrothersImageView.setImage(new Image(getClass().getResourceAsStream("/images/Logo_BaruschkaBrothers.png")));
        baruschkaBrothersImageView.fitWidthProperty().bind(widthProperty().multiply(0.30));
        baruschkaBrothersImageView.fitHeightProperty().bind(heightProperty().multiply(0.20));

        // Initialize baruschka brothers logo image view
        snakeBytesImageView.setSmooth(true);
        snakeBytesImageView.setImage(new Image(getClass().getResourceAsStream("/images/Logo_SnakeBytes.png")));
        snakeBytesImageView.fitWidthProperty().bind(widthProperty().multiply(0.30));
        snakeBytesImageView.fitHeightProperty().bind(heightProperty().multiply(0.20));
        snakeBytesImageView.setVisible(false); // TODO: Remove this line when an appropriate logo has been created

        // Initialize continue button
        continueLabel.setTextFill(Color.ALICEBLUE);
        continueLabel.setEffect(new DropShadow(5, Color.AZURE));
        continueLabel.setTextAlignment(TextAlignment.CENTER);
        continueLabel.setAlignment(Pos.CENTER);
        continueLabel.prefWidthProperty().bind(widthProperty().multiply(0.13));
        continueLabel.prefHeightProperty().bind(widthProperty().multiply(0.065));
        continueLabel.setVisible(false);

        // Initialize Loading Image
        loadingImageView.setSmooth(true);
        try {
			loadingImageView.setImage(new Image("/images/loading.gif"));
        } catch (Exception e) {
        	logger.catching(Level.DEBUG, e);
        }
        loadingImageView.fitHeightProperty().bind(heightProperty().multiply(0.07));
        loadingImageView.fitWidthProperty().bind(loadingImageView.fitHeightProperty());

        initializationFinishedProperty().addListener(this::onInitializationFinished);
        animationFinishedProperty().addListener(this::onAnimationFinished);

    }

    public void updateSpacing() {
        setPadding(new Insets(50. * (getWidth() / UIConstants.TARGET_WINDOW_WIDTH)));
    }

    public void updateFonts() {
        continueLabel.setFont(Font.font("Papyrus", 56. * UIConstants.getGlobalScalingFactor()));
    }

    public void playInAnimation() {
        FadeTransition ft = new FadeTransition(Duration.seconds(4), logoImageView);
        ft.setFromValue(0.25);
        ft.setToValue(1);
        ft.setInterpolator(Interpolator.EASE_IN);

        ScaleTransition st = new ScaleTransition(Duration.seconds(4), logoImageView);
        st.setFromX(0.75);
        st.setFromY(0.75);
        st.setToX(1.15);
        st.setToY(1.15);
        st.setInterpolator(Interpolator.EASE_IN);

        ParallelTransition pt = new ParallelTransition(ft, st);
        pt.setOnFinished((event) ->  setAnimationFinished(true));
        pt.play();
    }

    public void onInitializationFinished(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
        if (newValue && isAnimationFinished()) {
            loadingImageView.setVisible(false);
            continueLabel.setVisible(true);
            setOnMouseClicked((event) -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getMainView()));
        }
    }

    public void onAnimationFinished(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
        if (newValue && isInitializationFinished()) {
            loadingImageView.setVisible(false);
            continueLabel.setVisible(true);
            setOnMouseClicked((event) -> ClientApplication.getInstance().getMainWindow().getScene().setRoot(ClientApplication.getInstance().getMainView()));
        }
    }

    // Getter & Setter

    public final ObjectProperty<FXMLLoader> fxmlLoaderProperty() {
        return this.fxmlLoader;
    }


    public final javafx.fxml.FXMLLoader getFXMLLoader() {
        return this.fxmlLoaderProperty().get();
    }


    public final void setFXMLLoader(final javafx.fxml.FXMLLoader fxmlLoader) {
        this.fxmlLoaderProperty().set(fxmlLoader);
    }

    public final BooleanProperty initializationFinishedProperty() {
        return this.initializationFinished;
    }


    public final boolean isInitializationFinished() {
        return this.initializationFinishedProperty().get();
    }


    public final void setInitializationFinished(final boolean initializationFinished) {
        this.initializationFinishedProperty().set(initializationFinished);
    }

    public final BooleanProperty animationFinishedProperty() {
        return this.animationFinished;
    }


    public final boolean isAnimationFinished() {
        return this.animationFinishedProperty().get();
    }


    public final void setAnimationFinished(final boolean animationFinished) {
        this.animationFinishedProperty().set(animationFinished);
    }




}
