package dok.client.game;

import dok.commons.impl.InputProvider;
import dok.commons.statemachine.StateMachineCallback;
import dok.game.logic.Game;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;

public abstract class ClientGame extends Game implements Runnable {

	// Constants

	private final InputProvider inputProvider  = new InputProvider();

	// Constructor(s)

	public ClientGame(StateMachineCallback parent, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		super(parent, characterClassService, abilityClassService);
	}

	// Getter & Setter

	public final InputProvider getInputProvider() {
		return inputProvider;
	}

}
