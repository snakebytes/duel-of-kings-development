package dok.client.game.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.ClientGame;
import dok.client.game.statemachines.ai.AI_Master;
import dok.client.game.statemachines.player.Player_Master;
import dok.client.services.GameUIService;
import dok.commons.impl.CustomTimer.CustomTimerState;
import dok.commons.model.Lineup;
import dok.commons.statemachine.StateMachineCallback;
import dok.game.logic.statemachines.GameLogic_Master;
import dok.game.model.AILevel;
import dok.game.model.ComputerPlayer;
import dok.game.model.GameEvent;
import dok.game.model.GameResult;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.SwapRequest;
import dok.game.model.Trigger;
import dok.game.model.UserPlayer;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import dok.game.services.GameLogicService;
import dok.game.services.GameLogicWrapper;
import dok.game.services.PlayerInteractionService;
import dok.game.services.util.WaitingManager;

public class BotGame extends ClientGame implements GameLogicWrapper {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constants

	private final WaitingManager waitingManager;
	private final GameUIService gameUIService;
	private final Map<UUID, PlayerInteractionService> playerStateMachines = new LinkedHashMap<>();
	private GameLogicService gameLogicService;

	// Constructor(s)

	public BotGame(StateMachineCallback parent, GameUIService gameUIService, CharacterDataService characterClassService, AbilityDataService abilityClassService, AILevel aiLevel, List<Lineup> lineups, UUID userAccountId, String username) {
		super(parent, characterClassService, abilityClassService);

		// Validate parameters
		if (parent == null) throw new IllegalArgumentException("Parent must not be null");
		if (gameUIService == null) throw new IllegalArgumentException("GameUIService must not be null");
		if (characterClassService == null) throw new IllegalArgumentException("CharacterClassService must not be null");
		if (abilityClassService == null) throw new IllegalArgumentException("AbilityClassService must not be null");
		if (lineups == null) throw new IllegalArgumentException("Lineups must not be null");
		else if (lineups.size() < 2) throw new IllegalArgumentException("Lineups must contain at least two values");
		else if (lineups.get(0) == null) throw new IllegalArgumentException("The first Lineup must not be null");
		else if (lineups.get(0) == null) throw new IllegalArgumentException("The second Lineup must not be null");

		// Initialize GameUIService
		this.gameUIService = gameUIService;

		// Initialize WaitingManager
		this.waitingManager = new WaitingManager();

		// Setup the Game Logic State Machine
		setGameLogicService(new GameLogic_Master(null, this, this, characterClassService, abilityClassService, this));

		// Setup the Player State Machines
		getPlayers().add(new UserPlayer(userAccountId, username, lineups.get(0)));
		getPlayers().add(new ComputerPlayer(aiLevel, lineups.get(1)));
		getPlayerStateMachines().put(getPlayers().get(0).getPlayerId(), new Player_Master(this, getPlayers().get(0).getPlayerId(), getPlayers().get(0).getLineup(), getCharacterClassService(), getAbilityClassService(), getGameUIService(), getInputProvider(), new BotGamePlayerWrapper(getPlayers().get(0).getPlayerId(), getGameLogicService(), getWaitingManager())));
		getPlayerStateMachines().put(getPlayers().get(1).getPlayerId(), new AI_Master(null, new BotGamePlayerWrapper(getPlayers().get(1).getPlayerId(), getGameLogicService(), getWaitingManager()), aiLevel, abilityClassService, characterClassService));

	}

	// Methods

	@Override
	public void run() {
		start();
	}

	public void start() {
		logger.info("A BotGame has been started");
		// Begin the game by starting the game logic master state machine on a new daemon thread
		((GameLogic_Master) getGameLogicService()).transition(null, ((GameLogic_Master) getGameLogicService()).getStartState());
	}

	@Override
	public void onFinish(Object result) {
		logger.info("A BotGame has finished with the following result: " + result);
		getInputProvider().getConsumers().clear();
		getParent().onFinish(result);
	}

	// <GameLogicWrapper>

	@Override
	public void placeCharacters(UUID player) {
		logger.debug(player);
		Thread t = new Thread(() -> getPlayerStateMachines().get(player).placeCharacters());
		t.setDaemon(true);
		t.setName("PlayerService.placeCharacters");
		t.start();
	}

	@Override
	public void chooseCharacter(UUID player, ITargetFilter filter, Set<Position> availablePositions) {
		logger.debug("player: " + player + ", filter: " + filter + ", availPoses: " + availablePositions);
		Set<Position> availablePositionsCopy = new HashSet<>(availablePositions);
		Thread t = new Thread(() -> getPlayerStateMachines().get(player).performCharacterMarkerPlacement(filter, availablePositionsCopy));
		t.setDaemon(true);
		t.setName("PlayerService.chooseCharacter");
		t.start();
	}

	@Override
	public void performCharacterAction(final UUID player) {
		Thread t = new Thread(() -> getPlayerStateMachines().get(player).performAction());
		t.setDaemon(true);
		t.setName("PlayerService.performCharacterAction");
		t.start();
	}

	@Override
	public void performReaction(UUID player, List<Trigger> list, List<GameEvent> gameEvents) {
		Thread t = new Thread(() -> getPlayerStateMachines().get(player).performReaction(new ArrayList<>(list), gameEvents));
		t.setDaemon(true);
		t.setName("PlayerService.performReaction");
		t.start();
	}

	@Override
	public void handleSwapRequest(UUID player, SwapRequest swapRequest) {
		logger.debug("handleSwapRequest: {}, {}", player, swapRequest);
		Thread t = new Thread(() -> getPlayerStateMachines().get(player).handleSwapRequest(swapRequest));
		t.setDaemon(true);
		t.setName("PlayerService.handleSwapRequest");
		t.start();
	}

	@Override
	public void handleModelUpdate(UUID player, GameStateModel newModel) {
		logger.entry(player, newModel);
		getWaitingManager().startWaitingOn(player);
		Thread t = new Thread(() -> getPlayerStateMachines().get(player).handleModelUpdate(newModel.copy()));
		t.setDaemon(true);
		t.setName("PlayerService.handleModelUpdate");
		t.start();
		logger.exit();
	}

	@Override
	public boolean handleModelUpdate(GameStateModel gameStateModel) {

		// Copy the model because there is no serialization going on and we dont want to mess with the same object reference
		GameStateModel copy = gameStateModel.copy();

		// Broadcast the model
		for (UUID playerId : getPlayerIds()) {
			getWaitingManager().startWaitingOn(playerId);
			Thread t = new Thread(() -> getPlayerStateMachines().get(playerId).handleModelUpdate(copy));
			t.setDaemon(true);
			t.setName("PlayerService.handleModelUpdate");
			t.start();
		}

		// Remember Timer States
		CustomTimerState turnTimerState = gameStateModel.getTurnTimer().getState();
		CustomTimerState chooseTimerState = gameStateModel.getChooseTimer().getState();

		// Pause Timers
		if (turnTimerState == CustomTimerState.RUNNING) gameStateModel.getTurnTimer().pause();
		if (chooseTimerState == CustomTimerState.RUNNING) gameStateModel.getChooseTimer().pause();

		// Wait for everybody to finish overriding their models (& views)
		boolean result = getWaitingManager().waitForAll();

		// Resume Timers
		if (turnTimerState == CustomTimerState.RUNNING) gameStateModel.getTurnTimer().resume();
		if (chooseTimerState == CustomTimerState.RUNNING) gameStateModel.getChooseTimer().resume();

		return result;
	}

	@Override
	public void handleGameResult(UUID player, GameResult result) {
		try {
			Thread t = new Thread(() -> getPlayerStateMachines().get(player).handleGameResult(new GameResult(result)));
			t.setDaemon(true);
			t.setName("PlayerService.handleGameResult");
			t.start();
		} catch (IllegalStateException e) {
			logger.log(Level.FATAL, "Player " + player + " could not perform the given task task", e);
		}
	}

	// </GameLogicWrapper>

	// Getter & Setter

	public GameUIService getGameUIService() {
		return gameUIService;
	}

	public final GameLogicService getGameLogicService() {
		return gameLogicService;
	}

	protected final void setGameLogicService(GameLogicService GameLogicService) {
		this.gameLogicService = GameLogicService;
	}

	public final Map<UUID, PlayerInteractionService> getPlayerStateMachines() {
		return playerStateMachines;
	}

	public WaitingManager getWaitingManager() {
		return waitingManager;
	}

}
