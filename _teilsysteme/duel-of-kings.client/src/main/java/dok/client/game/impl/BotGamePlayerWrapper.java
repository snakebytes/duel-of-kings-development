package dok.client.game.impl;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.Action;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;
import dok.game.services.GameLogicService;
import dok.game.services.PlayerInteractionWrapper;
import dok.game.services.util.WaitingManager;

public final class BotGamePlayerWrapper implements PlayerInteractionWrapper {

	// Class Constants

    private static final Logger logger = LogManager.getLogger();

	// Constants

	private final UUID player;
	private final GameLogicService gameLogicService;
	private final WaitingManager waitingManager;

	// Constructor(s)

	public BotGamePlayerWrapper(UUID player, GameLogicService gameLogicService, WaitingManager waitingManager) {
		this.player = player;
		this.gameLogicService = gameLogicService;
		this.waitingManager = waitingManager;
	}

	// Methods

	@Override
	public void handleReaction(Reaction reaction) {
		logger.entry(reaction);
		logger.debug("handleReaction(" + reaction + ")");
		Thread t = new Thread(() -> getGameLogicService().handleReaction(getPlayer(), reaction));
		t.setDaemon(true);
		t.setName("BotGamePlayerWrapper.handleReaction");
		t.start();
		logger.exit();
	}

	@Override
	public void handleAction(Action action) {
		logger.entry(action);
		logger.debug("handleAction(" + action + ")");
		Thread t = new Thread(() -> getGameLogicService().handleAction(getPlayer(), action));
		t.setDaemon(true);
		t.setName("BotGamePlayerWrapper.handleAction");
		t.start();
		logger.exit();
	}

	@Override
	public void handleCharacterChoosen(Position pos) {
		logger.entry(pos);
		logger.debug("handleCharacterChoosen(" + pos + ")");
		Thread t = new Thread(() -> getGameLogicService().handleCharacterChoosen(getPlayer(), pos));
		t.setDaemon(true);
		t.setName("BotGamePlayerWrapper.handleCharacterChoosen");
		t.start();
		logger.exit();
	}

	@Override
	public void handleSwapRequest(SwapRequest swapRequest) {
		logger.entry(swapRequest);
		logger.debug("handleSwapRequest(" + swapRequest + ")");
		Thread t = new Thread(() -> getGameLogicService().handleSwapRequest(getPlayer(), swapRequest));
		t.setDaemon(true);
		t.setName("GameLogicStateMachineService.handleSwapRequest");
		t.start();
		logger.exit();
	}

	@Override
	public void handleSurrenderRequest() {
		logger.entry();
		logger.debug("handleSurrenderRequest()");
		Thread t = new Thread(() -> getGameLogicService().handleSurrenderRequest(getPlayer()));
		t.setDaemon(true);
		t.setName("GameLogicStateMachineService.handleSurrenderRequest");
		t.start();
		logger.exit();
	}

	@Override
	public void handleWorkDone() {
		getWaitingManager().stopWaitingOn(getPlayer());
	}

	// Getters & Setters

	protected final UUID getPlayer() {
		return player;
	}

	protected final GameLogicService getGameLogicService() {
		return gameLogicService;
	}

	public WaitingManager getWaitingManager() {
		return waitingManager;
	}

}
