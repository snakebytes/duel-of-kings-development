package dok.client.game.impl;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.ClientGame;
import dok.client.game.statemachines.player.Player_Master;
import dok.client.services.GameUIService;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.ObservableConnection;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.commons.statemachine.StateMachineCallback;
import dok.game.model.Action;
import dok.game.model.Player;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.network.model.ActionPacket;
import dok.game.network.model.CharacterSelectionPacket;
import dok.game.network.model.GameResultPacket;
import dok.game.network.model.ModelPacket;
import dok.game.network.model.ReactionPacket;
import dok.game.network.model.SurrenderPacket;
import dok.game.network.model.SwapRequestPacket;
import dok.game.network.model.WorkDonePacket;
import dok.game.services.PlayerInteractionWrapper;

public class DuelGame extends ClientGame implements PlayerInteractionWrapper, Runnable, ConnectionListener {

	// Class Constants

    private static final Logger logger = LogManager.getLogger();

	// Attributes

	private final Player_Master playerInteractionService;
	private final ObservableConnection connection;

	// Constructor(s)

	public DuelGame(ObservableConnection conn, StateMachineCallback parent, CharacterDataService characterClassService,
			AbilityDataService abilityClassService, GameUIService gameUIService, Player self, List<Player> players) {
		super(parent, characterClassService, abilityClassService);
		this.connection = conn;
		this.getPlayers().addAll(players);
		playerInteractionService = new Player_Master(parent, self.getPlayerId(), self.getLineup(), characterClassService, abilityClassService, gameUIService, getInputProvider(), this);
	}

	// <--- PlayerInteractionWrapper --->

	@Override
	public void handleReaction(Reaction reaction) {
		try {
			getConnection().sendPacket(new ReactionPacket(reaction));
		} catch (IOException e) {
			logger.catching(e);
		}
	}

	@Override
	public void handleAction(Action action) {
		try {
			getConnection().sendPacket(new ActionPacket(action));
		} catch (IOException e) {
			logger.catching(e);
		}
	}

	@Override
	public void handleCharacterChoosen(Position pos) {
		try {
			getConnection().sendPacket(new CharacterSelectionPacket(pos));
		} catch (IOException e) {
			logger.catching(e);
		}
	}

	@Override
	public void handleSwapRequest(SwapRequest swapRequest) {
		try {
			getConnection().sendPacket(new SwapRequestPacket(swapRequest));
		} catch (IOException e) {
			logger.catching(e);
		}
	}

	@Override
	public void handleSurrenderRequest() {
		try {
			getConnection().sendPacket(new SurrenderPacket());
		} catch (IOException e) {
			logger.catching(e);
		}
	}

	@Override
	public void handleWorkDone() {
		try {
			getConnection().sendPacket(new WorkDonePacket());
		} catch (IOException e) {
			logger.catching(e);
		}
	}

	@Override
	public void onFinish(Object result) {
		logger.debug("{} finished with result {}", getClass().getSimpleName(), result);
		if (!getConnection().removeListener(this)) logger.error("DuelGame has not been deregistered from network connection");
		getParent().onFinish(result);
	}

	// <--- Game --->

	@Override
	public void start() {
		getConnection().addListener(this);
	}

	// <--- Runnable --->

	@Override
	public void run() {
		start();
	}

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;
			logger.debug("Receiving packet " + packetReceivedEvent.getPacket() + " at " + (System.nanoTime() / 1000000.) + "ms");
			if (packetReceivedEvent.getPacket() instanceof ModelPacket) {
				ModelPacket modelPacket = (ModelPacket) packetReceivedEvent.getPacket();
				logger.debug("New Model is: " + modelPacket.getModel());
				getPlayerInteractionService().handleModelUpdate(modelPacket.getModel());
			} else if (packetReceivedEvent.getPacket() instanceof ActionPacket) {
				getPlayerInteractionService().performAction();
			} else if (packetReceivedEvent.getPacket() instanceof ReactionPacket) {
				ReactionPacket reactionPacket = (ReactionPacket) packetReceivedEvent.getPacket();
				getPlayerInteractionService().performReaction(reactionPacket.getList(), reactionPacket.getGameEvents());
			} else if (packetReceivedEvent.getPacket() instanceof CharacterSelectionPacket) {
				CharacterSelectionPacket characterSelectionPacket = (CharacterSelectionPacket) packetReceivedEvent.getPacket();
				getPlayerInteractionService().performCharacterMarkerPlacement(characterSelectionPacket.getFilter(), characterSelectionPacket.getAvailablePositions());
			} else if (packetReceivedEvent.getPacket() instanceof GameResultPacket) {
				GameResultPacket gameResultPacket = (GameResultPacket) packetReceivedEvent.getPacket();
				getPlayerInteractionService().handleGameResult(gameResultPacket.getGameResult());
			} else if (packetReceivedEvent.getPacket() instanceof SwapRequestPacket) {
				SwapRequestPacket swapRequestPacket = (SwapRequestPacket) packetReceivedEvent.getPacket();
				SwapRequest swapRequest = swapRequestPacket.getSwapRequest();
				if (swapRequest == null) {
					getPlayerInteractionService().placeCharacters();
				} else {
					getPlayerInteractionService().handleSwapRequest(swapRequest);
				}
			}
		}
	}

	// Getter & Setter

	public Player_Master getPlayerInteractionService() {
		return playerInteractionService;
	}

	public ObservableConnection getConnection() {
		return connection;
	}

}
