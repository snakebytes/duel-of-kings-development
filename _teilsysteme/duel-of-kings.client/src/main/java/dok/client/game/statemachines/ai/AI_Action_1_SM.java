package dok.client.game.statemachines.ai;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.ai.AI_Action_1_SM.AI_Action_1_SM_State;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.util.GameLogicUtils;
import dok.game.model.Ability;
import dok.game.model.Action;
import dok.game.model.Character;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameStateModelService;

public class AI_Action_1_SM extends StateMachineImpl<AI_Action_1_SM_State> implements StateMachineCallback {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum AI_Action_1_SM_State {
		GRINDTARGET
	}

	// Services

    private final AbilityDataService abilityClassService;
    private final CharacterDataService characterClassService;
    private final GameStateModelService gameStateModelService;

	// Attributes

    Character king;


	public AI_Action_1_SM(StateMachineCallback parent, AbilityDataService abilityClassService, CharacterDataService characterClassService, GameStateModelService gameStateModelService)
			throws IllegalArgumentException {
		super(parent, AI_Action_1_SM_State.GRINDTARGET);
		logger.entry(parent, abilityClassService, characterClassService, gameStateModelService);
        this.abilityClassService = abilityClassService;
        this.characterClassService = characterClassService;
        this.gameStateModelService = gameStateModelService;
        king = GameLogicUtils.getEnemyKing(gameStateModelService, gameStateModelService.getActivePlayer());
        transition(getStartState());
        logger.exit(this);
	}

	@Override
	protected void onEntry(AI_Action_1_SM_State state) {
		logger.entry(state);
		setActiveChild(new GrindTarget(this, king, abilityClassService, characterClassService, gameStateModelService));
		logger.exit();
	}

	@Override
	public void onFinish(Object result) {
		logger.entry(result);
		setActiveChild(null);
		if(getCurrentState() == AI_Action_1_SM_State.GRINDTARGET) {
			if(result instanceof HashMap && result != null) {
				logger.debug("result is HashMap and not null");
				HashMap<?,?> resmap = (HashMap<?,?>) result;
				if(resmap.keySet().toArray()[0] instanceof String) {
					logger.debug("resmap has Strings as keys");
					@SuppressWarnings("unchecked")
					HashMap<String, Object> results = (HashMap<String, Object>) resmap;
					if(results.get("skill") instanceof Ability && results.get("skill") != null
					&& results.get("target") instanceof Position && results.get("target") != null) {
						Ability skill = (Ability) results.get("skill");
						Position trgt = (Position) results.get("target");
						finish(logger.exit(new Action(skill.getOwningCharacterId(), trgt, skill.getAbilityClassId())));
						return;
					}
				}
			}
			finish(logger.exit(null));
		}
	}

	@Override
	protected void onExit(AI_Action_1_SM_State state) {}

}
