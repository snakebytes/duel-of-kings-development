package dok.client.game.statemachines.ai;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.ai.AI_Action_2_SM.AI_Action_2_SM_State;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.util.GameLogicUtils;
import dok.game.model.Ability;
import dok.game.model.Action;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameStateModelService;

public class AI_Action_2_SM extends StateMachineImpl<AI_Action_2_SM_State> implements StateMachineCallback {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum AI_Action_2_SM_State {
		FINDTHREATENEDENEMY,
		GRINDTARGET
	}

	// Services

	private final AbilityDataService abilityClassService;
    private final CharacterDataService characterClassService;
    private final GameStateModelService gameStateModelService;

	// Attributes

	public AI_Action_2_SM(StateMachineCallback parent, AbilityDataService abilityClassService, CharacterDataService characterClassService, GameStateModelService gameStateModelService)
			throws IllegalArgumentException {
		super(parent, AI_Action_2_SM_State.FINDTHREATENEDENEMY);
		logger.entry(parent, abilityClassService, characterClassService, gameStateModelService);
		this.abilityClassService = abilityClassService;
		this.characterClassService = characterClassService;
		this.gameStateModelService = gameStateModelService;
		transition(getStartState());
		logger.exit(this);
	}

	@Override
	protected void onEntry(AI_Action_2_SM_State state) {
		logger.entry(state);
		switch(state) {
		case FINDTHREATENEDENEMY:
			setActiveChild(new FindThreatenedEnemy(this, abilityClassService, characterClassService, gameStateModelService));
			break;
		case GRINDTARGET:
			setActiveChild(new GrindTarget(this, GameLogicUtils.getEnemyKing(gameStateModelService, gameStateModelService.getActivePlayer()), abilityClassService, characterClassService, gameStateModelService));
			break;
		}
		logger.exit();
	}

	@Override
	public void onFinish(Object result) {
		logger.entry(result);
		setActiveChild(null);
		switch(getCurrentState()) {
		case FINDTHREATENEDENEMY:
			/* Kept for comparison against new HashMap-based logic
			 * if(resp != null && resp.getKey() instanceof Ability && resp.getValue() instanceof Character) {
				Ability skill = (Ability) resp.getKey();
				Character cha = (Character) resp.getValue();
				if(skill == null || cha == null) {
					transition(AI_Action_2_SM_State.GRINDTARGET);
					logger.exit();
					return;
				} else {
					finish(logger.exit(new ActionImpl(skill.getOwningCharacterId(), cha.getPosition(), skill.getAbilityClassId())));
					logger.exit();
					return;
				}
			} else {
				transition(AI_Action_2_SM_State.GRINDTARGET);
				logger.exit();
				return;
			}*/
			if(result instanceof HashMap && result != null) {
				HashMap<?,?> resmap = (HashMap<?,?>) result;
				if(resmap.keySet().toArray()[0] instanceof String) {
					@SuppressWarnings("unchecked")
					HashMap<String, Object> results = (HashMap<String, Object>) resmap;
					if(results.get("skill") instanceof Ability && results.get("skill") != null
					&& results.get("target") instanceof Position && results.get("target") != null) {
						Ability skill = (Ability) results.get("skill");
						Position trgt = (Position) results.get("target");
						finish(logger.exit(new Action(skill.getOwningCharacterId(), trgt, skill.getAbilityClassId())));
						return;
					}
				}
			}
			transition(AI_Action_2_SM_State.GRINDTARGET);
			logger.debug("AI_Action_2: FindThreatenedEnemy -> GrindTarget");
			logger.exit();
			return;
		case GRINDTARGET:
			/* Kept for comparison against new HashMap-based logic
			 * if(resp != null && resp.getKey() instanceof Ability && resp.getValue() instanceof Character) {
				Ability skill = (Ability) resp.getKey();
				Character cha = (Character) resp.getValue();
				if(skill == null || cha == null) {
					finish(logger.exit(null));
					logger.exit();
					return;
				} else {
					finish(logger.exit(new ActionImpl(skill.getOwningCharacterId(), cha.getPosition(), skill.getAbilityClassId())));
					logger.exit();
					return;
				}
			} else {
				finish(logger.exit(null));
				return;
			}*/
			if(result instanceof HashMap && result != null) {
				HashMap<?,?> resmap = (HashMap<?,?>) result;
				if(resmap.keySet().toArray()[0] instanceof String) {
					@SuppressWarnings("unchecked")
					HashMap<String, Object> results = (HashMap<String, Object>) resmap;
					if(results.get("skill") instanceof Ability && results.get("skill") != null
					&& results.get("target") instanceof Position && results.get("target") != null) {
						Ability skill = (Ability) results.get("skill");
						Position trgt = (Position) results.get("target");
						finish(logger.exit(new Action(skill.getOwningCharacterId(), trgt, skill.getAbilityClassId())));
						return;
					}
				}
			}
		}
		finish(logger.exit(null));
	}

	@Override
	protected void onExit(AI_Action_2_SM_State state) {}

}
