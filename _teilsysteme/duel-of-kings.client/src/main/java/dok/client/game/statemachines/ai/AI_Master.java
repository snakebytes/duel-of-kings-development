package dok.client.game.statemachines.ai;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.ai.AI_Master.AI_Master_State;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.services.impl.GameStateModelProvider;
import dok.game.model.AILevel;
import dok.game.model.Action;
import dok.game.model.GameEvent;
import dok.game.model.GameResult;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.SwapRequest;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerInteractionService;
import dok.game.services.PlayerInteractionWrapper;

public class AI_Master extends StateMachineImpl<AI_Master_State> implements PlayerInteractionService, StateMachineCallback {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum AI_Master_State {
		IDLE,
		AI_REACTION,
		AI_PLACEMARKER,
		AI_PLACECHARS,
		AI_ACTION_1,
		AI_ACTION_2
	}

	// Services

	private final PlayerInteractionWrapper playerInteractionWrapper;
	private final AILevel aILevel;
    private final AbilityDataService abilityClassService;
    private final CharacterDataService characterClassService;
    private GameStateModelService gameStateModelService;

	// Attributes

	public AI_Master(StateMachineCallback parent, PlayerInteractionWrapper playerInteractionWrapper, AILevel aILevel, AbilityDataService abilityClassService, CharacterDataService characterClassService) {
		super(parent, AI_Master_State.IDLE);
		logger.entry(parent, playerInteractionWrapper, aILevel, abilityClassService, characterClassService);
		this.playerInteractionWrapper = playerInteractionWrapper;
		this.aILevel = aILevel;
		this.setGameStateModelService(new GameStateModelProvider());
		this.abilityClassService = abilityClassService;
		this.characterClassService = characterClassService;
		transition(null, getStartState());
		logger.exit(this);
	}

	// Methods

	// <StateMachineImpl>

	@Override
	protected void onEntry(AI_Master_State state) {
		logger.entry(state);
		switch(state) {
		case AI_ACTION_1:
//			try {
//				Thread.sleep(2250);
//			} catch (InterruptedException e) {
//				logger.catching(e);
//			}
			setActiveChild(new AI_Action_1_SM(this, getAbilityClassService(), getCharacterClassService(), getGameStateModelService()));
			logger.exit();
			break;
		case AI_ACTION_2:
//			try {
//				Thread.sleep(2250);
//			} catch (InterruptedException e) {
//				logger.catching(e);
//			}
			setActiveChild(new AI_Action_2_SM(this, getAbilityClassService(), getCharacterClassService(), getGameStateModelService()));
			logger.exit();
			break;
		case AI_PLACECHARS:
//			try {
//				Thread.sleep(1500);
//			} catch (InterruptedException e) {
//				logger.catching(e);
//			}
			getPlayerInteractionWrapper().handleSwapRequest(null);
			transition(AI_Master_State.IDLE);
			logger.exit();
			break;
		case AI_PLACEMARKER:
//			try {
//				Thread.sleep(1500);
//			} catch (InterruptedException e) {
//				logger.catching(e);
//			}
			getPlayerInteractionWrapper().handleCharacterChoosen(null);
			transition(AI_Master_State.IDLE);
			logger.exit();
			break;
		case AI_REACTION:
//			try {
//				Thread.sleep(1500);
//			} catch (InterruptedException e) {
//				logger.catching(e);
//			}
			getPlayerInteractionWrapper().handleReaction(null);
			transition(AI_Master_State.IDLE);
			logger.exit();
			break;
		case IDLE:
			break;
		}
		logger.exit();
	}

	@Override
	protected void onExit(AI_Master_State state) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object handle(Object input) {
		logger.entry(input);
		return super.handle(input);
	}

	// </StateMachineImpl>

	// <StateMachineCallback>

	@Override
	public void onFinish(Object result) {
		logger.entry(result);
		setActiveChild(null);
		switch(getCurrentState()) {
		case AI_ACTION_1:
		case AI_ACTION_2:
			logger.debug(result);
			getPlayerInteractionWrapper().handleAction((Action) result);
			transition(AI_Master_State.IDLE);
			logger.exit();
			break;
		case AI_PLACECHARS:
			logger.exit();
			break;
		case AI_PLACEMARKER:
			logger.exit();
			break;
		case AI_REACTION:
			logger.exit();
			break;
		case IDLE:
			logger.exit();
			break;
		}
	}

	// </StateMachineCallback>

	// <PlayerInteractionService>

	@Override
	public void performCharacterMarkerPlacement(ITargetFilter filter, Set<Position> alreadyChosen) throws IllegalStateException {
		logger.entry(filter, alreadyChosen);
		switch(getAILevel()) {
		case BEGINNER:
		case EASY:
			transition(AI_Master_State.AI_PLACEMARKER);
			logger.exit();
			break;
		case EXPERT:
			logger.exit();
			break;
		case HARD:
			logger.exit();
			break;
		case MASTER:
			logger.exit();
			break;
		case NORMAL:
			logger.exit();
			break;
		}
	}

	@Override
	public void performAction() throws IllegalStateException {
		logger.entry();
		switch(getAILevel()) {
		case BEGINNER:
			transition(AI_Master_State.AI_ACTION_1);
			logger.exit();
			break;
		case EASY:
			transition(AI_Master_State.AI_ACTION_2);
			logger.exit();
			break;
		case EXPERT:
			logger.exit();
			break;
		case HARD:
			logger.exit();
			break;
		case MASTER:
			logger.exit();
			break;
		case NORMAL:
			logger.exit();
			break;
		}
	}

	@Override
	public void performReaction(List<Trigger> possibleReactions, List<GameEvent> gameEvents) throws IllegalStateException {
		logger.entry(possibleReactions, gameEvents);
		switch(getAILevel()) {
		case BEGINNER:
		case EASY:
			transition(AI_Master_State.AI_REACTION);
			logger.exit();
			break;
		case EXPERT:
			logger.exit();
			break;
		case HARD:
			logger.exit();
			break;
		case MASTER:
			logger.exit();
			break;
		case NORMAL:
			logger.exit();
			break;
		}

	}

	@Override
	public void placeCharacters() throws IllegalStateException {
		logger.entry();
		switch(getAILevel()) {
		case BEGINNER:
		case EASY:
			transition(AI_Master_State.AI_PLACECHARS);
			logger.exit();
			break;
		case EXPERT:
			logger.exit();
			break;
		case HARD:
			logger.exit();
			break;
		case MASTER:
			logger.exit();
			break;
		case NORMAL:
			logger.exit();
			break;
		}

	}

	@Override
	public void handleSwapRequest(SwapRequest swapRequest) {
		logger.entry(swapRequest);
		if (getCurrentState() == AI_Master_State.AI_PLACECHARS) {
			logger.entry(swapRequest);
			getGameStateModelService().swap(null, swapRequest.getSource(), swapRequest.getTarget());
		}
		logger.exit();
	}

	@Override
	public void handleModelUpdate(GameStateModel newModel) {
		logger.entry(newModel.getFields());
		getGameStateModelService().overrideWith(newModel);
		getPlayerInteractionWrapper().handleWorkDone();
		logger.exit();
	}

	@Override
	public void handleGameResult(GameResult result) {
		logger.entry(result);
		finish(result);
		logger.exit(result);
	}

	// </PlayerInteractionService>

	// Getter & Setter

	public PlayerInteractionWrapper getPlayerInteractionWrapper() {
		return playerInteractionWrapper;
	}

	public AILevel getAILevel() {
		return aILevel;
	}

	private AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	private CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	private GameStateModelService getGameStateModelService() {
		return gameStateModelService;
	}

	private void setGameStateModelService(GameStateModelService gameStateModelService) {
		this.gameStateModelService = gameStateModelService;
	}

}
