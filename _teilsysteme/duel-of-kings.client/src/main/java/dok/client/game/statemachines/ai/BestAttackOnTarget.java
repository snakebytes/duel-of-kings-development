package dok.client.game.statemachines.ai;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.ai.BestAttackOnTarget.BestAttackOnTarget_State;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.util.GameLogicUtils;
import dok.game.model.Ability;
import dok.game.model.Character;
import dok.game.model.Position;
import dok.game.model.TargetFilter;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.constants.AbilityClassIds;
import dok.game.services.GameStateModelService;

public class BestAttackOnTarget extends StateMachineImpl<BestAttackOnTarget_State> {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

    // States

    public enum BestAttackOnTarget_State {
        FINDMAXDIRECT,
        COMPARESECONDARY,
        COMPARESKILLS
    }

    // Services

    private final AbilityDataService abilityClassService;
    private final CharacterDataService characterClassService;
    private final GameStateModelService gameStateModelService;

    // Attributes

    HashMap<String, Object> results = new HashMap<String, Object>();
    Position origTarget;
    boolean useSkills;
    ArrayList<Character> own = new ArrayList<>();
    ArrayList<Character> enemies = new ArrayList<>();


    public BestAttackOnTarget(boolean useSkills, StateMachineCallback parent, Position target, AbilityDataService abilityClassService, CharacterDataService characterClassService, GameStateModelService gameStateModelService) {
        super(parent, BestAttackOnTarget_State.FINDMAXDIRECT);
        logger.entry(useSkills, parent, target, abilityClassService, characterClassService, gameStateModelService);
        this.abilityClassService = abilityClassService;
        this.characterClassService = characterClassService;
        this.origTarget = target;
        results.put("damage", 0);
        this.gameStateModelService = gameStateModelService;
        this.useSkills = useSkills;
        for(Character c : gameStateModelService.getCharacters()) {
            if(c.getOwningPlayerId().equals(gameStateModelService.getActivePlayer())) {
            	if(GameLogicUtils.canAct(c, gameStateModelService, characterClassService)) {
            		own.add(c);
            	}
            } else enemies.add(c);
        }
        transition(getStartState());
        logger.exit(this);
    }

    @Override
    protected void onEntry(BestAttackOnTarget_State state) {
    	logger.entry(state);
        switch(state) {
        case COMPARESECONDARY:
            for(Character c : own) {
            	if(GameLogicUtils.canAct(c, gameStateModelService, characterClassService)) {
		            for(Character e : enemies) {
		                if(TargetFilter.ATTACK.test(gameStateModelService.getActivePlayer(), AbilityClassIds.ATTACK_ABILITY_CLASS_ID, c.getPosition(), e.getPosition(), 1, gameStateModelService, characterClassService, abilityClassService)) {
		                	int dmg = GameLogicUtils.getSkillDamageWithTarget(c.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID), e.getPosition(), origTarget, gameStateModelService, abilityClassService, characterClassService);
		                	if(results.get("damage") != null && dmg >= (int)results.get("damage")) {
		                		results.put("damage", dmg);
		                		results.put("skill", c.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID));
		                		results.put("target", e.getPosition());
		                	}
		                }
		            }
            	}
            }
            if(useSkills) {
            	transition(BestAttackOnTarget_State.COMPARESKILLS);
            	logger.exit();
				return;
            } else {
            	if(results.get("damage") == null || (int) results.get("damage") == 0 || results.get("skill") == null || results.get("target") == null) finish(logger.exit(null));
                else finish(logger.exit(results));
				return;
            }
        case COMPARESKILLS:
            for(Character c : own) {
            	if(GameLogicUtils.canUseSkills(c, gameStateModelService, characterClassService)) {
	                for(Ability a : c.getAbilities()) {
	                    for(Character e : enemies) {
	                        if(c.getTargetFilter().test(gameStateModelService.getActivePlayer(), a.getAbilityClassId(), c.getPosition(), e.getPosition(), 1, gameStateModelService, characterClassService, abilityClassService)) {
	                        	int dmg = GameLogicUtils.getSkillDamageWithTarget(a, e.getPosition(), origTarget, gameStateModelService, abilityClassService, characterClassService);
	                        	if(results.get("damage") != null && dmg >= (int)results.get("damage")) {
	                        		results.put("damage", dmg);
			                		results.put("skill", c.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID));
			                		results.put("target", e.getPosition());
	                        	}
	                        }
	                    }
	                }
            	}
            }
            if(results.get("damage") == null || (int) results.get("damage") == 0 || results.get("skill") == null || results.get("target") == null) finish(logger.exit(null));
            else finish(logger.exit(results));
			return;
        case FINDMAXDIRECT:
        	if(own.isEmpty()) {
        		finish(null);
        		logger.exit();
        		return;
        	}
            for(Character c : own) {
                Ability ab = c.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID);
                if(GameLogicUtils.canAct(c, gameStateModelService, characterClassService)) {
                	if(TargetFilter.ATTACK.test(gameStateModelService.getActivePlayer(), AbilityClassIds.ATTACK_ABILITY_CLASS_ID, c.getPosition(), origTarget, 1, gameStateModelService, characterClassService, abilityClassService)) {
	                    int d = GameLogicUtils.getSkillDamageWithTarget(ab, origTarget, origTarget, gameStateModelService, abilityClassService, characterClassService);
	                    if( results.get("damage") != null && d >= (int)results.get("damage")) {
	                    	results.put("damage", d);
	                		results.put("skill", c.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID));
	                		results.put("target", origTarget);
	                    }
	                }
                }
            }
            transition(BestAttackOnTarget_State.COMPARESECONDARY);
            logger.exit();
			return;
        }

    }

    @Override
    protected void onExit(BestAttackOnTarget_State state) {}

}
