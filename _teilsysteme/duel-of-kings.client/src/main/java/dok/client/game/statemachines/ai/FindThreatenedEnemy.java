package dok.client.game.statemachines.ai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.ai.FindThreatenedEnemy.FindThreatenedEnemy_State;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.model.Ability;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.GameStateModelService;

public class FindThreatenedEnemy extends StateMachineImpl<FindThreatenedEnemy_State> implements StateMachineCallback {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum FindThreatenedEnemy_State {
		SELECTNEXTENEMY,
		BESTATTACKONTARGET,
		REMOVEENEMY
	}

	// Attributes

	Ability skill; // Includes UUID of Source-Character
    List<Character> enemies;
    Character selEnemy;

    // Services

    private final AbilityDataService abilityClassService;
    private final CharacterDataService characterClassService;
    private final GameStateModelService gameStateModelService;


	public FindThreatenedEnemy(StateMachineCallback parent, AbilityDataService abilityClassService, CharacterDataService characterClassService, GameStateModelService gameStateModelService)
			throws IllegalArgumentException {
		super(parent, FindThreatenedEnemy_State.SELECTNEXTENEMY);
		logger.entry(parent, abilityClassService, characterClassService, gameStateModelService);
		this.abilityClassService = abilityClassService;
		this.characterClassService = characterClassService;
		this.gameStateModelService = gameStateModelService;
		enemies = gameStateModelService.getCharacters();
		for(Character c : new ArrayList<>(enemies)) {
			if(c.getOwningPlayerId().equals(gameStateModelService.getActivePlayer())) {
				enemies.remove(c);
			}
		}
		transition(getStartState());
		logger.exit(this);
	}

	@Override
	protected void onEntry(FindThreatenedEnemy_State state) {
		logger.entry(state);
		switch(state) {
		case BESTATTACKONTARGET:
			setActiveChild(new BestAttackOnTarget(false, this, selEnemy.getPosition(), abilityClassService, characterClassService, gameStateModelService));
			break;
		case REMOVEENEMY:
			enemies.remove(selEnemy);
			skill = null;
			selEnemy = null;
			if(enemies.isEmpty()) {
				finish(logger.exit(null));
			} else {
				transition(FindThreatenedEnemy_State.SELECTNEXTENEMY);
			}
			break;
		case SELECTNEXTENEMY:
			selEnemy = enemies.get(0);
			transition(FindThreatenedEnemy_State.BESTATTACKONTARGET);
			break;
		}

	}

	@Override
	protected void onExit(FindThreatenedEnemy_State state) {}

	@Override
	public void onFinish(Object result) {
		logger.entry(result);
		setActiveChild(null);
		if(getCurrentState() == FindThreatenedEnemy_State.BESTATTACKONTARGET) {
			if(result instanceof HashMap && result != null) {
				HashMap<?,?> resmap = (HashMap<?,?>) result;
				if(resmap.keySet().toArray()[0] instanceof String) {
					@SuppressWarnings("unchecked")
					HashMap<String, Object> results = (HashMap<String, Object>) resmap;
					if(results.get("skill") instanceof Ability && results.get("skill") != null
					&& results.get("damage") instanceof Integer && results.get("damage") != null && (int) results.get("damage") != 0
					&& results.get("target") instanceof Position && results.get("target") != null) {
						int healthleft = CharacterStateUtils.getCharacterAttributeValue(selEnemy, CharacterAttribute.HEALTH, characterClassService) - selEnemy.getDamageTaken();
						if(healthleft > (int) results.get("damage")) {
							transition(FindThreatenedEnemy_State.REMOVEENEMY);
							logger.exit();
							return;
						} else {
							finish(logger.exit(results));
							return;
						}
					}
				}
			}
			finish(logger.exit(null));
			return;
		}
	}

}
