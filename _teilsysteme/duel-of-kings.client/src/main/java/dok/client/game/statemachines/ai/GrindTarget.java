package dok.client.game.statemachines.ai;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.ai.GrindTarget.GrindTarget_State;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.util.GameLogicUtils;
import dok.game.model.Ability;
import dok.game.model.Character;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameStateModelService;

public class GrindTarget extends StateMachineImpl<GrindTarget_State> implements StateMachineCallback {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum GrindTarget_State {
		BESTATTACKONTARGET,
		CHANGETARGETFRONT
	}

	// Services

    private final AbilityDataService abilityClassService;
    private final CharacterDataService characterClassService;
    private final GameStateModelService gameStateModelService;

	// Attributes

    Character target;


	public GrindTarget(StateMachineCallback parent, Character target, AbilityDataService abilityClassService, CharacterDataService characterClassService, GameStateModelService gameStateModelService) throws IllegalArgumentException {
		super(parent, GrindTarget_State.BESTATTACKONTARGET);
		logger.entry(parent, target, abilityClassService, characterClassService, gameStateModelService);
        this.abilityClassService = abilityClassService;
        this.characterClassService = characterClassService;
        this.target = target;
        this.gameStateModelService = gameStateModelService;
        transition(getStartState());
        logger.exit(this);
	}



	@Override
	protected void onEntry(GrindTarget_State state) {
		logger.entry(state);
		switch(state) {
		case BESTATTACKONTARGET:
			setActiveChild(new BestAttackOnTarget(false, this, target.getPosition(), abilityClassService, characterClassService, gameStateModelService));
			break;
		case CHANGETARGETFRONT:
			target = GameLogicUtils.getFirstAliveOfCol(target.getPosition().getCol(), target.getOwningPlayerId(), gameStateModelService, characterClassService);
			if(target == null) finish(logger.exit(null));
			transition(GrindTarget_State.BESTATTACKONTARGET);
			break;
		}

	}

	@Override
	protected void onExit(GrindTarget_State state) {}

	@Override
	public void onFinish(Object result) {
		logger.entry(result);
		setActiveChild(null);
		if(getCurrentState() == GrindTarget_State.BESTATTACKONTARGET) {
			if(result instanceof HashMap && result != null) {
				HashMap<?,?> resmap = (HashMap<?,?>) result;
				if(resmap.keySet().toArray()[0] instanceof String) {
					@SuppressWarnings("unchecked")
					HashMap<String, Object> results = (HashMap<String, Object>) resmap;
					if(results.get("skill") instanceof Ability && results.get("skill") != null
					&& results.get("damage") instanceof Integer && results.get("damage") != null && (int) results.get("damage") != 0
					&& results.get("target") instanceof Position && results.get("target") != null) {
						finish(logger.exit(results));
						return;
					}
				}
			}
			if(!target.equals(GameLogicUtils.getFirstAliveOfCol(target.getPosition().getCol(), target.getOwningPlayerId(), gameStateModelService, characterClassService))) {
				transition(GrindTarget_State.CHANGETARGETFRONT);
				logger.exit();
			} else {
				finish(logger.exit(null));
			}
		}
	}

}
