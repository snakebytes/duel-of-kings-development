package dok.client.game.statemachines.player;

import java.util.UUID;
import java.util.function.Consumer;

import dok.client.game.statemachines.player.Player_Action.Player_Action_State;
import dok.client.services.GameUIService;
import dok.commons.ConsumableService;
import dok.commons.impl.CustomTimer;
import dok.commons.impl.CustomTimer.CustomTimerCallback;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.model.Ability;
import dok.game.model.AbilityClass;
import dok.game.model.Action;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.TargetFilter;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import dok.game.model.constants.AbilityClassIds;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class Player_Action extends StateMachineImpl<Player_Action_State> implements StateMachineCallback {

    // States

    public enum Player_Action_State {
        CHOOSE_CHARACTER,
        CHOOSE_ABILITY,
        CHOOSE_TARGET,
        GENERATE_ACTION
    }

    // Attributes

    private final UUID player;
    private final ConsumableService<Object> inputService;
    private final AbilityDataService abilityClassService;
    private final GameStateModel model;
    private final CharacterDataService characterClassService;
    private final GameUIService gameUIService;
    private final CustomTimer timer;
    private final CustomTimerCallback timerCallback = this::onTimerExpired;
    private final Consumer<Object> inputConsumer = this::handle;
    private Position source;
    private Position target;
    private Ability ability;
    private boolean skipping;

    // Constructor(s)

    public Player_Action(StateMachineCallback parent, UUID player, ConsumableService<Object> inputService, GameUIService gameUIService, GameStateModel model, CharacterDataService characterClassService, AbilityDataService abilityClassService, CustomTimer timer) throws IllegalArgumentException {
        super(parent, Player_Action_State.CHOOSE_CHARACTER);
        this.inputService = inputService;
        this.abilityClassService = abilityClassService;
        this.model = model;
        this.characterClassService = characterClassService;
        this.gameUIService = gameUIService;
        this.player = player;
        this.timer = timer;
        timer.addCallback(getTimerCallback());
        transition(null, getStartState());
    }

    // Methods

    private final void onTimerExpired(CustomTimer timer) {
        timer.removeCallback(getTimerCallback());
        finish(null);
    }

    @Override
    public Object handle(Object input) {

       if (getCurrentState() == Player_Action_State.CHOOSE_CHARACTER || getCurrentState() == Player_Action_State.CHOOSE_TARGET) {

           return super.handle(input);

       } else if (getCurrentState() == Player_Action_State.CHOOSE_ABILITY) {

            if (input != null && input instanceof Ability) {

                Ability abil = (Ability) input;
                AbilityClass abilClass = getAbilityClassService().getAbilityClass(abil.getAbilityClassId());
                if (!(abilClass.getHitFilter() == null && abilClass.getTargetFilter() == null) // Passives cannot be cast
                		&& !getModel().getCharacterAt(getSource()).isStunned()
                		&& abil.getTimeCounters() == 0
                		&& !(getModel().getCharacterAt(getSource()).isSilenced() && !abil.isBasic()) // Silence prevents characters from casting non-basic abilities
                		&& getModel().getCharacterAt(getSource()).wasAvailableAtTurnStart()
                		&& (abil.getAbilityClassId() == AbilityClassIds.SWAP_ABILITY_CLASS_ID
                			|| !getModel().getCharacterAt(getSource()).hasPerformedAction())
                	)
                {
                	setAbility(abil);
	                transition(Player_Action_State.CHOOSE_TARGET);
                }

            } else if (input != null && input instanceof Boolean && !(boolean)input) transition(Player_Action_State.CHOOSE_CHARACTER);
              else if (input == null) transition(Player_Action_State.GENERATE_ACTION);

        }

        return null;
    }

    // Events

    @Override
    public void onFinish(Object result) {
        setActiveChild(null);

        if (getCurrentState() == Player_Action_State.CHOOSE_CHARACTER) {

            if (result != null && result instanceof Position) {

                setSource((Position) result);
                transition(getCurrentState(), Player_Action_State.CHOOSE_ABILITY);

            } else if (result == null) transition(Player_Action_State.GENERATE_ACTION);
              else if (result != null && result instanceof Boolean && !(boolean)result) transition(Player_Action_State.CHOOSE_CHARACTER);

        } else  if (getCurrentState() == Player_Action_State.CHOOSE_TARGET) {

             if (result != null && result instanceof Position) {

                 setTarget((Position) result);
                 transition(getCurrentState(), Player_Action_State.GENERATE_ACTION);

             } else if (result == null) {
            	 setSkipping(true);
            	 transition(getCurrentState(), Player_Action_State.GENERATE_ACTION);
             } else if (result != null && result instanceof Boolean && !(boolean)result) transition(Player_Action_State.CHOOSE_CHARACTER);

        }

    }

    @Override
    public void interrupt() {
    	super.interrupt();
    	if (getCurrentState() != null) {
    		getModel().getChooseTimer().stop();
    		getModel().getTurnTimer().stop();
    	}
    }

    @Override
    protected void onEntry(Player_Action_State state) {
        switch (state) {
            case CHOOSE_CHARACTER:

                getGameUIService().showTaskMessage("Game.Task.ChooseAction.Character", null);
                setSource(null);
                setActiveChild(new Player_SelectTarget(this, getGameUIService(), getModel(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPlayer(), null, getSource(), TargetFilter.and(TargetFilter.OWN, TargetFilter.AVAILABLE_AT_TURN_START), getTimer()));
                break;

            case CHOOSE_ABILITY:

                getGameUIService().showTaskMessage("Game.Task.ChooseAction.Ability", null);
                setAbility(null);
                getInputService().addConsumer(getInputConsumer());
                getGameUIService().showCharacterAbilities(getModel().getCharacterAt(getSource()));
                break;

            case CHOOSE_TARGET:

                ITargetFilter targetFilter = getAbilityClassService().getAbilityClass(getAbility().getAbilityClassId()).getTargetFilter();
                if (targetFilter != null) {
                    getGameUIService().showTaskMessage("Game.Task.ChooseAction.Target", new Object[]{targetFilter});
                    setTarget(null);
                    setActiveChild(new Player_SelectTarget(this, getGameUIService(), getModel(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPlayer(), getAbility(), getSource(), targetFilter, getTimer()));
                } else transition(Player_Action_State.GENERATE_ACTION);
                break;

            case GENERATE_ACTION:

                if (!isSkipping() && getSource() != null && getAbility() != null) {
                    finish(new Action(getModel().getCharacterAt(getSource()).getId(), getTarget() != null ? getTarget() : null, getAbility().getAbilityClassId()));
                } else finish(null);
                break;

        }
    }

    @Override
    protected void onExit(Player_Action_State state) {
        if (state == Player_Action_State.CHOOSE_ABILITY) {
            getInputService().removeConsumer(getInputConsumer());
            getGameUIService().hideCharacterAbilityDisplay();
        }
    }

    // Getter & Setter

    public CustomTimer getTimer() {
        return timer;
    }

    public Position getSource() {
        return source;
    }

    public void setSource(Position source) {
        this.source = source;
    }

    public GameStateModel getModel() {
        return model;
    }

    public Position getTarget() {
        return target;
    }

    public void setTarget(Position target) {
        this.target = target;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }

    public CharacterDataService getCharacterClassService() {
        return characterClassService;
    }

    public AbilityDataService getAbilityClassService() {
        return abilityClassService;
    }

    public ConsumableService<Object> getInputService() {
        return inputService;
    }

    public UUID getPlayer() {
        return player;
    }

    public GameUIService getGameUIService() {
        return gameUIService;
    }

    public Consumer<Object> getInputConsumer() {
        return inputConsumer;
    }

    public CustomTimerCallback getTimerCallback() {
        return timerCallback;
    }

	public boolean isSkipping() {
		return skipping;
	}

	public void setSkipping(boolean skipping) {
		this.skipping = skipping;
	}

}
