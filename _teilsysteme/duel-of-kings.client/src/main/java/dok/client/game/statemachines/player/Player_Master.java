package dok.client.game.statemachines.player;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.player.Player_Master.Player_Master_State;
import dok.client.services.GameUIService;
import dok.commons.ConsumableService;
import dok.commons.impl.CustomTimer.CustomTimerState;
import dok.commons.model.Lineup;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.GameLogicConstants;
import dok.game.logic.services.impl.GameStateModelProvider;
import dok.game.model.AbilityClass;
import dok.game.model.Action;
import dok.game.model.CharacterClass;
import dok.game.model.GameEvent;
import dok.game.model.GameResult;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;
import dok.game.model.TargetFilter;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import dok.game.services.GameLineupService;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerInteractionService;
import dok.game.services.PlayerInteractionWrapper;

/**
 * Main state machine for all game logic regarding a specific player.<br>
 * Manages the {@linkplain GameStateModel} and provides the
 * {@linkplain GameStateModelService} to modify it.<br>
 * By implementing {@linkplain PlayerInteractionService}, this
 * can be, directly or indirectly, called by the GameLogic to let the player
 * perform a certain task through this state machine.<br>
 * Event though this service's methods do have a player parameter,
 * this implementation ignores it and assumes that the caller wants
 * this state machine's {@linkplain #player} to perform the action.<br>
 * Start state: {@linkplain Player_Master_State#IDLE}.<br>
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class Player_Master extends StateMachineImpl<Player_Master_State> implements StateMachineCallback, GameLineupService, PlayerInteractionService, Consumer<Object> {

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    // States

    /**
     * Contains all possible states for the
     * {@linkplain Player_Master} state machine.
     *
     * @author Konstantin Schaper
     */
    public enum Player_Master_State {

        /**
         * The {@linkplain Player_Master}
         * state machine starts and remains in this
         * state until the game logic requests a transition.<br>
         * Under normal circumstances, the state machine
         * transitions back to this state at least indirectly
         * whenever one of the following sub state machines
         * finishes:<br>
         * - {@link #REACTION}<br>
         * - {@link #CHOOSE_CHARACTER}<br>
         * - {@link #PLACE_CHARACTERS}<br>
         * - {@link #ACTION}<br>
         */
        IDLE,

        /**
         * Substitute state for the {@linkplain Player_Reaction} sub state machine.
         */
        REACTION,

        /**
         * Substitute state for the {@linkplain Player_SelectTarget} sub state machine.
         */
        CHOOSE_CHARACTER,

        /**
         * Substitute state for the {@linkplain Player_PlaceCharacters} sub state machine.
         */
        PLACE_CHARACTERS,

        /**
         * Substitute state for the {@linkplain Player_Action} sub state machine.
         */
        ACTION,

        /**
         * When transitioned to from one of the following states:<br>
         * - {@link #REACTION}<br>
         * - {@link #CHOOSE_CHARACTER}<br>
         * - {@link #PLACE_CHARACTERS}<br>
         * - {@link #ACTION}<br>
         * the state machine publishes the information to the corresponding
         * {@linkplain GameLogicStateMachineService} and transitions back to
         * {@link #IDLE}.
         */
        NOTIFY_GL
    }

    // Constants

    /**
     * The service responsible for providing user input.
     */
    private final ConsumableService<Object> inputService;

    /**
     * The service responsible for providing information about
     * {@linkplain CharacterClass}es.
     */
    private final CharacterDataService characterClassService;

    /**
     * The service responsible for providing information about
     * {@linkplain AbilityClass}es.
     */
    private final AbilityDataService abilityClassService;

    /**
     * The service responsible for forwarding information to the
     * game logic.
     */
    private final PlayerInteractionWrapper playerInteractionWrapper;

    /**
     * The service responsible for updating the Game UI.
     */
    private final GameUIService gameUIService;

    /**
     * The player which this state machine represents.
     */
    private final UUID player;

    /**
     * The lineup of {@linkplain #player}.
     */
    private final Lineup lineup;

    // Attributes

    /**
     * Maintains and manipulates the information about the game
     * which this state machine and all sub state machines operate on.
     * It is very likely to be overridden at some point.
     */
    private GameStateModelService gameStateModelService;

    /**
     * The target filter used by any direct or indirect invocations of the
     * {@linkplain Player_SelectTarget} sub state machine.
     */
    private ITargetFilter targetFilter;

    /**
     * List of triggers which the player can choose from.<br>
     * May already only be a portion of the original list of triggers
     * and does only contain triggers with required player input.<br>
     * Currently only used by {@linkplain Player_Reaction}.
     */
    private List<Trigger> possibleReactions;

    /**
     * Set of Positions that may be chosen by {@linkplain Player_Master_State#CHOOSE_CHARACTER}.
     * Relevant during TimeMarkerPlacingPhase.
     */
    private Set<Position> availablePositions;

    /**
     * The result of the most recent finished sub state machine.<br>
     * Automatically injected by the occurring event.
     */
    private Object result;

    /**
     * The game events to which to react during
     * {@linkplain Player_Master_State#REACTION}.
     */
    private List<GameEvent> gameEvents;

    // Constructor(s)

    /**
     * TODO Add constructor description
     *
     * @param parent The callback to inform when this state machine has finished
     * @param player See {@link #player}
     * @param lineup See {@link #lineup}
     * @param gameStateModelService See {@link #gameStateModelService}. Only the initial model.
     * @param characterClassService See {@link #characterClassService}
     * @param abilityClassService See {@link #abilityClassService}
     * @param gameUIService See {@link #gameUIService}
     * @param targetService See {@link #targetServicer}
     * @param playerInteractionWrapper See {@link #playerInteractionWrapper}
     * @throws IllegalArgumentException If any argument is null
     */
    public Player_Master(
            StateMachineCallback parent,
            UUID player,
            Lineup lineup,
            CharacterDataService characterClassService,
            AbilityDataService abilityClassService,
            GameUIService gameUIService,
            ConsumableService<Object> inputService,
            PlayerInteractionWrapper playerInteractionWrapper
        ) throws IllegalArgumentException {
        // Calling super constructor
        super(parent, Player_Master_State.IDLE);

        // Validate parameters
        if (parent == null) throw new IllegalArgumentException("Parent must not be null");
        if (player == null) throw new IllegalArgumentException("Player must not be null");
        if (lineup == null) throw new IllegalArgumentException("Lineup must not be null");
        if (characterClassService == null) throw new IllegalArgumentException("CharacterClassService must not be null");
        if (abilityClassService == null) throw new IllegalArgumentException("AbilityClassService must not be null");
        if (gameUIService == null) throw new IllegalArgumentException("GameUIService must not be null");
        if (inputService == null) throw new IllegalArgumentException("TargetService must not be null");
        if (playerInteractionWrapper == null) throw new IllegalArgumentException("GameLogicStateMachineService must not be null");

        // Initialize variables with parameter values
        this.characterClassService = characterClassService;
        this.abilityClassService = abilityClassService;
        this.gameUIService = gameUIService;
        this.playerInteractionWrapper = playerInteractionWrapper;
        this.player = player;
        this.lineup = lineup;
        this.inputService = inputService;
        this.gameStateModelService = new GameStateModelProvider();

        // Register input handler
        inputService.addConsumer(this);

        //
        getGameUIService().setChooseTimer(getGameStateModelService().getChooseTimer());
        getGameUIService().setTurnTimer(getGameStateModelService().getTurnTimer());

        // Transition to start state
        transition(null, getStartState());
    }

    // <PlayerStateMachineService>

    @Override
    public void placeCharacters() throws IllegalStateException {
    	if (getCurrentState() == null) return;
        if (getCurrentState() != Player_Master_State.IDLE && getActiveChild() != null) getActiveChild().interrupt();
        transition(getCurrentState(), Player_Master_State.PLACE_CHARACTERS);
    }

    @Override
    public void performCharacterMarkerPlacement(ITargetFilter filter, Set<Position> availablePositions) throws IllegalStateException {
    	if (getCurrentState() == null) return;
    	if (getCurrentState() != Player_Master_State.IDLE && getActiveChild() != null) getActiveChild().interrupt();
        setTargetFilter(filter);
        setAvailablePositions(availablePositions);
        transition(getCurrentState(), Player_Master_State.CHOOSE_CHARACTER);
    }

    @Override
    public void performAction() throws IllegalStateException {
        logger.entry();
        if (getCurrentState() == null) return;
        if (getCurrentState() != Player_Master_State.IDLE && getActiveChild() != null) getActiveChild().interrupt();
        transition(getCurrentState(), Player_Master_State.ACTION);
        logger.exit();
    }

    @Override
    public void performReaction(List<Trigger> possibleReactions, List<GameEvent> gameEvents) throws IllegalStateException {
        logger.entry(possibleReactions, gameEvents);
        if (getCurrentState() == null) return;
        if (getCurrentState() != Player_Master_State.IDLE && getActiveChild() != null) getActiveChild().interrupt();
        setPossibleReactions(possibleReactions);
        setGameEvents(gameEvents);
        transition(getCurrentState(), Player_Master_State.REACTION);
        logger.exit();
    }

    @Override
    public void handleSwapRequest(SwapRequest swapRequest) {
    	if (getCurrentState() == null) return;
        logger.debug("Handling swap request '" + swapRequest + "'");
        getGameStateModelService().swap(null, swapRequest.getSource(), swapRequest.getTarget());
        getGameUIService().updateModelDisplay(getGameStateModelService());
    }

    @Override
    public void handleGameResult(GameResult result) {
        logger.entry(result);
        if (getCurrentState() == null) return;
        getInputService().removeConsumer(this);
        finish(result);
        logger.exit();
    }

    @Override
    public synchronized void handleModelUpdate(GameStateModel newModel) {
        logger.entry(newModel);
        if (getCurrentState() == null) return;

        // Remember system time
        long tmpTime = System.nanoTime();

        // Override the model
        getGameStateModelService().overrideWith(newModel);

        // Log time needed to override model
        logger.debug("Time to override model: " + TimeUnit.MILLISECONDS.convert(System.nanoTime() - tmpTime, TimeUnit.NANOSECONDS) + "ms");

		// Remember Timer States
		CustomTimerState turnTimerState = newModel.getTurnTimer().getState();
		CustomTimerState chooseTimerState = newModel.getChooseTimer().getState();

		// Pause Timers
		if (turnTimerState == CustomTimerState.RUNNING) getGameStateModelService().getTurnTimer().pause();
		if (chooseTimerState == CustomTimerState.RUNNING) getGameStateModelService().getChooseTimer().pause();

        // Update the display and play animations
        getGameUIService().updateModelDisplay(newModel);

        // Log time needed to update display
        logger.debug("Time to update model display: " + TimeUnit.MILLISECONDS.convert(System.nanoTime() - tmpTime, TimeUnit.NANOSECONDS) + "ms");

		// Resume Timers
		if (turnTimerState == CustomTimerState.RUNNING) getGameStateModelService().getTurnTimer().resume();
		if (chooseTimerState == CustomTimerState.RUNNING) getGameStateModelService().getChooseTimer().resume();

        // Notify GameLogic that the work has been done
        getPlayerInteractionWrapper().handleWorkDone();
        logger.exit();
    }

    // </PlayerStateMachineService>

    // <GameSetupService>

    /**
     * @return {@link #lineup} if the requested player
     * equals this state machine's {@link #player}, else null
     */
    @Override
    public final Lineup getLineup(UUID player) {
        return getPlayer().equals(player) ? getLineup() : null;
    }

    // </GameSetupService>

    // Events

    @Override
    public void accept(Object input) {
        logger.entry(input);
        if (input != null && input instanceof Integer && (int)input == GameLogicConstants.SURRENDER_CODE) {
            logger.debug("Submitting surrender request...");
            getPlayerInteractionWrapper().handleSurrenderRequest();
        }
        logger.exit();
    }

    @Override
    public void onFinish(Object result) {
        if (!(getCurrentState() == null || getCurrentState() == Player_Master_State.IDLE)) {
            // Reset Variables
            setActiveChild(null);

            // Tell GL about the result
            setResult(result);
            transition(Player_Master_State.NOTIFY_GL);
        }
    }

    @Override
    protected void onEntry(Player_Master_State state) {
        switch (state) {
            case CHOOSE_CHARACTER:

                try {

                    logger.info("AvailablePositions: " + getAvailablePositions());
                    if (getTargetFilter().equals(TargetFilter.OWN)) {
                    	getGameUIService().showTaskMessage("Game.Task.ChooseOwnCharacter", null);
                    } else if (getTargetFilter().equals(TargetFilter.HOSTILE)) {
                    	getGameUIService().showTaskMessage("Game.Task.ChooseHostileCharacter", null);
                    }


                    final ITargetFilter finalFilter = getTargetFilter();
                    @SuppressWarnings("serial")
					ITargetFilter isAvailableFilter = new ITargetFilter() {

                        @Override
                        public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target,
                                int targetCount, GameStateModel model, CharacterDataService characterClassService,
                                AbilityDataService abilityClassService)
                        {
                            for (Position pos : getAvailablePositions()) if (pos.equals(target))
                            	return finalFilter.test(chooser, sourceAbilityClassId, source, target, targetCount, model, characterClassService, abilityClassService);
                            return false;
                        }

                    };

                    // Start the sub state machine
                    setActiveChild(new Player_SelectTarget(this, getGameUIService(), getGameStateModelService(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPlayer(), null, null, isAvailableFilter, getGameStateModelService().getChooseTimer()));

                } catch (Exception e) {

                    logger.log(Level.FATAL, "Sub state machine " + Player_SelectTarget.class.getName() + " could not be created.", e);
                    transition(Player_Master_State.IDLE);

                }

                break;

            case ACTION:

                try {

                    // Start the sub state machine
                    setActiveChild(new Player_Action(this, getPlayer(), getInputService(), getGameUIService(), getGameStateModelService(), getCharacterClassService(), getAbilityClassService(), getGameStateModelService().getTurnTimer()));

                } catch (Exception e) {

                    logger.log(Level.FATAL, "Sub state machine " + Player_Action.class.getName() + " could not be created.", e);
                    transition(Player_Master_State.IDLE);

                }

                break;

            case PLACE_CHARACTERS:

                try {

                    // Start the sub state machine
                    setActiveChild(new Player_PlaceCharacters(this, getPlayer(), getInputService(), getPlayerInteractionWrapper(), getGameStateModelService(), getCharacterClassService(), getAbilityClassService(), getGameUIService(), getGameStateModelService().getChooseTimer()));

                } catch (Exception e) {

                    logger.log(Level.FATAL, "Sub state machine " + Player_PlaceCharacters.class.getName() + " could not be created.", e);
                    transition(Player_Master_State.IDLE);

                }

                break;

            case REACTION:

                try {

                    // Initialize the timer
                    getGameUIService().setChooseTimer(getGameStateModelService().getChooseTimer());

                    // Start the sub state machine
                    setActiveChild(new Player_Reaction(this, getPlayer(), getGameUIService(), getGameStateModelService(), getPlayerInteractionWrapper(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPossibleReactions(), getGameStateModelService().getChooseTimer()));

                } catch (Exception e) {

                    logger.log(Level.FATAL, "Sub state machine " + Player_Reaction.class.getName() + " could not be created.", e);
                    transition(Player_Master_State.IDLE);

                }

                break;

            case NOTIFY_GL:

                if (getPreviousState() == Player_Master_State.CHOOSE_CHARACTER) {

                    if (getResult() != null && getResult() instanceof Position) {

                        getPlayerInteractionWrapper().handleCharacterChoosen((Position) getResult());

                    } else getPlayerInteractionWrapper().handleCharacterChoosen(null);

                } else if (getPreviousState() == Player_Master_State.PLACE_CHARACTERS) {

                    getPlayerInteractionWrapper().handleSwapRequest(null);

                } else if (getPreviousState() == Player_Master_State.REACTION) {

                    if (getResult() != null && getResult() instanceof Reaction) getPlayerInteractionWrapper().handleReaction((Reaction) getResult());
                    else getPlayerInteractionWrapper().handleReaction(null);

                } else if (getPreviousState() == Player_Master_State.ACTION) {

                     if (getResult() != null && getResult() instanceof Action) getPlayerInteractionWrapper().handleAction((Action) getResult());
                     else getPlayerInteractionWrapper().handleAction(null);

                }

                transition(Player_Master_State.IDLE);
                break;

            case IDLE:

                getGameUIService().showTaskMessage("Game.Message.OpponentIsChoosing", null);
                break;
        }
    }

    @Override
    protected void onExit(Player_Master_State state) {
    	// Do nothing
    }

    // Getters & Setters

    public final ITargetFilter getTargetFilter() {
        return targetFilter;
    }

    public final void setTargetFilter(ITargetFilter targetFilter) {
        this.targetFilter = targetFilter;
    }

    protected final ConsumableService<Object> getInputService() {
        return inputService;
    }

    protected final CharacterDataService getCharacterClassService() {
        return characterClassService;
    }

    protected final AbilityDataService getAbilityClassService() {
        return abilityClassService;
    }

    protected final PlayerInteractionWrapper getPlayerInteractionWrapper() {
        return playerInteractionWrapper;
    }

    protected final GameUIService getGameUIService() {
        return gameUIService;
    }

    protected final UUID getPlayer() {
        return player;
    }

    protected final Lineup getSetup() {
        return getLineup();
    }

    protected final List<Trigger> getPossibleReactions() {
        return possibleReactions;
    }

    protected final void setPossibleReactions(List<Trigger> possibleReactions) {
        this.possibleReactions = possibleReactions;
    }

    protected final Object getResult() {
        return result;
    }

    protected final void setResult(Object result) {
        this.result = result;
    }

    protected final GameStateModelService getGameStateModelService() {
        return gameStateModelService;
    }

    protected final void setGameStateModelService(GameStateModelService gameStateModelService) {
        this.gameStateModelService = gameStateModelService;
    }

    public Lineup getLineup() {
        return lineup;
    }

    public Set<Position> getAvailablePositions() {
        return availablePositions;
    }

    public void setAvailablePositions(Set<Position> availablePositions) {
        this.availablePositions = availablePositions;
    }

    public List<GameEvent> getGameEvents() {
        return gameEvents;
    }

    public void setGameEvents(List<GameEvent> gameEvents) {
        this.gameEvents = gameEvents;
    }

}
