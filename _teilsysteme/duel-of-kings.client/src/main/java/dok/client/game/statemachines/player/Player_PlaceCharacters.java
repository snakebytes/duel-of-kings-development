package dok.client.game.statemachines.player;

import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.player.Player_PlaceCharacters.Player_PlaceCharacters_State;
import dok.client.services.GameUIService;
import dok.commons.ConsumableService;
import dok.commons.impl.CustomTimer;
import dok.commons.impl.CustomTimer.CustomTimerCallback;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.SwapRequest;
import dok.game.model.TargetFilter;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.PlayerInteractionWrapper;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public final class Player_PlaceCharacters extends StateMachineImpl<Player_PlaceCharacters_State> implements StateMachineCallback {

	// Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum Player_PlaceCharacters_State {

		PICK_START_CHARACTER,
		PICK_END_CHARACTER,
		CHANGE_POSITIONS

	}

	// Constants

	private final UUID player;

	// Services

	private final ConsumableService<Object> inputService;
	private final CharacterDataService characterClassService;
	private final AbilityDataService abilityClassService;
	private final PlayerInteractionWrapper playerInteractionWrapper;
	private final GameStateModel gameStateModel;
	private final CustomTimer timer;
	private final GameUIService gameUIService;
	private final CustomTimerCallback timerCallback = this::onTimerExpired;

	// Attributes

	private Position tmpSource;
	private Position tmpTarget;

	// Constructor(s)

	public Player_PlaceCharacters(StateMachineCallback parent, UUID player, ConsumableService<Object> inputService, PlayerInteractionWrapper playerInteractionWrapper, GameStateModel gameStateModel, CharacterDataService characterClassService, AbilityDataService abilityClassService, GameUIService gameUIService, CustomTimer timer) throws IllegalArgumentException {
		super(parent, Player_PlaceCharacters_State.PICK_START_CHARACTER);
		this.player = player;
		this.inputService = inputService;
		this.characterClassService = characterClassService;
		this.abilityClassService = abilityClassService;
		this.playerInteractionWrapper = playerInteractionWrapper;
		this.gameStateModel = gameStateModel;
		this.gameUIService = gameUIService;
		this.timer = timer;
		timer.addCallback(getTimerCallback());
		transition(null, getStartState());
	}

	// Events

	public void onTimerExpired(CustomTimer timer) {
		timer.removeCallback(getTimerCallback());
		finish(null);
	}

    @Override
    public void interrupt() {
    	super.interrupt();
    	if (getCurrentState() != null) {
    		getGameStateModel().getChooseTimer().stop();
    		getGameStateModel().getTurnTimer().stop();
    	}
    }

	@Override
	public void onFinish(Object result) {
		setActiveChild(null);
		if (result == null) {

			try {
				finish(null);
			} catch (IllegalStateException e) {
				logger.catching(e);
			}

		} if (getCurrentState() == Player_PlaceCharacters_State.PICK_START_CHARACTER) {

			if (result != null && result instanceof Position) {

				setTmpSource((Position) result);
				transition(getCurrentState(), Player_PlaceCharacters_State.PICK_END_CHARACTER);

			} else transition(Player_PlaceCharacters_State.PICK_START_CHARACTER);

		} else if (getCurrentState() == Player_PlaceCharacters_State.PICK_END_CHARACTER) {

			if (result != null && result instanceof Position) {

				setTmpTarget((Position) result);
				transition(getCurrentState(), Player_PlaceCharacters_State.CHANGE_POSITIONS);

			} else transition(Player_PlaceCharacters_State.PICK_START_CHARACTER);
		}

	}

	@Override
	protected final void onEntry(Player_PlaceCharacters_State state) {
		switch (state) {
			case CHANGE_POSITIONS:

				if (getTmpTarget() != null && getTmpSource() != null) {
					getPlayerInteractionWrapper().handleSwapRequest(new SwapRequest(getTmpSource(), getTmpTarget()));
					setTmpTarget(null);
					setTmpSource(null);
				} else getLogger().warn("Cannot swap positions. Two characters are needed.");
				transition(getCurrentState(), Player_PlaceCharacters_State.PICK_START_CHARACTER);
				break;

			case PICK_START_CHARACTER:

				getGameUIService().showTaskMessage("Game.Task.PlaceCharacters", null);
				setTmpSource(null);
				setActiveChild(new Player_SelectTarget(this, getGameUIService(), getGameStateModel(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPlayer(), null, null, TargetFilter.OWN, getTimer()));
				break;

			case PICK_END_CHARACTER:

				setTmpTarget(null);
				setActiveChild(new Player_SelectTarget(this, getGameUIService(), getGameStateModel(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPlayer(), null, getTmpSource(), TargetFilter.and(TargetFilter.OWN, TargetFilter.OTHER), getTimer()));
				break;
		}
	}

	@Override
	protected final void onExit(Player_PlaceCharacters_State state) {}

	// Getters & Setters

	protected final UUID getPlayer() {
		return player;
	}

	protected final Position getTmpTarget() {
		return tmpTarget;
	}

	protected final void setTmpTarget(Position tmpTarget) {
		this.tmpTarget = tmpTarget;
	}

	protected final Position getTmpSource() {
		return tmpSource;
	}

	protected final void setTmpSource(Position tmpSource) {
		this.tmpSource = tmpSource;
	}

	private static final Logger getLogger() {
		return logger;
	}

	public CustomTimer getTimer() {
		return timer;
	}

	protected final ConsumableService<Object> getInputService() {
		return inputService;
	}

	protected final CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	protected final AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	protected final PlayerInteractionWrapper getPlayerInteractionWrapper() {
		return playerInteractionWrapper;
	}

	public GameStateModel getGameStateModel() {
		return gameStateModel;
	}

	public GameUIService getGameUIService() {
		return gameUIService;
	}

	public CustomTimerCallback getTimerCallback() {
		return timerCallback;
	}

}
