package dok.client.game.statemachines.player;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.game.statemachines.player.Player_Reaction.Player_Reaction_State;
import dok.client.services.GameUIService;
import dok.commons.ConsumableService;
import dok.commons.impl.CustomTimer;
import dok.commons.impl.CustomTimer.CustomTimerCallback;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.PlayerInteractionWrapper;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public final class Player_Reaction extends StateMachineImpl<Player_Reaction_State> implements StateMachineCallback, Consumer<Object> {

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    // States

    public enum Player_Reaction_State {

        INIT,
        SHOW_REACTIONS,
        CHOOSE_TARGET,
        NOTIFY_GL

    }

    // Constants

    private final UUID player;
    private final List<Trigger> possibleReactions;
    private final CustomTimerCallback timerCallback = this::onTimerExpired;
    private final CustomTimer timer;
    private final GameStateModel model;

    // Services

    private final GameUIService gameUIService;
    private final AbilityDataService abilityClassService;
    private final ConsumableService<Object> inputService;
    private final CharacterDataService characterClassService;
    private final PlayerInteractionWrapper playerInteractionWrapper;

    // Attributes

    private Position reactionTargetPosition;
    private Trigger reactionTrigger;

    // Constructor(s)

    public Player_Reaction(StateMachineCallback parent, UUID player, GameUIService gameUIService, GameStateModel model, PlayerInteractionWrapper playerInteractionWrapper, CharacterDataService characterClassService, AbilityDataService abilityClassService, ConsumableService<Object> inputService, List<Trigger> list, CustomTimer timer) throws IllegalArgumentException {
        super(parent, Player_Reaction_State.INIT);
        logger.entry(parent,player,gameUIService,model,playerInteractionWrapper,characterClassService,abilityClassService, inputService, list, timer);
        this.gameUIService = gameUIService;
        this.abilityClassService = abilityClassService;
        this.inputService = inputService;
        this.model = model;
        this.characterClassService = characterClassService;
        this.player = player;
        this.possibleReactions = list;
        this.playerInteractionWrapper = playerInteractionWrapper;
        this.timer = timer;
        timer.addCallback(getTimerCallback());
        transition(null, getStartState());
        logger.exit(this);
    }

    // Methods

    private final void onTimerExpired(CustomTimer timer) {
    	logger.entry(timer);
        if (getCurrentState() == Player_Reaction_State.SHOW_REACTIONS || getCurrentState() == Player_Reaction_State.CHOOSE_TARGET) {
            timer.removeCallback(getTimerCallback());
            finish(null);
        }
        logger.exit();
    }

    @Override
    public void interrupt() {
    	super.interrupt();
    	if (getCurrentState() != null) {
    		getModel().getChooseTimer().stop();
    		getModel().getTurnTimer().stop();
    	}
    }

    @Override
    public void accept(Object input) {
    	logger.entry(input);
        if (getCurrentState() == Player_Reaction_State.SHOW_REACTIONS && input != null && input instanceof Trigger) {
            setReactionTrigger((Trigger) input);
            getPossibleReactions().remove(input);
            transition(getCurrentState(), Player_Reaction_State.CHOOSE_TARGET);
        } else if (input == null) finish(null);
        logger.exit();
    }

    // Events

    @Override
    public void onFinish(Object result) {
    	logger.entry(result);
        setActiveChild(null);
        if (getCurrentState() == Player_Reaction_State.CHOOSE_TARGET) {
            if (result != null && result instanceof Position) {
                setReactionTargetPosition((Position)result);
                transition(Player_Reaction_State.NOTIFY_GL);
            } else if (result != null && result instanceof Boolean && !(boolean)result) {
                getPossibleReactions().add(getReactionTrigger());
                transition(Player_Reaction_State.SHOW_REACTIONS);
            } else
                transition(Player_Reaction_State.CHOOSE_TARGET);
        }
        logger.exit();
    }

    @Override
    protected void onEntry(Player_Reaction_State state) {
    	logger.entry(state);
        switch (state) {
            case INIT:

                transition(Player_Reaction_State.SHOW_REACTIONS);
                break;

            case SHOW_REACTIONS:

                if (getTimer().isExpired()) finish(null);
                else {
                    getInputService().addConsumer(this);
                    getGameUIService().showTaskMessage("Game.Task.ChooseReaction", null);
                    getGameUIService().showReactions(getPossibleReactions());
                }
                break;

            case CHOOSE_TARGET:

                getGameUIService().showTaskMessage("Game.Task.ChooseReactionTarget", null);
                if (getAbilityClassService().getTriggerClass(getReactionTrigger().getTriggerClassId()).getTargetFilter() == null)
                    transition(Player_Reaction_State.NOTIFY_GL);
                else
                    setActiveChild(new Player_SelectTarget(this, getGameUIService(), getModel(), getCharacterClassService(), getAbilityClassService(), getInputService(), getPlayer(), getReactionTrigger().getSourceAbility(), getReactionTargetPosition(), getAbilityClassService().getTriggerClass(getReactionTrigger().getTriggerClassId()).getTargetFilter(), getTimer()));
                break;

            case NOTIFY_GL:

                getPlayerInteractionWrapper().handleReaction(new Reaction(getReactionTrigger(), getReactionTargetPosition()));

                if (getPossibleReactions().size() == 0) {
                	finish(null);
                } else {
                	transition(Player_Reaction_State.SHOW_REACTIONS);
                }
                break;

        }

        logger.exit();
    }

    @Override
    protected void onExit(Player_Reaction_State state) {
        if (state == Player_Reaction_State.SHOW_REACTIONS) {
            getInputService().removeConsumer(this);
            getGameUIService().hideReactions();
        }
    }

    // Getter & Setter

    public Position getReactionTargetPosition() {
        return reactionTargetPosition;
    }

    public void setReactionTargetPosition(Position reactionTargetPosition) {
        this.reactionTargetPosition = reactionTargetPosition;
    }

    public GameUIService getGameUIService() {
        return gameUIService;
    }

    public GameStateModel getModel() {
        return model;
    }

    public CharacterDataService getCharacterClassService() {
        return characterClassService;
    }

    public AbilityDataService getAbilityClassService() {
        return abilityClassService;
    }

    public ConsumableService<Object> getInputService() {
        return inputService;
    }

    public UUID getPlayer() {
        return player;
    }

    public List<Trigger> getPossibleReactions() {
        return possibleReactions;
    }

    public PlayerInteractionWrapper getPlayerInteractionWrapper() {
        return playerInteractionWrapper;
    }

    public CustomTimerCallback getTimerCallback() {
        return timerCallback;
    }

    public CustomTimer getTimer() {
        return timer;
    }

    public Trigger getReactionTrigger() {
        return reactionTrigger;
    }

    public void setReactionTrigger(Trigger reactionTrigger) {
        this.reactionTrigger = reactionTrigger;
    }

}
