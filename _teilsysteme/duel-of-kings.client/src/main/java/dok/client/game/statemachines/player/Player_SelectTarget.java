package dok.client.game.statemachines.player;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Logger;

import dok.client.game.statemachines.player.Player_SelectTarget.Player_SelectTarget_State;
import dok.client.services.GameUIService;
import dok.commons.ConsumableService;
import dok.commons.impl.CustomTimer;
import dok.commons.impl.CustomTimer.CustomTimerCallback;
import dok.commons.model.constants.ModelConstants;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.model.Ability;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;

/**
 * Handles the task of the player performing a target selection.<br>
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public final class Player_SelectTarget extends StateMachineImpl<Player_SelectTarget_State> {

	// Class Constants

	private static final Logger logger = Logger.getLogger(Player_SelectTarget.class.getName());

	// States

	public enum Player_SelectTarget_State {

		CHOOSE_CHARACTER,
		ACCEPT

	}

	// Services

	private final GameUIService gameUIService;

	// Constants

	private final UUID player;
	private final GameStateModel model;
	private final ITargetFilter filter;
	private final ConsumableService<Object> inputService;
	private final Ability sourceAbility;
	private final Position source;
	private final CharacterDataService characterClassService;
	private final AbilityDataService abilityClassService;
	private final Consumer<Object> positionConsumer = this::handle;
	private final CustomTimerCallback timerCallback = this::onTimerExpired;

	// Attributes

	private Position target;

	// Constructor(s)

	public Player_SelectTarget(StateMachineCallback parent, GameUIService gameUIService, GameStateModel model, CharacterDataService characterClassService, AbilityDataService abilityClassService, ConsumableService<Object> inputService, UUID player, Ability sourceAbility, Position source, ITargetFilter filter, CustomTimer timer) throws IllegalArgumentException {
		super(parent, Player_SelectTarget_State.CHOOSE_CHARACTER);
		this.gameUIService = gameUIService;
		this.player = player;
		this.filter = filter;
		this.source = source;
		this.inputService = inputService;
		this.sourceAbility = sourceAbility;
		this.model = model;
		this.characterClassService = characterClassService;
		this.abilityClassService = abilityClassService;
		timer.addCallback(getTimerCallback());
		transition(null, getStartState());
	}

	// Methods

	private final void onTimerExpired(CustomTimer timer) {
		if (getCurrentState() == Player_SelectTarget_State.ACCEPT || getCurrentState() == Player_SelectTarget_State.CHOOSE_CHARACTER) {
			timer.removeCallback(getTimerCallback());
			finish(null);
		}
	}

	@Override
	public Object handle(Object input) {
		if (getCurrentState() == Player_SelectTarget_State.CHOOSE_CHARACTER) {
			if (input != null && input instanceof Position) {
				setTarget((Position) input);
				transition(getCurrentState(), Player_SelectTarget_State.ACCEPT);
			} else finish(input);
		} else logger.info("not in ChoseCharacterState.CHOOSE_CHARACTER state");
		return super.handle(input);
	}

	// Events

    @Override
    public void interrupt() {
    	super.interrupt();
    	if (getCurrentState() != null) {
    		getModel().getChooseTimer().stop();
    		getModel().getTurnTimer().stop();
    	}
    }

	@Override
	protected void onEntry(Player_SelectTarget_State state) {
		if (state == Player_SelectTarget_State.CHOOSE_CHARACTER) {

			setTarget(null);
			getInputService().addConsumer(getPositionConsumer());
			getGameUIService().enableSelectionHighlighting(getPlayer(), getSourceAbility(), getSource(), getModel(), getFilter());
			if (getSource() != null) getGameUIService().enableSourceHighlightning(getSource());

		} else if (state == Player_SelectTarget_State.ACCEPT) {

			if (getFilter().test(getPlayer(), getSourceAbility() == null ? ModelConstants.UNDEFINED_ID : getSourceAbility().getAbilityClassId(), getSource(), getTarget(), 0, getModel(), getCharacterClassService(), getAbilityClassService()))

				finish(getTarget());

			else

				transition(getCurrentState(), Player_SelectTarget_State.CHOOSE_CHARACTER);

		}
	}

	@Override
	protected void onExit(Player_SelectTarget_State state) {
		if (state == Player_SelectTarget_State.CHOOSE_CHARACTER) {
			getInputService().removeConsumer(getPositionConsumer());
			getGameUIService().disableSelectionHighlighting();
			getGameUIService().disableSourceHighlightning();
		}
	}

	// Getter & Setter

	public Consumer<Object> getPositionConsumer() {
		return positionConsumer;
	}

	public ConsumableService<Object> getInputService() {
		return inputService;
	}

	public Position getTarget() {
		return target;
	}

	public void setTarget(Position target) {
		this.target = target;
	}

	public ITargetFilter getFilter() {
		return filter;
	}

	public UUID getPlayer() {
		return player;
	}

	public Position getSource() {
		return source;
	}

	public CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	public AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	public GameStateModel getModel() {
		return model;
	}

	public CustomTimerCallback getTimerCallback() {
		return timerCallback;
	}

	public GameUIService getGameUIService() {
		return gameUIService;
	}

	public Ability getSourceAbility() {
		return sourceAbility;
	}

}
