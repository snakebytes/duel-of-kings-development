package dok.client.impl;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import dok.commons.network.core.NetworkConstants;

public class ClientApplicationUtilities {

	public static final void initializeSSL() throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException, KeyManagementException {

		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		ks.load(System.class.getResourceAsStream("/ssl/server.jks"), NetworkConstants.KEY_STORE_PASSWORD.toCharArray());

		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(ks);

		SSLContext sslcontext = SSLContext.getInstance("TLS");
		sslcontext.init(null, trustManagerFactory.getTrustManagers(), null);
		SSLContext.setDefault(sslcontext);

		System.setProperty("https.protocols", "TLSv1");
	}

}
