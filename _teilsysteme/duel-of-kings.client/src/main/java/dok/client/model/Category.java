package dok.client.model;

import javafx.scene.image.Image;

/**
 * Basic model for one type of configurable property of a CharacterClass.
 *
 * @author Steffen
 */
public abstract class Category implements Comparable<Category> {

	// Attributes

	/**
	 * An image by which the category will be displayed. Name is not saved in the model, but fetched dynamically from localization files.
	 */
	private Image icon = null;

	// Constructors

	public Category() {
		// do nothing
	}

	public Category(Image img) {
		this.icon = img;
	}

	// Methods

	public int compareTo(Category other) {
		if(this instanceof EnhancementSlotCategory) {
			if(other instanceof EnhancementSlotCategory) {
				return ((EnhancementSlotCategory) this).getSlot().compareTo((((EnhancementSlotCategory) other).getSlot()));
			} else if(other instanceof CharacterClassCategory) return 1;
			else return 0;
		} else if(this instanceof CharacterClassCategory) {
			if(other instanceof EnhancementSlotCategory) return -1;
			else return 0;
		} else return 0;
	}

	// Getters & Setters

	public Image getIcon() {
		return icon;
	}

	public void setIcon(Image icon) {
		this.icon = icon;
	}
}
