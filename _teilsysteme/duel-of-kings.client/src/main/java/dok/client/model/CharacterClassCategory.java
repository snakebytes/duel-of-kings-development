package dok.client.model;

import javafx.scene.image.Image;

/**
 * Subclass of {@link Category} for a displaying available CharacterClasses. Those classes are fetched by the View, so no additional data is required in this class.
 * @author Steffen
 */
public class CharacterClassCategory extends Category {

	public CharacterClassCategory(Image img) {
		super(img);
	}
}
