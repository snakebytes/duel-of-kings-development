package dok.client.model;

import dok.commons.model.EnhancementSlot;
import javafx.scene.image.Image;

/**
 * Subclass of {@link Category} for a single {@link EnhancementSlot}.
 * @author Steffen
 */
public class EnhancementSlotCategory extends Category {

	// Attributes

	/**
	 * The slot represented by this category.
	 */
	private EnhancementSlot slot;

	// Contructors

	public EnhancementSlotCategory(EnhancementSlot slot, Image icon) {
		super(icon);
		this.setSlot(slot);
	}

	// Getters & Setters

	public EnhancementSlot getSlot() {
		return slot;
	}

	public void setSlot(EnhancementSlot slot) {
		this.slot = slot;
	}
}
