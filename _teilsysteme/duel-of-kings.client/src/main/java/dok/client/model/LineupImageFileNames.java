package dok.client.model;

/**
 * Contains all valid imageKey-strings (filenames without extension) for images that act as portraits for Lineups.
 *
 * @author Steffen
 *
 */

public enum LineupImageFileNames {
    OFFENSIVE("Offensive"),
    DEFENSIVE("Defensive"),
    BALANCED("Balanced");

	// Attributes

    private final String imageKey;

    // Constructors

    private LineupImageFileNames(String key) {
        this.imageKey = key;
    }

    // Methods

    public static LineupImageFileNames getByString(String string) {
    	for(LineupImageFileNames name : LineupImageFileNames.values()) {
    		if(name.getImageKey() == string) return name;
    	}
    	return null;
    }

    // Getters & Setters

    public String getImageKey() {
    	return imageKey;
    }
}
