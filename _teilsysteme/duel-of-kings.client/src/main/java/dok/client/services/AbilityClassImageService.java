package dok.client.services;

import javafx.scene.image.Image;

public interface AbilityClassImageService {

	public Image getAbilityImage(int abilityClassId);
	public void preloadAbilityImage(int abilityClassId);

}
