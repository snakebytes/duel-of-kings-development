package dok.client.services;

import javafx.scene.image.Image;

public interface ApplicationImageService extends LineupImageService {

	public Image getImage(String key);

}
