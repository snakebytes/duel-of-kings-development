package dok.client.services;

import dok.game.model.Buff;
import javafx.scene.image.Image;

public interface BuffImageService {

	public Image getBuffImage(Class<? extends Buff> buffClass);
	public void preloadBuffImage(Class<? extends Buff> buffClass);

}
