package dok.client.services;

import javafx.scene.image.Image;

public interface CharacterClassImageService {

	public Image getCharacterClassImage(int characterClassId);
	public void preloadCharacterClassImage(int characterClassId);

}
