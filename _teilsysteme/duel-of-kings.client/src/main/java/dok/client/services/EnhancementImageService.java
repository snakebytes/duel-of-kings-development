package dok.client.services;

import dok.commons.model.EnhancementType;
import javafx.scene.image.Image;

public interface EnhancementImageService {

	public Image getEnhancementSlotImage(EnhancementType type);
	public Image getEnhancementImage(Integer enhancementId);

}
