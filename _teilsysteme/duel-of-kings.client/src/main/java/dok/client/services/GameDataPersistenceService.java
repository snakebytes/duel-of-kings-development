package dok.client.services;

import dok.commons.impl.ObservableServiceRegistry.RegisterableService;
import dok.commons.services.HibernateDataPersistenceService;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;

public interface GameDataPersistenceService extends CharacterDataService, AbilityDataService, RegisterableService, HibernateDataPersistenceService {

}
