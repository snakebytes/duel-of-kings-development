package dok.client.services;

import javafx.scene.image.Image;

public interface GameImageService extends BuffImageService, TriggerClassImageService, CharacterClassImageService, AbilityClassImageService, LineupImageService, EnhancementImageService, LineupEditorImageService {

	public Image getIcon(String key);
	public void preloadIcon(String key);
	public void preloadGameGraphics();

}
