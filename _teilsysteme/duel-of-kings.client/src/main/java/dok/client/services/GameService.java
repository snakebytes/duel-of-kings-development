package dok.client.services;

import java.util.List;

import dok.client.game.ClientGame;
import dok.commons.model.Lineup;
import dok.game.model.AILevel;
import dok.game.model.Player;

public interface GameService {

	public ClientGame getActiveGame();
	public void startDuelGame(List<Player> players, Player player);
	public void startBotGame(AILevel aiLevel, List<Lineup> lineups);

}
