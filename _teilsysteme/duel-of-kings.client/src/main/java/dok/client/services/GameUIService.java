package dok.client.services;

import java.util.List;
import java.util.UUID;

import dok.commons.impl.CustomTimer;
import dok.game.model.Ability;
import dok.game.model.Character;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.Trigger;
import dok.game.model.api.ITargetFilter;

public interface GameUIService {

	public void showTaskMessage(String localizedMessageKey, Object[] parameters);
	public void updateModelDisplay(GameStateModel newModel);

	// Action

	public void showCharacterAbilities(Character character);
	public void hideCharacterAbilityDisplay();

	// Reaction

	public void showReactions(List<Trigger> list);
	public void hideReactions();

	// Highlighting

	public void enableSelectionHighlighting(UUID chooser, Ability ability, Position source, GameStateModel model, ITargetFilter targetFilter);
	public void disableSelectionHighlighting();
	public void enableSourceHighlightning(Position pos);
	public void enableTargetHighlightning(Position pos);
	public void disableSourceHighlightning();
	public void disableTargetHighlightning();

	// Timers

	public void setTurnTimer(CustomTimer timer);
	public void setChooseTimer(CustomTimer timer);

}
