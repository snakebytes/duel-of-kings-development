package dok.client.services;

import javafx.scene.image.Image;

public interface LineupEditorImageService {

	public Image getLineupEditorIcon(String name);
}
