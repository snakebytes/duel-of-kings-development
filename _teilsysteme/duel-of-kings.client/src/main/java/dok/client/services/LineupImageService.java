package dok.client.services;

import javafx.scene.image.Image;

public interface LineupImageService {

	public Image getLineupImage(String key);

}
