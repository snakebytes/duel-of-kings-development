package dok.client.services;

import java.util.List;

import dok.client.services.LineupService.LineupServiceListener;
import dok.commons.impl.ObservableServiceRegistry.ObservableRegisterableService;
import dok.commons.model.Lineup;
import dok.commons.model.StatusCode;

public interface LineupService extends ObservableRegisterableService<LineupServiceListener> {

	public interface LineupServiceListener {

		public void onLineupSaved(Lineup lineup);
		public void onLineupsUpdated(List<Lineup> lineups);
		public default void onLineupSaveError(StatusCode errorCode){ /* Do nothing */ }

	}

	public List<Lineup> getLineups();
	public boolean saveLineup(Lineup lineup);

}
