package dok.client.services;

import dok.client.services.LoginService.LoginServiceListener;
import dok.commons.impl.ObservableServiceRegistry.ObservableRegisterableService;
import dok.commons.model.LoginSession;

public interface LoginService extends ObservableRegisterableService<LoginServiceListener> {

	public interface LoginServiceListener {
		public void onSuccessfulLogin(LoginSession loginSession);
	}

	public LoginSession getActiveSession();
	public String getActiveUserName();
	public void attemptLogin(String username, String password);

}
