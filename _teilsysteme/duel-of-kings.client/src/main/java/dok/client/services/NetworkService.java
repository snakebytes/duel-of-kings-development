package dok.client.services;

import dok.client.services.NetworkService.NetworkServiceListener;
import dok.commons.impl.ObservableServiceRegistry.ObservableRegisterableService;
import dok.commons.network.api.ObservableConnection;

public interface NetworkService extends ObservableRegisterableService<NetworkServiceListener> {

	public interface NetworkServiceListener {
		// TODO
	}

	public ObservableConnection getActiveConnection();

}
