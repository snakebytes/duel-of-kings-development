package dok.client.services;

import java.util.prefs.Preferences;

import dok.client.services.PreferencesService.PreferencesServiceListener;
import dok.commons.impl.ObservableServiceRegistry.ObservableRegisterableService;

public interface PreferencesService extends ObservableRegisterableService<PreferencesServiceListener> {

	public interface PreferencesServiceListener {
		// TODO
	}

	public Preferences getPreferences();

}
