package dok.client.services;

import java.time.Duration;

import dok.client.services.QueuingService.QueuingListener;
import dok.commons.impl.ObservableServiceRegistry.ObservableRegisterableService;
import dok.commons.network.api.ConnectionListener;
import dok.game.model.GameType;
import dok.game.network.model.QueuingPacket.QueuingResult;

public interface QueuingService extends ConnectionListener, ObservableRegisterableService<QueuingListener> {

	public interface QueuingListener {

		public void handleQueuingResult(QueuingResult result);
		public void handleQueueStatusChange(QueuingStatus oldStatus, QueuingStatus newStatus);

	}

	public enum QueuingStatus {

		IDLE,
		ATTEMPTING,
		ACTIVE

	}

	public void queueUp(GameType gameType, long lineupId);
	public void leaveQueue();
	public Duration getTimeInQueue();

}
