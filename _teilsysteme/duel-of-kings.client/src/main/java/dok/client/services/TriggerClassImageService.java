package dok.client.services;

import javafx.scene.image.Image;

public interface TriggerClassImageService {

	public Image getTriggerClassImage(int triggerClassId);

}
