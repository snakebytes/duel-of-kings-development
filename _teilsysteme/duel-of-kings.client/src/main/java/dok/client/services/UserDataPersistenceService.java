package dok.client.services;

import dok.client.services.UserDataPersistenceService.ClientUserDataPersistenceServiceListener;
import dok.commons.impl.ObservableServiceRegistry.ObservableRegisterableService;
import dok.commons.services.HibernateDataPersistenceService;

public interface UserDataPersistenceService extends HibernateDataPersistenceService, ObservableRegisterableService<ClientUserDataPersistenceServiceListener> {

	/**
	 * Implementations of this interface can listen to data manipulation events being
	 * fired by {@linkplain UserDataPersistenceService}s.
	 *
	 * @author Konstantin Schaper
	 * @since 0.2.1.2
	 */
	public interface ClientUserDataPersistenceServiceListener {


	}

}
