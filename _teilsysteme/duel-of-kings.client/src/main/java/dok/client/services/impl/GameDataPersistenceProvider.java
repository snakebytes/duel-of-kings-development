package dok.client.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;

import dok.commons.impl.ObservableServiceRegistry;
import dok.commons.model.EnhancementSlot;
import dok.commons.model.EnhancementType;
import dok.game.model.AbilityClass;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterClass;
import dok.game.model.CharacterEnhancementClass;
import dok.game.model.DamageType;
import dok.game.model.HitFilter;
import dok.game.model.HitFilterList;
import dok.game.model.TargetFilter;
import dok.game.model.TargetFilterList;
import dok.game.model.TriggerClass;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.constants.CharacterClassIds;
import dok.game.model.constants.EnhancementClassIds;
import dok.game.model.constants.TriggerClassIds;
import dok.game.model.effects.actions.AdaptiveBombEffect;
import dok.game.model.effects.actions.ArouseEffect;
import dok.game.model.effects.actions.AttackEffect;
import dok.game.model.effects.actions.AxeBashEffect;
import dok.game.model.effects.actions.BombardmentEffect;
import dok.game.model.effects.actions.BreathtakingSilenceEffect;
import dok.game.model.effects.actions.CallingPackEffect;
import dok.game.model.effects.actions.CurseEffect;
import dok.game.model.effects.actions.DefendEffect;
import dok.game.model.effects.actions.HealEffect;
import dok.game.model.effects.actions.PowerTransferEffect;
import dok.game.model.effects.actions.SilenceEffect;
import dok.game.model.effects.actions.SpearBlowEffect;
import dok.game.model.effects.actions.SwapEffect;
import dok.game.model.effects.actions.ThrowBombsEffect;
import dok.game.model.effects.actions.WardensCodexEffect;
import dok.game.model.effects.reactions.AdvTranquilityReactionEffect;
import dok.game.model.effects.reactions.DauntingDefenderReactionEffect;
import dok.game.model.effects.reactions.DauntingDefenderReapplyReactionEffect;
import dok.game.model.effects.reactions.EncourageReactionEffect;
import dok.game.model.effects.reactions.MagicHuntAddReactionEffect;
import dok.game.model.effects.reactions.MagicHuntRemoveReactionEffect;
import dok.game.model.effects.reactions.StormSprintReactionEffect;
import dok.game.model.effects.reactions.TranquilityReactionEffect;
import dok.game.model.effects.reactions.VeteranOfBattlesReactionEffect;
import dok.game.model.effects.reactions.WardensCodexReactionEffect;

public class GameDataPersistenceProvider implements dok.client.services.GameDataPersistenceService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private ObservableServiceRegistry registry;
	private SessionFactory sessionFactory;
	private Session mainSession;

	// Constructor(s)

	public GameDataPersistenceProvider() {
		// Do nothing
	}

	// <-- CharacterClassService --->

	@Override
	public CharacterClass getCharacterClass(int id) {
		try {
			return getMainSession().get(CharacterClass.class, id);
		} catch (Exception e) {
			logger.catching(e);
			return null;
		}
	}

	@Override
	public List<Integer> getValidEnhancementClassIds(EnhancementSlot slot, int characterClassId) {
		List<Integer> results = new ArrayList<>();
		CharacterClass cc = getCharacterClass(characterClassId);
		for(int id : cc.getAllowedEnhancementClassIds()) {
			CharacterEnhancementClass enh = getCharacterEnhancementClass(id);
			if(enh != null && enh.isValidFor(slot)) {
				results.add(id);
			}
		}
		return results;
	}

	@Override
	public CharacterEnhancementClass getCharacterEnhancementClass(int id) {
		try {
			return getMainSession().get(CharacterEnhancementClass.class, id);
		} catch (Exception e) {
			logger.catching(e);
			return null;
		}
	}

	@Override
	public List<CharacterClass> getAllCharacterClasses() {
		return getAll(CharacterClass.class);
	}

	// <--- AbilityClassService --->

	@Override
	public AbilityClass getAbilityClass(int abilityClassId) {
		try {
			return getMainSession().get(AbilityClass.class, abilityClassId);
		} catch (Exception e) {
			logger.catching(e);
			return null;
		}
	}

	@Override
	public TriggerClass getTriggerClass(int triggerClassId) {
		try {
			return getMainSession().get(TriggerClass.class, triggerClassId);
		} catch (Exception e) {
			logger.catching(e);
			return null;
		}
	}

	// <--- RegisterableService --->

	@Override
	public void onRegistration(ObservableServiceRegistry registry, Class<?> category) throws Exception {
		if (registry == null) return;
		else if (getRegistry() != null && registry.contains(this)) registry.deregisterServiceProvider(this);
		else {
			// Setup hibernate
			BootstrapServiceRegistry bootstrapServiceRegistry =
					new BootstrapServiceRegistryBuilder()
					.applyIntegrator(new GameDataMigrationProvider())
					.build();

			StandardServiceRegistry standardRegistry =
					new StandardServiceRegistryBuilder(bootstrapServiceRegistry)
					.configure("database/gameData.hibernate.cfg.xml")
					.build();

			Metadata metadata = new MetadataSources(standardRegistry)
					.getMetadataBuilder()
					.applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)
					.build();

			logger.info("Attempting to export gameData schema ...");

			// Export Create
			File file = new File("data/schema/generated/create/gameData.sql");
			if (file.exists()) file.delete();
			SchemaExport create = new SchemaExport()
					.setOutputFile(file.toString())
					.setFormat(true)
					.setDelimiter(";")
					.setHaltOnError(true);
			create.createOnly(EnumSet.of(TargetType.SCRIPT), metadata);

			// Export Update
			file = new File("data/schema/generated/update/gameData.sql");
			if (file.exists()) file.delete();
			SchemaUpdate update = new SchemaUpdate()
			.setOutputFile(file.toString())
			.setFormat(true)
			.setDelimiter(";")
			.setHaltOnError(true);
			update.execute(EnumSet.of(TargetType.SCRIPT), metadata);

			logger.info("GameData schema has been exported ...");

			setSessionFactory(metadata.getSessionFactoryBuilder().build());

			// Define registry
			setRegistry(registry);

			// Create test data
			createTestCharacterAndAbilityClasses();
			// TODO: Create game data
			//createCharacterAndAbilityClasses();
		}
	}

	@Override
	public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) throws Exception {
		if (!getRegistry().equals(registry)) return;
		else {
			// Close all open sessions
			Session session = getMainSession();
			if (session != null) {
				try {
					if (session.isOpen()) {
						if (session.getTransaction() != null && session.getTransaction().getStatus() == TransactionStatus.FAILED_COMMIT) session.getTransaction().rollback();
						session.clear();
						session.close();
					}
				} catch (Exception e) {
					logger.warn("Could not close session", e);
				}
			}

			// Close database connection
			sessionFactory.close();

			// Define registry
			setRegistry(null);
		}
	}

	// <--- ServerUserDataPersistenceService --->

	@Override
	public Session openSession() throws Exception {
		Session result;
		if ((result = getMainSession()) != null) return result;
		else {
			try {
				result = getSessionFactory().openSession();
				setMainSession(result);
				return result;
			} catch (Exception e) {
				logger.log(Level.ERROR, "New Session could not be opened", e);
				throw e;
			}
		}
	}

	@Override
	public void closeCurrentSession() {
		Session session = getMainSession();
		if (session == null) return;
		try {
			session.clear();
			session.close();
		} catch (Exception e) {
			logger.log(Level.ERROR, "Session could not be closed", e);
		}
	}

	// Test

	private TargetFilterList saveOrUpdate(TargetFilterList filterList) {
		TargetFilterList filterList2;
		if ((filterList2 = getObjectById(TargetFilterList.class, filterList.getId())) == null) {
			filterList.setId((long) save(filterList));
			return filterList;
		} else {
			return filterList2;
		}
	}

	private HitFilterList saveOrUpdate(HitFilterList filterList) {
		HitFilterList filterList2;
		if ((filterList2 = getObjectById(HitFilterList.class, filterList.getId())) == null) {
			filterList.setId((long) save(filterList));
			return filterList;
		} else {
			return filterList2;
		}
	}

	private EnhancementSlot saveOrUpdate(EnhancementSlot slot) {
		EnhancementSlot slot2;
		if ((slot2 = getObjectById(EnhancementSlot.class, slot.getId())) == null) {
			try {
				slot.setId((int) save(slot));
				return slot;
			} catch (Exception e) {
				logger.error("ERROR", e);
				return null;
			}
		} else return slot2;
	}

	private void createTestCharacterAndAbilityClasses() {

		try {

			// Meele Filter

			TargetFilterList meeleFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.HOSTILE, TargetFilter.ALIVE, TargetFilter.MEELE));
			TargetFilterList semiRangedAttackFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.HOSTILE, TargetFilter.ALIVE, TargetFilter.SEMI_RANGED));
			TargetFilterList semiRangedTargetFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.ALIVE, TargetFilter.SEMI_RANGED));
			TargetFilterList rangedFilter = saveOrUpdate(TargetFilter.and(TargetFilter.HOSTILE, TargetFilter.ALIVE));
			TargetFilterList attackTargetFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.ALIVE, TargetFilter.HOSTILE, TargetFilter.ATTACK));
			TargetFilterList otherAllyFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.OWN, TargetFilter.ALIVE, TargetFilter.OTHER));
			TargetFilterList ownAliveTargetFilter = saveOrUpdate(TargetFilter.and(TargetFilter.ALIVE, TargetFilter.OWN));
			TargetFilterList aliveHostileTargetFilter = saveOrUpdate(TargetFilter.and(TargetFilter.ALIVE, TargetFilter.HOSTILE));
			HitFilterList primaryFilter = saveOrUpdate(HitFilter.single(HitFilter.PRIMARY));
			HitFilterList piercingFilter = saveOrUpdate(HitFilter.single(HitFilter.PIERCING));
			HitFilterList adjacentRowFilter = saveOrUpdate(HitFilter.single(HitFilter.ADJACENT_ROW));
			HitFilterList crossingFilter = saveOrUpdate(
					HitFilter.combine(HitFilter.ADJACENT_ROW, HitFilter.ADJACENT_COLUMN));
			HitFilterList attackHitFilter = saveOrUpdate(HitFilter.single(HitFilter.ATTACK));
			HitFilterList rowHitFilter = saveOrUpdate(HitFilter.single(HitFilter.ROW));
			HitFilterList allHostileHitFilter = saveOrUpdate(HitFilter.single(HitFilter.ALL_HOSTILE));


			EnhancementSlot rankOneSlotA = saveOrUpdate(new EnhancementSlot(EnhancementType.CRYSTAL, 1));
			EnhancementSlot rankOneSlotB = saveOrUpdate(new EnhancementSlot(EnhancementType.RUNE, 1));
			EnhancementSlot rankTwoSlot = saveOrUpdate(new EnhancementSlot(EnhancementType.GENERIC, 2));
			EnhancementSlot rankThreeSlot = saveOrUpdate(new EnhancementSlot(EnhancementType.GENERIC, 3));

			Set<Integer> abilityIds;
			Map<CharacterAttribute, Integer> attributes;
			CharacterClass clazz;

			// Ability Crystals

			CharacterEnhancementClass wardensCodexCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.WARDENS_CODEX_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			wardensCodexCrystal.getAbilityClassIds().add(AbilityClassIds.WARDENS_CODE_ABILITY_CLASS_ID);
			saveOrUpdate(wardensCodexCrystal);

			CharacterEnhancementClass axeBashCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.AXE_BASH_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			axeBashCrystal.getAbilityClassIds().add(AbilityClassIds.AXE_BASH_ABILITY_CLASS_ID);
			saveOrUpdate(axeBashCrystal);

			CharacterEnhancementClass spearBlowCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.SPEAR_BLOW_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			spearBlowCrystal.getAbilityClassIds().add(AbilityClassIds.SPEAR_BLOW_ABILITY_CLASS_ID);
			saveOrUpdate(spearBlowCrystal);

			CharacterEnhancementClass healCrystal = new CharacterEnhancementClass(EnhancementClassIds.HEAL_CRYSTAL_ID,
					EnhancementType.CRYSTAL, 1);
			healCrystal.getAbilityClassIds().add(AbilityClassIds.HEAL_ABILITY_CLASS_ID);
			saveOrUpdate(healCrystal);

			CharacterEnhancementClass curseCrystal = new CharacterEnhancementClass(EnhancementClassIds.CURSE_CRYSTAL_ID,
					EnhancementType.CRYSTAL, 1);
			curseCrystal.getAbilityClassIds().add(AbilityClassIds.CURSE_ABILITY_CLASS_ID);
			saveOrUpdate(curseCrystal);

			CharacterEnhancementClass bombardmentCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.BOMBARDMENT_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			bombardmentCrystal.getAbilityClassIds().add(AbilityClassIds.BOMBARDMENT_ABILITY_CLASS_ID);
			saveOrUpdate(bombardmentCrystal);

			CharacterEnhancementClass arouseCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.AROUSE_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			arouseCrystal.getAbilityClassIds().add(AbilityClassIds.AROUSE_ABILITY_CLASS_ID);
			saveOrUpdate(arouseCrystal);

			// Rank 2

			CharacterEnhancementClass tranquilityCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.TRANQUILITY_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			tranquilityCrystal.getAbilityClassIds().add(AbilityClassIds.TRANQUILITY_ABILITY_CLASS_ID);
			saveOrUpdate(tranquilityCrystal);

			CharacterEnhancementClass veteranOfBattlesCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.VETERAN_OF_BATTLES_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			veteranOfBattlesCrystal.getAbilityClassIds().add(AbilityClassIds.VETERAN_OF_BATTLES_ABILITY_CLASS_ID);
			saveOrUpdate(veteranOfBattlesCrystal);

			CharacterEnhancementClass silenceCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.SILENCE_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			silenceCrystal.getAbilityClassIds().add(AbilityClassIds.SILENCE_ABILITY_CLASS_ID);
			saveOrUpdate(silenceCrystal);

			CharacterEnhancementClass callThePackCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.CALLING_THE_PACK_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			callThePackCrystal.getAbilityClassIds().add(AbilityClassIds.CALLING_THE_PACK_ABILITY_CLASS_ID);
			saveOrUpdate(callThePackCrystal);

			CharacterEnhancementClass dauntingDefenderCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.DAUNTING_DEFENDER_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			dauntingDefenderCrystal.getAbilityClassIds().add(AbilityClassIds.DAUNTING_DEFENDER_ABILITY_CLASS_ID);
			saveOrUpdate(dauntingDefenderCrystal);

			CharacterEnhancementClass adaptiveBombCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.ADAPTIVE_BOMB_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			adaptiveBombCrystal.getAbilityClassIds().add(AbilityClassIds.ADAPTIVE_BOMB_ABILITY_CLASS_ID);
			saveOrUpdate(adaptiveBombCrystal);

			CharacterEnhancementClass powerTransferCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.POWER_TRANSFER_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			powerTransferCrystal.getAbilityClassIds().add(AbilityClassIds.POWER_TRANSFER_ABILITY_CLASS_ID);
			saveOrUpdate(powerTransferCrystal);

			// Rank 3

			CharacterEnhancementClass advTranquilityCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.ADV_TRANQUILITY_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			advTranquilityCrystal.getAbilityClassIds().add(AbilityClassIds.ADV_TRANQUILITY_ABILITY_CLASS_ID);
			saveOrUpdate(advTranquilityCrystal);

			CharacterEnhancementClass btSilenceCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.BREATHTAKING_SILENCE_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			btSilenceCrystal.getAbilityClassIds().add(AbilityClassIds.BREATHTAKING_SILENCE_ABILITY_CLASS_ID);
			saveOrUpdate(btSilenceCrystal);

			CharacterEnhancementClass stormSprintCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.STORM_SPRINT_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			stormSprintCrystal.getAbilityClassIds().add(AbilityClassIds.STORM_SPRINT_ABILITY_CLASS_ID);
			saveOrUpdate(stormSprintCrystal);

			CharacterEnhancementClass throwBombsCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.THROW_BOMBS_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			throwBombsCrystal.getAbilityClassIds().add(AbilityClassIds.THROW_BOMBS_ABILITY_CLASS_ID);
			saveOrUpdate(throwBombsCrystal);

			CharacterEnhancementClass magicHuntCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.MAGIC_HUNT_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			magicHuntCrystal.getAbilityClassIds().add(AbilityClassIds.MAGIC_HUNT_ABILITY_CLASS_ID);
			saveOrUpdate(magicHuntCrystal);

			CharacterEnhancementClass encourageCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.ENCOURAGE_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			encourageCrystal.getAbilityClassIds().add(AbilityClassIds.ENCOURAGE_ABILITY_CLASS_ID);
			saveOrUpdate(encourageCrystal);

			// <Sword>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 20);
			attributes.put(CharacterAttribute.ARMOR, 2);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.SWORD_CHARACTER_CLASS_ID, meeleFilter, primaryFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(wardensCodexCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(veteranOfBattlesCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(encourageCrystal.getId());
			saveOrUpdate(clazz);

			// </Sword>

			// <Axe>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 15);
			attributes.put(CharacterAttribute.ARMOR, 1);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 3);

			// Add

			clazz = new CharacterClass(CharacterClassIds.AXE_CHARACTER_CLASS_ID, meeleFilter, adjacentRowFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(axeBashCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(callThePackCrystal.getId());
			saveOrUpdate(clazz);

			// </Axe>

			// <Spear>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 1);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 4);

			// Add

			clazz = new CharacterClass(CharacterClassIds.SPEAR_CHARACTER_CLASS_ID, meeleFilter, piercingFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(spearBlowCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(stormSprintCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(dauntingDefenderCrystal.getId());
			saveOrUpdate(clazz);

			// </Spear>

			// <Staff>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 0);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 1);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 3);

			// Add

			clazz = new CharacterClass(CharacterClassIds.STAFF_CHARACTER_CLASS_ID, meeleFilter, primaryFilter,
					DamageType.MAGICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(healCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(tranquilityCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(advTranquilityCrystal.getId());
			saveOrUpdate(clazz);

			// </Staff>

			// <Bow>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 0);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.BOW_CHARACTER_CLASS_ID, rangedFilter, primaryFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(curseCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(silenceCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(btSilenceCrystal.getId());
			saveOrUpdate(clazz);

			// </Bow>

			// <Crossbow>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 1);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 2);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.CROSSBOW_CHARACTER_CLASS_ID, semiRangedAttackFilter, piercingFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(arouseCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(magicHuntCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(powerTransferCrystal.getId());
			saveOrUpdate(clazz);

			// </Crossbow>

			// <Handcannon>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 0);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.HANDCANNON_CHARACTER_CLASS_ID, semiRangedAttackFilter,
					crossingFilter, DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			clazz.getAllowedEnhancementClassIds().add(bombardmentCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(adaptiveBombCrystal.getId());
			clazz.getAllowedEnhancementClassIds().add(throwBombsCrystal.getId());
			saveOrUpdate(clazz);

			// </Handcannon>

			LinkedHashMap<String, Double> properties;

			// <Attack>

			properties = new LinkedHashMap<>();
			properties.put("cost", 2d);
			properties.put("secondaryDamageMultiplier", 0.5);

			AbilityClass attackAbilityClass = new AbilityClass(AbilityClassIds.ATTACK_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), AttackEffect.class, attackTargetFilter, attackHitFilter, properties);
			saveOrUpdate(attackAbilityClass);

			// </Attack>

			// <Swap>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);

			AbilityClass swapAbilityClass = new AbilityClass(AbilityClassIds.SWAP_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), SwapEffect.class, otherAllyFilter, primaryFilter, properties);
			saveOrUpdate(swapAbilityClass);

			// </Swap>

			// <Defend>

			properties = new LinkedHashMap<>();
			properties.put("cost", 1d);
			properties.put("duration", 1d);
			properties.put("armorBonus", 1d);
			properties.put("magicResistanceBonus", 1d);

			AbilityClass defendAbilityClass = new AbilityClass(AbilityClassIds.DEFEND_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), DefendEffect.class, null, primaryFilter, properties);
			saveOrUpdate(defendAbilityClass);

			// </Defend>

			// <Warden's Codex>

			properties = new LinkedHashMap<>();
			properties.put("cost", 1d);
			properties.put("duration", 1d);

			AbilityClass wardensCodexAbilityClass = new AbilityClass(AbilityClassIds.WARDENS_CODE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), WardensCodexEffect.class, null,
					primaryFilter, properties);
			saveOrUpdate(wardensCodexAbilityClass);

			// </Warden's Codex>

			// <Axe Bash>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("stunCountersAmount", 1d);
			properties.put("secondaryDamageMultiplier", 0.5);

			AbilityClass axeBashAbilityClass = new AbilityClass(AbilityClassIds.AXE_BASH_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), AxeBashEffect.class,
					attackTargetFilter,
					adjacentRowFilter, properties);
			saveOrUpdate(axeBashAbilityClass);

			// </Axe Bash>

			// <Curse>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("duration", 2d);
			properties.put("attackDamageMalus", 1d);

			AbilityClass curseAbilityClass = new AbilityClass(AbilityClassIds.CURSE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), CurseEffect.class,
					attackTargetFilter,
					adjacentRowFilter, properties);
			saveOrUpdate(curseAbilityClass);

			// </Curse>

			// <Heal>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("healingAmount", 2d);

			AbilityClass healAbilityClass = new AbilityClass(AbilityClassIds.HEAL_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), HealEffect.class,
					ownAliveTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(healAbilityClass);

			// </Heal>

			// <Spear Blow>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("timeCountersAmount", 1d);
			properties.put("secondaryDamageMultiplier", 0.5d);

			AbilityClass spearBlowAbilityClass = new AbilityClass(AbilityClassIds.SPEAR_BLOW_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), SpearBlowEffect.class,
					attackTargetFilter,
					piercingFilter, properties);
			saveOrUpdate(spearBlowAbilityClass);

			// </Spear Blow>

			// <Arouse>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("stunCountersAmount", 1d);
			properties.put("stunShortenAmount", 1d);

			AbilityClass arouseAbilityClass = new AbilityClass(AbilityClassIds.AROUSE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), ArouseEffect.class,
					ownAliveTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(arouseAbilityClass);

			// </Arouse>

			// <Bombardment>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("damageAmount", 1d);

			AbilityClass bombardmentAbilityClass = new AbilityClass(AbilityClassIds.BOMBARDMENT_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), BombardmentEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(bombardmentAbilityClass);

			// </Bombardment>

			// <Tranquility>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("healingAmount", 2d); // Relevant f�r den Tooltip,
													// NICHT f�r die
													// Funkionalit�t

			AbilityClass tranquilityAbilityClass = new AbilityClass(AbilityClassIds.TRANQUILITY_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(tranquilityAbilityClass);

			// </Tranquility>

			// <Silence>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("duration", 2d);

			AbilityClass silenceAbilityClass = new AbilityClass(AbilityClassIds.SILENCE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), SilenceEffect.class,
					aliveHostileTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(silenceAbilityClass);

			// </Silence>

			// <Veteran of Battles>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("blockValue", 3d); // Relevant f�r den Tooltip, NICHT
												// f�r die Funkionalit�t

			AbilityClass veteranAbilityClass = new AbilityClass(AbilityClassIds.VETERAN_OF_BATTLES_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(veteranAbilityClass);

			// </Veteran of Battles>

			// <Calling the Pack>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("dmgToUndamaged", 1d);
			properties.put("dmgToDamaged", 2d);

			AbilityClass packAbilityClass = new AbilityClass(AbilityClassIds.CALLING_THE_PACK_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), CallingPackEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(packAbilityClass);

			// </Calling the Pack>

			// <Daunting Defender>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);

			AbilityClass dauntingDefenderAbilityClass = new AbilityClass(
					AbilityClassIds.DAUNTING_DEFENDER_ABILITY_CLASS_ID, properties.get("cost").intValue(), null, null,
					null, properties);
			saveOrUpdate(dauntingDefenderAbilityClass);

			// </Daunting Defender>

			// <Adaptive Bomb>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("dmgBonus", 2d);
			properties.put("secondaryDamageMultiplier", 0.5d);

			AbilityClass adaptiveBombClass = new AbilityClass(AbilityClassIds.ADAPTIVE_BOMB_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), AdaptiveBombEffect.class,
					attackTargetFilter,
					attackHitFilter, properties);
			saveOrUpdate(adaptiveBombClass);

			// </Adaptive Bomb>

			// <Power Transfer>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("duration", 1d);
			properties.put("atkValue", 1d);

			AbilityClass powerTransferClass = new AbilityClass(AbilityClassIds.POWER_TRANSFER_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), PowerTransferEffect.class,
					semiRangedTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(powerTransferClass);

			// </Power Transfer>

			// <Throw Bombs>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("bombAmount", 3d);
			properties.put("bombDamage", 2d);
			properties.put("secondaryDamageMultiplier", 0.5);

			AbilityClass throwBombsClass = new AbilityClass(AbilityClassIds.THROW_BOMBS_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), ThrowBombsEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(throwBombsClass);

			// </Throw Bombs>

			// <Magic Hunt>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("atkValue", 1d); // Nur f�r den Tooltip relevant,
											// NICHT f�r die Funktionalit�t
			properties.put("duration", 0d);

			AbilityClass magicHuntClass = new AbilityClass(AbilityClassIds.MAGIC_HUNT_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(magicHuntClass);

			// </Magic Hunt>

			// <Storm Sprint>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);

			AbilityClass chargeRunnerClass = new AbilityClass(AbilityClassIds.STORM_SPRINT_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(chargeRunnerClass);

			// </Storm Sprint>

			// <Breathtaking Silence>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("duration", 1d);

			AbilityClass breathSilenceClass = new AbilityClass(AbilityClassIds.BREATHTAKING_SILENCE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), BreathtakingSilenceEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(breathSilenceClass);

			// </Breathtaking Silence>

			// <Encourage>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("markerAmount", 2d); // Nur f�r den Skill-Tooltip
												// relevant

			AbilityClass encourageClass = new AbilityClass(AbilityClassIds.ENCOURAGE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(encourageClass);

			// </Encourage>

			// <Advanced Tranquility>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("healingAmount", 4d); // Nur f�r den Tooltip
													// relevant, NICHT f�r die
													// Funktionalit�t
			properties.put("healCount", 2d); // Nur f�r den Tooltip relevant,
												// NICHT f�r die Funktionalit�t

			AbilityClass advTranquilityAbilityClass = new AbilityClass(AbilityClassIds.ADV_TRANQUILITY_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(advTranquilityAbilityClass);

			// </Advanced Tranquility>

			// <--------- Triggers ------------>

			// <Warden's Codex Trigger>

			TriggerClass wardensCodexTriggerClass = new TriggerClass(TriggerClassIds.WARDENS_CODE_TRIGGER_CLASS_ID,
					null, primaryFilter, WardensCodexReactionEffect.class, true);
			saveOrUpdate(wardensCodexTriggerClass);

			// <Warden's Codex Trigger>

			// <Tranquility Trigger>

			properties = new LinkedHashMap<>();
			properties.put("healingAmount", 2d);

			TriggerClass tranquilityTriggerClass = new TriggerClass(TriggerClassIds.TRANQUILITY_TRIGGER_CLASS_ID, null,
					null, TranquilityReactionEffect.class, false, properties);
			saveOrUpdate(tranquilityTriggerClass);

			// </Tranquility Trigger>

			// <Veteran of Battles Trigger>

			properties = new LinkedHashMap<>();
			properties.put("duration", 0d);
			properties.put("blockValue", 3d); // Relevant f�r die
												// Funktionalit�t, NICHT den
												// Tooltip!

			TriggerClass veteranTriggerClass = new TriggerClass(TriggerClassIds.VETERAN_OF_BATTLES_TRIGGER_CLASS_ID,
					null, null, VeteranOfBattlesReactionEffect.class, false, properties);
			saveOrUpdate(veteranTriggerClass);

			// </Veteran of Battles Trigger>

			// <Daunting Defender Trigger>

			properties = new LinkedHashMap<>();
			properties.put("secondaryDamageMultiplier", 0.5);

			TriggerClass dauntingDefenderTriggerClass = new TriggerClass(
					TriggerClassIds.DAUNTING_DEFENDER_TRIGGER_CLASS_ID, null, null,
					DauntingDefenderReactionEffect.class, false, properties);
			saveOrUpdate(dauntingDefenderTriggerClass);

			// </Daunting Defender Trigger>

			// <Magic Hunt Add Trigger>

			properties = new LinkedHashMap<>();
			properties.put("atkValue", 1d);
			properties.put("duration", 0d);

			TriggerClass magicHuntAddTriggerClass = new TriggerClass(TriggerClassIds.MAGIC_HUNT_ADD_TRIGGER_CLASS_ID,
					null, null, MagicHuntAddReactionEffect.class, false, properties);
			saveOrUpdate(magicHuntAddTriggerClass);

			// </Magic Hunt Add Trigger>

			// <Magic Hunt Remove Trigger>

			properties = new LinkedHashMap<>();

			TriggerClass magicHuntRemoveTriggerClass = new TriggerClass(
					TriggerClassIds.MAGIC_HUNT_REMOVE_TRIGGER_CLASS_ID, null, null, MagicHuntRemoveReactionEffect.class,
					false, properties);
			saveOrUpdate(magicHuntRemoveTriggerClass);

			// </Magic Hunt Remove Trigger>

			// <Charge Runner Trigger>

			properties = new LinkedHashMap<>();
			properties.put("timeMarkersChar", 1d);
			properties.put("timeMarkersAbil", 1d);

			TriggerClass chargeRunnerTriggerClass = new TriggerClass(TriggerClassIds.STORM_SPRINT_TRIGGER_CLASS_ID,
					null, null, StormSprintReactionEffect.class, false, properties);
			saveOrUpdate(chargeRunnerTriggerClass);

			// </Charge Runner Trigger>

			// <Encourage Trigger>

			properties = new LinkedHashMap<>();
			properties.put("timeMarkersAmt", 2d);

			TriggerClass encourageTriggerClass = new TriggerClass(TriggerClassIds.ENCOURAGE_TRIGGER_CLASS_ID, null,
					null, EncourageReactionEffect.class, false, properties);
			saveOrUpdate(encourageTriggerClass);

			// </Encourage Trigger>

			// <Advanced Tranquility Trigger>

			properties = new LinkedHashMap<>();
			properties.put("healingAmount", 4d);
			properties.put("healCount", 2d);

			TriggerClass advTranquilityTriggerClass = new TriggerClass(TriggerClassIds.ADV_TRANQUILITY_TRIGGER_CLASS_ID,
					null, null, AdvTranquilityReactionEffect.class, false, properties);
			saveOrUpdate(advTranquilityTriggerClass);

			// </Advanced Tranquility Trigger>

			// <Daunting Defender Reapply Trigger>

			properties = new LinkedHashMap<>();

			TriggerClass dauntingDefenderReapplyTriggerClass = new TriggerClass(
					TriggerClassIds.DAUNTING_DEFENDER_REAPPLY_TRIGGER_CLASS_ID, null, null,
					DauntingDefenderReapplyReactionEffect.class, false, properties);
			saveOrUpdate(dauntingDefenderReapplyTriggerClass);

			// </Daunting Defender Reapply Trigger>

		} catch (Exception e) {
			logger.warn("GameTestData could not be created", e);
		}

	}

	// Getter & Setter

	public ObservableServiceRegistry getRegistry() {
		return registry;
	}

	private void setRegistry(ObservableServiceRegistry registry) {
		this.registry = registry;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getMainSession() {
		return mainSession;
	}

	private void setMainSession(Session mainSession) {
		this.mainSession = mainSession;
	}

}
