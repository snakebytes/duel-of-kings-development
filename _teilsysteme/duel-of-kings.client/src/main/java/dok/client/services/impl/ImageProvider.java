package dok.client.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.services.ApplicationImageService;
import dok.client.services.GameImageService;
import dok.commons.model.EnhancementType;
import dok.game.model.Buff;
import javafx.scene.image.Image;

public class ImageProvider implements GameImageService, ApplicationImageService {

    // Class Constants

    private static final Logger LOGGER = LogManager.getLogger();

    // Attributes

    private final Map<String, Image> images = new HashMap<>();
    private final Map<String, Image> iconImages = new HashMap<>();
    private final Map<Class<? extends Buff>, Image> buffImages = new HashMap<>();
    private final Map<Integer, Image> characterClassImages = new HashMap<>();
    private final Map<Integer, Image> abilityClassImages = new HashMap<>();
    private final Map<Integer, Image> triggerClassImages = new HashMap<>();
    private final Map<String, Image> lineupImages = new HashMap<>();
    private final Map<Integer, Image> characterClassEnhancementImages = new HashMap<>();
    private final Map<EnhancementType, Image> enhancementSlotImages = new HashMap<>();
    private final Map<Integer, Image> enhancementImages = new HashMap<>();
    private final Map<String, Image> lineupEditorIconImages = new HashMap<>();

    // Methods

    @Override
    public void preloadGameGraphics() {
        preloadIcon("background");
        preloadIcon("armor");
        preloadIcon("attack_damage");
        preloadIcon("damage");
        preloadIcon("health");
        preloadIcon("king");
        preloadIcon("magic_resistance");
        preloadIcon("source");
        preloadIcon("target");
        preloadIcon("time_counters");
    }

    @Override
    public void preloadIcon(String key) {
        getIcon(key);
    }

    @Override
    public void preloadBuffImage(Class<? extends Buff> buffClass) {
        getBuffImage(buffClass);
    }

    @Override
    public void preloadCharacterClassImage(int characterClassId) {
        getCharacterClassImage(characterClassId);
    }

    @Override
    public void preloadAbilityImage(int abilityClassId) {
        getAbilityImage(abilityClassId);
    }

    @Override
    public Image getCharacterClassImage(int characterClassId) {
        LOGGER.entry(characterClassId);
        Image img = getCharacterClassImages().get(characterClassId);
        if (img == null) {
            img = new Image(getClass().getResourceAsStream("/images/characters/" + characterClassId + ".png"));
            getCharacterClassImages().put(characterClassId, img);
        }
        return LOGGER.exit(img);
    }

    @Override
    public Image getIcon(String key) {
        LOGGER.entry(key);
        Image img = getIconImages().get(key);
        if (img == null) {
            img = new Image(getClass().getResourceAsStream("/images/icons/game/" + key + ".png"));
            getIconImages().put(key, img);
        }
        return LOGGER.exit(img);
    }

    @Override
    public Image getBuffImage(Class<? extends Buff> buffClass) {
        LOGGER.entry(buffClass);
        Image img = getBuffImages().get(buffClass);
        if (img == null) {
            img = new Image(getClass().getResourceAsStream("/images/buffs/" + buffClass.getSimpleName() + ".png"));
            getBuffImages().put(buffClass, img);
        }
        return LOGGER.exit(img);
    }

    @Override
    public Image getLineupImage(String key) {
        LOGGER.entry(key);
        Image img = getLineupImages().get(key);
        if (img == null) {
            try {
                img = new Image(getClass().getResourceAsStream("/images/lineups/" + key + ".png"));
                getLineupImages().put(key, img);
            } catch (Exception e) {
                LOGGER.catching(Level.WARN, e);
            }
        }
        return LOGGER.exit(img);
    }

    @Override
    public Image getAbilityImage(int abilityClassId) {
        LOGGER.entry(abilityClassId);
        Image img = getAbilityClassImages().get(abilityClassId);
        if (img == null) {
            try {
                img = new Image(getClass().getResourceAsStream("/images/abilities/" + abilityClassId + ".png"));
                getAbilityClassImages().put(abilityClassId, img);
            } catch (Exception e) {
                LOGGER.catching(Level.WARN, e);
            }
        }
        return LOGGER.exit(img);
    }

    @Override
    public Image getImage(String key) {
         LOGGER.entry(key);
            Image img = getImages().get(key);
            if (img == null) {
                img = new Image(getClass().getResourceAsStream("/images/" + key + ".png"));
                getImages().put(key, img);
            }
            return LOGGER.exit(img);
    }

    @Override
    public Image getTriggerClassImage(int triggerClassId) {
        LOGGER.entry(triggerClassId);
        Image img = getTriggerClassImages().get(triggerClassId);
        if (img == null) {
            try {
                img = new Image(getClass().getResourceAsStream("/images/triggers/" + triggerClassId + ".png"));
            } catch (Exception e) {
                LOGGER.catching(Level.WARN, e);
            }
            getTriggerClassImages().put(triggerClassId, img);
        }
        return LOGGER.exit(img);
    }

	@Override
	public Image getEnhancementSlotImage(EnhancementType type) {
		LOGGER.entry(type);
        Image img = getEnhancementSlotImages().get(type);
        if (img == null) {
            try {
                img = new Image(getClass().getResourceAsStream("/images/enhancementslots/" + type + ".png"));
            } catch (Exception e) {
                LOGGER.catching(Level.WARN, e);
            }
            getEnhancementSlotImages().put(type, img);
        }
        return LOGGER.exit(img);
	}

	@Override
	public Image getEnhancementImage(Integer enhancementId) {
		LOGGER.entry(enhancementId);
        Image img = getEnhancementImages().get(enhancementId);
        if (img == null) {
            try {
                img = new Image(getClass().getResourceAsStream("/images/enhancements/" + enhancementId + ".png"));
            } catch (Exception e) {
                LOGGER.catching(Level.WARN, e);
            }
            getEnhancementImages().put(enhancementId, img);
        }
        return LOGGER.exit(img);
	}

	@Override
	public Image getLineupEditorIcon(String name) {
		LOGGER.entry(name);
        Image img = getLineupEditorIconImages().get(name);
        if (img == null) {
            try {
                img = new Image(getClass().getResourceAsStream("/images/icons/editor/" + name + ".png"));
            } catch (Exception e) {
                LOGGER.catching(Level.WARN, e);
            }
            getLineupEditorIconImages().put(name, img);
        }
        return LOGGER.exit(img);
	}

    // Getter & Setter

    public Map<Integer, Image> getCharacterClassImages() {
        return characterClassImages;
    }

    public Map<Integer, Image> getAbilityClassImages() {
        return abilityClassImages;
    }

    public Map<String, Image> getIconImages() {
        return iconImages;
    }

    public Map<Class<? extends Buff>, Image> getBuffImages() {
        return buffImages;
    }

    public Map<Integer, Image> getTriggerClassImages() {
        return triggerClassImages;
    }

    public Map<String, Image> getImages() {
        return images;
    }

    public Map<String, Image> getLineupImages() {
        return lineupImages;
    }

    public Map<Integer, Image> getCharacterClassEnhancementImages() {
        return characterClassEnhancementImages;
    }

	public Map<EnhancementType, Image> getEnhancementSlotImages() {
		return enhancementSlotImages;
	}

	public Map<Integer, Image> getEnhancementImages() {
		return enhancementImages;
	}

	public Map<String, Image> getLineupEditorIconImages() {
		return lineupEditorIconImages;
	}

}
