package dok.client.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.services.LineupService;
import dok.client.services.NetworkService;
import dok.commons.impl.ObservableServiceRegistry;
import dok.commons.model.Lineup;
import dok.commons.model.StatusCode;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.game.network.model.LineupUpdatePacket;
import dok.game.network.model.SaveLineupPacket;
import dok.game.network.model.SaveLineupResultPacket;

public class LineupProvider implements LineupService, ConnectionListener {

	private final Set<LineupServiceListener> listeners = new HashSet<>();

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private ObservableServiceRegistry registry;
	private final List<Lineup> lineups = new ArrayList<>();

	// Constructor(s)

	public LineupProvider() {
		super();
	}

	// <--- LineupService --->

	@Override
	public boolean saveLineup(Lineup lineup) {
		try {
			Connection conn = getRegistry().getServiceProvider(NetworkService.class).getActiveConnection();
			SaveLineupPacket saveLineupPacket = new SaveLineupPacket(lineup);
			conn.sendPacket(saveLineupPacket);
			return true;
		} catch (IOException e) {
			logger.catching(e);
			return false;
		}
	}

	@Override
	public List<Lineup> getLineups() {
		return lineups;
	}

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;
			if (packetReceivedEvent.getPacket() instanceof SaveLineupResultPacket) {
				SaveLineupResultPacket saveLineupResultPacket = (SaveLineupResultPacket) packetReceivedEvent.getPacket();
				if (saveLineupResultPacket.getStatusCode() == StatusCode.OK) {
					Lineup lineup = saveLineupResultPacket.getLineup();
					getLineups().add(lineup);
					getListeners().forEach((listener) -> listener.onLineupSaved(lineup));
				} else {
					getListeners().forEach((listener) -> listener.onLineupSaveError(saveLineupResultPacket.getStatusCode()));
					// TODO: Error message
				}
			} else if (packetReceivedEvent.getPacket() instanceof LineupUpdatePacket) {
				LineupUpdatePacket lineupUpdatePacket = (LineupUpdatePacket) packetReceivedEvent.getPacket();
				if (lineupUpdatePacket.getStatusCode() == StatusCode.OK) {
					getLineups().clear();
					List<Lineup> lineups = ((LineupUpdatePacket) packetReceivedEvent.getPacket()).getResult();
					getLineups().addAll(lineups);
					getListeners().forEach((listener) -> listener.onLineupsUpdated(lineups));
				} else {
					// TODO: Error message
				}
			}
		}
	}

	// <--- RegisterableService --->

	@Override
	public void onRegistration(ObservableServiceRegistry registry, Class<?> category) {
		if (registry == null) return;
		else if (getRegistry() != null && registry.contains(this)) registry.deregisterServiceProvider(this);
		else {
			try {
				setRegistry(registry);

				// Start listening to active connection
				registry.getServiceProvider(NetworkService.class).getActiveConnection().addListener(this);

			} catch (NoSuchElementException e) {

				logger.catching(e);
				throw e;

			}
		}
	}

	@Override
	public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) {
		if (!getRegistry().equals(registry)) return;
		else {
			try {

				registry.getServiceProvider(NetworkService.class).getActiveConnection().removeListener(this);

			} catch (Exception e) {

				// Thats fine

			}
			setRegistry(null);
		}
	}

	// <--- CustomService --->

	@Override
	public boolean addListener(LineupServiceListener listener) throws IllegalArgumentException {
		if (listener == null) throw new IllegalArgumentException("Listener must not be null");
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(LineupServiceListener listener) throws IllegalArgumentException {
		if (listener == null) throw new IllegalArgumentException("Listener must not be null");
		return getListeners().remove(listener);
	}

	// Getter & Setter

	public ObservableServiceRegistry getRegistry() {
		return registry;
	}

	public void setRegistry(ObservableServiceRegistry registry) {
		this.registry = registry;
	}

	public Set<LineupServiceListener> getListeners() {
		return listeners;
	}

}
