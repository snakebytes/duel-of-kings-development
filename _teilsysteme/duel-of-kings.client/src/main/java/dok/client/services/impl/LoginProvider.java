package dok.client.services.impl;

import java.io.IOException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.services.LoginService;
import dok.client.services.NetworkService;
import dok.commons.impl.ObservableServiceRegistry;
import dok.commons.model.LoginSession;
import dok.commons.model.StatusCode;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.commons.network.api.packets.LoginResultPacket;
import dok.commons.network.core.packets.LoginPacketImpl;
import dok.game.network.model.LineupUpdatePacket;

/**
 * Implementation of the {@linkplain LoginService} interface.
 *
 * @author Konstantin Schaper
 * @since 0.1.0
 */
public class LoginProvider implements LoginService, ConnectionListener {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private final Set<LoginServiceListener> listeners = new HashSet<>();
	private LoginSession activeSession;
	private ObservableServiceRegistry registry;

	// Constructor(s)

	public LoginProvider() {
		super();
	}

	// Methods

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;
			if (packetReceivedEvent.getPacket() instanceof LoginResultPacket) {
				LoginResultPacket loginResultPacket = (LoginResultPacket) packetReceivedEvent.getPacket();

				if (loginResultPacket.getStatusCode() == StatusCode.OK) {
					setActiveSession(loginResultPacket.getLoginSession());
					getListeners().forEach((listener) -> listener.onSuccessfulLogin(getActiveSession()));
					logger.info(getActiveSession() + " has been successfully logged in");

					// Get updated lineups
					try {
						connectionEvent.getSource().sendPacket(new LineupUpdatePacket());
					} catch (IOException e) {
						logger.catching(e);
					}
				}

			}
		}
	}

	// <--- LoginService --->

	@Override
	public String getActiveUserName() {
		return "undefined";
	}

	@Override
	public void attemptLogin(String username, String password) {

		// Remember login name
		try {
			Preferences prefs = getRegistry().getServiceProvider(Preferences.class);
			if (prefs.getBoolean("Login_RememberName", false))
				prefs.put("Login_Name", getActiveUserName());
		} catch (Exception e) {
			logger.error("LoginName-Preference could not be saved", e);
		}

		// Send login request
		try {
			getConn().sendPacket(new LoginPacketImpl(username, password));
		} catch (IOException e) {
			logger.error("LoginPacket could not be sent", e);
		}

	}

	@Override
	public void onRegistration(ObservableServiceRegistry registry, Class<?> category) {
		if (registry == null) return;
		else if (getRegistry() != null && registry.contains(this)) registry.deregisterServiceProvider(this);
		else {
			try {
				setRegistry(registry);

				// Start listening to active connection
				registry.getServiceProvider(NetworkService.class).getActiveConnection().addListener(this);

			} catch (NoSuchElementException e) {

				logger.catching(e);
				throw e;

			}
		}
	}

	@Override
	public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) {
		if (!getRegistry().equals(registry)) return;
		else {
			try {

				// Start listening to active connection
				registry.getServiceProvider(NetworkService.class).getActiveConnection().removeListener(this);

			} catch (NoSuchElementException e) {

				logger.catching(e);

			}
			setRegistry(null);
		}
	}

	// <--- CustomService --->

	@Override
	public boolean addListener(LoginServiceListener listener) {
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(LoginServiceListener listener) {
		return getListeners().remove(listener);
	}

	// Getter & Setter

	@Override
	public LoginSession getActiveSession() {
		return activeSession;
	}

	public void setActiveSession(LoginSession activeSession) {
		this.activeSession = activeSession;
	}

	private Connection getConn() {
		try {
			return getRegistry().getServiceProvider(NetworkService.class).getActiveConnection();
		} catch (Exception e) {
			return null;
		}
	}

	public Set<LoginServiceListener> getListeners() {
		return listeners;
	}

	public ObservableServiceRegistry getRegistry() {
		return registry;
	}

	public void setRegistry(ObservableServiceRegistry registry) {
		this.registry = registry;
	}

}
