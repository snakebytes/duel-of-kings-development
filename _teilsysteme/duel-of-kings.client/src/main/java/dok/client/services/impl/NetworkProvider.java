package dok.client.services.impl;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.services.NetworkService;
import dok.client.services.PreferencesService;
import dok.commons.impl.ObservableServiceRegistry;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.ObservableConnection;
import dok.commons.network.core.NetworkConstants;
import dok.commons.network.core.SocketConnection;
import dok.commons.network.core.util.SSLContextFactory;

public class NetworkProvider implements NetworkService {

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    // Constants

    private final Set<NetworkServiceListener> listeners = new HashSet<>();
    private final ConnectionListener initialListener;

    // Attributes

    private ObservableServiceRegistry registry;
    private ObservableConnection activeConnection;

    // Constructor(s)

    public NetworkProvider(ConnectionListener initialListener) {
        this.initialListener = initialListener;
    }

    // Methods

    @Override
    public void onRegistration(ObservableServiceRegistry registry, Class<?> category) throws Exception {

        if (registry == null) return;
        else if (getRegistry() != null && registry.contains(this)) registry.deregisterServiceProvider(this);
        else {
            try {
                // Remember registry
                setRegistry(registry);

                if (SSLContextFactory.getClientContext() == null) throw new IllegalStateException("SSLContext could not be initialized");

                // Connect to Server
                try {

                    setActiveConnection(new SocketConnection(SSLContextFactory.getClientContext(), new InetSocketAddress(
                            getRegistry().getServiceProvider(PreferencesService.class).getPreferences().get("Server_Address", InetAddress.getLocalHost().toString())
                            , NetworkConstants.SERVER_PORT), getInitialListener()));

                } catch (Exception e) {

                    logger.log(Level.FATAL, "Connection to server could not be established", e);
                    throw e;

                }

            } catch (Exception e) {

                throw e;

            }
        }
    }

    @Override
    public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) {
        getActiveConnection().close();
        setActiveConnection(null);
        setRegistry(null);
    }

    @Override
    public boolean addListener(NetworkServiceListener listener) {
        return getListeners().add(listener);
    }

    @Override
    public boolean removeListener(NetworkServiceListener listener) {
        return getListeners().remove(listener);
    }

    // Getter & Setter

    @Override
    public ObservableConnection getActiveConnection() {
        return activeConnection;
    }

    public Set<NetworkServiceListener> getListeners() {
        return listeners;
    }

    public ObservableServiceRegistry getRegistry() {
        return registry;
    }

    public void setRegistry(ObservableServiceRegistry registry) {
        this.registry = registry;
    }

    private void setActiveConnection(ObservableConnection activeConnection) {
        this.activeConnection = activeConnection;
    }

    public ConnectionListener getInitialListener() {
        return initialListener;
    }

}
