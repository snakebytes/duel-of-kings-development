package dok.client.services.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.services.PreferencesService;
import dok.commons.impl.FilePreferencesFactory;
import dok.commons.impl.ObservableServiceRegistry;

public class PreferencesProvider implements PreferencesService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public PreferencesProvider() throws BackingStoreException {
		System.setProperty("java.util.prefs.PreferencesFactory", FilePreferencesFactory.class.getName());
	    System.setProperty(FilePreferencesFactory.SYSTEM_PROPERTY_FILE, "data/app.config");

	    setPreferences(Preferences.userRoot());

	    logger.debug("There are " + preferences.keys().length + " preference keys:");
	    for (String s : preferences.keys())
	      logger.debug("preferences[" + s + "] = " + preferences.get(s, null));
	}

	// Attributes

	private final Set<PreferencesServiceListener> listeners = new HashSet<>();
	private ObservableServiceRegistry registry;
	private Preferences preferences;

	@Override
	public void onRegistration(ObservableServiceRegistry registry, Class<?> category) {
		// TODO Nothing
	}

	@Override
	public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) {
		// TODO Nothing
	}

	@Override
	public boolean addListener(PreferencesServiceListener listener) {
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(PreferencesServiceListener listener) {
		return getListeners().remove(listener);
	}

	@Override
	public Preferences getPreferences() {
		return preferences;
	}

	public Set<PreferencesServiceListener> getListeners() {
		return listeners;
	}

	public ObservableServiceRegistry getRegistry() {
		return registry;
	}

	public void setRegistry(ObservableServiceRegistry registry) {
		this.registry = registry;
	}

	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}

}
