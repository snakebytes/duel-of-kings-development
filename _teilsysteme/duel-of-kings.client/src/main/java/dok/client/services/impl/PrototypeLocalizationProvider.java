package dok.client.services.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.LocalizationService;

public class PrototypeLocalizationProvider implements LocalizationService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger(PrototypeLocalizationProvider.class.getName());
	public static final String UNDEFINED = "?";

	// Constants

	private final ResourceBundle resourceBundle;

	public PrototypeLocalizationProvider() throws IOException {
		resourceBundle = ResourceBundle.getBundle("languages.Lang");
	}

	// Getter & Setter

	@Override
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	@Override
	public String getText(String key, Object[] parameters) throws NullPointerException, MissingResourceException, ClassCastException, IllegalArgumentException {
		logger.entry(key, parameters);
		try {
			return logger.exit(MessageFormat.format(getResourceBundle().getString(key), parameters));
		} catch (Exception e) {
			logger.catching(Level.DEBUG, e);
		}
		return logger.exit(UNDEFINED);
	}

	@Override
	public String getText(String key) {
		return getText(key, null);
	}

}
