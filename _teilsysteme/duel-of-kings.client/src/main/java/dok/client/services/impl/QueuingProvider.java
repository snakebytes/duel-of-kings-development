package dok.client.services.impl;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.client.services.GameService;
import dok.client.services.NetworkService;
import dok.client.services.QueuingService;
import dok.commons.impl.ObservableServiceRegistry;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ObservableConnection;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.game.model.GameType;
import dok.game.network.model.QueuingPacket;
import dok.game.network.model.QueuingPacket.QueuingPacketType;
import dok.game.network.model.QueuingPacket.QueuingResult;
import dok.game.network.model.StartGamePacket;

public class QueuingProvider implements QueuingService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constants

	private final Set<QueuingListener> listeners = new HashSet<>();
	private final StopWatch queueTimer = new StopWatch();
	private final GameService gameService;


	// Attributes

	private ObservableServiceRegistry registry;
	private QueuingStatus status;

	// Constructor(s)

	public QueuingProvider(GameService gameService) {
		super();
		this.gameService = gameService;
		status = QueuingStatus.IDLE;
	}

	// Methods

	// <--- Connection Listener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		logger.entry(connectionEvent);
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;
			if (packetReceivedEvent.getPacket() instanceof QueuingPacket) {
				QueuingPacket queuingPacket = (QueuingPacket) packetReceivedEvent.getPacket();
				if (queuingPacket.getPacketType() == QueuingPacketType.RESULT) {
					if (getStatus() == QueuingStatus.ATTEMPTING) {
						if (queuingPacket.getQueuingResult() == QueuingResult.SUCCESSFULLY_ENTERED) {
							setStatus(QueuingStatus.ACTIVE);
							getQueueTimer().start();
						} else setStatus(QueuingStatus.IDLE);
						new ArrayList<>(getListeners()).forEach((listener) -> listener.handleQueuingResult(queuingPacket.getQueuingResult()));
					} else if (getStatus() == QueuingStatus.ACTIVE) {
						if (queuingPacket.getQueuingResult() == QueuingResult.SUCCESSFULLY_LEFT) {
							setStatus(QueuingStatus.IDLE);
							getQueueTimer().stop();
							getQueueTimer().reset();
						} else
							logger.debug("Queue could not be left");
					}
				}
			} else if (packetReceivedEvent.getPacket() instanceof StartGamePacket) {
				StartGamePacket startGamePacket = (StartGamePacket) packetReceivedEvent.getPacket();

				// Start the player side of the game according to game type
				switch (startGamePacket.getGameType()) {
					case Duel:
						getGameService().startDuelGame(startGamePacket.getPlayers(), startGamePacket.getPlayer());
						break;
				}

				// Change Queuing Status
				if (getStatus() == QueuingStatus.ACTIVE) {
					setStatus(QueuingStatus.IDLE);
					getQueueTimer().stop();
					getQueueTimer().reset();
				}

			}
		}
		logger.exit();
	}

	// <--- Registerable Service --->

	@Override
	public void onRegistration(ObservableServiceRegistry registry, Class<?> category) {
		if (registry == null) return;
		else if (getRegistry() != null && registry.contains(this)) registry.deregisterServiceProvider(this);
		else {
			try {
				((ObservableConnection) registry.getServiceProvider(NetworkService.class).getActiveConnection()).addListener(this);
			} catch (NoSuchElementException e) {
				logger.catching(e);
			}
			setRegistry(registry);
		}
	}

	@Override
	public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) {
		if (!getRegistry().equals(registry)) return;
		else {
			// TODO: Handle queuing status
			try {
				((ObservableConnection) registry.getServiceProvider(NetworkService.class).getActiveConnection()).removeListener(this);
			} catch (NoSuchElementException e) {
				logger.catching(e);
			}
			setRegistry(null);
		}
	}

	// <--- QueuingService --->

	@Override
	public Duration getTimeInQueue() {
		return Duration.ofMillis(getQueueTimer().getNanoTime() / 1000000);
	}

	@Override
	public void queueUp(GameType gameType, long lineupId) throws IllegalStateException {
		try {
			if (getStatus() != QueuingStatus.IDLE) throw new IllegalStateException();
			getConnection().sendPacket(new QueuingPacket(QueuingPacketType.JOIN, gameType, lineupId));
			setStatus(QueuingStatus.ATTEMPTING);
		} catch (IOException e) {
			logger.catching(e);
			// TODO What happens, if the packet could not be sent
		} catch (NullPointerException e) {
			// TODO What happens if no connection is present
			logger.catching(e);
		}
	}

	@Override
	public void leaveQueue() throws IllegalStateException {
		try {
			if (!(getStatus() == QueuingStatus.ACTIVE || getStatus() == QueuingStatus.ATTEMPTING)) throw new IllegalStateException();
			getConnection().sendPacket(new QueuingPacket(QueuingPacketType.LEAVE));
		} catch (IOException e) {
			logger.catching(e);
			// TODO What happens, if the packet could not be sent
		} catch (NullPointerException e) {
			// TODO What happens if no connection is present
			logger.catching(e);
		}
	}

	// <--- CustomService --->

	@Override
	public boolean addListener(QueuingListener listener) {
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(QueuingListener listener) {
		return getListeners().remove(listener);
	}

	// Getter & Setter

	private Connection getConnection() {
		if (getRegistry() == null) return null;
		else try {
			return getRegistry().getServiceProvider(NetworkService.class).getActiveConnection();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public Set<QueuingListener> getListeners() {
		return listeners;
	}

	public StopWatch getQueueTimer() {
		return queueTimer;
	}

	public ObservableServiceRegistry getRegistry() {
		return registry;
	}

	private void setRegistry(ObservableServiceRegistry registry) {
		this.registry = registry;
	}

	public QueuingStatus getStatus() {
		return status;
	}

	private void setStatus(QueuingStatus status) {
		QueuingStatus oldStatus = this.status;
		this.status = status;
		new ArrayList<>(getListeners()).forEach((listener) -> listener.handleQueueStatusChange(oldStatus, status));
	}

	public GameService getGameService() {
		return gameService;
	}

}
