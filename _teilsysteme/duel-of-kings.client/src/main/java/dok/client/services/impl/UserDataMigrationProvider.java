package dok.client.services.impl;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.internal.util.jdbc.DriverDataSource;
import org.hibernate.boot.Metadata;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

public class UserDataMigrationProvider implements Integrator {

	private static final Logger logger = LogManager.getLogger();

	@Override
	public void integrate(Metadata metadata, SessionFactoryImplementor sessionFactory,
			SessionFactoryServiceRegistry serviceRegistry) {
		logger.info("Starting userData migration");

		Properties properties = ((SessionFactoryImpl) sessionFactory).getProperties();

		Flyway flyway = new Flyway();
		flyway.setDataSource(new DriverDataSource(getClass().getClassLoader(), "org.hsqldb.jdbc.JDBCDriver", (String) properties.get("hibernate.connection.url"), (String) properties.get("hibernate.connection.username"), (String) properties.get("hibernate.connection.password")));
		flyway.setLocations("/database.schema.migration.userData");

		MigrationInfo current = flyway.info().current();
		if (current == null) {
			logger.info("No existing DB found");
		}
		else {
			logger.info("Current userData version is {}", current.getVersion());
		}

		flyway.migrate();

		logger.info("Completed userData migration to version {}", flyway.info().current().getVersion());

	}

	@Override
	public void disintegrate(SessionFactoryImplementor sessionFactory, SessionFactoryServiceRegistry serviceRegistry) {
		// Do nothing
	}

}
