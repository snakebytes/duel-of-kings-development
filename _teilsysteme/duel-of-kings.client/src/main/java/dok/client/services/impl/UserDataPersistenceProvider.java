package dok.client.services.impl;

import java.io.File;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.hibernate.tool.schema.TargetType;

import dok.client.services.UserDataPersistenceService;
import dok.commons.impl.ObservableServiceRegistry;

/**
 * Implementation of the {@linkplain UserDataPersistenceService} interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
public class UserDataPersistenceProvider implements UserDataPersistenceService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private final List<ClientUserDataPersistenceServiceListener> listeners = new LinkedList<>();
	private final Map<Thread, Session> sessionPool = new HashMap<>();
	private SessionFactory sessionFactory;

	// Constructor(s)

	public UserDataPersistenceProvider() {
		// Do nothing
	}

	// Methods

	@Override
	public Session openSession() throws Exception {
		Session result;
		if ((result = getSessionPool().get(Thread.currentThread())) != null) return result;
		else {
			try {
				result = getSessionFactory().openSession();
				getSessionPool().put(Thread.currentThread(), result);
				return result;
			} catch (Exception e) {
				logger.log(Level.WARN, "New Session could not be opened", e);
				throw e;
			}
		}
	}

	@Override
	public void closeCurrentSession() {
		Session session = getSessionPool().get(Thread.currentThread());
		if (session == null) return;
		try {
			session.clear();
			session.close();
		} catch (Exception e) {
			logger.log(Level.WARN, "Session could not be closed", e);
		} finally {
			getSessionPool().remove(Thread.currentThread());
		}
	}

	// <--- ObservableRegisterableService --->

	@Override
	public boolean addListener(ClientUserDataPersistenceServiceListener listener) {
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(ClientUserDataPersistenceServiceListener listener) {
		return getListeners().remove(listener);
	}

	// Events

	@Override
	public void onRegistration(ObservableServiceRegistry registry, Class<?> category) throws Exception {

		BootstrapServiceRegistry bootstrapServiceRegistry =
				new BootstrapServiceRegistryBuilder()
				.applyIntegrator(new UserDataMigrationProvider())
				.build();

		StandardServiceRegistry standardRegistry =
				new StandardServiceRegistryBuilder(bootstrapServiceRegistry)
				.configure("database/userData.hibernate.cfg.xml")
				.build();

		Metadata metadata = new MetadataSources(standardRegistry)
				.getMetadataBuilder()
				.applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)
				.build();

		logger.info("Attempting to export schema ...");

		// Export Create
		File file = new File("data/schema/generated/create/userData.sql");
		if (file.exists()) file.delete();
		SchemaExport create = new SchemaExport()
				.setOutputFile(file.toString())
				.setFormat(true)
				.setDelimiter(";")
				.setHaltOnError(true);
		create.createOnly(EnumSet.of(TargetType.SCRIPT), metadata);

		// Export Update
		file = new File("data/schema/generated/update/userData.sql");
		if (file.exists()) file.delete();
		SchemaUpdate update = new SchemaUpdate()
		.setOutputFile(file.toString())
		.setFormat(true)
		.setDelimiter(";")
		.setHaltOnError(true);
		update.execute(EnumSet.of(TargetType.SCRIPT), metadata);

		logger.info("Schema has been exported ...");

		sessionFactory = metadata.getSessionFactoryBuilder().build();
	}

	@Override
	public void onDeregistration(ObservableServiceRegistry registry, Class<?> category) throws Exception {
		sessionFactory.close();
	}

	// Getter & Setter

	private List<ClientUserDataPersistenceServiceListener> getListeners() {
		return listeners;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Map<Thread, Session> getSessionPool() {
		return sessionPool;
	}

}
