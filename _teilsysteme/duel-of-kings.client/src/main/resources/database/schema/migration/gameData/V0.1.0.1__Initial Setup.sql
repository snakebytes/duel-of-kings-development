create sequence hibernate_sequence start with 1 increment by 1;

    create table ABILITY_CLASS_IDS (
        ability_class_id integer not null,
        abilityClassIds integer
    );

    create table ABILITY_CLASS_PROPERTIES (
        mapOwner integer not null,
        property_value double,
        properties_KEY varchar(255) not null,
        primary key (mapOwner, properties_KEY)
    );

    create table AbilityClass (
        id integer not null,
        abilityEffectClass varchar(255),
        timeCountersCost integer not null,
        hitFilter_hitfilter_id bigint,
        targetFilter_id bigint,
        primary key (id)
    );

    create table BASE_ATTRIBUTES_MAP (
        mapOwner integer not null,
        attribute_value integer,
        baseAttributes_KEY varchar(255) not null,
        primary key (mapOwner, baseAttributes_KEY)
    );

    create table CHARACTER_CLASS (
        id integer not null,
        baseDamageType integer,
        baseHitFilter_hitfilter_id bigint,
        baseTargetFilter_id bigint,
        primary key (id)
    );

    create table CHARACTER_CLASS_EnhancementSlot (
        CharacterClass_id integer not null,
        enhancementSlots_id integer not null,
        primary key (CharacterClass_id, enhancementSlots_id)
    );

    create table CharacterClass_allowedEnhancementClassIds (
        CharacterClass_id integer not null,
        allowedEnhancementClassIds integer
    );

    create table CharacterEnhancementClass (
        id integer not null,
        rank integer not null,
        type integer,
        primary key (id)
    );

    create table CharacterEnhancementClass_abilityClassIds (
        CharacterEnhancementClass_id integer not null,
        abilityClassIds integer
    );

    create table ENHANCEMENT_CLASS_ATTRIBUTES_MAP (
        mapOwner integer not null,
        attribute_value integer,
        attributes_KEY varchar(255) not null,
        primary key (mapOwner, attributes_KEY)
    );

    create table EnhancementSlot (
        id integer not null,
        rank integer not null,
        type integer,
        primary key (id)
    );

    create table HIT_FILTER_LIST_FILTERS (
        filter bigint not null,
        filters integer
    );

    create table HitFilterList (
        hitfilter_id bigint not null,
        primary key (hitfilter_id)
    );

    create table TARGET_FILTER_LIST_FILTERS (
        filter bigint not null,
        filters integer
    );

    create table TargetFilterList (
        id bigint not null,
        primary key (id)
    );

    create table TRIGGER_CLASS_PROPERTIES (
        mapOwner integer not null,
        property_value varbinary(255),
        properties_KEY varchar(255) not null,
        primary key (mapOwner, properties_KEY)
    );

    create table TriggerClass (
        id integer not null,
        effectClass varchar(255),
        hitFilter varbinary(65535),
        reaction boolean not null,
        targetFilter varbinary(65535),
        primary key (id)
    );

    alter table ABILITY_CLASS_IDS
        add constraint FKijla8e26c7ne568fhqfk98lou
        foreign key (ability_class_id)
        references CHARACTER_CLASS;

    alter table ABILITY_CLASS_PROPERTIES
        add constraint FKr0813vnwbiikcwg5dims7ih8t
        foreign key (mapOwner)
        references AbilityClass;

    alter table AbilityClass
        add constraint FKqqtth4h6pnlhuqe08ggn51nho
        foreign key (hitFilter_hitfilter_id)
        references HitFilterList;

    alter table AbilityClass
        add constraint FK9r4eyens83u9in7rf5mcxbc2f
        foreign key (targetFilter_id)
        references TargetFilterList;

    alter table BASE_ATTRIBUTES_MAP
        add constraint FKbqxkhli4kvucui8gp8e2mqngr
        foreign key (mapOwner)
        references CHARACTER_CLASS;

    alter table CHARACTER_CLASS
        add constraint FK3vrq9ywfs8yv53r4y5s7u2sg4
        foreign key (baseHitFilter_hitfilter_id)
        references HitFilterList;

    alter table CHARACTER_CLASS
        add constraint FK34dvlnhg1yxo8tg4shvx2rdnl
        foreign key (baseTargetFilter_id)
        references TargetFilterList;

    alter table CHARACTER_CLASS_EnhancementSlot
        add constraint FKjqgllvo13d6wdmrjc6k8d1xmu
        foreign key (enhancementSlots_id)
        references EnhancementSlot;

    alter table CHARACTER_CLASS_EnhancementSlot
        add constraint FKe0xq86wq4nujvhs29l1ving0b
        foreign key (CharacterClass_id)
        references CHARACTER_CLASS;

    alter table CharacterClass_allowedEnhancementClassIds
        add constraint FKam6gjcco8xl0e8s9wyup1ffby
        foreign key (CharacterClass_id)
        references CHARACTER_CLASS;

    alter table CharacterEnhancementClass_abilityClassIds
        add constraint FKbdinw2n7kjhgi4o7yuvi959tb
        foreign key (CharacterEnhancementClass_id)
        references CharacterEnhancementClass;

    alter table ENHANCEMENT_CLASS_ATTRIBUTES_MAP
        add constraint FKlhu1ragc0d5nq4bkrt5b2301i
        foreign key (mapOwner)
        references CharacterEnhancementClass;

    alter table HIT_FILTER_LIST_FILTERS
        add constraint FKlvlbgteyabn6kss9ff5iyrm6f
        foreign key (filter)
        references HitFilterList;

    alter table TARGET_FILTER_LIST_FILTERS
        add constraint FKtq81v7ucvrbar7vf90iuc4f5y
        foreign key (filter)
        references TargetFilterList;

    alter table TRIGGER_CLASS_PROPERTIES
        add constraint FKigeqatij6ay0vhm4aeipkpb29
        foreign key (mapOwner)
        references TriggerClass;
