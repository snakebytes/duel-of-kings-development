package dok.client.services.impl;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class PrototypeLocalizationProviderTest {

	@Test
	public void testMethod() {
		PrototypeLocalizationProvider provider = null;
		try {
			provider = new PrototypeLocalizationProvider();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
		Assert.assertNotNull(provider);
		Assert.assertEquals("Fertig", provider.getText("GameView.CancelButton.Text"));
	}

}
