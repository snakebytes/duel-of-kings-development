package dok.commons.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Account implements Serializable {

    // Class Constants

    private static final long serialVersionUID = 8783407156484638507L;

    // Attributes

    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    private String email;
    private String passwordHash;

    @Enumerated
    private AccountStatus status;

    @Enumerated
    private AccountRole role;

	@ElementCollection
	@JoinTable(
			name="ACCOUNT_SETTINGS",
			joinColumns = @JoinColumn(name="mapOwner"))
	@Column(name="setting_value")
    private Map<String, String> settings = new HashMap<>();
    private Date bannedUntil;
    private int cashCurrencyBalance;
    private int ingameCurrencyBalance;

    // Constructor(s)

    public Account() {
        //do nothing
    }

    public Account(String name, String email, AccountRole role) {
        super();
        id = UUID.randomUUID();
        this.name = name;
        this.email = email;
        this.role = role;
    }

    // Methods

    @Override
    public boolean equals(Object other) {
        if(other == null || !(other instanceof Account)) return false;
        return this.getId().equals(((Account)other).getId());
    }

	@Override
	public int hashCode() {
		return new HashCodeBuilder(11,37)
				.append(getClass())
				.append(getId())
				.toHashCode();
	}

    @Override
    public String toString() {
    	return getClass().getSimpleName() + "{"
    			+ " id: " + getId()
    			+ " name: " + getName()
    			+ " email: " + getEmail()
    			+ " passwordHash: " + getPasswordHash()
    			+ " status: " + getStatus()
    			+ " role: " + getRole()
    			+ " settings: " + getSettings()
    			+ " bannedUntil: " + getBannedUntil()
    			+ " cashCurrencyBalance: " + getCashCurrencyBalance()
    			+ " ingameCurrencyBalance: " + getIngameCurrencyBalance()
    			+ " }";
    }


    // Getters & Setters

    public UUID getId() {
        return id;
    }

    public void setId(UUID newId) {
        this.id = newId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    protected void setSettings(Map<String, String> newSettings) {
    	this.settings.clear();
    	this.settings.putAll(newSettings);
    }

    public Date getBannedUntil() {
        return bannedUntil;
    }

    public void setBannedUntil(Date bannedUntil) {
        this.bannedUntil = bannedUntil;
    }

    public int getCashCurrencyBalance() {
        return cashCurrencyBalance;
    }

    public void setCashCurrencyBalance(int cashCurrencyBalance) {
        this.cashCurrencyBalance = cashCurrencyBalance;
    }

    public int getIngameCurrencyBalance() {
        return ingameCurrencyBalance;
    }

    public void setIngameCurrencyBalance(int ingameCurrencyBalance) {
        this.ingameCurrencyBalance = ingameCurrencyBalance;
    }
}
