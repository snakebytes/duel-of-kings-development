package dok.commons.model;

public enum AccountRole {
	PLAYER,
	MODERATOR,
	ADMIN,
	GUEST
}
