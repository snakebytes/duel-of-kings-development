package dok.commons.model;

public enum AccountStatus {
	PENDING,
	CONFIRMED
}
