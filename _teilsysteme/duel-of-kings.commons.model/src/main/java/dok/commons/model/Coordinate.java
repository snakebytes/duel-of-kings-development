package dok.commons.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;


@Entity
public class Coordinate implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -2739388610662436092L;

	// Attributes

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private int row;
	private int column;

	// Constructors

	public Coordinate() {
		// Do nothing
	}

	public Coordinate(int row, int col) {
		this.row = row;
		this.column = col;
	}

	public Coordinate(Pair<Integer, Integer> pair) {
		this.row = pair.getKey();
		this.column = pair.getValue();
	}

	// Methods

	public Pair<Integer, Integer> toPair() {
		return new MutablePair<Integer, Integer>(getRow(), getColumn());
	}

	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Coordinate)) return false;
		Coordinate otherC = (Coordinate) other;
		return getRow() == otherC.getRow() && getColumn() == otherC.getColumn();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(7,97)
				.append(getClass())
				.append(getRow())
				.append(getColumn())
				.toHashCode();
	}

	@Override
	public String toString() {
		return "Coordinate { Row: " + getRow() + ", Column: " + getColumn() + " }";
	}

	// Getters & Setters

	public int getRow() {
		return row;
	}

	protected void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	protected void setColumn(int column) {
		this.column = column;
	}

	public long getId() {
		return id;
	}

	protected void setId(long id) {
		this.id = id;
	}
}
