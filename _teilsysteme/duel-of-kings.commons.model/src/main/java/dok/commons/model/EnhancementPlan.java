package dok.commons.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyJoinColumn;

/**
 * Contains the enhancement types being used in each enhancement slot
 * of a single character in a {@linkplain Lineup}.
 *
 * @author Konstantin Schaper
 * @author Steffen M�ller
 * @since 0.2.6.1
 *
 */
@Entity
public class EnhancementPlan implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -8270272674614608603L;

	// Attributes

	@Id
	@GeneratedValue
	@Column(name = "enhancement_plan_id")
	private UUID id;

	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name = "enhancement_plans", joinColumns = @JoinColumn(name = "id"))
	@Column
	@MapKeyJoinColumn(name = "enhancement_plan_id")
	private Map<EnhancementSlot, Integer> enhancementMap = new HashMap<>();

	// Constructors

	/**
	 * Default constructor for java bean specification.
	 */
	public EnhancementPlan() {
		// do nothing
	}

	/**
	 * Initializes the created enhancement plan with the given values.
	 *
	 * @param values The mapping of enhancement slots to enhancement type ids
	 */
	public EnhancementPlan(Map<EnhancementSlot, Integer> values) {
		enhancementMap.putAll(values);
	}

	// Getters & Setters

	/**
	 * @return The mapping of enhancement slots to enhancement type ids
	 */
	public final Map<EnhancementSlot, Integer> getEnhancementMap() {
		return enhancementMap;
	}

	protected final void setEnhancementMap(Map<EnhancementSlot, Integer> enhancementMap) {
		this.enhancementMap = enhancementMap;
	}

}
