package dok.commons.model;

/**
 * The type of an {@linkplain EnhancementSlot} or CharacterEnhancementClass.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public enum EnhancementType {

	/**
	 * An enhancement slot of type 'rune' can only hold enhancements of the same type,
	 * an enhancement of type 'rune' only fits in enhancement slots of the same type.
	 */
	RUNE,

	/**
	 * An enhancement slot of type 'crystal' can only hold enhancements of the same type,
	 * an enhancement of type 'crystal' only fits in enhancement slots of the same type.
	 */
	CRYSTAL,

	/**
	 * An enhancement slot of type 'generic' can hold any type of enhancement,
	 * an enhancement of type 'generic' fits in any enhancement slot.
	 */
	GENERIC

}
