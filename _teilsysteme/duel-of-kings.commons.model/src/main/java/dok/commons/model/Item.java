package dok.commons.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Item implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -5318950731047791904L;

	// Attributes

	@Id
	private long id;
	private String name;

	// Constructor(s)

	public Item() {
		// Do nothing
	}

	// Methods

	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Item)) return false;
		return this.getId() == ((Item)other).getId();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(71,3)
				.append(getClass())
				.append(getId())
				.toHashCode();
	}

	// Getters & Setters

	public long getId() {
		return id;
	}
	protected void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



}
