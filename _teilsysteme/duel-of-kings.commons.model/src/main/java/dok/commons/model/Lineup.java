package dok.commons.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.Pair;

@Entity
public class Lineup implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 4328629806315704704L;

	// Attributes


	@Id
	@GeneratedValue
	private long id;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "lineup_character_class_ids", joinColumns = @JoinColumn(name = "join_table_id"))
	@MapKeyColumn(name = "coordinate")
	private Map<Coordinate, Integer> characterClassIds = new HashMap<>();

	@ManyToMany(fetch=FetchType.EAGER, cascade={CascadeType.MERGE})
	@JoinTable(name="lineup_enhancements",
	    joinColumns=@JoinColumn(name="lineup_enhancements_id"),
	    inverseJoinColumns=@JoinColumn(name="enhancements", unique=false))
	@MapKeyJoinColumn(name="coordinate")
	private Map<Coordinate, EnhancementPlan> enhancements = new HashMap<>();

	@OneToOne(cascade={CascadeType.MERGE})
	private Coordinate kingPosition;
	private String imageKey;
	private String title;
	private String description;
	private UUID accountId;

	// Constructor(s)

	public Lineup() {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				enhancements.put(new Coordinate(i, j), new EnhancementPlan());
			}
		}
	}

	public Lineup(long id) {
		this();
		setId(id);
	}

	public Lineup(long id, String imageKey) {
		this(id);
		setImageKey(imageKey);
	}

	public Lineup(long id, String imageKey, String title) {
		this(id, imageKey);
		setTitle(title);
	}

	public Lineup(long id, String imageKey, String title, String description) {
		this(id, imageKey, title);
		setDescription(description);
	}

	public Lineup(Lineup other) {
		this();
		this.id = other.getId();
		this.characterClassIds.putAll(other.getCharacterClassIds());
		this.enhancements.putAll(other.getEnhancementPlans());
		this.kingPosition = other.getKingPosition();
		this.imageKey = other.getImageKey();
		this.title = other.getTitle();
		this.description = other.getDescription();
		this.accountId = other.getAccountId();
	}

	// Methods

	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "id: " + getId()
				+ ", title: " + getTitle()
				+ ", imageKey: " + getImageKey()
				+ ", characterClassIds: " + getCharacterClassIds()
				+ ", enhancementPlans: " + getEnhancementPlans()
				+ "}";
	};

	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Lineup)) return false;
		return this.getId() == ((Lineup)other).getId();
	}

	public int hashCode() {
		return new HashCodeBuilder(67,29)
				.append(getClass())
				.append(getId())
				.toHashCode();
	}

	// Getter & Setter

	public String getImageKey() {
		return imageKey;
	}

	public Integer getRowCount() {
		Integer result = -1;
		for (Coordinate pos : getCharacterClassIds().keySet())
			if (result < pos.toPair().getKey()) result = pos.toPair().getKey();
		return (Integer) (result + 1);
	}

	public Integer getColCount() {
		Integer result = -1;
		for (Coordinate pos : getCharacterClassIds().keySet())
			if (result < pos.toPair().getValue()) result = pos.toPair().getValue();
		return (Integer) (result + 1);
	}

	public int getCharacterClassIdAt(Pair<Integer, Integer> pos) {
		return getCharacterClassIds().containsKey(new Coordinate(pos)) ? getCharacterClassIds().get(new Coordinate(pos)) : -1;
	}

//	public int getAbilityClassIdAt(Pair<Integer, Integer> pos) {
//		return getAbilityClassIds().containsKey(new Coordinate(pos)) ? getAbilityClassIds().get(pos) : -1;
//	}

	public Map<Coordinate, Integer> getCharacterClassIds() {
		return characterClassIds;
	}

	protected void setCharacterClassIds(Map<Coordinate, Integer> newMap) {
		characterClassIds = newMap;
	}

//	public Map<Pair<Integer, Integer>, Integer> getAbilityClassIds() {
//		return abilityClassIds;
//	}

	public Coordinate getKingPosition() {
		return kingPosition;
	}

	public void setKingPosition(Coordinate kingPosition) {
		this.kingPosition = kingPosition;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setImageKey(String imageKey) {
		this.imageKey = imageKey;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UUID getAccountId() {
		return accountId;
	}

	public void setAccountId(UUID accountId) {
		this.accountId = accountId;
	}

	public Map<Coordinate, EnhancementPlan> getEnhancementPlans() {
		return enhancements;
	}

	public void setEnhancements(Map<Coordinate, EnhancementPlan> enhancements) {
		this.enhancements = enhancements;
	}

}
