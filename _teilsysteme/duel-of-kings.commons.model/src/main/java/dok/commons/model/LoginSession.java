package dok.commons.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class LoginSession implements Serializable {

    // Class Constants

    private static final long serialVersionUID = 5360859343114185251L;

    // Attributes

    @Id
    private UUID sessionId;

    private UUID accountId;
    private Date loggedIn;
    private Date loggedOut = null;

    @Enumerated
    private LoginMethod loginMethod;

    // Constructors

    public LoginSession() {
        //do nothing
    }

    public LoginSession(UUID accountId, Date loggedInTime, LoginMethod method) {
    	sessionId = UUID.randomUUID();
    	this.accountId = accountId;
    	this.loggedIn = loggedInTime;
    	this.setLoginMethod(method);
    }

    // Methods

    @Override
    public boolean equals(Object other) {
        if(other == null || !(other instanceof LoginSession)) return false;
        return this.getSessionId().equals(((LoginSession)other).getSessionId());
    }

    public int hashCode() {
        return new HashCodeBuilder(61,17)
                .append(getClass())
                .append(getSessionId())
                .toHashCode();
    }

    // Getters & Setters

    public Date getLoggedIn() {
        return loggedIn;
    }
    public void setLoggedIn(Date loggedIn) {
        this.loggedIn = loggedIn;
    }
    public Date getLoggedOut() {
        return loggedOut;
    }
    public void setLoggedOut(Date loggedOut) {
        this.loggedOut = loggedOut;
    }
    public UUID getAccountId() {
        return accountId;
    }
    protected void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }
    public UUID getSessionId() {
        return sessionId;
    }
    protected void setSessionId(UUID id) {
        this.sessionId = id;
    }
	public LoginMethod getLoginMethod() {
		return loginMethod;
	}
	protected void setLoginMethod(LoginMethod loginMethod) {
		this.loginMethod = loginMethod;
	}

}
