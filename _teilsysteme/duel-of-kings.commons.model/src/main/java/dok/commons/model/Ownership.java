package dok.commons.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * This type displays the many-to-many-relationship from Items (available at the shop) to Accounts. It further provides the Purchase which granted ownership, if there is one.
 * @author Steffen M�ller
 */

@Entity
public class Ownership implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -389260144191499099L;

	// Constructor(s)

	public Ownership() {
		// Do nothing
	}

	// Attributes

	@Id
	private UUID id;

	private UUID accountId;

	@OneToOne
	private Item item;

	@OneToOne
	private Purchase purchase;

	// Methods

	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Ownership)) return false;
		return this.getId().equals(((Ownership)other).getId());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(67,13)
				.append(getClass())
				.append(getId())
				.toHashCode();
	}

	// Getters & Setters

	public UUID getAccountId() {
		return accountId;
	}
	protected void setAccountId(UUID accountId) {
		this.accountId = accountId;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public Purchase getPurchase() {
		return purchase;
	}
	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}
	public UUID getId() {
		return id;
	}
	protected void setId(UUID id) {
		this.id = id;
	}


}
