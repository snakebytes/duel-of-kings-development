package dok.commons.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * This class presents an exchange of a certain amount of ingame-currency and an Item. It only holds one account-Id and assumes that that account is loosing currency and gaining the item.
 * @author Steffen M�ller
 */

@Entity
public class Purchase implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 7175030729805407933L;

	// Constructor(s)

	public Purchase() {
		// Do nothing
	}

	// Attributes

	@Id
	private UUID id;

	private UUID accountId;
	private float amount;

	@OneToOne
	private Item item;

	@Enumerated
	private IngameCurrency currency;
	private Date date;

	// Methods

	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Purchase)) return false;
		return this.getId().equals(((Purchase)other).getId());
	}

	public int hashCode() {
		return new HashCodeBuilder(67,7)
				.append(getClass())
				.append(getId())
				.toHashCode();
	}

	// Getters & Setters

	public UUID getAccountId() {
		return accountId;
	}
	protected void setAccountId(UUID accountId) {
		this.accountId = accountId;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public IngameCurrency getCurrency() {
		return currency;
	}
	public void setCurrency(IngameCurrency currency) {
		this.currency = currency;
	}
	public UUID getId() {
		return id;
	}
	protected void setId(UUID id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}


}
