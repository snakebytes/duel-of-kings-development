package dok.commons.model;

/**
 *	Http-equivalent status codes for server response packets.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
public enum StatusCode {

	// Enum Values

	/**
	 * No status code has been supplied.
	 */
	UNDEFINED(-1),

	/**
	 * Request was successful.
	 */
	OK(200),

	/**
	 * The request has been fulfilled and resulted in a new resource being created.
	 */
	CREATED(201),

	/**
	 * The request has been accepted for processing, but the processing has not been completed.<br>
	 * The request might or might not eventually be acted upon, as it might be disallowed when processing actually takes place.
	 */
	ACCEPTED(202),

	/**
	 * The server cannot or will not process the request due to something that is perceived to be a client error.
	 */
	BAD_REQUEST(400),

	/**
	 * Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided.
	 */
	UNAUTHORIZED(401),

	/**
	 * The request was a valid request, but the server is refusing to respond to it.<br>
	 * The client does not have the necessary permission to access the requested resource/functionality.
	 */
	FORBIDDEN(403),

	/**
	 * The requested resource could not be found but may be available again in the future.<br>
	 * Subsequent requests by the client are permissible.
	 */
	NOT_FOUND(404),

	/**
	 * The requested resource may not be retrieved, or is already in use.
	 */
	LOCKED(423),

	/**
	 * The user has sent too many requests in a given amount of time.
	 */
	TOO_MANY_REQUESTS(429),

	/**
	 * A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
	 */
	INTERNAL_SERVER_ERROR(500),

	/**
	 * The server either does not recognize the request method, or it lacks the ability to fulfill the request.<br>
	 * Usually this implies future availability.
	 */
	NOT_IMPLEMENTED(501),

	/**
	 * The resource/service is currently unavailable (because it is overloaded or down for maintenance).<br>
	 * This is generally a temporary state.
	 */
	SERVICE_UNAVAILABLE(503);

	// Attributes

	private final int code;

	// Constructor

	private StatusCode(int code) {
		this.code = code;
	}

	// Getter & Setter

	public int getCode() {
		return code;
	}

}
