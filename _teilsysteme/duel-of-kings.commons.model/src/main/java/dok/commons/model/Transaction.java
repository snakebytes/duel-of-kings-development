package dok.commons.model;

import java.io.Serializable;
import java.util.Currency;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class Transaction implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 4706787081839489959L;

	// Attributes

	@Id
	private UUID id;

	private UUID accountId;
	private float price;
	private String currency;
	private Date date;

	// Constructor(s)

	public Transaction() {
		// Do nothing
	}

	// Methods

	@Override
	public boolean equals(Object other) {
		if(other == null || !(other instanceof Transaction)) return false;
		return this.getId().equals(((Transaction)other).getId());
	}

	public int hashCode() {
		return new HashCodeBuilder(29,17)
				.append(getClass())
				.append(getId())
				.toHashCode();
	}

	// Getters & Setters

	public UUID getAccountId() {
		return accountId;
	}
	protected void setAccountId(UUID accountId) {
		this.accountId = accountId;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Currency getCurrency() {
		return Currency.getInstance(currency);
	}

	public void setCurrency(Currency currency) {
		this.currency = currency.getCurrencyCode();
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public UUID getId() {
		return id;
	}
	protected  void setId(UUID id) {
		this.id = id;
	}



}
