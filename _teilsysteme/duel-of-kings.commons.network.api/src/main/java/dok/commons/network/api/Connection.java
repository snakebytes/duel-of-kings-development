package dok.commons.network.api;

import java.io.IOException;

/**
 * Responsible for managing a specific network connection.<br>
 * This includes sending and receiving {@link Packet}s and informing {@link ConnectionListener}s.<br>
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface Connection extends Runnable {

	/**
	 * Sends a packet to the destination affiliated with this
	 * connection.
	 *
	 * @param packet The packet to be sent
	 * @throws IOException If the packet could not be sent
	 */
	public void sendPacket(Packet packet) throws IOException;

	/**
	 * Starts reading packets from the sockets input stream.<br>
	 * Blocks until the Connection is closed or an exception occurs.
	 *
	 * @throws IOException When an error occurs while reading from the underlying stream
	 * @throws ClassNotFoundException When a packet to be read could not be deserialized
	 */
	public void readPackets() throws ClassNotFoundException, IOException;

	/**
	 * Tells the connection to close.
	 *
	 * @return Whether the connection has been successfully closed
	 */
	public boolean close();

	/**
	 * @return If the connection is currently tied to a running network socket.
	 */
	public boolean isConnected();

	/**
	 * @return The number of packets successfully sent through this connection
	 */
	public int getPacketsSent();

	/**
	 * @return The number of packets successfully received and processed by this connection
	 */
	public int getPacketsReceived();
}
