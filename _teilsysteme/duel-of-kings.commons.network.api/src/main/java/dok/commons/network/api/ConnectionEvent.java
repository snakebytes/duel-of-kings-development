package dok.commons.network.api;

import dok.commons.CustomEvent;

/**
 * Super interface for every event being fired by {@linkplain Connection}s.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface ConnectionEvent extends CustomEvent {

	/**
	 * @return The connection this event has been fired on
	 */
	public Connection getSource();

}
