package dok.commons.network.api;

/**
 * Functional interface for handling {@link ConnectionEvent}s fired by
 * {@link Connection}s.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface ConnectionListener {

	/**
	 * Called on the implementing class when the specified
	 * event is fired on the connection to which
	 * this listener has been added.
	 *
	 * @param connectionEvent The event which has been fired
	 */
	public void handle(ConnectionEvent connectionEvent);


}
