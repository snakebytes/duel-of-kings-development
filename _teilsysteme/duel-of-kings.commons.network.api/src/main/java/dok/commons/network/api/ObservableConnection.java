package dok.commons.network.api;

public interface ObservableConnection extends Connection {

	/**
	 * Registers a listener for all upcoming {@linkplain ConnectionEvent}s
	 * fired by this Connection.
	 *
	 * @param listener The listener to add
	 * @return Whether the listener has been added
	 */
	public boolean addListener(ConnectionListener listener);

	/**
	 * Unregisters a previously registered listener.
	 *
	 * @param listener The listener to remove
	 * @return Whether the listener was registered and has been successfully unregistered
	 */
	public boolean removeListener(ConnectionListener listener);

}
