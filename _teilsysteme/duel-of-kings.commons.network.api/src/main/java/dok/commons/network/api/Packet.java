package dok.commons.network.api;

import java.io.Serializable;

/**
 * Super interface for all information containers sent
 * and received by {@linkplain Connection}s.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface Packet extends Serializable {

	/**
	 * The source is defined by the receiving connection
	 * and does not change.
	 *
	 * @return The connection this packet originated from
	 */
	public Connection getSource();

	/**
	 * Called by the receiving connection to
	 * define the source of a packet.<br>
	 * Called by the sending connection to
	 * reset any previously defined source connection.<br>
	 * <br>
	 * This method is part of an <b> Internal API:</b> <i>It should not be called by developers.</i>
	 */
	public void setSource(Connection source);

}
