package dok.commons.network.api.events;

import dok.commons.network.api.ConnectionEvent;

/**
 * Tagging interface.
 *
 * @author Konstantin Schaper
 */
public interface ConnectionEstablishedEvent extends ConnectionEvent {

}
