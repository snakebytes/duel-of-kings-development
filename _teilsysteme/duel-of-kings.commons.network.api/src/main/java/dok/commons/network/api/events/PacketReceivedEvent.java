package dok.commons.network.api.events;

import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.Packet;

/**
 * Interface for events fired when a Packet has been received.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface PacketReceivedEvent extends ConnectionEvent {

	/**
	 * Returns the Packet this event contains
	 */
	public Packet getPacket();

}
