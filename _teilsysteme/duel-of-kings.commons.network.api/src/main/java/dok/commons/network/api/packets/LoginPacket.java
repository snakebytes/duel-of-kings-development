package dok.commons.network.api.packets;

import dok.commons.network.api.Packet;

public interface LoginPacket extends Packet {

	public String getAccountName();
	public String getAccountPassword();

}
