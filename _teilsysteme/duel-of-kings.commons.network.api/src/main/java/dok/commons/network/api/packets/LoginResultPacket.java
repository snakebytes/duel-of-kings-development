package dok.commons.network.api.packets;

import dok.commons.model.LoginSession;
import dok.commons.model.StatusCode;
import dok.commons.network.api.Packet;

public interface LoginResultPacket extends Packet {

	/**
	 * The result of the Login.
	 *
	 * @return The successful login session
	 */
	public LoginSession getLoginSession();

	/**
	 * The {@linkplain StatusCode} associated with this result packet.
	 *
	 * @return The predefined result code.
	 */
	public StatusCode getStatusCode();

}
