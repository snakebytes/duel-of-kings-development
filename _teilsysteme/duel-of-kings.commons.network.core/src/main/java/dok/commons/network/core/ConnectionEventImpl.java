package dok.commons.network.core;

import dok.commons.impl.CustomEventImpl;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;

public abstract class ConnectionEventImpl extends CustomEventImpl implements ConnectionEvent {

	//Attributes

	private final Connection source;

	//Constructor(s)

	public ConnectionEventImpl(Connection source) {
		this.source = source;
	}

	//Getter & Setter

	public final Connection getSource() {
		return source;
	}

}