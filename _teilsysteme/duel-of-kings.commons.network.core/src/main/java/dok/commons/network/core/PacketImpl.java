package dok.commons.network.core;

import dok.commons.network.api.Connection;
import dok.commons.network.api.Packet;

/**
 * General implementation of the {@linkplain Packet} interface.<br>
 * Should be subclassed to specify the information sent.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public abstract class PacketImpl implements Packet {

	// Class Constants

	private static final long serialVersionUID = -8734483796691512001L;

	// Attributes

	private Connection source;

	// Constructor(s)

	public PacketImpl() {
		super();
	}

	// Getters & Setters

	@Override
	public final Connection getSource() {
		return source;
	}

	@Override
	public void setSource(Connection source) {
		this.source = source;
	}

}