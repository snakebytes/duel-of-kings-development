package dok.commons.network.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.HandshakeCompletedEvent;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.ObservableConnection;
import dok.commons.network.api.Packet;
import dok.commons.network.core.events.ConnectionClosedEventImpl;
import dok.commons.network.core.events.ConnectionEstablishedEventImpl;
import dok.commons.network.core.events.PacketReceivedEventImpl;

public class SocketConnection implements ObservableConnection, HandshakeCompletedListener, Runnable {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private final SSLSocket socket;
	private ObjectInputStream inputStream;
	private ObjectOutputStream outputStream;
	private final Set<ConnectionListener> listeners = new HashSet<ConnectionListener>();
	private int packetsSent;
	private int packetsReceived;

	// Constructor(s)

	public SocketConnection(SSLSocket s) throws IOException
	{
		logger.entry(s);

		this.socket = s;

		getSocket().setEnabledCipherSuites(getSocket().getSupportedCipherSuites());
		getSocket().setTcpNoDelay(true);
		setOutputStream(new ObjectOutputStream(new BufferedOutputStream(getSocket().getOutputStream())));
		setInputStream(new ObjectInputStream(new BufferedInputStream(getSocket().getInputStream())));
		fireEvent(new ConnectionEstablishedEventImpl(this));

		logger.exit(this);
	}

	public SocketConnection(SSLContext context, InetSocketAddress address) throws IOException {
		logger.entry(address);

		// Create Socket
		logger.info("Attempting to connect to " + address);
		socket = (SSLSocket) context.getSocketFactory().createSocket(address.getAddress(), address.getPort());
		socket.addHandshakeCompletedListener(this);
		socket.setKeepAlive(true);
		socket.setEnableSessionCreation(true);
		socket.setTcpNoDelay(true);

		// Start the SSL Handshake
		socket.startHandshake();

		logger.exit(this);
	}

	public SocketConnection(SSLContext context, InetSocketAddress address, ConnectionListener... listeners) throws IOException {
		logger.entry(address, listeners);

		// Create Socket
		logger.info("Attempting to connect to " + address);
		socket = (SSLSocket) context.getSocketFactory().createSocket(address.getAddress(), address.getPort());
		socket.addHandshakeCompletedListener(this);
		socket.setKeepAlive(true);
		socket.setEnableSessionCreation(true);
		socket.setEnabledCipherSuites(socket.getSupportedCipherSuites());
		socket.setTcpNoDelay(true);

		// Initialize listeners
		Arrays.asList(listeners).forEach((listener) -> addListener(listener));

		// Start the SSL Handshake
		socket.startHandshake();

		logger.exit(this);
	}

	public SocketConnection(InetSocketAddress inetSocketAddress, ConnectionListener initialListener) throws IOException {
		// Create Socket
		logger.info("Attempting to connect to " + inetSocketAddress);
		socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(inetSocketAddress.getAddress(), inetSocketAddress.getPort());
		socket.addHandshakeCompletedListener(this);
		socket.setKeepAlive(true);
		socket.setEnableSessionCreation(true);
		socket.setEnabledCipherSuites(socket.getSupportedCipherSuites());
		socket.setTcpNoDelay(true);

		// Initialize listeners
		addListener(initialListener);

		// Start the SSL Handshake
		socket.startHandshake();

		logger.exit(this);
	}

	// Methods

	@Override
	public void sendPacket(Packet packet) throws IOException {
		if (getOutputStream() == null) setOutputStream(new ObjectOutputStream(new BufferedOutputStream(getSocket().getOutputStream())));
		packet.setSource(null);
		getOutputStream().reset();
		getOutputStream().writeObject(packet);
		getOutputStream().flush();
		packetsSent++;
		logger.info("A Packet has been successfully sent (" + packet + ")");
	}

	@Override
	public boolean addListener(ConnectionListener listener) {
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(ConnectionListener listener) {
		return getListeners().remove(listener);
	}

	@Override
	public void run() {
		while (!Thread.interrupted())
			try {
				readPackets();
			} catch (SocketException | EOFException e) {
				logger.catching(Level.DEBUG, e);
				close();
				break;
			} catch (Exception e) {
				logger.catching(Level.DEBUG, e);
				close();
				break;
			}
	}

	public void readPackets() throws ClassNotFoundException, IOException {
		if (getInputStream() == null) setInputStream(new ObjectInputStream(new BufferedInputStream(getSocket().getInputStream())));
		Object obj = null;
		while ((obj = getInputStream().readObject()) != null) {
			logger.debug("Receiving Object: " + obj);
			if(obj instanceof Packet) {
				Packet p = (Packet) obj;
				p.setSource(this);
				packetsReceived++;
				fireEvent(new PacketReceivedEventImpl(this, p));
			}
		}
	}

	@Override
	public boolean close() {
		try {
			if (getSocket().isClosed()) return true;
			getSocket().close();
			fireEvent(new ConnectionClosedEventImpl(this));
			logger.info("Connection has been successfully closed");
			return true;
		} catch (IOException e) {
			logger.log(Level.DEBUG, "An exception occurred when closing the connection", e);
			return false;
		}
	}

	protected final void fireEvent(ConnectionEvent event) {
		logger.debug("The connection event " + event + " is being fired on " + getListeners().size() + " listeners.");
		for (ConnectionListener connectionListener : new HashSet<>(getListeners())) {
			try {
				connectionListener.handle(event);
			} catch (Throwable e) {
				logger.error("An error occurred while forwarding the event " + event + " to the listener " + connectionListener, e);
			}
			if (event.isConsumed()) break;
		}
	}

	@Override
	public final void handshakeCompleted(HandshakeCompletedEvent event) {
		try {
			if (getOutputStream() == null) setOutputStream(new ObjectOutputStream(new BufferedOutputStream(getSocket().getOutputStream())));
			if (getInputStream() == null) setInputStream(new ObjectInputStream(new BufferedInputStream(getSocket().getInputStream())));
			fireEvent(new ConnectionEstablishedEventImpl(this));
			logger.info("Connection established");
		} catch (IOException e) {
			close();
		}
	}

	// Getter & Setter

	public Set<ConnectionListener> getListeners() {
		return listeners;
	}
	public SSLSocket getSocket() {
		return socket;
	}
	@Override
	public boolean isConnected() {
		return getSocket().isConnected();
	}

	public ObjectInputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(ObjectInputStream inputStream) {
		this.inputStream = inputStream;
	}

	public ObjectOutputStream getOutputStream() {
		return outputStream;
	}

	public void setOutputStream(ObjectOutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public int getPacketsSent() {
		return packetsSent;
	}

	public int getPacketsReceived() {
		return packetsReceived;
	}
}
