package dok.commons.network.core.events;

import dok.commons.network.api.Connection;
import dok.commons.network.api.events.ConnectionClosedEvent;
import dok.commons.network.core.ConnectionEventImpl;

public class ConnectionClosedEventImpl extends ConnectionEventImpl implements ConnectionClosedEvent {

	//Constructor(s)

	public ConnectionClosedEventImpl(Connection source) {
		super(source);
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
