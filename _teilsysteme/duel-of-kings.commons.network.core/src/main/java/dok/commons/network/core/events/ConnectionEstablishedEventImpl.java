package dok.commons.network.core.events;

import dok.commons.network.api.Connection;
import dok.commons.network.api.events.ConnectionEstablishedEvent;
import dok.commons.network.core.ConnectionEventImpl;

public class ConnectionEstablishedEventImpl extends ConnectionEventImpl implements ConnectionEstablishedEvent {

	public ConnectionEstablishedEventImpl(Connection source) {
		super(source);
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
