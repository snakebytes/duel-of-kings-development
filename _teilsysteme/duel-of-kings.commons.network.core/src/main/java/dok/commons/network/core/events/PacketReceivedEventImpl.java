package dok.commons.network.core.events;

import dok.commons.network.api.Connection;
import dok.commons.network.api.Packet;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.commons.network.core.ConnectionEventImpl;

public class PacketReceivedEventImpl extends ConnectionEventImpl implements PacketReceivedEvent {

	//Attributes

	private final Packet packet;

	//Constructor(s)

	public PacketReceivedEventImpl(Connection source, Packet packet) {
		super(source);
		this.packet = packet;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{" + getPacket().toString() + "}";
	}

	//Getter & Setter

	public final Packet getPacket() {
		return packet;
	}

}
