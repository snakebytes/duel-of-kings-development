package dok.commons.network.core.packets;

import dok.commons.network.api.packets.LoginPacket;
import dok.commons.network.core.PacketImpl;

public class LoginPacketImpl extends PacketImpl implements LoginPacket {

	// Class Constants

	private static final long serialVersionUID = -1284564598064140518L;

	// Attributes

	private String accountName;
	private String accountPassword;

	// Constructor(s)

	public LoginPacketImpl(String requestedName, String accountPassoword) {
		setAccountName(requestedName.trim());
		setAccountPassword(accountPassoword.trim());
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "accountName: " + getAccountName()
				+ ", accountPassword: " + getAccountPassword()
				+ "}";
	}

	// Getter & Setter

	@Override
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String requestedName) {
		this.accountName = requestedName;
	}

	@Override
	public String getAccountPassword() {
		return accountPassword;
	}

	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

}
