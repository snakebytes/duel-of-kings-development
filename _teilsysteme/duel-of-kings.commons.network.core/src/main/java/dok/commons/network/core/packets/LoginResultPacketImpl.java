package dok.commons.network.core.packets;

import dok.commons.model.LoginSession;
import dok.commons.model.StatusCode;
import dok.commons.network.api.packets.LoginResultPacket;
import dok.commons.network.core.PacketImpl;

public class LoginResultPacketImpl extends PacketImpl implements LoginResultPacket {

	// Class Constants

	private static final long serialVersionUID = -7635827926710157539L;

	// Attributes

	private final LoginSession loginSession;
	private final StatusCode statusCode;

	// Constructor(s)

	public LoginResultPacketImpl(StatusCode statusCode) {
		this.loginSession = null;
		this.statusCode = statusCode;
	}

	public LoginResultPacketImpl(StatusCode statusCode, LoginSession loginSession) {
		this.loginSession = loginSession;
		this.statusCode = statusCode;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "statusCode: " + getStatusCode()
				+ ", loginSession: " + getLoginSession()
				+ "}";
	}

	// Getter & Setter

	@Override
	public LoginSession getLoginSession() {
		return loginSession;
	}

	@Override
	public StatusCode getStatusCode() {
		return statusCode;
	}


}
