package dok.commons.network.core.util;

import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.network.core.NetworkConstants;

public class SSLContextFactory {

	private static final Logger logger = LogManager.getLogger();

    private static final String PROTOCOL = "TLS";
    private static final SSLContext SERVER_CONTEXT;
    private static final SSLContext CLIENT_CONTEXT;

    static {

        SSLContext serverContext = null;
        SSLContext clientContext = null;

        try {

        	logger.info("Attempting to initialize server context ...");

        	KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(SSLContextFactory.class.getResourceAsStream("/ssl/cacerts.jks"), NetworkConstants.KEY_STORE_PASSWORD.toCharArray());

            // Set up key manager factory to use our key store
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, NetworkConstants.KEY_STORE_PASSWORD.toCharArray());

	        // Initialize the SSLContext to work with our key managers.
	        serverContext = SSLContext.getInstance(PROTOCOL);
	        serverContext.init(kmf.getKeyManagers(), null, null);

	        logger.info("Server context has been initialized");

	    } catch (Exception e) {

	    	logger.error("Failed to initialize the Server SSLContext ...");

	    }

        try {

        	logger.info("Attempting to initialize client context ...");

            // truststore
            KeyStore ts = KeyStore.getInstance("JKS");
            ts.load(SSLContextFactory.class.getResourceAsStream("/ssl/cacerts.jks"), NetworkConstants.KEY_STORE_PASSWORD.toCharArray());

            // Truststore
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ts);

            clientContext = SSLContext.getInstance(PROTOCOL);
            clientContext.init(null, tmf.getTrustManagers(), null);

            logger.info("Client context has been initialized");

        } catch (Exception e) {

            logger.error("Failed to initialize the Client SSLContext ...");

        }

        SERVER_CONTEXT = serverContext;
        CLIENT_CONTEXT = clientContext;
    }


    public static SSLContext getServerContext() {
        return SERVER_CONTEXT;
    }

    public static SSLContext getClientContext() {
        return CLIENT_CONTEXT;
    }

    private SSLContextFactory() {
        // Unused
    }
}