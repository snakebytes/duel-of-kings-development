package dok.commons;

import java.util.function.Consumer;

/**
 * Super interface for all non-registerable services.<br>
 * Services should be able to handle data and functionality requests
 * and also provide the result of those requests to their consumers.<br>
 *
 * @param <R> The type of service consumer that can listen to this service.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */

public interface ConsumableService<R> {

	public void addConsumer(Consumer<R> consumer);
	public void removeConsumer(Consumer<R> consumer);

}
