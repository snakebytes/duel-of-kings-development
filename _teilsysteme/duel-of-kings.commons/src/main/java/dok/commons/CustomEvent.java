package dok.commons;

/**
 * Interface for defining consumable events.
 *
 * @author Konstantin Schaper
 * @version 1.0.0
 */

public interface CustomEvent {

	public void consume();
	public boolean isConsumed();

}
