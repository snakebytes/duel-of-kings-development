package dok.commons;

/**
 * Convenience interface for the implementation of the observable pattern.
 *
 * @author Konstantin Schaper
 * @since 0.1.0
 *
 * @param <T> The accepted listener type
 */
public interface CustomObservable<T> {

	public boolean addListener(T listener);
	public boolean removeListener(T listener);

}
