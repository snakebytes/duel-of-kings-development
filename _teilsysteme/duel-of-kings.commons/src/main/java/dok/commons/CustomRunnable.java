package dok.commons;

/**
 * Interface for defining stoppable tasks.
 *
 * @author Konstantin Schaper
 * @version 1.0.0
 */

public interface CustomRunnable extends Runnable {

	/**
	 * Tells the runnable to stop it's task.<br>
	 * It is on the implementing runnable to check for {@link #isStopping()}
	 * and stop in return, so this does not actually stop the task.
	 *
	 * @param value self-explanatory
	 */
	public void setStopping(boolean value);

	public boolean isStopping();

}
