package dok.commons;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Indicates that all fields of the annotated class
 * are immutable.
 *
 * @author Konstantin Schaper
 * @since 0.2.7.1
 */
@Target(ElementType.TYPE)
public @interface Immutable {

}
