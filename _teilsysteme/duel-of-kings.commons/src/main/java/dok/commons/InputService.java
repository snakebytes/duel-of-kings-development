package dok.commons;

import java.util.function.Consumer;

public interface InputService extends ConsumableService<Object>, Consumer<Object> {

}
