package dok.commons;

import java.util.ResourceBundle;

/**
 * Provides localized texts to display.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 *
 * @see ResourceBundle
 */
public interface LocalizationService {

	public ResourceBundle getResourceBundle();
	public String getText(String key, Object[] parameters);
	public String getText(String key);

}
