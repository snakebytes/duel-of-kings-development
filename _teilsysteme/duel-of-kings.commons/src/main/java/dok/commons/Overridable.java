package dok.commons;

/**
 *
 * @param <T> The type of objects that can override this {@linkplain Overridable}.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface Overridable<T> {

	public void overrideWith(T other);

}
