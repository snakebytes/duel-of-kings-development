package dok.commons.impl;

import dok.commons.CustomEvent;

/**
 * Abstract implementation of the {@link CustomEvent} interface.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public abstract class CustomEventImpl implements CustomEvent {

	//Attributes

	private boolean consumed = false;

	//Methods

	@Override
	public final void consume() {
		consumed = true;
	}

	//Getter & Setter

	@Override
	public final boolean isConsumed() {
		return consumed;
	}

}
