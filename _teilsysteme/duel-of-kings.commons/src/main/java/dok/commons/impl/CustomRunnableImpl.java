package dok.commons.impl;

import dok.commons.CustomRunnable;

/**
 * Implementation the runnable interface.<br>
 * Not ready for consumption.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public abstract class CustomRunnableImpl implements CustomRunnable {

	//Attributes

	private boolean stopped = false;

	//Methods

	public boolean isStopping() {
		return stopped;
	}

	public void setStopping(boolean value) {
		stopped = value;
	}

}
