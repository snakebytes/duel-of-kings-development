package dok.commons.impl;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.Overridable;

public final class CustomTimer implements Runnable, Serializable, Overridable<CustomTimer> {

    // Class Constants

	public static final Duration DEFAULT_TICK_TIMEOUT = Duration.ofMillis(100);
	private static final long serialVersionUID = 1L;
    private static final Logger logger = LogManager.getLogger();

    // Inner Classes

    public interface CustomTimerCallback {

        public void timerExpired(CustomTimer timer);

    }

    public enum CustomTimerState {

        RUNNING,
        PAUSED,
        STOPPED,
        EXPIRED

    }

    // Attributes

    private Duration tickTimeout;
    private Duration duration;
    private transient final Set<CustomTimerCallback> callbacks = new HashSet<>();
    private Duration timeElapsed = Duration.ZERO;
    private CustomTimerState state = CustomTimerState.STOPPED;
    private transient Thread timerThread;

    // Constructor(s)

    public CustomTimer(Duration duration) {
        logger.entry(duration);
        setDuration(duration);
        setTickTimeout(DEFAULT_TICK_TIMEOUT);
        logger.exit(this);
    }

    public CustomTimer(Duration duration, CustomTimerCallback callback) {
        logger.entry(duration, callback);
        setDuration(duration);
        setTickTimeout(DEFAULT_TICK_TIMEOUT);
        getCallbacks().add(callback);
        logger.exit(this);
    }

    public CustomTimer(Duration duration, Duration tickTimeout) {
        logger.entry(duration, tickTimeout);
        setDuration(duration);
        setTickTimeout(tickTimeout);
        logger.exit(this);
    }

    public CustomTimer(Duration duration, Duration tickTimeout, CustomTimerCallback callback) {
        logger.entry(duration, tickTimeout, callback);
        setDuration(duration);
        setTickTimeout(tickTimeout);
        getCallbacks().add(callback);
        logger.exit(this);
    }

    /**
     * Creates a new timer with the same values as the specified timer.<br>
     * Does copy the state but does not start the timer,
     * even if the other timer is stated as running.
     * Also does not copy the registered callbacks.
     *
     * @param other The timer to override this timer's values with
     */
    public CustomTimer(CustomTimer other) {
        this(other.getDuration(), other.getTickTimeout());
        setTimeElapsed(other.getTimeElapsed());
        setState(other.getState());
    }

    public CustomTimer() {
    	setDuration(Duration.ZERO);
    	setTickTimeout(DEFAULT_TICK_TIMEOUT);
    }

    // Methods

    @Override
    public void overrideWith(CustomTimer other) {
        logger.entry(other);
        setDuration(other.getDuration());
        setTickTimeout(other.getTickTimeout());
        setTimeElapsed(other.getTimeElapsed());
        if (isRunning() && other.isStopped()) stop();
        else if (isRunning() && other.isExpired()) {
        	stop();
        	setState(CustomTimerState.EXPIRED);
        } else if (isStopped() && other.isRunning()) {
        	start();
        } else if (isExpired() && other.isRunning()) {
        	restart();
        } else if (isPaused() && other.isRunning()) {
        	resume();
        } else if (isRunning() && other.isPaused()) {
        	pause();
        } else if ((isStopped() && other.isPaused())
        		|| (isStopped() && other.isExpired())) {
        	setState(other.getState());
        }
        logger.exit();
    }

    private void startTimerThread() {
    	 // Start the timer daemon thread
        setTimerThread(new Thread(this::run));
        getTimerThread().setDaemon(true);
        getTimerThread().setName("CustomTimerThread");
        getTimerThread().start();
    }

    public void start() {
        logger.entry();
        if (isRunning() || isExpired()) return;
        else {

            // Change the timer state
            setState(CustomTimerState.RUNNING);

            try {
            	startTimerThread();
            	logger.trace(getClass().getSimpleName() + " started");
            } catch (Exception e) {
                logger.catching(e);
                setState(CustomTimerState.STOPPED);
            }
        }
        logger.exit();
    }

    public void restart() {
    	logger.entry();
    	 if (isRunning() || !isExpired()) return;
         else {
        	// Reset time elapsed
        	reset();

	        // Change the timer state
	        setState(CustomTimerState.RUNNING);

	        try {
	        	startTimerThread();
	        	logger.trace(getClass().getSimpleName() + " restarted");
	        } catch (Exception e) {
	            logger.catching(e);
	            setState(CustomTimerState.STOPPED);
	        }
        }
    	logger.exit();
    }

    public void stop() {
        logger.entry();
        if (!(getState() == CustomTimerState.STOPPED)) {
            if (getTimerThread() != null) getTimerThread().interrupt();
            setState(CustomTimerState.STOPPED);
            logger.trace(getClass().getSimpleName() + " stopped");
        }
        logger.exit();
    }

    public void pause() {
        logger.entry();
        if (getState() == CustomTimerState.RUNNING) {
            setState(CustomTimerState.PAUSED);
            logger.trace(getClass().getSimpleName() + " paused");
        }
        logger.exit();
    }

    public void resume() {
        logger.entry();
        if (getState() == CustomTimerState.PAUSED) {
            setState(CustomTimerState.RUNNING);
            logger.trace(getClass().getSimpleName() + " resumed");
        }
        logger.exit();
    }

    public void reset() {
        logger.entry();
        setTimeElapsed(Duration.ZERO);
        logger.trace(getClass().getSimpleName() + " reset");
        logger.exit();
    }

    // Callbacks

    public boolean addCallback(CustomTimerCallback callback) {
        logger.entry();
        return logger.exit(getCallbacks().add(callback));
    }

    public boolean removeCallback(CustomTimerCallback callback) {
        logger.entry();
        return logger.exit(getCallbacks().remove(callback));
    }

    // Runnable

    @Override
    public synchronized void run() {

        // Do the timer work
        while (getTimeLeft().compareTo(Duration.ZERO) > 0) {
            if (Thread.interrupted()) {
                logger.debug("CustomTimer has been stopped");
                return;
            } else if (!isPaused()) {
                try {
                	synchronized (getTimerThread()) {
                		getTimerThread().wait((long) getTickTimeout().toMillis());
                	}
                } catch (InterruptedException e) {
                    logger.debug("CustomTimer has been stopped");
                    return;
                }
                setTimeElapsed(getTimeElapsed().plus(getTickTimeout()));
            }
        }

        setState(CustomTimerState.EXPIRED);

        logger.debug("CustomTimer expired after " + (getTimeElapsed().toMillis() / 1000) + " seconds");
        new HashSet<>(getCallbacks()).forEach((callback) -> callback.timerExpired(this));
    }

    // Getter & Setter

    public Duration getDuration() {
        return duration;
    }

    private Set<CustomTimerCallback> getCallbacks() {
        return callbacks;
    }

    public Duration getTimeElapsed() {
        return timeElapsed;
    }

    public Duration getTimeLeft() {
        return getDuration().minus(getTimeElapsed());
    }

    private void setTimeElapsed(Duration timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    public boolean isPaused() {
        return getState() == CustomTimerState.PAUSED;
    }

    public boolean isRunning() {
        return getState() == CustomTimerState.RUNNING;
    }

    public boolean isStopped() {
        return getState() == CustomTimerState.STOPPED;
    }

    public boolean isExpired() {
        return getState() == CustomTimerState.EXPIRED;
    }

    public Duration getTickTimeout() {
        return tickTimeout;
    }

    public CustomTimerState getState() {
        return state;
    }

    private void setState(CustomTimerState state) {
        this.state = state;
    }

    private void setTickTimeout(Duration tickTimeout) {
        this.tickTimeout = tickTimeout;
    }

    private void setDuration(Duration duration) {
        this.duration = duration;
    }

    private Thread getTimerThread() {
        return timerThread;
    }

    private void setTimerThread(Thread timerThread) {
        this.timerThread = timerThread;
    }

}
