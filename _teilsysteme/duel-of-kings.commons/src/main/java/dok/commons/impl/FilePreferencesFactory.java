package dok.commons.impl;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * PreferencesFactory implementation that stores the preferences in a user-defined file. To use it,
 * set the system property <tt>java.util.prefs.PreferencesFactory</tt> to
 * <tt>net.infotrek.util.prefs.FilePreferencesFactory</tt>
 * <p/>
 * The file defaults to [user.home]/.fileprefs, but may be overridden with the system property
 * <tt>net.infotrek.util.prefs.FilePreferencesFactory.file</tt><br>
 * <br>
 * By <i>Konstantin Schaper</i>: Changed logging framework to apache's log4j and made some application specific changes.
 *
 * @author David Croft (<a href="http://www.davidc.net">www.davidc.net</a>)
 * @version $Id: FilePreferencesFactory.java 282 2009-06-18 17:05:18Z david $
 */
public class FilePreferencesFactory implements PreferencesFactory
{
  private static final Logger log = LogManager.getLogger();

  Preferences rootPreferences;
  public static final String SYSTEM_PROPERTY_FILE = "net.infotrek.util.prefs.FilePreferencesFactory.file";

  public Preferences systemRoot()
  {
    return userRoot();
  }

  public Preferences userRoot()
  {
    if (rootPreferences == null) {
      log.debug("Instantiating root preferences");

      rootPreferences = new FilePreferences(null, "");
    }
    return rootPreferences;
  }

  private static File preferencesFile;

  static {
	  String prefsFile = System.getProperty(SYSTEM_PROPERTY_FILE);
      if (prefsFile == null || prefsFile.length() == 0) {
        prefsFile = System.getProperty("user.home") + File.separator + ".fileprefs";
      }
      preferencesFile = new File(prefsFile); //.getAbsoluteFile()
      log.info("Preferences file is " + preferencesFile);
      if (!preferencesFile.exists()) {
    	log.info("Attempting to create preferences file ...");
		try {
			new File(preferencesFile.getParent()).mkdirs();
			preferencesFile.createNewFile();
			log.info("Preferences file has been successfully created");
		} catch (IOException e) {
			log.info("Preferences file could not be created", e);
		}
      }
  }

  public static File getPreferencesFile()
  {
    return preferencesFile;
  }

}