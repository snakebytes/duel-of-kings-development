package dok.commons.impl;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class GIFImageView extends ImageView {

	// Properties

	private final ObjectProperty<Duration> duration = new SimpleObjectProperty<>();
	private final ObservableList<Image> images = FXCollections.observableArrayList();
	private final IntegerProperty currentImageIndex = new SimpleIntegerProperty();
	private final ObjectProperty<Animation> runningAnimation = new SimpleObjectProperty<>();

	// Constructor(s)

	public GIFImageView(Image[] images, Duration duration) {
		super();
		getImages().addAll(images);
		setDuration(duration);
	}

	// Methods

	public void play() {
		// Build the animation
		Timeline timeline = new Timeline();
		List<Image> tmpImages = new ArrayList<>(getImages());

		for (Image image : tmpImages) {
			KeyValue kv = new KeyValue(imageProperty(), image);
			KeyFrame kf = new KeyFrame(getDuration().divide(tmpImages.size()), kv);
			timeline.getKeyFrames().add(kf);
		}

		// Play the animation
		setRunningAnimation(timeline);
		getRunningAnimation().playFromStart();
	}

	// Getter & Setter

	public final ObjectProperty<Duration> durationProperty() {
		return this.duration;
	}


	public final javafx.util.Duration getDuration() {
		return this.durationProperty().get();
	}


	public final void setDuration(final javafx.util.Duration duration) {
		this.durationProperty().set(duration);
	}

	protected final ObservableList<Image> getImages() {
		return images;
	}

	public final IntegerProperty currentImageIndexProperty() {
		return this.currentImageIndex;
	}


	public final int getCurrentImageIndex() {
		return this.currentImageIndexProperty().get();
	}


	public final void setCurrentImageIndex(final int currentImageIndex) {
		this.currentImageIndexProperty().set(currentImageIndex);
	}

	public final ReadOnlyObjectProperty<Animation> runningAnimationProperty() {
		return this.runningAnimation;
	}


	public final javafx.animation.Animation getRunningAnimation() {
		return this.runningAnimationProperty().get();
	}


	protected final void setRunningAnimation(final javafx.animation.Animation runningAnimation) {
		this.runningAnimation.set(runningAnimation);
	}



}
