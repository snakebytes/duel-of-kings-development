package dok.commons.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.InputService;

public class InputProvider implements InputService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constants

	private final Set<Consumer<Object>> consumers = new HashSet<>();

	// Methods

	@Override
	public void addConsumer(Consumer<Object> consumer) {
		logger.entry(consumer);
		logger.exit(getConsumers().add(consumer));
	}

	@Override
	public void removeConsumer(Consumer<Object> consumer) {
		logger.entry(consumer);
		logger.exit(getConsumers().remove(consumer));
	}

	@Override
	public void accept(Object result) {
		logger.entry(result);
		for (Consumer<Object> consumer : new HashSet<>(getConsumers()))
			if (getConsumers().contains(consumer)) {
				logger.debug("{} consumes {}", consumer, result);
				consumer.accept(result);
			}
		logger.exit();
	}

	// Getters & Setters

	public final Set<Consumer<Object>> getConsumers() {
		return consumers;
	}

}
