package dok.commons.impl;

import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

public class MultiTask extends Task<List<Object>> {

	// Class Constants

	 private static final Logger logger = LogManager.getLogger();

	// Properties

	private final DoubleProperty subProgress = new SimpleDoubleProperty(0);
	private final DoubleProperty subWorkDone = new SimpleDoubleProperty(0);
	private final DoubleProperty subTotalWork = new SimpleDoubleProperty(0);
	private final StringProperty subProgressMessage = new SimpleStringProperty("");
	private final ObjectProperty<SubTask<?>> currentSubTask = new SimpleObjectProperty<>();

	// Attributes

	private final ObservableList<SubTask<?>> remainingTasks = FXCollections.observableArrayList();
	private final ObservableList<Object> results = FXCollections.observableArrayList();
	private int totalWork;

	// Constructor(s)

	public MultiTask() {
		super();
	}

	public MultiTask(SubTask<?>... tasks) {
		super();
		getRemainingTasks().addAll(tasks);
	}

	// Methods

	@Override
	protected List<Object> call() throws Exception {
		logger.entry();

		totalWork = getRemainingTasks().size();
		while (getRemainingTasks().size() > 0) {
			setCurrentSubTask(getRemainingTasks().remove(0));

			if (isCancelled()) break;
			Platform.runLater(() -> {
				subProgressMessage.bind(getCurrentSubTask().messageProperty());
				subProgress.bind(getCurrentSubTask().progressProperty());
				subWorkDone.bind(getCurrentSubTask().workDoneProperty());
				subTotalWork.bind(getCurrentSubTask().totalWorkProperty());
			});

			getCurrentSubTask().setParent(this);

			Thread t = new Thread(getCurrentSubTask());
			t.setDaemon(true);
			t.setName("SubTaskThread");
			t.start();

			try {
				getResults().add(getCurrentSubTask().get());
			} catch (InterruptedException | ExecutionException e) {
				logger.throwing(e);
				throw e;
			} catch (CancellationException e) {
				logger.catching(e);
				continue;
			}

			Platform.runLater(() -> updateProgress(getWorkDone() + 1, totalWork));
		}

		return logger.exit(getResults());
	}

	@Override
	protected void cancelled() {
		logger.entry();
		super.cancelled();
		logger.exit();
	}

	// Getters & Setters


	public ReadOnlyListWrapper<SubTask<?>> getReadonlyRemainingTasks() {
		return new ReadOnlyListWrapper<>(remainingTasks);
	}

	protected ObservableList<SubTask<?>> getRemainingTasks() {
		return remainingTasks;
	}

	public final ReadOnlyDoubleProperty subProgressProperty() {
		return this.subProgress;
	}


	public final double getSubProgress() {
		return this.subProgressProperty().get();
	}


	protected final void setSubProgress(final double subProgress) {
		this.subProgress.set(subProgress);
	}


	public final ReadOnlyDoubleProperty subTotalWorkProperty() {
		return this.subTotalWork;
	}


	public final double getSubTotalWork() {
		return this.subTotalWorkProperty().get();
	}


	protected final void setSubTotalWork(final double subTotalWork) {
		this.subTotalWork.set(subTotalWork);
	}


	public final ReadOnlyStringProperty subProgressMessageProperty() {
		return this.subProgressMessage;
	}


	public final java.lang.String getSubProgressMessage() {
		return this.subProgressMessageProperty().get();
	}


	protected final void setSubProgressMessage(final java.lang.String subProgressMessage) {
		this.subProgressMessage.set(subProgressMessage);
	}

	public final ReadOnlyObjectProperty<SubTask<?>> currentSubTaskProperty() {
		return this.currentSubTask;
	}


	public final SubTask<?> getCurrentSubTask() {
		return this.currentSubTaskProperty().get();
	}


	protected final void setCurrentSubTask(SubTask<?> currentSubTask) {
		this.currentSubTask.set(currentSubTask);
	}

	public final ReadOnlyDoubleProperty subWorkDoneProperty() {
		return this.subWorkDone;
	}


	public final double getSubWorkDone() {
		return this.subWorkDoneProperty().get();
	}


	protected final void setSubWorkDone(final double subWorkDone) {
		this.subWorkDone.set(subWorkDone);
	}

	public ObservableList<Object> getResults() {
		return results;
	}

}
