package dok.commons.impl;

import java.util.HashSet;
import java.util.Set;

import javax.imageio.spi.ServiceRegistry;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.CustomObservable;
import dok.commons.impl.ObservableServiceRegistry.ServiceRegistryListener;

/**
 * A registry for service provider instances.<br>
 * <br>
 * A <i>service</i> is a well-known set of interfaces and (usually abstract) classes.
 * A <i>service provider</i> is a specific implementation of a service.
 * The classes in a provider typically implement the interface or subclass the class defined by the service itself.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 * @see ServiceRegistry
 */
public class ObservableServiceRegistry implements CustomObservable<ServiceRegistryListener> {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Inner Classes

	public static interface RegisterableService {

		/**
	     * Called when an object implementing this interface is added to
	     * the given <code>category</code> of the given
	     * <code>registry</code>.  The object may already be registered
	     * under another category or categories.
	     *
	     * @param registry a {@linkplain ObservableServiceRegistry} where this
	     * object has been registered.
	     * @param category a <code>Class</code> object indicating the
	     * registry category under which this object has been registered.
		 * @throws Exception
	     */
	    void onRegistration(ObservableServiceRegistry registry, Class<?> category) throws Exception;

	    /**
	     * Called when an object implementing this interface is removed
	     * from the given <code>registry</code>. The object may still be registered
	     * under another category or categories.
	     *
	     * @param registry a {@linkplain ObservableServiceRegistry} from which this
	     * object is being (wholly or partially) deregistered.
	     * @param category a <code>Class</code> object indicating the
	     * registry category from which this object is being deregistered.
	     */
	    void onDeregistration(ObservableServiceRegistry registry, Class<?> category) throws Exception;

	}

	/**
	 * Super interface for all services to be registered with a {@linkplain ObservableServiceRegistry}.<br>
	 * Services should be able to handle data and functionality requests
	 * and also provide the result of those requests to their consumers.<br>
	 *
	 * @param <T> The type of service consumer that can listen to this service.
	 *
	 * @author Konstantin Schaper
	 * @version 0.1.0
	 */
	public static interface ObservableRegisterableService<T> extends CustomObservable<T>, RegisterableService {

		/**
		 * Registers a listener on this service to listen to any events being fired.
		 *
		 * @param listener The listener to register
		 */
		public boolean addListener(T listener);

		/**
		 * Deregisters a listener from this service.
		 *
		 * @param listener The listener to deregister
		 */
		public boolean removeListener(T listener);

	}

	public interface ServiceRegistryListener {

		public <T> void onServiceRegistration(ObservableServiceRegistry registry, T provider, Class<T> category);
		public <T> void onServiceDeregistration(ObservableServiceRegistry registry, T provider, Class<T> category);

	}

	// Constants

	private final Set<ServiceRegistryListener> listeners = new HashSet<>();
	private final ServiceRegistry backingRegistry;

	// Constructor(s)

	public ObservableServiceRegistry(Set<Class<?>> services) {
		if (services == null) throw new IllegalArgumentException("ServiceCategories must not be null");
		backingRegistry = new ServiceRegistry(services.iterator());
	}

	// Methods

	public <T> boolean registerServiceProvider(T provider, Class<T> category) throws Exception {
		if (getServiceProvider(category) != null) throw new IllegalStateException("There already is a provider registered in the given category");
		if (provider == null) throw new IllegalArgumentException("Provider must not be null");

		// Perform the change
		boolean result = getBackingRegistry().registerServiceProvider(provider, category); //getServicesMap().putIfAbsent(category, provider) == null;

		if (result) {
			// Notify the service provider if possible
			if (provider instanceof RegisterableService) {
				RegisterableService registerable = (RegisterableService) provider;
				try {
					registerable.onRegistration(this, category);
				} catch (Exception e) {
					logger.throwing(Level.DEBUG, e);
					throw e;
				}
			}

			// Notify listeners
			getListeners().forEach((listener) -> {
				try {
					listener.onServiceRegistration(this, provider, category);
				} catch (Exception e) {
					logger.catching(Level.DEBUG, e);
				}
			});
		}

		// Return the result
		return result;
	}

	public <T> boolean deregisterServiceProvider(Class<T> category, T provider) throws IllegalArgumentException {
		if (getServiceProvider(category) != null && !getServiceProvider(category).equals(provider)) return false;
		if (provider == null) return false;

		// Perform the change
		boolean result = getBackingRegistry().deregisterServiceProvider(provider, category);

		if (result) {

			// Notify listeners
			getListeners().forEach((listener) -> {
				try {
					listener.onServiceDeregistration(this, provider, category);
				} catch (Exception e) {
					logger.catching(Level.DEBUG, e);
				}
			});

			// Notify the service provider if possible
			if (provider instanceof RegisterableService) {
				RegisterableService registerable = (RegisterableService) provider;
				try {
					registerable.onDeregistration(this, category);
				} catch (Exception e) {
					logger.catching(Level.DEBUG, e);
					return false;
				}
			}

		}

		// Return the result
		return result;
	}

	public <T> boolean deregisterServiceProvider(Class<T> category) throws IllegalArgumentException {
		T provider = getServiceProvider(category);
		return provider == null ? false : deregisterServiceProvider(category, provider);
	}

	public <T> void deregisterServiceProvider(T provider) throws IllegalArgumentException {
		if (provider == null) throw new IllegalArgumentException("Provider must not be null");
		getBackingRegistry().getCategories().forEachRemaining((category) -> {
			if (category.isAssignableFrom(provider.getClass())) {
				@SuppressWarnings("unchecked")
				Class<T> providerCategory = (Class<T>) category;
				deregisterServiceProvider(providerCategory, provider);
			}
		});
	}

	public boolean contains(Object provider) {
		return getBackingRegistry().contains(provider);
	}

	public <T> T getServiceProvider(Class<T> category) {
		try {
			return (T) getBackingRegistry().getServiceProviders(category, true).next();
		} catch (Exception e) {
			return null;
		}
	}

	// <--- CustomObservable --->

	@Override
	public boolean addListener(ServiceRegistryListener listener) {
		return getListeners().add(listener);
	}

	@Override
	public boolean removeListener(ServiceRegistryListener listener) {
		return getListeners().remove(listener);
	}

	public void deregisterAll() {
		getBackingRegistry().getCategories().forEachRemaining((category) -> {
			try {
				deregisterServiceProvider(category);
			} catch (Exception e) {
				logger.log(Level.WARN, "An exception occurred while deregistering a service", e);
			}
		});
	}

	// Getter & Setter

	public Set<ServiceRegistryListener> getListeners() {
		return listeners;
	}

	protected final ServiceRegistry getBackingRegistry() {
		return backingRegistry;
	}

}
