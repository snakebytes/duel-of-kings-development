package dok.commons.impl;

import javafx.concurrent.Task;

public abstract class SubTask<T> extends Task<T> {

	private MultiTask parent;

	public MultiTask getParent() {
		return parent;
	}

	public void setParent(MultiTask parent) {
		this.parent = parent;
	}

}
