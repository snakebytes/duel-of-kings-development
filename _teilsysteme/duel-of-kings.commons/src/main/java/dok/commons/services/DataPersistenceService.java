package dok.commons.services;

import java.io.Serializable;
import java.util.List;

/**
 * Manages the connection to an underlying data store
 * to persist objects to and retrieve objects from.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
public interface DataPersistenceService {

	/**
	 * Persists the given object to the underlying data store.<br>
	 * There can only be one object with a single id at any time.
	 *
	 * @param obj The object to persist
	 * @return The (generated) id of the object or null, if the object could not be saved
	 */
	public Object save(Serializable object);

	/**
	 * Updates the persisted representation of the given object in the underlying data store.
	 *
	 * @param obj The object whose persisted representation to update
	 * @return Whether the given object's persisted representation has been successfully updated
	 */
	public boolean update(Serializable object);

	/**
	 * Either persists the given object or updates the persisted representation of this object
	 * if it had already been persisted.<br>
	 *
	 * @param object The object to store
	 * @return Whether the given object has been successfully saved or updated
	 */
	public boolean saveOrUpdate(Serializable object);

	/**
	 * Deletes the persisted representation of the given object in the underlying data store.
	 *
	 * @param obj The object whose persisted representation to delete
	 * @return Whether the persisted representation of the given object has been successfully deleted
	 */
	public boolean delete(Serializable object);

	/**
	 * Returns the object represented in the underlying data store with the given id.
	 *
	 * @param <T> The type of objects to get
	 * @param type The class of the object to retrieve
	 * @param id The unique identifier of the object
	 * @return The single object associated with the given id or null, if there is no object of the given class with the given id or if an error occurred on retrieval
	 */
	public <T> T getObjectById(Class<T> type, Serializable id);

	/**
	 * Fetches all objects of the given type from the underlying data store.<br>
	 * Always returns an instance of {@linkplain java.util.List}.
	 *
	 * @param <T> The type of objects to get
	 * @param type The type of objects to get
	 * @return A list of all objects of the given type which are persisted in the underlying data store or an empty list,
	 *  		if an error occurred on retrieval (of course there just might not be any store objects of the given type)
	 */
	public <T> List<T> getAll(Class<T> type);

}
