package dok.commons.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;

/**
 * Primary service interface for interacting with hibernate.<br>
 * It is up to the implementing class to connect to the desired data store and
 * to generate sessions.<br>
 * Contains general implementations for data manipulation.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
public interface HibernateDataPersistenceService extends DataPersistenceService {

	// Sessions

	/**
	 * Opens a new session and associates it with the thread which called this
	 * method.
	 *
	 * @return A new {@linkplain Session} or a previously opened one which is
	 *         associated with the current thread
	 * @throws Exception
	 *             If an error occurred while trying to open a new session
	 */
	public Session openSession() throws Exception;

	/**
	 * Closes the session associated with this thread.
	 *
	 * @throws Exception
	 *             If the session could not be closed due to an error
	 */
	public void closeCurrentSession() throws Exception;

	// Data Manipulation

	@Override
	public default Object save(Serializable object) {
		Transaction transaction = null;
		Object id = null;
		try {
			Session session = openSession();
			transaction = session.beginTransaction();
			id = session.save(object);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			return null;
		}
		return id;
	}

	@Override
	public default boolean update(Serializable object) {
		Transaction transaction = null;
		try {
			Session session = openSession();
			transaction = session.beginTransaction();
			session.update(object);
			transaction.commit();
			return true;
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			return false;
		}
	}

	@Override
	public default boolean saveOrUpdate(Serializable object) {

		Transaction transaction = null;
		try {
			Session session = openSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(object);
			transaction.commit();
			return true;
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			return false;
		}
	}

	@Override
	public default boolean delete(Serializable object) {
		Transaction transaction = null;
		try {
			Session session = openSession();
			transaction = session.beginTransaction();
			session.delete(object);
			transaction.commit();
			return true;
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			return false;
		}
	}

	@Override
	public default <T> T getObjectById(Class<T> type, Serializable id) {
		T result;
		try {
			Session session = openSession();
			result = session.get(type, id);
		} catch (Exception e) {
			return null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public default <T> List<T> getAll(Class<T> type) {
		List<T> result;
		try {
			Session session = openSession();
			result = (List<T>) session.createCriteria(type).list().stream().distinct().collect(Collectors.toList());
			return result;
		} catch (Exception e) {
			return new ArrayList<T>();
		}
	}

	/**
	 *
	 * @param type
	 *            The class of which to retrieve objects
	 * @param restrictions
	 *            The restrictions to apply to the retrieval
	 * @return A list of objects of the given class, matching the given criteria
	 * @throws Exception
	 *             When any error occurs while performing the operation
	 */
	@SuppressWarnings("unchecked")
	public default <T> List<T> getByCriteria(Class<T> type, Criterion... restrictions) throws Exception {
		Session session = openSession();
		List<?> result;
		Criteria criteria;
		try {
			criteria = session.createCriteria(type);
			for (Criterion criterion : restrictions) {
				criteria.add(criterion);
			}
			result = criteria.list();
			return (List<T>) result;
		} catch (Exception e) {
			throw e;
		}
	}

}
