package dok.commons.statemachine;

/**
 * Used by {@link StateMachine}s to inform their parent that
 * they have reached their end-state.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface StateMachineCallback {

	/**
	 * Called on the implementing class when the corresponding
	 * (child) state machine reaches it's end-state.<br>
	 * There is no additional information about the child state machine provided
	 * as there should be only one active sub state machine per {@linkplain StateMachine}.<br>
	 * Functions as a pseudo state which injects certain information into the parent.
	 *
	 * @param result The result value which the sub state machine produced
	 */
	public void onFinish(Object result);

}
