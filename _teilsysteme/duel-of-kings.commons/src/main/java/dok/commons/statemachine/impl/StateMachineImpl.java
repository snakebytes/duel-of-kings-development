package dok.commons.statemachine.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.statemachine.StateMachine;
import dok.commons.statemachine.StateMachineCallback;

/**
 * Defines the Implementation of a generic finite-state machine.<br>
 * This class is not ready for consumption and has to be extended first.
 *
 * @param <S> An enum containing all possible states.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 *
 * @see StateMachine
 */

public abstract class StateMachineImpl<S extends Enum<?>> implements StateMachine<S>  {

	// Constants

	private static final Logger LOGGER = LogManager.getLogger();

	// Attributes

	private final S startState;
	private StateMachineCallback parent;
	private S currentState;
	private S previousState;
	private StateMachine<?> activeChild = null;

	// Constructor(s)

	/**
	 * Every implementing class should define it's constructors in the following form:<br>
	 * <br><code>
	 * 	super(parent, startState) <br>
	 * 	// Initialization of variables defined by parameters <br>
	 * 	transition(null, getStartState());<br>
	 * </code><br>
	 * This is to guarantee that all necessary information is provided before
	 * the first {@linkplain #onEntry(Enum)} event fires.
	 *
	 * @param parent See {@linkplain #getParent()}
	 * @param startState See {@linkplain #getStartState()}
	 * @throws IllegalArgumentException If the start state is null
	 */
	public StateMachineImpl(StateMachineCallback parent, S startState) throws IllegalArgumentException {
		LOGGER.entry(parent, startState);
		this.parent = parent;
		if (startState != null) {
			this.startState = startState;
		} else throw new IllegalArgumentException("Start state must not be null");
		LOGGER.exit(this);
	}

	//Methods

	/**
	 * Performs the actions to describe a transition from one state to another, in the following order:<br>
	 * - Checks if the statemachine has already finished, if not<br>
	 * - Triggers {@link #onExit(Enum)} on 'fromState'.<br>
	 * - Calls {@link #setPreviousState(Enum)} and {@link #setCurrentState(Enum)} to change the actual data.<br>
	 * - Triggers {@link #onEntry(Enum)} on 'toState'.<br>
	 *
	 * @param fromState Former state, can be null
	 * @param toState New state, can be null
	 * @throws IllegalArgumentException When both parameters are null
	 *
	 * @see {@link #handle(Object)}
	 */
	public final void transition(S fromState, S toState) throws IllegalArgumentException {
		LOGGER.entry(fromState, toState);
		LOGGER.debug("{} transitions from {} to {}", getClass().getSimpleName(), fromState, toState);
		if (fromState == null && toState == null) throw new IllegalArgumentException("One of the arguments must not be null");
		if (fromState == null && toState != getStartState()) {
			//throw new IllegalStateException("Cannot switch from null-state to a non-start-state.");
			LOGGER.debug("Cannot switch from null-state to a non-start-state.");
			LOGGER.exit();
			return;
		}
		if (fromState != null) onExit(fromState);
		setPreviousState(fromState);
		setCurrentState(toState);
		if (toState != null) onEntry(toState);
		LOGGER.exit();
	}

	/**
	 * Transitions from this state machine's current state
	 * to the given state.<br>
	 * Internally calls {@linkplain #transition(Enum, Enum)},
	 * so this is just a convenience method.
	 *
	 * @param toState the state to transition to
	 */
	public final void transition(S toState) {
		transition(getCurrentState(), toState);
	}

	/**
	 * Immediately transitions from the current state to
	 * the end state and informs the parent about the result.<br>
	 * Does trigger onExit on the state that was active before
	 * this method was called.<br>
	 * Will not inform the parent if the state machine has already
	 * finished or if it has not been started yet.<br>
	 * Interrupts any active child state machine.<br>
	 * <br>
	 * <i>NOTE:</i> This does <b>not</b> invoke onFinish on itself,
	 * even if the class implements the {@linkplain StateMachineCallback} interface.
	 *
	 * @param result The value to inject into the parent by calling it's
	 * {@linkplain StateMachineCallback#onFinish(Object)} method
	 */
	protected final void finish(Object result) {
		LOGGER.entry(result);
		if (getCurrentState() == null) return;
		LOGGER.debug(this.getClass().getSimpleName() + " finished with result '" + result + "'");
		if (getActiveChild() != null) getActiveChild().interrupt();
		onExit(getCurrentState());
		setCurrentState(null);
		if (getParent() != null) getParent().onFinish(result);
		LOGGER.exit();
	}

	/**
	 * @return The result of the active sub statemachine's {@linkplain #handle(Object)} method or null, if the is no active sub statemachine
	 */
	@Override
	public Object handle(Object input) {
		LOGGER.entry(input);
		LOGGER.debug(this.getClass().getSimpleName() + " handles '" + input + "'");
		if (getActiveChild() != null) return LOGGER.exit(getActiveChild().handle(input));
		else return LOGGER.exit(null);
	}

	/**
	 * Simply transitions to the null state and interrupts
	 * any active child.
	 */
	@Override
	public void interrupt() {
		LOGGER.entry();
		LOGGER.debug("Interrupting (child) state machine of type " + getClass().getSimpleName());

		// Interrupt active child if available
		if (getActiveChild() != null && getActiveChild().getCurrentState() != null) {
			getActiveChild().interrupt();
			setActiveChild(null);
		}

		// Attempt to transition to null state
		if (getCurrentState() == null) return;
		else {
			onExit(getCurrentState());
			setCurrentState(null);
		}
		LOGGER.exit();
	}

	//Abstract Methods

	/**
	 * Triggered by {@link #transition(Enum, Enum)} when a certain state has
	 * been reached but it is not the end-state.
	 *
	 * @param state State being reached
	 * @see #onExit(Enum)
	 */
	protected abstract void onEntry(S state);

	/**
	 * Triggered by {@link #transition(Enum, Enum)} when a certain state
	 * is being exited.
	 *
	 * @param state State being exited
	 * @see #onEntry(Enum)
	 */
	protected abstract void onExit(S state);

	//Getter & Setter

	/**
	 * The start state of a state machine is immutable and
	 * has to be specified on instantiation.
	 *
	 * @return The state which this state machine started in.
	 */
	public final S getStartState() {
		return startState;
	}

	@Override
	public final S getCurrentState() {
		return currentState;
	}

	/**
	 * The parent of a state machine does not change.
	 */
	@Override
	public final StateMachineCallback getParent() {
		return parent;
	}

	private final void setCurrentState(S newState) {
		this.currentState = newState;
	}

	@Override
	public final StateMachine<?> getActiveChild() {
		return activeChild;
	}

	protected final void setActiveChild(StateMachine<?> activeChild) {
		this.activeChild = activeChild;
	}

	public final S getPreviousState() {
		return previousState;
	}

	private final void setPreviousState(S previousState) {
		this.previousState = previousState;
	}

}
