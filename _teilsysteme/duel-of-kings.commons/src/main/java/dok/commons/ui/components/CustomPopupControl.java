package dok.commons.ui.components;

import javafx.scene.Parent;
import javafx.scene.control.PopupControl;

public class CustomPopupControl extends PopupControl {

	public CustomPopupControl(Parent content) {
		super();
		getScene().setRoot(content);
	}

}
