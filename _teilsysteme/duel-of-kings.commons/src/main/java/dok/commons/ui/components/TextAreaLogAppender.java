package dok.commons.ui.components;

import java.io.Serializable;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

@Plugin(name="TextAreaLogAppender", category="Core", elementType="appender", printObject=true)
public class TextAreaLogAppender extends AbstractAppender {

	// Class Constants

	private static final long serialVersionUID = -4499961556682439431L;

	// Class Attributes

	private static volatile TextArea javaFXLogDisplay = null;
	private static volatile JTextArea swingLogDisplay = null;

	// Constructor(s)

	protected TextAreaLogAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
		super(name, filter, layout, ignoreExceptions);
	}

	// Methods

	@Override
	public synchronized void append(LogEvent event) {
        final String message = new String(getLayout().toByteArray(event));

        try {
            try {
                if (getJavaFXLogDisplay() != null)
                    Platform.runLater(() -> getJavaFXLogDisplay().appendText(message));
                else if (getSwingLogDisplay() != null)
                	SwingUtilities.invokeLater(() -> getSwingLogDisplay().append(message));
                else
                	System.err.println(message);
            } catch (final Exception e) {
            	if (!ignoreExceptions()) {
                    throw new AppenderLoggingException(e);
                }
            }
        } catch (final IllegalStateException e) {
            // TODO
        }
	}

    @PluginFactory
    public static TextAreaLogAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new TextAreaLogAppender(name, filter, layout, true);
    }

	// Getter & Setter

	public static TextArea getJavaFXLogDisplay() {
		return javaFXLogDisplay;
	}

	public static void setJavaFXLogDisplay(TextArea javaFXLogDisplay) {
		TextAreaLogAppender.javaFXLogDisplay = javaFXLogDisplay;
	}

	public static JTextArea getSwingLogDisplay() {
		return swingLogDisplay;
	}

	public static void setSwingLogDisplay(JTextArea swingLogDisplay) {
		TextAreaLogAppender.swingLogDisplay = swingLogDisplay;
	}

}
