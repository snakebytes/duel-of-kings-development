package dok.commons.impl;

import org.junit.Assert;
import org.junit.Test;

public class CustomEventImplTest {

	CustomEventImpl bimbo = new CustomEventImpl(){};

	@Test
	public void consumeTEST() {
		Assert.assertFalse(bimbo.isConsumed());
		bimbo.consume();
		Assert.assertTrue(bimbo.isConsumed());
	}

}
