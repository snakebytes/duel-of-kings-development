package dok.commons.impl;

import org.junit.Assert;
import org.junit.Test;

public class CustomRunnableImplTest {
	CustomRunnableImpl bimbo = new CustomRunnableImpl(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
		}};
	
	@Test
	public void stoppingTEST() {
		Assert.assertFalse(bimbo.isStopping());
		bimbo.setStopping(true);
		Assert.assertTrue(bimbo.isStopping());
		bimbo.setStopping(false);
		Assert.assertFalse(bimbo.isStopping());
	}
}
