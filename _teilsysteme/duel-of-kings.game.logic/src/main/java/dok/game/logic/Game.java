package dok.game.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.model.Lineup;
import dok.commons.statemachine.StateMachineCallback;
import dok.game.model.Player;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameLineupService;
import dok.game.services.PlayerService;

public abstract class Game implements StateMachineCallback, PlayerService, GameLineupService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Services

	private final StateMachineCallback parent;
	private final AbilityDataService abilityClassService;
	private final CharacterDataService characterClassService;

	// Constants

	private final List<Player> players;

	// Constructor(s)

	public Game(StateMachineCallback parent, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		this.abilityClassService = abilityClassService;
		this.characterClassService = characterClassService;
		this.parent = parent;
		this.players = new ArrayList<>();

	}

	public Game(StateMachineCallback parent, CharacterDataService characterClassService, AbilityDataService abilityClassService, Player... participants) {
		this.abilityClassService = abilityClassService;
		this.characterClassService = characterClassService;
		this.parent = parent;

		this.players = new ArrayList<>();

		getPlayers().addAll(Arrays.asList(participants));

		shufflePlayers();
	}

	// Abstract Methods

	public abstract void start();

	// <PlayerListService>

	@Override
	public Player getPlayer(UUID id) {
		for (Player p : new ArrayList<>(getPlayers()))
			if (p.getPlayerId().equals(id))
				return p;
		return null;
	}

	@Override
	public final List<Player> getPlayers() {
		return players;
	}

	@Override
	public List<UUID> getPlayerIds() {
		return getPlayers().stream().map((Function<Player, UUID>) e -> e.getPlayerId()).collect(Collectors.toList());
	}

	@Override
	public final void setPlayerPosition(UUID player, int position) {
		logger.info("Player " + player + "'s starting position is set to " + position);
		getPlayers().remove(player);
		getPlayers().add(position, getPlayer(player));
	}

	@Override
	public final void shufflePlayers() {
		logger.debug("Players are being shuffled");
		Collections.shuffle(getPlayers());
	}

	// </PlayerListService>

	// <GameLineupService>

	@Override
	public final Lineup getLineup(UUID player) {
		for (Player p : new ArrayList<>(getPlayers()))
			if (p.getPlayerId().equals(player))
				return p.getLineup();
		return null;
	}

	// </GameLineupService>

	// Getter & Setter

	public final AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	public final CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	public final StateMachineCallback getParent() {
		return parent;
	}

}
