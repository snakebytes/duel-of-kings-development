package dok.game.logic;

import dok.game.model.Buff;
import dok.game.model.buffs.SilenceBuff;
import dok.game.model.buffs.StunBuff;

public abstract class GameLogicConstants {

	// General

	public static final int TIME_PER_ACTION = 18; // s
	public static final int AVAILABLE_ACTIONS_PER_TURN = 4;
	public static final int STUN_COUNTERS_THRESHOLD = 9;
	public static final int SURRENDER_CODE = -1337;
	public static final int UNDEFINED_ID = -1;
	public static final int INDEFINITE = 0;
	public static final int EFFECT_HISTORY_THRESHOLD = 10;
	public static final int FREE_SWAPS_PER_TURN = 2;
	public static final int TIME_MARKER_CHOOSE_TIME = 15; //s

	// Buffs

	public static final Class<? extends Buff> STUN_BUFF_CLASS = StunBuff.class;
	public static final Class<? extends Buff> SILENCE_BUFF_CLASS = SilenceBuff.class;

	// Lineups

	public static final int OFFENSIVE_LINEUP_ID = 301;
	public static final int DEFENSIVE_LINEUP_ID = 302;
	public static final int BALANCED_LINEUP_ID = 303;

}
