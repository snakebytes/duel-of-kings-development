package dok.game.logic.services.impl;

import dok.game.model.Buff;
import dok.game.model.CharacterAttribute;
import dok.game.model.GameEvent;
import dok.game.model.buffs.CurseBuff;
import dok.game.model.buffs.DefendBuff;
import dok.game.model.buffs.MagicHuntBuff;
import dok.game.model.buffs.TransferBuff;
import dok.game.model.buffs.TransferDebuff;
import dok.game.services.GameStateModelService;

public class BuffEventHandler {

	public void onBuffAdded(GameEvent event, Buff buff, GameStateModelService gameStateModelService) {
		if (buff instanceof CurseBuff) {
			CurseBuff curseBuff = (CurseBuff) buff;
			gameStateModelService.modifyBonusAttribute(buff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, -curseBuff.getAttackDamageMalus());
		} else if (buff instanceof DefendBuff) {
			DefendBuff defendBuff = (DefendBuff) buff;
			gameStateModelService.modifyBonusAttribute(defendBuff.getTargetCharacterId(), CharacterAttribute.ARMOR, defendBuff.getArmorBonus());
			gameStateModelService.modifyBonusAttribute(defendBuff.getTargetCharacterId(), CharacterAttribute.MAGIC_RESISTANCE, defendBuff.getMagicResistanceBonus());
		} else if (buff instanceof MagicHuntBuff) {
			MagicHuntBuff magicHuntBuff = (MagicHuntBuff) buff;
			gameStateModelService.modifyBonusAttribute(magicHuntBuff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, magicHuntBuff.getAttackDamageBonus());
		} else if (buff instanceof TransferBuff) {
			TransferBuff transferBuff = (TransferBuff) buff;
			gameStateModelService.modifyBonusAttribute(transferBuff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, transferBuff.getAttackDamageBonus());
		} else if (buff instanceof TransferDebuff) {
			TransferDebuff transferDebuff = (TransferDebuff) buff;
			gameStateModelService.modifyBonusAttribute(transferDebuff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, -transferDebuff.getAttackDamageMalus());
		}
	}

	public void onBuffRemove(GameEvent sourceEvent, Buff buff, GameStateModelService gameStateModelService) {
		if (buff instanceof CurseBuff) {
			CurseBuff curseBuff = (CurseBuff) buff;
			gameStateModelService.modifyBonusAttribute(buff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, curseBuff.getAttackDamageMalus());
		} else if (buff instanceof DefendBuff) {
			DefendBuff defendBuff = (DefendBuff) buff;
			gameStateModelService.modifyBonusAttribute(defendBuff.getTargetCharacterId(), CharacterAttribute.ARMOR, -defendBuff.getArmorBonus());
			gameStateModelService.modifyBonusAttribute(defendBuff.getTargetCharacterId(), CharacterAttribute.MAGIC_RESISTANCE, -defendBuff.getMagicResistanceBonus());
		} else if (buff instanceof MagicHuntBuff) {
			MagicHuntBuff magicHuntBuff = (MagicHuntBuff) buff;
			gameStateModelService.modifyBonusAttribute(magicHuntBuff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, -magicHuntBuff.getAttackDamageBonus());
		} else if (buff instanceof TransferBuff) {
			TransferBuff transferBuff = (TransferBuff) buff;
			gameStateModelService.modifyBonusAttribute(transferBuff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, -transferBuff.getAttackDamageBonus());
		} else if (buff instanceof TransferDebuff) {
			TransferDebuff transferDebuff = (TransferDebuff) buff;
			gameStateModelService.modifyBonusAttribute(transferDebuff.getTargetCharacterId(), CharacterAttribute.ATTACK_DAMAGE, transferDebuff.getAttackDamageMalus());
		}
	}

}
