package dok.game.logic.services.impl;

import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.Damage;
import dok.game.model.GameStateModel;
import dok.game.model.api.CharacterDataService;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.HealthModificationCalculationService;

public class DamageCalculationProvider implements HealthModificationCalculationService<Damage> {

	@Override
	public int getCalculatedAmount(Damage damage, GameStateModel model, CharacterDataService characterClassService) {
		Character target = model.getCharacterAt(damage.getTarget());
		int actualDamage = damage.getBaseAmount();

		// Recalculate resisted amount
		switch (damage.getDamageType()) {
			case MAGICAL:
				damage.setResistedAmount(CharacterStateUtils.getCharacterAttributeValue(target, CharacterAttribute.MAGIC_RESISTANCE, characterClassService));
				break;
			case PHYSICAL:
				damage.setResistedAmount(CharacterStateUtils.getCharacterAttributeValue(target, CharacterAttribute.ARMOR, characterClassService));
				break;
		}

		// Reduce damage by resisted amount
		actualDamage = Math.max(1, actualDamage - damage.getResistedAmount());

		// Reduce by block value
		actualDamage = Math.max(0, actualDamage - damage.getBlockedAmount());

		// Return calculated amount
		return actualDamage;
	}

}
