package dok.game.logic.services.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.logic.util.GameLogicUtils;
import dok.game.model.Ability;
import dok.game.model.AbilityClass;
import dok.game.model.ActionEffect;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.Damage;
import dok.game.model.DamageType;
import dok.game.model.Effect;
import dok.game.model.Healing;
import dok.game.model.Position;
import dok.game.model.ReactionEffect;
import dok.game.model.TargetType;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.buffs.BlockBuff;
import dok.game.model.buffs.CurseBuff;
import dok.game.model.buffs.DauntingDefenderBuff;
import dok.game.model.buffs.DefendBuff;
import dok.game.model.buffs.MagicHuntBuff;
import dok.game.model.buffs.SilenceBuff;
import dok.game.model.buffs.StunBuff;
import dok.game.model.buffs.TransferBuff;
import dok.game.model.buffs.TransferDebuff;
import dok.game.model.buffs.WardensCodexBuff;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.effects.actions.AdaptiveBombEffect;
import dok.game.model.effects.actions.ArouseEffect;
import dok.game.model.effects.actions.AttackEffect;
import dok.game.model.effects.actions.AxeBashEffect;
import dok.game.model.effects.actions.BombardmentEffect;
import dok.game.model.effects.actions.BreathtakingSilenceEffect;
import dok.game.model.effects.actions.CallingPackEffect;
import dok.game.model.effects.actions.CurseEffect;
import dok.game.model.effects.actions.DefendEffect;
import dok.game.model.effects.actions.HealEffect;
import dok.game.model.effects.actions.PowerTransferEffect;
import dok.game.model.effects.actions.SilenceEffect;
import dok.game.model.effects.actions.SpearBlowEffect;
import dok.game.model.effects.actions.SwapEffect;
import dok.game.model.effects.actions.ThrowBombsEffect;
import dok.game.model.effects.actions.WardensCodexEffect;
import dok.game.model.effects.reactions.AdvTranquilityReactionEffect;
import dok.game.model.effects.reactions.DauntingDefenderReactionEffect;
import dok.game.model.effects.reactions.DauntingDefenderReapplyReactionEffect;
import dok.game.model.effects.reactions.EncourageReactionEffect;
import dok.game.model.effects.reactions.MagicHuntAddReactionEffect;
import dok.game.model.effects.reactions.MagicHuntRemoveReactionEffect;
import dok.game.model.effects.reactions.StormSprintReactionEffect;
import dok.game.model.effects.reactions.TranquilityReactionEffect;
import dok.game.model.effects.reactions.VeteranOfBattlesReactionEffect;
import dok.game.model.effects.reactions.WardensCodexReactionEffect;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.EffectExecutionService;
import dok.game.services.GameStateModelService;

public class EffectExecutionProvider implements EffectExecutionService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// <--- EffectExecutionService --->

	@Override
	public void execute(Effect effect, GameStateModelService gameStateModelService, CharacterDataService characterDataService, AbilityDataService abilityDataService) {
		logger.entry(effect, gameStateModelService, characterDataService, abilityDataService);

		// <-- Actions --->
		if (effect instanceof ActionEffect) {
		ActionEffect actionEffect = (ActionEffect) effect;

			// <--- Adaptive Bomb --->
			if (actionEffect instanceof AdaptiveBombEffect) {

				AdaptiveBombEffect adaptiveBombEffect = (AdaptiveBombEffect) actionEffect;

				// Handle Time Cost
				handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				int dmg = 0;
				// Find Primary target and calculate damage from its armor
				for (Position pos : adaptiveBombEffect.getTargetedPositions().keySet()) {
					if(adaptiveBombEffect.getTargetedPositions().get(pos).equals(TargetType.PRIMARY)) {
						dmg = CharacterStateUtils.getCharacterAttributeValue(gameStateModelService.getCharacterAt(pos), CharacterAttribute.ARMOR, characterDataService)
								+ adaptiveBombEffect.getProperties().get("dmgBonus").intValue();
					}
				}
				// Apply modified damage to all targets
				for (Position pos : adaptiveBombEffect.getTargetedPositions().keySet()) {
					int dmgt = dmg;
					if (adaptiveBombEffect.getTargetedPositions().get(pos) == TargetType.SECONDARY) {
						logger.debug("TARGET IS SECONDARY, damage was: " + dmg);
						dmgt = (int) Math.round((double) dmg * adaptiveBombEffect.getProperties().get("secondaryDamageMultiplier").doubleValue());
						logger.debug("TARGET IS SECONDARY, damage is: " + dmgt);
					}
					gameStateModelService.applyDamage(new Damage(dmgt, adaptiveBombEffect, pos,
							adaptiveBombEffect.getTargetedPositions().get(pos), DamageType.PHYSICAL), characterDataService);
				}

			// <--- Arouse --->
			} else if (actionEffect instanceof ArouseEffect) {

				ArouseEffect arouseEffect = (ArouseEffect) actionEffect;

				// Handle Time Cost
				handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);


				for (Position pos : arouseEffect.getTargetedPositions().keySet()) {
					Character target = gameStateModelService.getCharacterAt(pos);
					if (target.isStunned()) {
						for(int i = 0; i < arouseEffect.getProperties().get("stunShortenAmount").intValue(); i++) {
							gameStateModelService.reduceBuffDuration(target.getBuff(StunBuff.class));
						}
					} else {
						gameStateModelService.modifyStunCounters(arouseEffect, target.getId(), -arouseEffect.getProperties().get("stunCountersAmount").intValue());
					}
				}

			// <--- Attack --->
			} else if (actionEffect instanceof AttackEffect) {

				AttackEffect attackEffect = (AttackEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Handle attack
				this.handleAttack(attackEffect, gameStateModelService, characterDataService, abilityDataService);

			// <--- AxeBash --->
			} else if (actionEffect instanceof AxeBashEffect) {

				AxeBashEffect axeBashEffect = (AxeBashEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Handle attack
				this.handleAttack(axeBashEffect, gameStateModelService, characterDataService, abilityDataService);

				// Add stun counter to all targets
				for (Position target : axeBashEffect.getTargetedPositions().keySet())
					gameStateModelService.modifyStunCounters(axeBashEffect, gameStateModelService.getCharacterIdFromPosition(target), axeBashEffect.getProperties().get("stunCountersAmount").intValue());

			// <--- Bombardment --->
			} else if (actionEffect instanceof BombardmentEffect) {

				BombardmentEffect bombardmentEffect = (BombardmentEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Deal damage
				for (Position target : bombardmentEffect.getTargetedPositions().keySet())
					gameStateModelService.applyDamage(new Damage(bombardmentEffect.getProperties().get("damageAmount").intValue(), bombardmentEffect, target,
							bombardmentEffect.getTargetedPositions().get(target), DamageType.PHYSICAL), characterDataService);

			// <--- Breathtaking Silence --->
			} else if (actionEffect instanceof BreathtakingSilenceEffect) {

				BreathtakingSilenceEffect breathtakingSilenceEffect = (BreathtakingSilenceEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Silence all targets
				for (Position pos : breathtakingSilenceEffect.getTargetedPositions().keySet()) {
					Character target = gameStateModelService.getCharacterAt(pos);
					gameStateModelService.addBuff(new SilenceBuff(gameStateModelService.getActivePlayer(), breathtakingSilenceEffect,
							target.getId(), breathtakingSilenceEffect.getProperties().get("duration").intValue()));
				}

			// <--- Calling the Pack --->
			} else if (actionEffect instanceof CallingPackEffect) {

				CallingPackEffect callingPackEffect = (CallingPackEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Deal damage to all targets accordingly
				for (Position pos : callingPackEffect.getTargetedPositions().keySet()) {
					int d = callingPackEffect.getProperties().get("dmgToUndamaged").intValue();
					if(gameStateModelService.getCharacterAt(pos).getDamageTaken() > 0) {
						d = callingPackEffect.getProperties().get("dmgToDamaged").intValue();
					}
					gameStateModelService.applyDamage(new Damage(d, callingPackEffect, pos,
							callingPackEffect.getTargetedPositions().get(pos), DamageType.PHYSICAL), characterDataService);
				}

			// <--- Curse --->
			} else if (actionEffect instanceof CurseEffect) {

				CurseEffect curseEffect = (CurseEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Add buff to targets
				for (Position target : curseEffect.getTargetedPositions().keySet()) {
						gameStateModelService.addBuff(new CurseBuff(
								gameStateModelService.getActivePlayer(),
								curseEffect,
								gameStateModelService.getCharacterIdFromPosition(target),
								curseEffect.getProperties().get("duration").intValue(),
								curseEffect.getProperties().get("attackDamageMalus").intValue()));
				}

			// <--- Defend --->
			} else if (actionEffect instanceof DefendEffect) {

				DefendEffect defendEffect = (DefendEffect) actionEffect;

				// Handle Time Cost
				gameStateModelService.modifyTimeCounters(defendEffect.getSourceAbility().getOwningCharacterId(), 1);
		        gameStateModelService.modifyTimeCounters(defendEffect.getSourceAbility(),
		        		abilityDataService.getAbilityClass(defendEffect.getSourceAbility().getAbilityClassId()).getTimeCountersCost());

		        // Add buff to caster
				gameStateModelService.addBuff(new DefendBuff(
						gameStateModelService.getActivePlayer(),
						defendEffect,
						defendEffect.getSourceAbility().getOwningCharacterId(),
						defendEffect.getProperties().get("duration").intValue(),
						defendEffect.getProperties().get("armorBonus").intValue(),
						defendEffect.getProperties().get("magicResistanceBonus").intValue()));

			// <--- Heal --->
			} else if (actionEffect instanceof HealEffect) {

				HealEffect healEffect = (HealEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Heal all targets
				for (Position pos : healEffect.getTargetedPositions().keySet()) {
					gameStateModelService.applyHealing(new Healing(healEffect.getProperties().get("healingAmount").intValue(),
							healEffect, pos, healEffect.getTargetedPositions().get(pos)), characterDataService);
				}

			// <--- Power Transfer --->
			} else if (actionEffect instanceof PowerTransferEffect) {

				PowerTransferEffect powerTransferEffect = (PowerTransferEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Add buff or debuff to targets accordingly
				for(Position pos : powerTransferEffect.getTargetedPositions().keySet()) {
					if(CharacterStateUtils.isCharacterAlive(gameStateModelService.getCharacterAt(pos), characterDataService)) {
						gameStateModelService.addBuff(new TransferDebuff(gameStateModelService.getActivePlayer(), powerTransferEffect, gameStateModelService.getCharacterIdFromPosition(pos),
								powerTransferEffect.getProperties().get("duration").intValue(), powerTransferEffect.getProperties().get("atkValue").intValue()));
						Position buffpos = new Position(pos.getPlayer(), pos.getRow() + 1, pos.getCol());
						UUID charaId = gameStateModelService.getCharacterIdFromPosition(buffpos);
						if(CharacterStateUtils.isCharacterAlive(gameStateModelService.getCharacter(charaId), characterDataService)) {
							gameStateModelService.addBuff(new TransferBuff(gameStateModelService.getActivePlayer(), powerTransferEffect, gameStateModelService.getCharacterIdFromPosition(buffpos),
									powerTransferEffect.getProperties().get("duration").intValue(), powerTransferEffect.getProperties().get("atkValue").intValue()));
						}
					}
				}

			// <--- Silence --->
			} else if (actionEffect instanceof SilenceEffect) {

				SilenceEffect silenceEffect = (SilenceEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Silence all targets
				for (Position pos : silenceEffect.getTargetedPositions().keySet()) {
					Character target = gameStateModelService.getCharacterAt(pos);
					if(!target.isSilenced()) {
						gameStateModelService.addBuff(new SilenceBuff(gameStateModelService.getActivePlayer(), silenceEffect, target.getId(),
								silenceEffect.getProperties().get("duration").intValue()));
					}
				}

			// <--- Spear Blow --->
			} else if (actionEffect instanceof SpearBlowEffect) {

				SpearBlowEffect spearBlowEffect = (SpearBlowEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Handle attack
				this.handleAttack(spearBlowEffect, gameStateModelService, characterDataService, abilityDataService);

				// Add time counters to all targets
				for (Position pos : spearBlowEffect.getTargetedPositions().keySet()) {
					gameStateModelService.modifyTimeCounters(pos, spearBlowEffect.getProperties().get("timeCountersAmount").intValue());
				}

			// <--- Swap --->
			} else if (actionEffect instanceof SwapEffect) {

				SwapEffect swapEffect = (SwapEffect) actionEffect;

				// Handle the "Swap"-Ruling
				if (gameStateModelService.getRemainingFreeSwaps() <= 0)
					gameStateModelService.modifyTimeCounters(swapEffect.getSourceAbility().getOwningCharacterId(), 1);
				else
					gameStateModelService.reduceRemainingFreeSwaps();

				// Perform the swap
				gameStateModelService.swap(swapEffect, swapEffect.getSourceAbility().getOwningCharacterId(), swapEffect.getTargetedPositions().keySet().iterator().next());

			// <--- Throw Bomb --->
			} else if (actionEffect instanceof ThrowBombsEffect) {

				ThrowBombsEffect throwBombsEffect = (ThrowBombsEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Grab all living enemies
				List<Character> list = gameStateModelService.getCharacters().stream()
						.filter((character) -> CharacterStateUtils.isCharacterAlive(character, characterDataService) &&
								!character.getOwningPlayerId().equals(gameStateModelService.getCharacter(throwBombsEffect.getSourceCharacterId()).getOwningPlayerId()))
						.collect(Collectors.toList());
				Collections.shuffle(list);

				// Body those fools
				for(int i = 0; i < throwBombsEffect.getProperties().get("bombAmount").intValue(); i++) {
					if (list.size() == 0) break;
					Character target = list.remove(0);

					// Simulate Attack against target
					Character self = gameStateModelService.getCharacter(throwBombsEffect.getSourceCharacterId());
					Ability ab = self.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID);
					AbilityClass ac = abilityDataService.getAbilityClass(AbilityClassIds.ATTACK_ABILITY_CLASS_ID);
			        HashSet<Position> targets = new HashSet<Position>();
			        targets.add(target.getPosition());
			        Map<Position, TargetType> hm = ac.getHitFilter().generateTargets(ab, targets, gameStateModelService, characterDataService, abilityDataService);
			        abilityDataService.getAbilityClass(AbilityClassIds.ATTACK_ABILITY_CLASS_ID).getProperties();
					AttackEffect ae = new AttackEffect(ab, hm, abilityDataService.getAbilityClass(AbilityClassIds.ATTACK_ABILITY_CLASS_ID).getProperties());

					// Apply damage manually to all targets that would be hit by an attack
					for (Position pos : ae.getTargetedPositions().keySet()) {
						int dmg = throwBombsEffect.getProperties().get("bombDamage").intValue();
						if (throwBombsEffect.getTargetedPositions().get(pos) == TargetType.SECONDARY) {
							logger.debug("TARGET IS SECONDARY, damage was: " + dmg);
							dmg = (int) Math.round((double) dmg * throwBombsEffect.getProperties().get("secondaryDamageMultiplier").doubleValue());
							logger.debug("TARGET IS SECONDARY, damage is: " + dmg);
						}
						gameStateModelService.applyDamage(new Damage(dmg, throwBombsEffect, pos, throwBombsEffect.getTargetedPositions().get(pos),
								DamageType.PHYSICAL), characterDataService);
					}
				}

			// <--- Warden's Codex --->
			} else if (actionEffect instanceof WardensCodexEffect) {

				WardensCodexEffect wardensCodexEffect = (WardensCodexEffect) actionEffect;

				// Handle Time Cost
				this.handleTimeCosts(actionEffect, gameStateModelService, characterDataService, abilityDataService);

				// Add buff to caster
				gameStateModelService.addBuff(new WardensCodexBuff(
						gameStateModelService.getActivePlayer(),
						wardensCodexEffect,
						wardensCodexEffect.getSourceAbility().getOwningCharacterId(),
						wardensCodexEffect.getProperties().get("duration").intValue()));

			}

		// <-- Reactions --->
		} else if (effect instanceof ReactionEffect) {
			ReactionEffect reactionEffect = (ReactionEffect) effect;

			if (reactionEffect instanceof WardensCodexReactionEffect) {

				WardensCodexReactionEffect wardensCodexReactionEffect = (WardensCodexReactionEffect) reactionEffect;

				Position target = null;
				Map<Position, TargetType> targets = wardensCodexReactionEffect.getTrigger().getSourceEvent().getSourceEffect().getTargetedPositions();
				target = wardensCodexReactionEffect.getTrigger().getSourceEvent().getSourceEffect().getTargetedPositions().keySet()
						.stream().filter((pos) -> targets.get(pos) == TargetType.PRIMARY).findFirst().get();
				gameStateModelService.swap(wardensCodexReactionEffect, wardensCodexReactionEffect.getSourceAbility().getOwningCharacterId(), target);
				gameStateModelService.removeBuff(wardensCodexReactionEffect.getSourceAbility().getOwningCharacterId(), WardensCodexBuff.class);

			} else if (reactionEffect instanceof VeteranOfBattlesReactionEffect) {

				VeteranOfBattlesReactionEffect veteranOfBattlesReactionEffect = (VeteranOfBattlesReactionEffect) reactionEffect;

				gameStateModelService.addBuff(new BlockBuff(gameStateModelService.getActivePlayer(), veteranOfBattlesReactionEffect, veteranOfBattlesReactionEffect.getSourceCharacterId(),
						veteranOfBattlesReactionEffect.getProperties().get("duration").intValue(), veteranOfBattlesReactionEffect.getProperties().get("blockValue").intValue()));

			} else if (reactionEffect instanceof TranquilityReactionEffect) {

				TranquilityReactionEffect tranquilityReactionEffect = (TranquilityReactionEffect) reactionEffect;

				int lowestHP = Integer.MAX_VALUE;
				Position target = null;
				for(UUID id : gameStateModelService.getCharacterIds(gameStateModelService.getActivePlayer())) {
					Character c = gameStateModelService.getCharacter(id);
					if(CharacterStateUtils.isCharacterAlive(c, characterDataService)) {
						int hp = CharacterStateUtils.getCharacterAttributeValue(c, CharacterAttribute.HEALTH, characterDataService) - c.getDamageTaken();
						if(hp < lowestHP && c.getDamageTaken() > 0) {
							lowestHP = hp;
							target = c.getPosition();
						}
					}
				}

				if (target != null)
					gameStateModelService.applyHealing(new Healing(tranquilityReactionEffect.getProperties().get("healingAmount").intValue(), tranquilityReactionEffect,
						target, TargetType.PRIMARY), characterDataService);

			} else if (reactionEffect instanceof StormSprintReactionEffect) {

				StormSprintReactionEffect stormSprintReactionEffect = (StormSprintReactionEffect) reactionEffect;

				gameStateModelService.modifyTimeCounters(stormSprintReactionEffect.getSourceCharacterId(),
						-1 * stormSprintReactionEffect.getProperties().get("timeMarkersChar").intValue());

				for(Ability ab : gameStateModelService.getCharacter(stormSprintReactionEffect.getSourceCharacterId()).getAbilities()) {
					gameStateModelService.modifyTimeCounters(ab, stormSprintReactionEffect.getProperties().get("timeMarkersAbil").intValue());
				}

			} else if (reactionEffect instanceof MagicHuntAddReactionEffect) {

				MagicHuntAddReactionEffect magicHuntAddReactionEffect = (MagicHuntAddReactionEffect) reactionEffect;

		        Buff b = new MagicHuntBuff(gameStateModelService.getActivePlayer(), magicHuntAddReactionEffect, magicHuntAddReactionEffect.getSourceCharacterId(),
		        		magicHuntAddReactionEffect.getProperties().get("duration").intValue(), magicHuntAddReactionEffect.getProperties().get("atkValue").intValue());
		        gameStateModelService.addBuff(b);

			} else if (reactionEffect instanceof MagicHuntRemoveReactionEffect) {

				MagicHuntRemoveReactionEffect magicHuntRemoveReactionEffect = (MagicHuntRemoveReactionEffect) reactionEffect;

				gameStateModelService.removeBuff(magicHuntRemoveReactionEffect.getSourceCharacterId(), MagicHuntBuff.class);

			} else if (reactionEffect instanceof EncourageReactionEffect) {

				EncourageReactionEffect encourageReactionEffect = (EncourageReactionEffect) reactionEffect;

				Character source = gameStateModelService.getCharacter(encourageReactionEffect.getSourceCharacterId());
				if(source.getPosition().getCol() != 0) {
					Character c = gameStateModelService.getCharacterAt(new Position(source.getOwningPlayerId(), source.getPosition().getRow(), source.getPosition().getCol()-1));
					if(CharacterStateUtils.isCharacterAlive(c, characterDataService)) {
						for(int i = 0; i < encourageReactionEffect.getProperties().get("timeMarkersAmt").intValue(); i++) {
							gameStateModelService.reduceTimeCounters(c);
						}
					}
				}
				if(source.getPosition().getCol() != 2) {
					Character c = gameStateModelService.getCharacterAt(new Position(source.getOwningPlayerId(), source.getPosition().getRow(), source.getPosition().getCol()+1));
					if(CharacterStateUtils.isCharacterAlive(c, characterDataService)) {
						for(int i = 0; i < encourageReactionEffect.getProperties().get("timeMarkersAmt").intValue(); i++) {
							gameStateModelService.reduceTimeCounters(c);
						}
					}
				}

			} else if (reactionEffect instanceof DauntingDefenderReactionEffect) {

				DauntingDefenderReactionEffect dauntingDefenderReactionEffect = (DauntingDefenderReactionEffect) reactionEffect;

				if(gameStateModelService.getCharacter(dauntingDefenderReactionEffect.getSourceCharacterId()).hasBuff(DauntingDefenderBuff.class)) {
					gameStateModelService.removeBuff(dauntingDefenderReactionEffect.getSourceCharacterId(), DauntingDefenderBuff.class);

					Character target = gameStateModelService.getCharacter(dauntingDefenderReactionEffect.getTrigger().getSourceEvent().getSourceEffect().getSourceCharacterId());
					target = GameLogicUtils.getFirstAliveOfCol(target.getPosition().getCol(), target.getOwningPlayerId(), gameStateModelService, characterDataService);

					Character self = gameStateModelService.getCharacter(dauntingDefenderReactionEffect.getSourceCharacterId());
					Ability ab = self.getAbility(AbilityClassIds.ATTACK_ABILITY_CLASS_ID);
					AbilityClass ac = abilityDataService.getAbilityClass(AbilityClassIds.ATTACK_ABILITY_CLASS_ID);
			        HashSet<Position> targets = new HashSet<Position>();
			        targets.add(target.getPosition());
			        Map<Position, TargetType> hm = ac.getHitFilter().generateTargets(ab, targets, gameStateModelService, characterDataService, abilityDataService);

			        for (Position t : hm.keySet()) {

						// Get the damage to be dealt
						int amount = CharacterStateUtils.getCharacterAttributeValue(self, CharacterAttribute.ATTACK_DAMAGE, characterDataService);

						// Manipulate the damage if the target is secondary
						if (hm.get(t) == TargetType.SECONDARY) {
							logger.debug("TARGET IS SECONDARY, damage was: " + amount);
							amount = (int) Math.round((double) amount * dauntingDefenderReactionEffect.getProperties().get("secondaryDamageMultiplier").doubleValue());
							logger.debug("TARGET IS SECONDARY, damage is: " + amount);
						}

						// Let the model service apply the damage
						gameStateModelService.applyDamage(new Damage(amount,dauntingDefenderReactionEffect,t,dauntingDefenderReactionEffect.getTargetedPositions().get(t),
								gameStateModelService.getCharacter(dauntingDefenderReactionEffect.getSourceAbility().getOwningCharacterId()).getDamageType()), characterDataService);
					}
				}

			} else if (reactionEffect instanceof DauntingDefenderReapplyReactionEffect) {

				DauntingDefenderReapplyReactionEffect dauntingDefenderReapplyReactionEffect = (DauntingDefenderReapplyReactionEffect) reactionEffect;

				if(!gameStateModelService.getCharacter(dauntingDefenderReapplyReactionEffect.getSourceCharacterId()).hasBuff(DauntingDefenderBuff.class)) {
					gameStateModelService.addBuff(new DauntingDefenderBuff(gameStateModelService.getActivePlayer(), dauntingDefenderReapplyReactionEffect,
							dauntingDefenderReapplyReactionEffect.getSourceCharacterId(), 0));
				}

			} else if (reactionEffect instanceof AdvTranquilityReactionEffect) {

				AdvTranquilityReactionEffect advTranquilityReactionEffect = (AdvTranquilityReactionEffect) reactionEffect;

				for(int i = 0; i < advTranquilityReactionEffect.getProperties().get("healCount").intValue(); i++) {
					int lowestHP = 99;
					Position target = null;
					for(UUID id : gameStateModelService.getCharacterIds(gameStateModelService.getActivePlayer())) {
						Character c = gameStateModelService.getCharacter(id);
						if(CharacterStateUtils.isCharacterAlive(c, characterDataService)) {
							int hp = CharacterStateUtils.getCharacterAttributeValue(c, CharacterAttribute.HEALTH, characterDataService) - c.getDamageTaken();
							if(hp < lowestHP && c.getDamageTaken() > 0) {
								lowestHP = hp;
								target = c.getPosition();
							}
						}
					}

					if (target != null)
						gameStateModelService.applyHealing(new Healing(advTranquilityReactionEffect.getProperties().get("healingAmount").intValue(),
							advTranquilityReactionEffect, target, TargetType.PRIMARY), characterDataService);
				}

			}

		}

		logger.exit();
	}

	// Convenience Methods

	protected final void handleTimeCosts(ActionEffect actionEffect, GameStateModelService gameStateModelService, CharacterDataService characterDataService, AbilityDataService abilityDataService) {
		gameStateModelService.modifyTimeCounters(actionEffect.getSourceAbility().getOwningCharacterId(), 2);
        gameStateModelService.modifyTimeCounters(actionEffect.getSourceAbility(), abilityDataService.getAbilityClass(actionEffect.getSourceAbility().getAbilityClassId()).getTimeCountersCost());
	}

	protected final void handleAttack(AttackEffect attackEffect, GameStateModelService gameStateModelService, CharacterDataService characterDataService, AbilityDataService abilityDataService) {
		// Iterate over targets
		for (Position target : attackEffect.getTargetedPositions().keySet()) {

			// Get the damage to be dealt
			int amount = CharacterStateUtils.getCharacterAttackDamage(gameStateModelService.getCharacter(attackEffect.getSourceAbility().getOwningCharacterId()), characterDataService);

			// Manipulate the damage if the target is secondary
			if (attackEffect.getTargetedPositions().get(target) == TargetType.SECONDARY) {
				logger.debug("TARGET IS SECONDARY, damage was: " + amount);
				amount = (int) Math.round((double) amount * attackEffect.getProperties().get("secondaryDamageMultiplier").doubleValue());
				logger.debug("TARGET IS SECONDARY, damage is: " + amount);
			}

			// Let the model service apply the damage
			gameStateModelService.applyDamage(new Damage(amount, attackEffect, target, attackEffect.getTargetedPositions().get(target),
					gameStateModelService.getCharacter(attackEffect.getSourceAbility().getOwningCharacterId()).getDamageType()), characterDataService);
		}
	}

}
