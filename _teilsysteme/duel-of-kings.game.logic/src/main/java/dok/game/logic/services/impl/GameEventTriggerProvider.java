package dok.game.logic.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.Ability;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.Damage;
import dok.game.model.Effect;
import dok.game.model.GameEvent;
import dok.game.model.GameEventType;
import dok.game.model.GameStateModel;
import dok.game.model.TargetType;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.buffs.DauntingDefenderBuff;
import dok.game.model.buffs.WardensCodexBuff;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.constants.TriggerClassIds;
import dok.game.model.effects.actions.AttackEffect;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.GameEventTriggerService;

public class GameEventTriggerProvider implements GameEventTriggerService {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Methods

	@Override
    public Trigger generateEventTriggers(Ability ability, GameEvent sourceEvent, GameStateModel model, CharacterDataService characterClassService, AbilityDataService abilityClassService) {

        if (sourceEvent.getEventType() == GameEventType.CHARACTER_UPGRADED
            && ((Character)sourceEvent.getParameters()[0]).getId().equals(ability.getOwningCharacterId())) {

            // Passive Effect: Veteran of Battles
            if(ability.getAbilityClassId() == AbilityClassIds.VETERAN_OF_BATTLES_ABILITY_CLASS_ID
                    && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService))
            {
                return new Trigger(sourceEvent, TriggerClassIds.VETERAN_OF_BATTLES_TRIGGER_CLASS_ID, ability);
            }

        } else if (sourceEvent.getEventType() == GameEventType.DAMAGE_APPLIED) {

            // Passive Effect: Magic Hunt (removing markers)
        	Damage d = (Damage) sourceEvent.getParameters()[0];
            if(ability.getAbilityClassId() == AbilityClassIds.MAGIC_HUNT_ABILITY_CLASS_ID
            && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService)
            && d.getSource().getSourceCharacterId().equals(ability.getOwningCharacterId())) {
            	return new Trigger(sourceEvent, TriggerClassIds.MAGIC_HUNT_REMOVE_TRIGGER_CLASS_ID, ability);
            }

        } else if (sourceEvent.getEventType() == GameEventType.TURN_STARTED) {

            // Passive Effect: Tranquility
            if(ability.getAbilityClassId() == AbilityClassIds.TRANQUILITY_ABILITY_CLASS_ID
            && model.getCharacter(ability.getOwningCharacterId()).getOwningPlayerId().equals(model.getActivePlayer())
            && model.getCharacter(ability.getOwningCharacterId()).isReady()
            && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService)) {
                return new Trigger(sourceEvent, TriggerClassIds.TRANQUILITY_TRIGGER_CLASS_ID, ability);
            }

            // Passive Effect: Advanced Tranquility
            else if(ability.getAbilityClassId() == AbilityClassIds.ADV_TRANQUILITY_ABILITY_CLASS_ID
            && model.getCharacter(ability.getOwningCharacterId()).getOwningPlayerId().equals(model.getActivePlayer())
            && model.getCharacter(ability.getOwningCharacterId()).isReady()
            && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService)) {
                return new Trigger(sourceEvent, TriggerClassIds.ADV_TRANQUILITY_TRIGGER_CLASS_ID, ability);
            }

            // Passive Effect: Encourage
            else if(ability.getAbilityClassId() == AbilityClassIds.ENCOURAGE_ABILITY_CLASS_ID
            && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService)
            && model.getCharacter(ability.getOwningCharacterId()).getOwningPlayerId().equals(model.getActivePlayer())) {
            	return new Trigger(sourceEvent, TriggerClassIds.ENCOURAGE_TRIGGER_CLASS_ID, ability);
            }

            // Passive Effect: Reapply Daunting Defender
            else if(ability.getAbilityClassId() == AbilityClassIds.DAUNTING_DEFENDER_ABILITY_CLASS_ID
            && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService)
            && !model.getCharacter(ability.getOwningCharacterId()).hasBuff(DauntingDefenderBuff.class)) {
            	return new Trigger(sourceEvent, TriggerClassIds.DAUNTING_DEFENDER_REAPPLY_TRIGGER_CLASS_ID, ability);
            }

        } else if (sourceEvent.getEventType() == GameEventType.BUFF_ADDED) {

            // Passive Effect: Magic Hunt (adding markers)
            Buff b = (Buff) sourceEvent.getParameters()[0];
            if(ability.getAbilityClassId() == AbilityClassIds.MAGIC_HUNT_ABILITY_CLASS_ID
            && CharacterStateUtils.isCharacterAlive(model.getCharacter(ability.getOwningCharacterId()), characterClassService)
            && !b.isPositive()
            && b.getTargetCharacterId().equals(ability.getOwningCharacterId())) {
                return new Trigger(sourceEvent, TriggerClassIds.MAGIC_HUNT_ADD_TRIGGER_CLASS_ID, ability);
            }

        } else if(sourceEvent.getEventType() == GameEventType.ABILITY_CAST) {

        	if(ability.getAbilityClassId() == AbilityClassIds.STORM_SPRINT_ABILITY_CLASS_ID
        	&& sourceEvent.getSourceEffect().getSourceCharacterId().equals(ability.getOwningCharacterId())) {
        		return new Trigger(sourceEvent, TriggerClassIds.STORM_SPRINT_TRIGGER_CLASS_ID, ability);
        	}

        }

        // Return default value:
        return logger.exit(GameEventTriggerService.super.generateEventTriggers(ability, sourceEvent, model, characterClassService, abilityClassService));
    }

	@Override
	public Trigger generateEventTriggers(Buff buff, GameEvent sourceEvent, GameStateModel model, CharacterDataService characterClassService, AbilityDataService abilityClassService) {

		Character buffTarget = model.getCharacter(buff.getTargetCharacterId());

		if (sourceEvent.getEventType() == GameEventType.DAMAGE_APPLIED
		&& CharacterStateUtils.isCharacterAlive(buffTarget, characterClassService)
		&& buffTarget.hasBuff(DauntingDefenderBuff.class)
		&& ((Damage)sourceEvent.getParameters()[0]).getTarget().equals(buffTarget.getPosition()))
		{
			return logger.exit(new Trigger(sourceEvent, TriggerClassIds.DAUNTING_DEFENDER_TRIGGER_CLASS_ID, buff));

        } else if(sourceEvent.getEventType() == GameEventType.ABILITY_CAST) {
        	Effect abilityEffect = sourceEvent.getSourceEffect();

        	if (AttackEffect.class.isAssignableFrom(abilityEffect.getClass())
        			&& buff instanceof WardensCodexBuff
    				&& !model.getCharacter(abilityEffect.getSourceCharacterId()).getOwningPlayerId()
    				.equals(model.getCharacter(buff.getTargetCharacterId()).getOwningPlayerId())
    				&& abilityEffect.getTargetedPositions().get(model.getCharacterPosition(buff.getTargetCharacterId())) != TargetType.PRIMARY) {
    			return new Trigger(sourceEvent, TriggerClassIds.WARDENS_CODE_TRIGGER_CLASS_ID, buff);
    		}
        }

		// Return default value:
		return logger.exit(GameEventTriggerService.super.generateEventTriggers(buff, sourceEvent, model, characterClassService, abilityClassService));
	}

}
