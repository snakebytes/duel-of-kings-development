package dok.game.logic.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.logic.GameLogicConstants;
import dok.game.model.Ability;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterEnhancementClass;
import dok.game.model.Damage;
import dok.game.model.Effect;
import dok.game.model.GameEvent;
import dok.game.model.GameEventType;
import dok.game.model.GameStateModel;
import dok.game.model.Healing;
import dok.game.model.Position;
import dok.game.model.api.CharacterDataService;
import dok.game.model.buffs.BlockBuff;
import dok.game.model.buffs.StunBuff;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.GameEventHandler;
import dok.game.services.GameStateModelService;
import dok.game.services.HealthModificationCalculationService;

/**
 *
 * Implementation of the {@linkplain GameStateModelService} interface.
 *
 * @author Konstantin Schaper
 * @since 0.1.0
 */
public final class GameStateModelProvider extends GameStateModelService {

	// Class Constants

	private static final long serialVersionUID = -4419767559475459804L;
	private static final Logger LOGGER = LogManager.getLogger();

	// Attributes

	private final transient HealthModificationCalculationService<Damage> damageCalculationService = new DamageCalculationProvider();
	private final transient HealthModificationCalculationService<Healing> healingCalculationService = new HealingCalculationProvider();
	protected final transient HashSet<GameEventHandler> handlers = new HashSet<>();

	// Constructor(s)

	public GameStateModelProvider() {
		super();
	}

	protected GameStateModelProvider(GameStateModel other) {
		super(other);
	}

	// Methods

	@Override
	public GameStateModelProvider copy() {
		return LOGGER.exit(new GameStateModelProvider(this));
	}

	@Override
	public GameStateModelProvider clone() {
		LOGGER.entry();
		return LOGGER.exit(new GameStateModelProvider(this));
	}

	@Override
	public void overrideWith(GameStateModel other) {
		// Override Character Map
		getCharacterMap().clear();
		getCharacterMap().putAll(other.getCharacterMap());

		// Override Fields
		getFields().clear();
		getFields().putAll(other.getFields());

		// Copy possible reactions
		getPossibleReactionsList().clear();
		getPossibleReactionsList().putAll(other.getPossibleReactionsList());

		// Copy effect stack
		getEffectStack().clear();
		getEffectStack().addAll(other.getEffectStack());

		// Copy effect history
		getEffectHistory().clear();
		getEffectHistory().addAll(other.getEffectHistory());

		// Override starting player
		setStartingPlayer(other.getStartingPlayer());

		// Override Active Player
		setActivePlayer(other.getActivePlayer());

		// Override Priority Player
		setPriorityPlayer(other.getPriorityPlayer());

		// Override Round Count
		setRoundCount(other.getRoundCount());

		// Override performed actions count
		setPerformedActionsCount(other.getPerformedActionsCount());

		// Override remaining free swaps
		remainingFreeSwaps = other.getRemainingFreeSwaps();

		// Override current game phase
		setCurrentGamePhase(other.getCurrentGamePhase());

		// Override current game events
		getCurrentGameEvents().clear();
		getCurrentGameEvents().addAll(other.getCurrentGameEvents());

		// Override Timers
		getTurnTimer().overrideWith(other.getTurnTimer());
		getChooseTimer().overrideWith(other.getChooseTimer());
	}

	// Model Operations

	@Override
	public void reduceRemainingFreeSwaps() {
		remainingFreeSwaps = Math.max(--remainingFreeSwaps, 0);
	}

	@Override
	public void resetRemainingFreeSwaps() {
		remainingFreeSwaps = GameLogicConstants.FREE_SWAPS_PER_TURN;
	}

	@Override
	public void fireEvent(GameEvent event) {
		try {
			synchronized (getHandlers()) {
				getHandlers().forEach((consumer) -> consumer.handleEvent(event));
			}
		} catch (Throwable e) {
			LOGGER.error("Event [" + event + "] could not be fired", e);
		}

	}

	@Override
	public void setCharacterWasAvailableAtTurnStart(UUID characterId, boolean flag) {
		getCharacter(characterId).setAvailableAtTurnStart(flag);
	}

	@Override
	public void setCharacterPerformedAction(UUID characterId, boolean flag) {
		getCharacter(characterId).setPerformedAction(flag);
	}

	@Override
	public void swap(Effect sourceEffect, UUID source, Position target) {
		LOGGER.entry(source, target);
        swap(sourceEffect, getPosition(source), target);
        LOGGER.exit();
	}

	@Override
	public void modifyBonusAttribute(UUID characterId, CharacterAttribute attribute, int amount) {
		LOGGER.entry(characterId, attribute, amount);
		if (amount >= 0)
			getCharacter(characterId).addBonusAttributeAmount(attribute, amount);
		else
			getCharacter(characterId).removeBonusAttributeAmount(attribute, Math.abs(amount));
		LOGGER.exit();
	}

	@Override
	public void modifyTimeCounters(Ability abil, int amount) {
		LOGGER.entry(abil, amount);
		abil.setTimeCounters(abil.getTimeCounters() + amount);
		LOGGER.exit();
	}

	@Override
	public void modifyTimeCounters(UUID characterId, int amount) {
		LOGGER.entry(characterId, amount);

		if (characterId == null) return;
		if (getCharacter(characterId) == null) return;

		LOGGER.debug("Modifying Time Counters on Character (" + characterId + ") by " + amount);

		if (amount >= 0)
			getCharacter(characterId).addTimeCounters(amount);
		else
			getCharacter(characterId).removeTimeCounters(amount);

		LOGGER.exit();
	}

	@Override
	public void modifyTimeCounters(Position pos, int amount) {
		LOGGER.entry(pos, amount);

		if (pos == null) return;
		if (getCharacterAt(pos) == null) return;

		LOGGER.debug("Modifying Time Counters on Character at " + pos + " by " + amount);

		if (amount >= 0)
			getCharacterAt(pos).addTimeCounters(amount);
		else
			getCharacterAt(pos).removeTimeCounters(Math.abs(amount));

		LOGGER.exit();
	}

	@Override
	public void addRound() {
		LOGGER.entry();
		setRoundCount(getRoundCount() + 1);
		LOGGER.exit();
	}

    @Override
    public void applyDamage(Damage damage, CharacterDataService characterClassService) {
    	LOGGER.entry(damage, characterClassService);

    	// No damage can be dealt by the starting player within the first turn of the game
    	//if (getRoundCount() <= 1 && getStartingPlayer().equals(getCharacter(damage.getSource().getSourceCharacterId()).getOwningPlayerId())) return;

    	Character target = getCharacterAt(damage.getTarget());

    	// Dead Characters dont take damage and dont trigger dying events
    	if (CharacterStateUtils.isCharacterDead(target, characterClassService)) {
    		LOGGER.exit();
    		return;
    	}

    	// BlockBuff
    	BlockBuff blockBuff;
    	if ((blockBuff = target.getBuff(BlockBuff.class)) != null) {
    		if (blockBuff.getBlockValue() > 0) {

	    		// Calculated damage before block
	    		int tmpDamageAmount = getDamageCalculationService().getCalculatedAmount(damage, this, characterClassService);

	    		// Damage that would be dealt is greater than the block amount
	    		if (tmpDamageAmount >= blockBuff.getBlockValue()) {

	    			damage.setBlockedAmount(damage.getBlockedAmount() + blockBuff.getBlockValue());
	        		blockBuff.setBlockValue(0);
	        		target.removeBuff(blockBuff);

	    		} else {

	    			damage.setBlockedAmount(damage.getBlockedAmount() + tmpDamageAmount);
	        		blockBuff.setBlockValue(blockBuff.getBlockValue() - tmpDamageAmount);

	    		}

    		} else target.removeBuff(blockBuff);
    	}

    	// Increase target's damage taken (cannot exceed it's health)
    	int actualDamage = Math.max(0, getDamageCalculationService().getCalculatedAmount(damage, this, characterClassService));
    	target.setDamageTaken(Math.min(CharacterStateUtils.getCharacterAttributeValue(target, CharacterAttribute.HEALTH, characterClassService),
    								   target.getDamageTaken() + actualDamage));

    	target.setTookDamageThisTurn(true);

    	// Should character die ?
    	if (target.getDamageTaken() >= CharacterStateUtils.getCharacterAttributeValue(target, CharacterAttribute.HEALTH, characterClassService)) {
    		// Remove buffs
    		for (Buff b : new ArrayList<>(target.getBuffs())) {
    			removeBuff(target.getId(), b.getClass());
    		}

    		// Reduce time counters
    		target.setTimeCounters(0);

    		// Fire Event
    		fireEvent(new GameEvent(GameEventType.CHARACTER_DIED, damage.getSource(), target));
    	}

    	// Fire Event
    	if (actualDamage > 0) fireEvent(new GameEvent(GameEventType.DAMAGE_APPLIED, damage.getSource(), damage));

    	LOGGER.exit();
    }

	@Override
	public void applyHealing(Healing healing, CharacterDataService characterClassService) {
		LOGGER.entry(healing, characterClassService);

		Character target = getCharacterAt(healing.getTarget());

		// Dead characters dont receive healing
		if (CharacterStateUtils.isCharacterDead(target, characterClassService)) {
			LOGGER.exit();
			return;
		}

		// Reduce damage taken aka heal the target
		int actualHealing = Math.max(0, getHealingCalculationService().getCalculatedAmount(healing, this, characterClassService));
		target.setDamageTaken(Math.max(0, target.getDamageTaken() - actualHealing));

		// Fire Event
    	if (actualHealing > 0) fireEvent(new GameEvent(GameEventType.HEALING_APPLIED, healing.getSource(), healing));

		LOGGER.exit();
	}

    @Override
    public void addBuff(Buff buff) {
    	LOGGER.entry(buff);

    	Character target = getCharacter(buff.getTargetCharacterId());
    	Buff b = null;

    	// Check whether the character already has a buff of the given type
    	if ((b = target.getBuff(buff.getClass())) != null) {
    		b.refreshDuration();
    		if (b.getStacks() + buff.getStacks() <= b.getMaxStacks())
    			b.setStacks(b.getStacks() + buff.getStacks());

    		// Fire Event
    		fireEvent(new GameEvent(GameEventType.BUFF_REFRESHED, buff.getSourceEffect(), buff));

    	} else {

    		// Add the model representation of the buff to the target
    		target.addBuff(buff);

    		// Fire Event
    		fireEvent(new GameEvent(GameEventType.BUFF_ADDED, buff.getSourceEffect(), buff));

    	}

    	LOGGER.exit();
    }

	@Override
	public void removeBuff(UUID characterTarget, Class<? extends Buff> buffClass) {
		LOGGER.entry(characterTarget, buffClass);

		// Check if target even has the buff to be removed
		Buff buff = null;
		if ((buff = getCharacter(characterTarget).getBuff(buffClass)) == null) {

			LOGGER.debug("Target does not have the buff to be removed");
			return;

		} else {

			if (buff.isStackCompensationInsteadRemoval() && buff.getStacks() > 0) {

				buff.refreshDuration();
				buff.setStacks(Math.max(0, buff.getStacks() - 1));

				// Fire Event
				Buff finalBuff = buff;
				fireEvent(new GameEvent(GameEventType.BUFF_REFRESHED, finalBuff.getSourceEffect(), finalBuff));

			} else {

				if (buff.getClass().equals(StunBuff.class) && getCharacter(characterTarget).isReady()) {
					getCharacter(characterTarget).setAvailableAtTurnStart(true);
				}

				// Remove the model representation of the buff from the target
				getCharacter(buff.getTargetCharacterId()).removeBuff(buff);

				// Fire Event
				Buff finalBuff = buff;
				fireEvent(new GameEvent(GameEventType.BUFF_REMOVED, finalBuff.getSourceEffect(), finalBuff));

			}
		}

		LOGGER.exit();
	}

	@Override
	public void reduceBuffDuration(Buff buff) {
		LOGGER.entry(buff);

		if (buff.getDurationLeft() <= 0) return;
		buff.setDurationLeft(buff.getDurationLeft() - 1);
		if (buff.getDurationLeft() <= 0) {
			removeBuff(buff.getTargetCharacterId(), buff.getClass());
		}

		LOGGER.exit();
	}

	@Override
	public void modifyStunCounters(Effect source, UUID characterId, int amount) {
		LOGGER.entry(source, characterId, amount);

		Character character = getCharacter(characterId);

		// Stunned characters cannot get stun counters placed on them
		if (character.isStunned()) return;

		character.setStunCounters(source, Math.max(0, Math.min(character.getStunCounters() + amount, GameLogicConstants.STUN_COUNTERS_THRESHOLD)));

		if (amount > 0) fireEvent(new GameEvent(GameEventType.STUN_COUNTERS_ADDED, source, character, amount));
		else if (amount < 0) fireEvent(new GameEvent(GameEventType.STUN_COUNTERS_REMOVED, source, character, amount));

		// Stun the character if the number of stun counters on it exceed the global stun counter threshold
        if (character.getStunCounters() >= GameLogicConstants.STUN_COUNTERS_THRESHOLD) {
            character.addBuff(new StunBuff(getActivePlayer(), source, characterId, 2));
            character.setStunCounters(source, 0);
            fireEvent(new GameEvent(GameEventType.STUNNED, source, character));
        }

        LOGGER.exit();
	}

	@Override
	public void modifyStunCounters(Effect source, Position pos, int amount) {
		modifyStunCounters(source, getCharacterAt(pos).getId(), amount);
	}

	@Override
	public void removeStunCounter(Effect source, UUID characterId) {
		LOGGER.entry(source, characterId);
		modifyStunCounters(source, characterId, -1);
		LOGGER.exit();
	}

	@Override
	public void toggleDamageTaken(UUID characterId) {
		LOGGER.entry(characterId);

		Character character = getCharacter(characterId);
		character.setTookDamageThisTurn(!character.tookDamageThisTurn());

		LOGGER.exit();
	}

	@Override
	public void reduceTimeCounters(Character character) {
		LOGGER.entry(character);
		this.getCharacter(character.getId()).setTimeCounters(Math.max(0, character.getTimeCounters() - 1));
		LOGGER.exit();
	}

	@Override
	public void reduceTimeCounters(Ability ability) {
		LOGGER.entry(ability);
		((Ability) ability).setTimeCounters(Math.max(0, ability.getTimeCounters() - 1));
		LOGGER.exit();
	}

	@Override
	public void swap(Effect sourceEffect, Position p1, Position p2) {
		LOGGER.entry(p1,p2);

		// Validate Input
		if (!p1.getPlayer().equals(p2.getPlayer())) throw new IllegalArgumentException("Owner of two swapping characters must be the same.");
		if (p1.getRow() >= getRowCount(p1.getPlayer()) || p1.getCol() >= getColCount(p1.getPlayer())
			|| p2.getRow() >= getRowCount(p2.getPlayer()) || p2.getCol() >= getColCount(p2.getPlayer())) throw new IllegalArgumentException("Positions must be within field boundaries.");

		// Actually Swap Characters
		Character c1 = (Character) getCharacterAt(p1);
		Character c2 = (Character) getCharacterAt(p2);
		Map<Pair<Integer, Integer>, Character> field = getFields().get(p1.getPlayer());
		field.put(new MutablePair<Integer,Integer>(p1.getRow(), p1.getCol()), c2);
		field.put(new MutablePair<Integer,Integer>(p2.getRow(), p2.getCol()), c1);

		// Change the position within the characters' models
		Position tmpPosition = c1.getPosition();
		c1.setPosition(new Position(c2.getPosition()));
		c2.setPosition(new Position(tmpPosition));

		// Fire Event
		fireEvent(new GameEvent(GameEventType.CHARACTERS_SWAPPED, sourceEffect, c1, c2));

		LOGGER.exit();
	}

    // Event Handling

	@Override
	public boolean addEventHandler(GameEventHandler handler) {
		LOGGER.entry(handler);
		boolean result = getHandlers().add(handler);
		return LOGGER.exit(result);
	}

	@Override
	public boolean removeEventHandler(GameEventHandler handler) {
		LOGGER.entry(handler);
		boolean result = getHandlers().remove(handler);
		return LOGGER.exit(result);
	}

	@Override
	public void addToEffectHistory(Effect effect) {
		int size = getEffectHistory().size();
		if (size >= GameLogicConstants.EFFECT_HISTORY_THRESHOLD) getEffectHistory().remove(size - 1);
		getEffectHistory().add(0, effect);
	}

	@Override
	public void activateEnhancement(Effect sourceEffect, int activatingUpgradeRank, UUID characterId, CharacterEnhancementClass enhancement) {
		// Obtain character instance
		Character character = getCharacter(characterId);

		// Changes attributes
		try {
			for (CharacterAttribute attributeType : enhancement.getAttributes().keySet())
				modifyBonusAttribute(characterId, attributeType, Math.abs(enhancement.getAttributes().get(attributeType)));
		} catch (Exception e) {
			LOGGER.catching(e);
		}

		// Add abilities
		try {
			for (int abilityClassId : enhancement.getAbilityClassIds())
				character.getAbilities().add(new Ability(characterId, abilityClassId));
		} catch (Exception e) {
			LOGGER.catching(e);
		}

		// Fire event
		fireEvent(new GameEvent(GameEventType.ENHANCEMENT_ACTIVATED, sourceEffect, character, activatingUpgradeRank, enhancement.getId()));
	}

	@Override
	public void deactivateEnhancement(Effect sourceEffect, UUID characterId, CharacterEnhancementClass enhancement) {
		// Obtain character instance
		Character character = getCharacter(characterId);

		// Changes attributes
		try {
			for (CharacterAttribute attributeType : enhancement.getAttributes().keySet())
				modifyBonusAttribute(characterId, attributeType, Math.negateExact(Math.abs(enhancement.getAttributes().get(attributeType))));
		} catch (Exception e) {
			LOGGER.catching(e);
		}

		// Remove abilities
		try {
			for (int abilityClassId : enhancement.getAbilityClassIds())
				character.getAbilities().remove(character.getAbility(abilityClassId));
		} catch (Exception e) {
			LOGGER.catching(e);
		}

		// Fire event
		fireEvent(new GameEvent(GameEventType.ENHANCEMENT_DEACTIVATED, sourceEffect, character, enhancement.getId()));
	}

	public HealthModificationCalculationService<Damage> getDamageCalculationService() {
		return damageCalculationService;
	}

	public HealthModificationCalculationService<Healing> getHealingCalculationService() {
		return healingCalculationService;
	}

	protected Set<GameEventHandler> getHandlers() {
		return handlers;
	}

}
