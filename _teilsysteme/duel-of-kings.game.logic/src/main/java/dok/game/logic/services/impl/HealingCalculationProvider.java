package dok.game.logic.services.impl;

import dok.game.model.GameStateModel;
import dok.game.model.Healing;
import dok.game.model.api.CharacterDataService;
import dok.game.services.HealthModificationCalculationService;

public class HealingCalculationProvider implements HealthModificationCalculationService<Healing> {

	@Override
	public int getCalculatedAmount(Healing healing, GameStateModel model, CharacterDataService characterClassService) {
		return healing.getBaseAmount();
	}

}
