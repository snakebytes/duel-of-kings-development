package dok.game.logic.statemachines;

import java.util.UUID;

import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.statemachines.GameLogic_Endsequence.GLSEndsequenceState;
import dok.game.model.GameResult;
import dok.game.model.GameResultType;
import dok.game.services.GameLogicWrapper;
import dok.game.services.PlayerService;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class GameLogic_Endsequence extends StateMachineImpl<GLSEndsequenceState> {

	// States

	public enum GLSEndsequenceState {
		INFORM_PLAYERS
	}

	// Services

	private final PlayerService playerListService;
	private final GameLogicWrapper gameLogicWrapper;

	// Attributes

	private final UUID loosingPlayer;

	// Constructor(s)

	public GameLogic_Endsequence(StateMachineCallback parent, GameLogicWrapper gameLogicWrapper, PlayerService playerListService, UUID loosingPlayer) throws IllegalArgumentException {
		super(parent, GLSEndsequenceState.INFORM_PLAYERS);
		this.loosingPlayer = loosingPlayer;
		this.playerListService = playerListService;
		this.gameLogicWrapper = gameLogicWrapper;
		transition(null, getStartState());
	}

	// Events

	@Override
	protected void onEntry(GLSEndsequenceState state) {
		switch (state) {
			case INFORM_PLAYERS:
				getPlayerListService().getPlayerIds().forEach(
						(playerid) -> gameLogicWrapper.handleGameResult(playerid,
								new GameResult(getLoosingPlayer() == null ? GameResultType.DRAW : playerid.equals(getLoosingPlayer()) ? GameResultType.LOSS : GameResultType.WIN)));
				finish(null);
				break;
		}
	}

	@Override
	protected void onExit(GLSEndsequenceState state) {
		// Do nothing

	}

	protected UUID getLoosingPlayer() {
		return loosingPlayer;
	}

	protected PlayerService getPlayerListService() {
		return playerListService;
	}

	protected GameLogicWrapper getGameLogicWrapper() {
		return gameLogicWrapper;
	}

}
