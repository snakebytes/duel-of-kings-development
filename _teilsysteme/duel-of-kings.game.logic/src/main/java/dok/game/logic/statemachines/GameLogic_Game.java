package dok.game.logic.statemachines;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.model.EnhancementSlot;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.services.impl.BuffEventHandler;
import dok.game.logic.services.impl.GameEventTriggerProvider;
import dok.game.logic.statemachines.GameLogic_Game.GLSGameState;
import dok.game.model.Ability;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterEnhancementClass;
import dok.game.model.Damage;
import dok.game.model.GameEvent;
import dok.game.model.GameEventType;
import dok.game.model.GamePhase;
import dok.game.model.ReactionEffect;
import dok.game.model.Trigger;
import dok.game.model.TriggerClass;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.GameEventHandler;
import dok.game.services.GameEventTriggerService;
import dok.game.services.GameLineupService;
import dok.game.services.GameLogicWrapper;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerService;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class GameLogic_Game extends StateMachineImpl<GLSGameState> implements StateMachineCallback, GameEventHandler {

	// Class Constants

	private static final Logger LOGGER = LogManager.getLogger();

	// States

	public enum GLSGameState {
		GAME_INIT,
		TURN,
		UPGRADE,
		SWAP_ACTIVE_PLAYER,
		END_OF_ROUND
	}

	// Constants

	private final GameStateModelService gameStateModel;
	private final PlayerService playerListService;
	private final CharacterDataService characterClassService;
	private final AbilityDataService abilityClassService;
	private final GameLogicWrapper gameLogicWrapper;
	private final GameLineupService lineupService;
	private final GameEventTriggerService triggerService;
	private final BuffEventHandler buffEventHandler;

	// Attributes

	private int rank = 0;

	// Constructor(s)

	public GameLogic_Game(StateMachineCallback parent,
			GameLogicWrapper gameLogicWrapper,
			PlayerService playerListService,
			GameStateModelService gameStateModelService,
            CharacterDataService characterClassService,
            AbilityDataService abilityClassService,
            GameLineupService lineupService)
			throws IllegalArgumentException
	{
		super(parent, GLSGameState.GAME_INIT);
		this.gameStateModel = gameStateModelService;
		this.playerListService = playerListService;
		this.characterClassService = characterClassService;
		this.abilityClassService = abilityClassService;
		this.gameLogicWrapper = gameLogicWrapper;
		this.lineupService = lineupService;
		this.triggerService = new GameEventTriggerProvider();
		this.buffEventHandler = new BuffEventHandler();
		transition(getStartState());
	}

	// Events

	public Object handle(Object input) {
		try {
			LOGGER.entry(input);
			if (input != null && input instanceof Pair) {
				Pair<?,?> pairInput = (Pair<?,?>) input;
				if (pairInput.getKey() != null && pairInput.getKey() instanceof UUID
					&& pairInput.getValue() != null && pairInput.getValue() instanceof Boolean
					&& !(Boolean)pairInput.getValue())
				{
					finish(pairInput.getKey());
				}
			}
			return super.handle(input);
		} catch (Throwable e) {
			finish(e);
			return e;
		}
	}

	@Override
	public void onFinish(Object result) {
		setActiveChild(null);
		if (getCurrentState() == GLSGameState.TURN) {
			if (result instanceof Throwable) finish(result);
			else transition(GLSGameState.SWAP_ACTIVE_PLAYER);
		}
	}

	@Override
	protected void onEntry(GLSGameState state) {
		switch (state) {
			case GAME_INIT:

				getGameStateModel().addEventHandler(this); // TODO : Remove EventHandler if not needed anymore
				getGameStateModel().addRound();
				getGameStateModel().setActivePlayer(getPlayerListService().getPlayerIds().get(0));
				getGameStateModel().setPriorityPlayer(getGameStateModel().getActivePlayer());
				transition(GLSGameState.UPGRADE);
				break;

			case END_OF_ROUND:

				getGameStateModel().addRound();
				if (getGameStateModel().getRoundCount() > 15) finish(null);
				else if (getGameStateModel().getRoundCount() == 11 || getGameStateModel().getRoundCount() == 6) {
					transition(GLSGameState.UPGRADE);
				} else transition(GLSGameState.TURN);
				break;

			case UPGRADE:

				getGameStateModel().setCurrentGamePhase(GamePhase.UPGRADE);

				// Increment global character rank
				setRank(getRank() + 1);

				for (UUID playerid : getPlayerListService().getPlayerIds()) {
					Map<Pair<Integer, Integer>, Character> field = getGameStateModel().getField(playerid);
					for (Pair<Integer, Integer> pos : field.keySet()) {

						// Temporary variables
						Character character = field.get(pos);
						boolean wasDeadBeforeUpgrade = CharacterStateUtils.isCharacterDead(character, getCharacterClassService());

						// Update the character's rank
						character.setRank(getRank());

						// Check if the character even has any possible enhancements
						if (character.getEnhancementPlan() != null)
							// Iterate over all slots corresponding to this character
							for (Entry<EnhancementSlot, Integer> entry : character.getEnhancementPlan().getEnhancementMap().entrySet()) {

								// Only activate slots who are related to the present upgrade's rank
								if (entry.getKey().getRank() == getRank()) {

									// Fetch the enhancement class
									CharacterEnhancementClass enhancement = getCharacterClassService().getCharacterEnhancementClass(entry.getValue());

									// Perform the activation
									getGameStateModel().activateEnhancement(null, getRank(), character.getId(), enhancement);

								}

							}

						if (!wasDeadBeforeUpgrade)
							 // Fire Upgrade Finished Event
							handleEvent(new GameEvent(GameEventType.CHARACTER_UPGRADED, null, character, getRank()));
						else
							// Upgrades which increase the health of a character cannot bring it back to life
							character.setDamageTaken(CharacterStateUtils.getCharacterAttributeValue(character, CharacterAttribute.HEALTH, getCharacterClassService()));
					}
				}

				// Broadcast the model to all players
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());

				transition(GLSGameState.TURN);
				break;

			case SWAP_ACTIVE_PLAYER:

				UUID nextPlayer = getPlayerListService().getNextPlayerId(getGameStateModel().getActivePlayer());
				getGameStateModel().setActivePlayer(getPlayerListService().getFollowingPlayerId(getGameStateModel().getActivePlayer()));
				getGameStateModel().setPriorityPlayer(getGameStateModel().getActivePlayer());
				if (nextPlayer == null) {
					transition(GLSGameState.END_OF_ROUND);
				} else {
					transition(GLSGameState.TURN);
				}
				break;

			case TURN:

				setActiveChild(new GameLogic_Turn(this, getGameLogicWrapper(), getGameStateModel(), getPlayerListService(), getCharacterClassService(), getAbilityClassService(), this));
				break;
		}
	}

	@Override
	protected void onExit(GLSGameState state) {
		// Do nothing
	}

    // Triggers

	@Override
	public void handleEvent(GameEvent sourceEvent) {
		LOGGER.entry(sourceEvent);

		LOGGER.debug("A " + GameEvent.class.getSimpleName() + " occurred: " + sourceEvent);

		// Redefine the GameStateModel
		getGameStateModel().getCurrentGameEvents().add(sourceEvent);

		// Check if the king died and finish the game in that case
		if (sourceEvent.getEventType() == GameEventType.CHARACTER_DIED) {
			if (sourceEvent.getParameters().length > 0 && sourceEvent.getParameters()[0] != null && sourceEvent.getParameters()[0] instanceof Character) {
				Character chr = (Character) sourceEvent.getParameters()[0];
				if (chr.isKing()) {
					finish(chr.getOwningPlayerId());
					return;
				}
			}
		}

		// Handle ability upgrades (remove minor version of the ability)
		if(sourceEvent.getEventType() == GameEventType.CHARACTER_UPGRADED) {

			Character c = (Character) sourceEvent.getParameters()[0];

			// Tranquility
			Ability tranquility = c.getAbility(AbilityClassIds.TRANQUILITY_ABILITY_CLASS_ID);
			Ability advTranquility = c.getAbility(AbilityClassIds.ADV_TRANQUILITY_ABILITY_CLASS_ID);
			if(tranquility != null && advTranquility != null) {
				c.getAbilities().remove(tranquility);
			}

		}

		// Add stun counters on damage
		if (sourceEvent.getEventType() == GameEventType.DAMAGE_APPLIED) {
			Damage eventDamage = (Damage) sourceEvent.getParameters()[0];
			getGameStateModel().modifyStunCounters(sourceEvent.getSourceEffect(), eventDamage.getTarget(), 1);
		}

		// Potentially generate triggers
        for (UUID playerid : getPlayerListService().getPlayerIds()) {

            for (UUID characterId : getGameStateModel().getCharacterIds(playerid)) {

                Character character = getGameStateModel().getCharacter(characterId);

                // Trigger Abilities
                for (Ability ability : character.getAbilities()) {
                	Trigger trigger = getTriggerService().generateEventTriggers(
                			ability,
                			sourceEvent,
                    		getGameStateModel(),
							getCharacterClassService(),
							getAbilityClassService());

                	LOGGER.debug("-> Trigger of {} is {}", ability, trigger);

                    if (trigger != null) {
                    	TriggerClass triggerClass = getAbilityClassService().getTriggerClass(trigger.getTriggerClassId());
                    	if (triggerClass != null) {
	                    	if (triggerClass.isReaction())
	                    		getGameStateModel().getPossibleReactionsList().get(playerid).add(trigger);
	                    	else {
	                        	try {
	                        		getGameStateModel()
	                        		.getEffectStack()
	                        		.add((ReactionEffect) triggerClass
									.getEffectClass()
									.getConstructor(Trigger.class, Map.class, Map.class)
									.newInstance(trigger, null, triggerClass.getProperties()));
								} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
										| InvocationTargetException | NoSuchMethodException | SecurityException e) {
									LOGGER.log(Level.FATAL, "AbilityEffect could not be created", e);
								}
	                    	}
                    	} else LOGGER.warn("trigger class for trigger with id {} is null.", trigger.getTriggerClassId());
                    }
                }

                //Trigger Buffs
                for (Buff buff : character.getBuffs()) {
                    Trigger trigger = getTriggerService().generateEventTriggers(
                    		buff,
                    		sourceEvent,
                    		getGameStateModel(),
							getCharacterClassService(),
							getAbilityClassService());

                    LOGGER.debug("-> Trigger of {} is {}", buff, trigger);

                    if (trigger != null) {
                    	TriggerClass triggerAbilityClass = getAbilityClassService().getTriggerClass(trigger.getTriggerClassId());
                    	if (triggerAbilityClass != null) {
	                    	if (triggerAbilityClass.isReaction())
	                    		getGameStateModel()
	                    		.getPossibleReactionsList()
	                    		.get(playerid).add(trigger);
	                    	else {
	                        	try {
	                        		getGameStateModel()
	                        		.getEffectStack()
	                        		.add((ReactionEffect) triggerAbilityClass
									.getEffectClass()
									.getConstructor(Trigger.class, Map.class, Map.class)
									.newInstance(trigger, null, triggerAbilityClass.getProperties()));
								} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
										| InvocationTargetException | NoSuchMethodException | SecurityException e) {
									LOGGER.log(Level.FATAL, "AbilityEffect could not be created", e);
								}
	                    	}
                    	} else {
                    		// Do nothing
                    	}
                    }
                }

            }
        }

        // Handle buff events
        if (sourceEvent.getEventType() == GameEventType.BUFF_ADDED) {
			getBuffEventHandler().onBuffAdded(sourceEvent, (Buff) sourceEvent.getParameters()[0], getGameStateModel());
		} else if (sourceEvent.getEventType() == GameEventType.BUFF_REMOVED) {
			getBuffEventHandler().onBuffRemove(sourceEvent, (Buff) sourceEvent.getParameters()[0], getGameStateModel());
		}

        LOGGER.exit();
	}

	// Getter & Setter

	protected final GameStateModelService getGameStateModel() {
		return gameStateModel;
	}

	protected final PlayerService getPlayerListService() {
		return playerListService;
	}

	protected final CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	protected final AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	protected final GameLogicWrapper getGameLogicWrapper() {
		return gameLogicWrapper;
	}

	protected GameLineupService getLineupService() {
		return lineupService;
	}

	protected int getRank() {
		return rank;
	}

	protected void setRank(int rank) {
		this.rank = rank;
	}

	protected GameEventTriggerService getTriggerService() {
		return triggerService;
	}

	protected BuffEventHandler getBuffEventHandler() {
		return buffEventHandler;
	}

}
