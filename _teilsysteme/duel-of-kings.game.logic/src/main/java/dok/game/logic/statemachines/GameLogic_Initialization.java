package dok.game.logic.statemachines;

import java.time.Duration;
import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.impl.CustomTimer;
import dok.commons.model.Coordinate;
import dok.commons.model.Lineup;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.statemachines.GameLogic_Initialization.GameLogicInitializationState;
import dok.game.model.Ability;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterClass;
import dok.game.model.GamePhase;
import dok.game.model.Position;
import dok.game.model.SwapRequest;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameLineupService;
import dok.game.services.GameLogicWrapper;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerService;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class GameLogic_Initialization extends StateMachineImpl<GameLogicInitializationState>
		implements StateMachineCallback {

	// Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum GameLogicInitializationState {
		DETERMINE_STARTING_PLAYER, PLAYER_PLACE_CHARACTERS, INITIALIZATION, TIME_MARKER_PHASE
	}

	// Services

	private final GameLineupService lineupService;
	private final PlayerService playerListService;
	private final GameLogicWrapper gameLogicWrapper;
	private final GameStateModelService gameStateModel;
	private final CharacterDataService characterClassService;

	// Attributes

	private Integer index = -1;

	// Constructor(s)

	public GameLogic_Initialization(StateMachineCallback parent, PlayerService playerListService,
			GameLogicWrapper gameLogicWrapper, GameStateModelService gameStateModelService,
			CharacterDataService characterClassService, GameLineupService lineupService)
					throws IllegalArgumentException {
		super(parent, GameLogicInitializationState.DETERMINE_STARTING_PLAYER);
		this.playerListService = playerListService;
		this.gameLogicWrapper = gameLogicWrapper;
		this.gameStateModel = gameStateModelService;
		this.lineupService = lineupService;
		this.characterClassService = characterClassService;
		transition(null, getStartState());
	}

	// Methods

	private final void onTimerExpired(CustomTimer timer) {
		if (getCurrentState() == GameLogicInitializationState.PLAYER_PLACE_CHARACTERS)
			transition(GameLogicInitializationState.INITIALIZATION);
	}

	@Override
	public Object handle(Object input) {
		try {

			if (getCurrentState() == GameLogicInitializationState.PLAYER_PLACE_CHARACTERS) {
				if (input != null && input instanceof Pair) {
					Pair<?, ?> pairInput = (Pair<?, ?>) input;
					if (pairInput.getKey() instanceof UUID) {
						UUID sourcePlayerId = (UUID) pairInput.getKey();
						if (sourcePlayerId.equals(getPlayerListService().getPlayerIds().get(getIndex()))) {
							if (pairInput.getValue() != null && pairInput.getValue() instanceof SwapRequest) {
								// Cast input to SwapRequest
								SwapRequest swapRequest = (SwapRequest) pairInput.getValue();
								// Perform the swap against own data model
								getGameStateModel().swap(null, swapRequest.getSource(), swapRequest.getTarget());
								// Broadcast the swap to all players
								getPlayerListService().getPlayerIds().forEach(
										(playerid) -> getGameLogicWrapper().handleSwapRequest(playerid, swapRequest));
							} else
								transition(GameLogicInitializationState.INITIALIZATION);
						}
					}
				}
			}
			return super.handle(input);

		} catch (Throwable e) {

			finish(e);
			return e;

		}
	}

	// Events

	@Override
	public void interrupt() {
		try {
			super.interrupt();
			getGameStateModel().getChooseTimer().stop();
		} catch (Exception e) {
			logger.catching(Level.DEBUG, e);
		}
	}

	@Override
	public final void onFinish(Object result) {
		setActiveChild(null);
		if (getCurrentState() == GameLogicInitializationState.TIME_MARKER_PHASE)
			finish(null);
	}

	@Override
	protected final void onEntry(GameLogicInitializationState state) {
		switch (state) {
		case DETERMINE_STARTING_PLAYER:
			// getPlayerListService().shufflePlayers(); // Currently handled by
			// the Game class's constructor
			getGameStateModel().setStartingPlayer(getPlayerListService().getPlayerIds().get(0));
			transition(getCurrentState(), GameLogicInitializationState.INITIALIZATION);
			break;
		case PLAYER_PLACE_CHARACTERS:
			getGameLogicWrapper().placeCharacters(getPlayerListService().getPlayerIds().get(getIndex()));
			break;
		case TIME_MARKER_PHASE:
			setActiveChild(new GameLogic_TimeMarkerPlacing(this, getGameLogicWrapper(), getGameStateModel(),
					getPlayerListService().getPlayerIds().get(0), getPlayerListService().getPlayerIds().get(1),
					getPlayerListService()));
			break;
		case INITIALIZATION:
			setIndex(getIndex() + 1);
			if (getIndex() < 2) {

				// Create Field
				UUID player = getPlayerListService().getPlayerIds().get(getIndex());
				logger.debug("Attempting to create field for player " + player);
				HashMap<Pair<Integer, Integer>, Character> field = new HashMap<>();
				Lineup lineup = getLineupService().getLineup(player);
				for (Integer row = 0; row < lineup.getRowCount(); row++) {
					for (Integer col = 0; col < lineup.getColCount(); col++) {
						// Fetch the Data
						Pair<Integer, Integer> pos = new MutablePair<Integer, Integer>(row, col);
						int characterClassId = lineup.getCharacterClassIdAt(pos);
						CharacterClass characterClass = getCharacterClassService().getCharacterClass(characterClassId);

						logger.debug("characterClassId: " + characterClassId);
						logger.debug("characterClass: " + characterClass);

						// Instantiate the Character
						Character c = new Character(new Position(player, row, col),
								characterClass.getBaseTargetFilter(), characterClass.getBaseHitFilter(),
								characterClass.getBaseDamageType(), lineup.getKingPosition().toPair().equals(pos),
								lineup.getEnhancementPlans().get(new Coordinate(pos)),
								lineup.getCharacterClassIdAt(pos));

						// Double health if c is king
						if (c.isKing())
							c.addBonusAttributeAmount(CharacterAttribute.HEALTH,
									getCharacterClassService().getCharacterClass(c.getCharacterClassId())
											.getBaseAttributeValue(CharacterAttribute.HEALTH));

						// Add the abilities of the CharacterClass to the
						// character
						for (int abilityClassId : characterClass.getAbilityClassIds()) {
							c.getAbilities().add(new Ability(c.getId(), abilityClassId));
						}

						// Add the chosen ability from the lineup to the
						// character
						// TODO: Abilities durch Verbesserungen ersetzen
						// int abilityId = lineup.getAbilityClassIdAt(pos);
						// if (abilityId > -1) c.getAbilities().add(new
						// Ability(c.getId(), abilityId));

						// Save it for later usage
						field.put(pos, c);

						// Logging
						if (field.containsKey(pos))
							logger.debug("Character " + c + " added at [" + pos.getKey() + "," + pos.getValue() + "]");
					}
				}
				getGameStateModel().setField(player, field);

				// Define active player
				getGameStateModel().setActivePlayer(player);

				// Define active game phase
				getGameStateModel().setCurrentGamePhase(GamePhase.PLACE_CHARACTERS);

				// Initialize the Timer
				if (getGameStateModel().getChooseTimer() != null)
					getGameStateModel().getChooseTimer().stop();
				getGameStateModel().setChooseTimer(new CustomTimer(Duration.ofSeconds(30), this::onTimerExpired));
				getGameStateModel().getChooseTimer().start();

				// Broadcast the changed model to all players
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());

				// Ask the player to perform free swaps
				transition(GameLogicInitializationState.PLAYER_PLACE_CHARACTERS);

			} else {
				// Define active game phase
				getGameStateModel().setCurrentGamePhase(GamePhase.PLACE_TIME_COUNTERS);

				// Broadcast the model to all players
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());

				// Perform the transition
				transition(getCurrentState(), GameLogicInitializationState.TIME_MARKER_PHASE);
			}
			break;
		}
	}

	@Override
	protected final void onExit(GameLogicInitializationState state) {
		if (state == GameLogicInitializationState.TIME_MARKER_PHASE) {
			getGameStateModel().getChooseTimer().stop();
		}
	}

	// Getter & Setter

	protected final GameLineupService getLineupService() {
		return lineupService;
	}

	protected final PlayerService getPlayerListService() {
		return playerListService;
	}

	protected final GameLogicWrapper getGameLogicWrapper() {
		return gameLogicWrapper;
	}

	protected final GameStateModelService getGameStateModel() {
		return gameStateModel;
	}

	protected Integer getIndex() {
		return index;
	}

	protected void setIndex(Integer index) {
		this.index = index;
	}

	protected final CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

}
