package dok.game.logic.statemachines;

import java.util.UUID;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.services.impl.GameStateModelProvider;
import dok.game.logic.statemachines.GameLogic_Master.GameLogicMasterState;
import dok.game.model.Action;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameLineupService;
import dok.game.services.GameLogicService;
import dok.game.services.GameLogicWrapper;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerService;

/**
 * Defines the behavior of a state machine for a one vs. one game of 'Duel of Kings'.<br>
 * <br>
 * <i>Implements requirement SL001.</i>
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class GameLogic_Master extends StateMachineImpl<GameLogicMasterState> implements StateMachineCallback, GameLogicService {

	// Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum GameLogicMasterState {

		GAMELOGIC_INITIALIZATION,
		GAMELOGIC_GAME,
		GAMELOGIC_ENDSEQUENCE

	}

	// Constants

	private final GameLogicWrapper gameLogicWrapper;
	private final PlayerService playerListService;
	private final CharacterDataService characterClassService;
	private final AbilityDataService abilityClassService;
	private final GameStateModelService gameStateModel;
	private final GameLineupService gameLineupService;

	// Attributes

	private UUID loosingPlayer;
	private Throwable childStateMachineError;

	// Constructor(s)

	public GameLogic_Master(StateMachineCallback parent,
			GameLogicWrapper gameLogicWrapper,
			PlayerService playerListService,
			CharacterDataService characterClassService,
            AbilityDataService abilityClassService,
            GameLineupService gameLineupService)
	{
		super(parent, GameLogicMasterState.GAMELOGIC_INITIALIZATION);
		this.gameLogicWrapper = gameLogicWrapper;
		this.playerListService = playerListService;
		this.characterClassService = characterClassService;
		this.abilityClassService = abilityClassService;
		this.gameStateModel = new GameStateModelProvider();
		this.gameLineupService = gameLineupService;
	}

	// Events

	@Override
	public final void onFinish(Object result) {
		setActiveChild(null);
		if (getCurrentState() != null)
			switch (getCurrentState()) {
				case GAMELOGIC_INITIALIZATION:
					if (result instanceof Throwable) {
						setChildStateMachineError((Throwable) result);
						transition(getCurrentState(), GameLogicMasterState.GAMELOGIC_ENDSEQUENCE);
					} else transition(getCurrentState(), GameLogicMasterState.GAMELOGIC_GAME);
					break;
				case GAMELOGIC_GAME:
					if (result instanceof UUID) setLoosingPlayer((UUID) result);
					else if (result instanceof Throwable) setChildStateMachineError((Throwable) result);
					transition(getCurrentState(), GameLogicMasterState.GAMELOGIC_ENDSEQUENCE);
					break;
				case GAMELOGIC_ENDSEQUENCE:
					if (result instanceof Throwable) finish(result);
					else if (getChildStateMachineError() != null) finish(getChildStateMachineError());
					else finish(result);
					break;
			}
	}

	@Override
	protected final void onEntry(GameLogicMasterState state) {
		switch (state) {
			case GAMELOGIC_INITIALIZATION:
				setActiveChild(new GameLogic_Initialization(this, getPlayerListService(), getGameLogicWrapper(), getGameStateModel(), getCharacterClassService(), getGameLineupService()));
				break;
			case GAMELOGIC_ENDSEQUENCE:
				setActiveChild(new GameLogic_Endsequence(this, getGameLogicWrapper(), getPlayerListService(), getLoosingPlayer()));
				break;
			case GAMELOGIC_GAME:
				setActiveChild(new GameLogic_Game(this, getGameLogicWrapper(), getPlayerListService(), getGameStateModel(), getCharacterClassService(), getAbilityClassService(), getGameLineupService()));
				break;
		}
	}

	@Override
	protected final void onExit(GameLogicMasterState state) {
		if (getActiveChild() != null && getActiveChild().getCurrentState() != null)
			getActiveChild().interrupt();
	}

	// <GameLogicStatemachineService>

	@Override
	public void handleReaction(UUID sourcePlayerId, Reaction reaction) {
		logger.entry(sourcePlayerId, reaction);
		if (getCurrentState() == GameLogicMasterState.GAMELOGIC_GAME) handle(new MutablePair<UUID, Reaction>(sourcePlayerId, reaction));
	}

	@Override
	public void handleAction(UUID sourcePlayerId, Action action) {
		if (getCurrentState() == GameLogicMasterState.GAMELOGIC_GAME) handle(new MutablePair<UUID, Action>(sourcePlayerId, action));
	}

	@Override
	public void handleCharacterChoosen(UUID sourcePlayerId, Position pos) {
		if (getCurrentState() == GameLogicMasterState.GAMELOGIC_INITIALIZATION) handle(new MutablePair<UUID, Position>(sourcePlayerId, pos));
	}

	@Override
	public void handleSwapRequest(UUID sourcePlayerId, SwapRequest swapRequest) {
		if (getCurrentState() == GameLogicMasterState.GAMELOGIC_INITIALIZATION) handle(new MutablePair<UUID, SwapRequest>(sourcePlayerId, swapRequest));
	}

	@Override
	public void handleSurrenderRequest(UUID sourcePlayerId) {
		setLoosingPlayer(sourcePlayerId);
		transition(GameLogicMasterState.GAMELOGIC_ENDSEQUENCE);
	}

	// </GameLogicStatemachineService>

	// Getter & Setter

	protected GameStateModelService getGameStateModel() {
		return gameStateModel;
	}

	protected GameLineupService getGameLineupService() {
		return gameLineupService;
	}

	protected AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	protected CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	protected PlayerService getPlayerListService() {
		return playerListService;
	}

	protected GameLogicWrapper getGameLogicWrapper() {
		return gameLogicWrapper;
	}

	protected UUID getLoosingPlayer() {
		return loosingPlayer;
	}

	protected void setLoosingPlayer(UUID loosingPlayer) {
		this.loosingPlayer = loosingPlayer;
	}

	protected Throwable getChildStateMachineError() {
		return childStateMachineError;
	}

	protected void setChildStateMachineError(Throwable childStateMachineError) {
		this.childStateMachineError = childStateMachineError;
	}

}
