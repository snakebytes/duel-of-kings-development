package dok.game.logic.statemachines;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.impl.CustomTimer;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.GameLogicConstants;
import dok.game.logic.statemachines.GameLogic_TimeMarkerPlacing.GameLogicTimeMarkerPlacingPhaseState;
import dok.game.model.Character;
import dok.game.model.Position;
import dok.game.model.TargetFilter;
import dok.game.services.GameLogicWrapper;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerService;

/**
 *
 * <br>
 * <i>Implements requirement SL001.</i>
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public final class GameLogic_TimeMarkerPlacing extends StateMachineImpl<GameLogicTimeMarkerPlacingPhaseState> {

	// Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum GameLogicTimeMarkerPlacingPhaseState {
		INIT, PLAYER_CHOOSE_CHAR, MARK_CHARACTERS_AS_GOOD, MARK_CHARACTERS_AS_BAD, RANDOMIZE_REST
	}

	// Services

	private final PlayerService playerService;
	private final GameLogicWrapper gameLogicWrapper;
	private final GameStateModelService gameStateModel;

	// Constants
	private final UUID player1, player2;

	// Attributes

	private UUID chooser;
	private final Map<UUID, Set<Position>> availablePositions = new HashMap<>();
	private Position target;
	private int counter;

	// Constructor(s)

	public GameLogic_TimeMarkerPlacing(StateMachineCallback parent, GameLogicWrapper gameLogicWrapper,
			GameStateModelService gameStateModelService, UUID player1, UUID player2, PlayerService playerService)
					throws IllegalArgumentException {
		super(parent, GameLogicTimeMarkerPlacingPhaseState.INIT);
		this.gameLogicWrapper = gameLogicWrapper;
		this.gameStateModel = gameStateModelService;
		this.player1 = player1;
		this.player2 = player2;
		this.playerService = playerService;

		// Player 1
		getAvailablePositions().put(getPlayer1(), new HashSet<>());
		for (Character c : getGameStateModel().getField(getPlayer1()).values()) {
			getAvailablePositions().get(getPlayer1()).add(getGameStateModel().getPosition(c.getId()));
		}

		// Player 2
		getAvailablePositions().put(getPlayer2(), new HashSet<>());
		for (Character c : getGameStateModel().getField(getPlayer2()).values()) {
			getAvailablePositions().get(getPlayer2()).add(getGameStateModel().getPosition(c.getId()));
		}

		transition(null, getStartState());
	}

	// Methods

	@Override
	public Object handle(Object input) {
		try {

			if (getCurrentState() == GameLogicTimeMarkerPlacingPhaseState.PLAYER_CHOOSE_CHAR) {
				if (input != null && input instanceof Pair) {
					Pair<?, ?> pairInput = (Pair<?, ?>) input;
					if (pairInput.getKey() instanceof UUID) {
						UUID sourcePlayerId = (UUID) pairInput.getKey();
						if (sourcePlayerId.equals(getChooser())) {

							if (pairInput.getValue() != null && pairInput.getValue() instanceof Position)
								setTarget((Position) pairInput.getValue());

							if (getCounter() == 1 || getCounter() == 2)
								transition(getCurrentState(),
										GameLogicTimeMarkerPlacingPhaseState.MARK_CHARACTERS_AS_GOOD);
							else if (getCounter() == 3 || getCounter() == 4)
								transition(getCurrentState(),
										GameLogicTimeMarkerPlacingPhaseState.MARK_CHARACTERS_AS_BAD);
						}
					}
				}
			}
			return super.handle(input);

		} catch (Throwable e) {

			finish(e);
			return e;

		}
	}

	// Events

	protected void onTimerExpire(CustomTimer timer) {
		if (getCurrentState() == GameLogicTimeMarkerPlacingPhaseState.PLAYER_CHOOSE_CHAR) {
			if (getCounter() == 1 || getCounter() == 2)
				transition(getCurrentState(), GameLogicTimeMarkerPlacingPhaseState.MARK_CHARACTERS_AS_GOOD);
			else if (getCounter() == 3 || getCounter() == 4)
				transition(getCurrentState(), GameLogicTimeMarkerPlacingPhaseState.MARK_CHARACTERS_AS_BAD);
		}
	}

	@Override
	protected void onEntry(GameLogicTimeMarkerPlacingPhaseState state) {
		Iterator<Position> it;
		switch (state) {
		case INIT:

			// (Re-)Start Timer
			if (getGameStateModel().getChooseTimer() != null && getGameStateModel().getChooseTimer().isRunning())
				getGameStateModel().getChooseTimer().stop();
			getGameStateModel().setChooseTimer(new CustomTimer(
					Duration.ofSeconds(GameLogicConstants.TIME_MARKER_CHOOSE_TIME), this::onTimerExpire));
			getGameStateModel().getChooseTimer().start();

			// Inkrement counter
			setCounter(getCounter() + 1);

			// Switch State
			if (getCounter() == 5)
				transition(getCurrentState(), GameLogicTimeMarkerPlacingPhaseState.RANDOMIZE_REST);
			else
				transition(getCurrentState(), GameLogicTimeMarkerPlacingPhaseState.PLAYER_CHOOSE_CHAR);

			break;

		case MARK_CHARACTERS_AS_BAD:

			UUID opponent = getPlayerService().getFollowingPlayerId(getChooser());
			it = getAvailablePositions().get(opponent).iterator();
			logger.info("AvailablePositionsCount: " + getAvailablePositions().get(opponent).size());
			while (getTarget() == null || !getAvailablePositions().get(opponent).contains(getTarget())) {
				Position newTarget = it.next();
				setTarget(newTarget);
				logger.info("Trying to set new Random target to " + newTarget);
			}
			getAvailablePositions().get(opponent).remove(getTarget());
			logger.info("AvailablePositionsCountAfterRemovalOfTarget: " + getAvailablePositions().get(opponent).size());

			//
			getGameStateModel().modifyTimeCounters(getTarget(), 3);
			logger.info("TimerCounters of character at position " + getTarget() + " = "
					+ getGameStateModel().getCharacterAt(getTarget()).getTimeCounters());

			// Broadcast new model
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Switch State
			transition(getCurrentState(), GameLogicTimeMarkerPlacingPhaseState.INIT);
			break;

		case MARK_CHARACTERS_AS_GOOD:

			it = getAvailablePositions().get(getChooser()).iterator();
			logger.info("AvailablePositionsCount: " + getAvailablePositions().get(getChooser()).size());
			while (getTarget() == null || !getAvailablePositions().get(getChooser()).contains(getTarget())) {
				Position newTarget = it.next();
				setTarget(newTarget);
				logger.info("Trying to set new Random target to " + newTarget);
			}
			getAvailablePositions().get(getChooser()).remove(getTarget());
			logger.info(
					"AvailablePositionsCountAfterRemovalOfTarget: " + getAvailablePositions().get(getChooser()).size());

			logger.info("TimerCounters of character at position " + getTarget() + " = "
					+ getGameStateModel().getCharacterAt(getTarget()).getTimeCounters());

			// Broadcast new model
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Switch State
			transition(getCurrentState(), GameLogicTimeMarkerPlacingPhaseState.INIT);
			break;

		case PLAYER_CHOOSE_CHAR:

			setTarget(null);

			switch (getCounter()) {
			case 1:
				setChooser(getPlayer1());
				getGameStateModel().setActivePlayer(getChooser());
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());
				getGameLogicWrapper().chooseCharacter(getChooser(), TargetFilter.OWN,
						getAvailablePositions().get(getChooser()));
				break;
			case 2:
				setChooser(getPlayer2());
				getGameStateModel().setActivePlayer(getChooser());
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());
				getGameLogicWrapper().chooseCharacter(getChooser(), TargetFilter.OWN,
						getAvailablePositions().get(getChooser()));
				break;
			case 3:
				setChooser(getPlayer1());
				getGameStateModel().setActivePlayer(getChooser());
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());
				getGameLogicWrapper().chooseCharacter(getChooser(), TargetFilter.HOSTILE,
						getAvailablePositions().get(getPlayerService().getFollowingPlayerId(getChooser())));
				break;
			case 4:
				setChooser(getPlayer2());
				getGameStateModel().setActivePlayer(getChooser());
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());
				getGameLogicWrapper().chooseCharacter(getChooser(), TargetFilter.HOSTILE,
						getAvailablePositions().get(getPlayerService().getFollowingPlayerId(getChooser())));
				break;
			}

			break;
		case RANDOMIZE_REST:

			List<Position> positions;

			// Randomize Player 1 Characters
			positions = new ArrayList<>(getAvailablePositions().get(getPlayer1()));
			Collections.shuffle(positions);

			logger.info("Put 1 Time Counter on the first 4 of " + positions.size()
					+ " remaining characters of player 1 (" + getPlayer1() + ").");

			for (int i = 0; i < 4; i++) { // 4 Characters with 1 Counter
				Position pos = positions.remove(0);
				getGameStateModel().modifyTimeCounters(getGameStateModel().getCharacterAt(pos).getId(), 1);
			}

			logger.info("Put 2 Time Counters on the first 3 of " + positions.size()
					+ " remaining characters of player 1 (" + getPlayer1() + ").");

			for (int i = 0; i < 3; i++) { // 3 Characters with 2 Counter
				Position pos = positions.remove(0);
				getGameStateModel().modifyTimeCounters(getGameStateModel().getCharacterAt(pos).getId(), 2);
			}

			// Randomize Player2 Characters
			positions = new ArrayList<>(getAvailablePositions().get(getPlayer2()));
			Collections.shuffle(positions);

			logger.info("Put 1 Time Counter on the first 4 of " + positions.size()
					+ " remaining characters of player 2 (" + getPlayer2() + ").");

			for (int i = 0; i < 4; i++) { // 4 Characters with 1 Counter
				Position pos = positions.remove(0);
				getGameStateModel().modifyTimeCounters(getGameStateModel().getCharacterAt(pos).getId(), 1);
			}

			logger.info("Put 2 Time Counters on the first 3 of " + positions.size()
					+ " remaining characters of player 2 ( " + getPlayer2() + ").");

			for (int i = 0; i < 3; i++) { // 3 Characters with 2 Counter
				Position pos = positions.remove(0);
				getGameStateModel().modifyTimeCounters(getGameStateModel().getCharacterAt(pos).getId(), 2);
			}

			// Cleanup
			getGameStateModel().getChooseTimer().stop();

			// Broadcast new model
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			finish(null);
			break;
		}
	}

	@Override
	protected void onExit(GameLogicTimeMarkerPlacingPhaseState state) {
		// Do nothing
	}

	// Getters & Setters

	protected Position getTarget() {
		return target;
	}

	public void setTarget(Position target) {
		this.target = target;
	}

	public GameStateModelService getGameStateModel() {
		return gameStateModel;
	}

	public Map<UUID, Set<Position>> getAvailablePositions() {
		return availablePositions;
	}

	public UUID getPlayer1() {
		return player1;
	}

	public UUID getPlayer2() {
		return player2;
	}

	public GameLogicWrapper getGameLogicWrapper() {
		return gameLogicWrapper;
	}

	public UUID getChooser() {
		return chooser;
	}

	public void setChooser(UUID chooser) {
		this.chooser = chooser;
	}

	public Integer getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public PlayerService getPlayerService() {
		return playerService;
	}

}
