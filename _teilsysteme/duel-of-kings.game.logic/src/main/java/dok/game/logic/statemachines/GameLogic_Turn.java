package dok.game.logic.statemachines;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.impl.CustomTimer;
import dok.commons.statemachine.StateMachineCallback;
import dok.commons.statemachine.impl.StateMachineImpl;
import dok.game.logic.GameLogicConstants;
import dok.game.logic.services.impl.EffectExecutionProvider;
import dok.game.logic.statemachines.GameLogic_Turn.GameLogic_TurnState;
import dok.game.model.Ability;
import dok.game.model.AbilityClass;
import dok.game.model.Action;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.Effect;
import dok.game.model.GameEvent;
import dok.game.model.GameEventType;
import dok.game.model.GamePhase;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.TargetType;
import dok.game.model.Trigger;
import dok.game.model.TriggerClass;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.EffectExecutionService;
import dok.game.services.GameEventHandler;
import dok.game.services.GameLogicWrapper;
import dok.game.services.GameStateModelService;
import dok.game.services.PlayerService;

public class GameLogic_Turn extends StateMachineImpl<GameLogic_TurnState> {

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// States

	public enum GameLogic_TurnState {
		START_OF_TURN, END_OF_TURN, NEXT_PRIORITY, RESET_PRIORITY, PLAYER_PERFORM_CHARACTER_ACTION, VALIDATE_ACTION, CAST_ABILITY, PREPARE_FOR_REACTION, PLAYER_REACTION, VALIDATE_REACTION, HANDLE_REACTION, HANDLE_PRIORITY_PASS, PERFORM_EFFECT, ACTUAL_END_OF_TURN;
	}

	// Attributes

	private final GameEventHandler gameEventHandler;
	private final GameLogicWrapper gameLogicWrapper;
	private final CharacterDataService characterClassService;
	private final AbilityDataService abilityClassService;
	private final Map<UUID, Boolean> passed = new HashMap<>();
	private final GameStateModelService gameStateModel;
	private final PlayerService playerListService;
	private final List<Character> stunnedCharacters = new ArrayList<>();
	private final UUID activePlayer;
	private final EffectExecutionService effectExecutionService;

	private boolean endOfTurn;
	private Reaction currentReaction;
	private Action currentAction;
	private Effect currentEffect;
	private boolean hasReacted;

	// Constructor(s)

	public GameLogic_Turn(StateMachineCallback parent, GameLogicWrapper gameLogicWrapper,
			GameStateModelService gameStateModelService, PlayerService playerListService,
			CharacterDataService characterClassService, AbilityDataService abilityClassService,
			GameEventHandler gameEventHandler) throws IllegalArgumentException {
		super(parent, GameLogic_TurnState.START_OF_TURN);

		// Initialize Parameters
		this.gameStateModel = gameStateModelService;
		this.playerListService = playerListService;
		this.characterClassService = characterClassService;
		this.abilityClassService = abilityClassService;
		this.gameLogicWrapper = gameLogicWrapper;
		this.gameEventHandler = gameEventHandler;
		this.effectExecutionService = new EffectExecutionProvider();
		this.activePlayer = gameStateModelService.getActivePlayer();

		// Initialize Maps
		getGameStateModel().getPossibleReactionsList().clear();
		for (UUID playerid : playerListService.getPlayerIds()) {

			// Initialize possibleReactionsList map entries
			getGameStateModel().getPossibleReactionsList().put(playerid, new ArrayList<>());

			// Initialize passing map
			getPassed().put(playerid, false);
		}

		// Initialize Stunned Characters
		getGameStateModel().getCharacters().forEach((character) -> {
			if (character.isStunned())
				getStunnedCharacters().add(character);
		});

		// Transition to start state
		transition(getStartState());
	}

	// Methods

	@Override
	public Object handle(Object input) {
		try {

			logger.entry(input);
			logger.debug("priorityPlayer is " + getGameStateModel().getPriorityPlayer());
			if (input instanceof Pair<?, ?>) {
				Pair<?, ?> pairInput = (Pair<?, ?>) input;
				if (pairInput.getKey() instanceof UUID) {
					UUID sourcePlayerId = (UUID) pairInput.getKey();
					if (getActivePlayer().equals(sourcePlayerId)
							&& getCurrentState() == GameLogic_TurnState.PLAYER_PERFORM_CHARACTER_ACTION) {

						if (pairInput.getValue() != null && pairInput.getValue() instanceof Action) {

							setCurrentAction((Action) pairInput.getValue());
							transition(GameLogic_TurnState.VALIDATE_ACTION);

						} else {

							transition(GameLogic_TurnState.END_OF_TURN);

						}

					} else if (getGameStateModel().getPriorityPlayer().equals(sourcePlayerId)
							&& (getCurrentState() == GameLogic_TurnState.PLAYER_REACTION
									|| getCurrentState() == GameLogic_TurnState.HANDLE_REACTION)) {

						if (pairInput.getValue() != null && pairInput.getValue() instanceof Reaction) {

							setCurrentReaction((Reaction) pairInput.getValue());
							transition(GameLogic_TurnState.VALIDATE_REACTION);

						} else {

							transition(GameLogic_TurnState.HANDLE_PRIORITY_PASS);
						}

					}

				}
			}

			return super.handle(input);

		} catch (Throwable e) {

			finish(e);
			return e;

		}

	}

	protected int getNumberOfPossibleActions(UUID player) {

		int result = 0;
		for (Character chr : getGameStateModel().getCharacters().stream()
				.filter((chr) -> chr.getOwningPlayerId().equals(player)).collect(Collectors.toList())) {
			if (CharacterStateUtils.isCharacterAvailable(chr, getCharacterClassService()))
				result++;
		}
		return result;

	}

	protected boolean allPlayersPassed() {
		for (UUID player : getPassed().keySet())
			if (!getPassed().get(player))
				return false;
		return true;
	}

	protected boolean noPossibleReactions() {
		for (UUID player : getGameStateModel().getPossibleReactionsList().keySet())
			if (getGameStateModel().getPossibleReactionsList().get(player).size() > 0)
				return false;
		return true;
	}

	// Events

	protected void onTurnTimerExpired(CustomTimer timer) {
		if (getCurrentState() == GameLogic_TurnState.PLAYER_PERFORM_CHARACTER_ACTION) {
			transition(getCurrentState(), GameLogic_TurnState.END_OF_TURN);
		}
	}

	protected void onReactionTimerExpired(CustomTimer timer) {
		if (getCurrentState() == GameLogic_TurnState.PLAYER_REACTION
				|| getCurrentState() == GameLogic_TurnState.VALIDATE_REACTION
				|| getCurrentState() == GameLogic_TurnState.HANDLE_REACTION) {
			transition(getCurrentState(), GameLogic_TurnState.HANDLE_PRIORITY_PASS);
		}
	}

	@Override
	protected void onEntry(GameLogic_TurnState state) {

		logger.entry(state);
		logger.debug("Entering state: " + state);

		switch (state) {
		case START_OF_TURN:

			// Define Game Phase

			getGameStateModel().setCurrentGamePhase(GamePhase.START_OF_TURN);

			// Reset actions count

			getGameStateModel().setPerformedActionsCount(0);

			// Reset free swaps count
			getGameStateModel().resetRemainingFreeSwaps();

			// Reduce time counters on characters and their abilities of active
			// player
			for (UUID characterId : getGameStateModel().getCharacterIds(getActivePlayer())) {

				Character character = getGameStateModel().getCharacter(characterId);

				// Reset Performed Action Flag
				getGameStateModel().setCharacterPerformedAction(characterId, false);

				// Reduce Time Counters
				getGameStateModel().reduceTimeCounters(character);

				for (Ability ability : character.getAbilities()) {
					getGameStateModel().reduceTimeCounters(ability);
				}

				// Reset and initialize swap flags
				getGameStateModel().setCharacterWasAvailableAtTurnStart(characterId,
						CharacterStateUtils.isCharacterAvailable(character, getCharacterClassService()));
			}

			// Initialize Turn Timer
			getGameStateModel().setTurnTimer(new CustomTimer(
					Duration.ofSeconds(Math.min(GameLogicConstants.AVAILABLE_ACTIONS_PER_TURN,
							getNumberOfPossibleActions(getActivePlayer())) * GameLogicConstants.TIME_PER_ACTION),
					this::onTurnTimerExpired));

			// Trigger onStartOfTurn
			getGameEventHandler().handleEvent(new GameEvent(GameEventType.TURN_STARTED, null, new Object[0]));

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			transition(getCurrentState(), GameLogic_TurnState.NEXT_PRIORITY);
			break;

		case ACTUAL_END_OF_TURN:

			// START OF DEPRECATION : Due to changes in the base rules set, stun
			// counters are no longer being removed at end of turn.
			// for (Character character : getGameStateModel().getCharacters()) {
			// if (character.tookDamageThisTurn() && !character.isStunned() &&
			// !getStunnedCharacters().contains(character)) {
			// getGameStateModel().modifyStunCounters(character.getId(), 1);
			// getGameStateModel().toggleDamageTaken(character.getId());
			// } else if (!character.tookDamageThisTurn() &&
			// !character.getOwningPlayerId().equals(getActivePlayer()))
			// getGameStateModel().removeStunCounter(character.getId());
			// }
			// END OF DEPRECATION
			finish(null);
			break;

		case CAST_ABILITY:

			logger.debug("Attempting to perform ability effect: " + getCurrentEffect());

			// Add the AbilityEffect to the effectstack
			if (getCurrentEffect() != null) {

				logger.debug("Adding ability effect to effect stack: " + getCurrentEffect());

				// Add the ability effect to the stack
				getGameStateModel().getEffectStack().add(getCurrentEffect());

				// Increase the number of actions performed if the ability being
				// cast is not "swap"
				if (getCurrentAction().getAbilityClassId() != AbilityClassIds.SWAP_ABILITY_CLASS_ID) {
					logger.debug("Increasing action count");
					getGameStateModel().setCharacterPerformedAction(getCurrentEffect().getSourceCharacterId(), true);
					getGameStateModel().setPerformedActionsCount(getGameStateModel().getPerformedActionsCount() + 1);
				}

				// Trigger abilites and buffs
				GameEvent event = new GameEvent(GameEventType.ABILITY_CAST, getCurrentEffect());
				logger.debug("Firing event: " + event);
				getGameEventHandler().handleEvent(event);

				// Broadcast the model to all players
				logger.debug("Firing model update");
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());

				// Transition to next state
				setCurrentEffect(null);
				setCurrentAction(null);
				transition(getCurrentState(), GameLogic_TurnState.NEXT_PRIORITY);

			} else {

				transition(getCurrentState(), GameLogic_TurnState.PLAYER_PERFORM_CHARACTER_ACTION);

			}
			break;

		case HANDLE_REACTION:

			// Add the AbilityEffect to the effectstack
			if (getCurrentEffect() != null) {

				logger.debug("Attempting to handle reaction {} with effect {}", getCurrentReaction(),
						getCurrentEffect());

				// Perform model operations
				if (getGameStateModel().getEffectStack().add(getCurrentEffect()))
					logger.debug("The effect has been added to the effect stack");
				else
					logger.debug("The effect could not be added from the effect stack");

				if (getGameStateModel().getPossibleReactionsList().get(getGameStateModel().getPriorityPlayer())
						.remove(getCurrentReaction().getReactionTrigger()))
					logger.debug("The reaction trigger has been removed from the possible reactions list");
				else {
					logger.debug("The reaction trigger {} could not be removed from the possible reactions list {} ",
							getCurrentReaction().getReactionTrigger(), getGameStateModel().getPossibleReactionsList()
									.get(getGameStateModel().getPriorityPlayer()));
				}

				// Broadcast the model to all players
				getGameLogicWrapper().handleModelUpdate(getGameStateModel());

				// Cleanup
				logger.debug("Clearing current effect");
				setCurrentEffect(null);
				logger.debug("Clearing current reaction");
				setCurrentReaction(null);
				logger.debug("Setting hasReacted to true");
				setHasReacted(true);

			} else {

				transition(GameLogic_TurnState.HANDLE_PRIORITY_PASS);

			}
			break;

		case END_OF_TURN:

			// remember that this state was entered
			setEndOfTurn(true);

			// reduce durationLeft of buffs whose activePlayerOnCreation is the
			// next player to be the active player by 1
			for (UUID playerid : getPlayerListService().getPlayerIds()) {

				for (UUID characterId : getGameStateModel().getCharacterIds(playerid)) {

					Character character = getGameStateModel().getCharacter(characterId);

					for (Buff buff : new ArrayList<>(character.getBuffs())) {
						if (buff.getActivePlayerOnCreation()
								.equals(getPlayerListService().getFollowingPlayerId(getActivePlayer()))
								&& buff.getDuration() > GameLogicConstants.INDEFINITE) {
							getGameStateModel().reduceBuffDuration(buff);
						}
					}
				}
			}

			// trigger end of turn
			getGameEventHandler().handleEvent(new GameEvent(GameEventType.TURN_ENDED, null, new Object[0]));

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			transition(getCurrentState(), GameLogic_TurnState.NEXT_PRIORITY);
			break;

		case HANDLE_PRIORITY_PASS:

			getPassed().put(getGameStateModel().getPriorityPlayer(), !isHasReacted());
			getGameStateModel().getTurnTimer().resume();
			getGameStateModel().getChooseTimer().stop();
			transition(GameLogic_TurnState.NEXT_PRIORITY);
			break;

		case NEXT_PRIORITY:

			// Update priorityPlayer
			getGameStateModel().setPriorityPlayer(
					getPlayerListService().getFollowingPlayerId(getGameStateModel().getPriorityPlayer()));

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Transition accordingly
			if (allPlayersPassed() || noPossibleReactions()) {
				if (getGameStateModel().getEffectStack().size() == 0) {
					transition(getCurrentState(), GameLogic_TurnState.RESET_PRIORITY);
				} else {
					transition(getCurrentState(), GameLogic_TurnState.PERFORM_EFFECT);
				}
			} else {
				transition(getCurrentState(), GameLogic_TurnState.PREPARE_FOR_REACTION);
			}
			break;

		case PERFORM_EFFECT:

			// Pause Turn Timer
			getGameStateModel().getTurnTimer().pause();

			// Broadcast the model to all players
			// getPlayerListService().getPlayerIds().forEach((playerid) ->
			// getGameLogicWrapper().handleModelUpdate(playerid,
			// getGameStateModel()));

			// Pop effect from the stack
			final Effect effect = getGameStateModel().getEffectStack().pop();

			logger.debug("Attempting to resolve effect: " + effect);

			// Resolve the effect against own model
			getEffectExecutionService().execute(effect, getGameStateModel(), getCharacterClassService(),
					getAbilityClassService());

			// Add effect to EffectHistory
			getGameStateModel().addToEffectHistory(effect);

			// Resume Turn Timer
			getGameStateModel().getTurnTimer().resume();

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Transition to next state
			transition(getCurrentState(), GameLogic_TurnState.RESET_PRIORITY);

			break;

		case PLAYER_PERFORM_CHARACTER_ACTION:

			// Update current game phase
			getGameStateModel().setCurrentGamePhase(GamePhase.ACTION);

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Call the player to perform the action
			getGameLogicWrapper().performCharacterAction(getActivePlayer());
			break;

		case PLAYER_REACTION:

			// Update current game phase
			getGameStateModel().setCurrentGamePhase(GamePhase.REACTION);

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Call the player to perform the reaction(s)
			getGameLogicWrapper().performReaction(getGameStateModel().getPriorityPlayer(),
					getGameStateModel().getPossibleReactionsList().get(getGameStateModel().getPriorityPlayer()),
					getGameStateModel().getCurrentGameEvents());
			break;

		case PREPARE_FOR_REACTION:

			getGameStateModel().getTurnTimer().pause();
			getGameStateModel().setChooseTimer(new CustomTimer(Duration.ofSeconds(10), this::onReactionTimerExpired));
			getGameStateModel().getChooseTimer().start();
			setHasReacted(false);

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			transition(getCurrentState(), GameLogic_TurnState.PLAYER_REACTION);
			break;

		case RESET_PRIORITY:

			// Reset Possible Reactions & Passing-value
			for (UUID playerid : getPlayerListService().getPlayerIds()) {
				getPassed().put(playerid, false);
				getGameStateModel().getPossibleReactionsList().get(playerid).clear();
			}

			// Reset game events
			getGameStateModel().getCurrentGameEvents().clear();

			// Reset priorityPlayer
			getGameStateModel().setPriorityPlayer(getActivePlayer());

			// Broadcast the model to all players
			getGameLogicWrapper().handleModelUpdate(getGameStateModel());

			// Transition accordingly
			if (noPossibleReactions() && getGameStateModel().getEffectStack().size() == 0) {

				if (isEndOfTurn()) {

					transition(getCurrentState(), GameLogic_TurnState.ACTUAL_END_OF_TURN);

				} else {

					if (getGameStateModel().getPerformedActionsCount() >= GameLogicConstants.AVAILABLE_ACTIONS_PER_TURN
							|| getGameStateModel().getTurnTimer().isExpired()) {
						transition(getCurrentState(), GameLogic_TurnState.END_OF_TURN);
					} else {
						transition(getCurrentState(), GameLogic_TurnState.PLAYER_PERFORM_CHARACTER_ACTION);
					}

				}

			} else {

				transition(getCurrentState(), GameLogic_TurnState.NEXT_PRIORITY);

			}

			break;

		case VALIDATE_ACTION:
			boolean isValid = true;
			// Validate the action

			// TODO

			// Setup the constructor information
			Set<Position> target = new HashSet<>();
			target.add(getCurrentAction().getTargetPosition());
			int abilityClassId = getCurrentAction().getAbilityClassId();
			AbilityClass abilityClass = getAbilityClassService().getAbilityClass(abilityClassId);
			Ability ability = getGameStateModel().getCharacter(getCurrentAction().getSourceCharacterId())
					.getAbility(abilityClassId);
			logger.info("AbilityClass to validate: " + abilityClass);
			logger.info("AbilityClassHitFilter to generate targets: " + abilityClass.getHitFilter());
			Map<Position, TargetType> targets = abilityClass.getHitFilter().generateTargets(ability, target,
					getGameStateModel(), getCharacterClassService(), getAbilityClassService());

			// Copy ability class properties to simulate eager fetching
			Map<String, Double> abilityClassProperties = new HashMap<>();
			abilityClassProperties.putAll(abilityClass.getProperties());

			// Create the Action Effect
			try {
				setCurrentEffect((Effect) abilityClass.getAbilityEffectClass()
						.getConstructor(Ability.class, Map.class, Map.class)
						.newInstance(ability, targets, abilityClassProperties));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				logger.catching(e);
				isValid = false;
			}

			// Handle the result
			if (isValid) {
				transition(getCurrentState(), GameLogic_TurnState.CAST_ABILITY);
			}
			break;

		case VALIDATE_REACTION:
			isValid = true;
			// Validate the reaction

			// TODO

			// Setup the constructor information
			target = new HashSet<>();
			target.add(getCurrentReaction().getTarget());
			Trigger trigger = getCurrentReaction().getReactionTrigger();
			int triggerClassId = trigger.getTriggerClassId();
			TriggerClass triggerClass = getAbilityClassService().getTriggerClass(triggerClassId);
			targets = triggerClass.getHitFilter().generateTargets(trigger.getSourceAbility(), target,
					getGameStateModel(), getCharacterClassService(), getAbilityClassService());

			// Create the Reaction Effect
			try { // Trigger != Ability
				setCurrentEffect(
						(Effect) triggerClass.getEffectClass().getConstructor(Trigger.class, Map.class, Map.class)
								.newInstance(trigger, targets, triggerClass.getProperties()));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				logger.catching(e);
				isValid = false;
			}

			if (isValid) {
				transition(getCurrentState(), GameLogic_TurnState.HANDLE_REACTION);
			}
			break;

		default:

			logger.debug("Default case: Illegal State");
			throw new IllegalStateException("Default case: Illegal State");
		}

		logger.debug("End of OnEntry");

	}

	@Override
	protected void onExit(GameLogic_TurnState state) {
		if (state == GameLogic_TurnState.START_OF_TURN) {
			getGameStateModel().getTurnTimer().start();
		}
	}

	/**
	 * Stops all timers in addition to calling the super-method.
	 */
	@Override
	public void interrupt() {
		try {
			super.interrupt();
			getGameStateModel().getChooseTimer().stop();
			getGameStateModel().getTurnTimer().stop();
		} catch (Exception e) {
			logger.catching(Level.ERROR, e);
		}
	}

	// Getter & Setter

	protected Action getCurrentAction() {
		return currentAction;
	}

	protected void setCurrentAction(Action currentAction) {
		this.currentAction = currentAction;
	}

	protected final GameLogicWrapper getGameLogicWrapper() {
		return gameLogicWrapper;
	}

	protected final CharacterDataService getCharacterClassService() {
		return characterClassService;
	}

	protected final AbilityDataService getAbilityClassService() {
		return abilityClassService;
	}

	protected final GameStateModelService getGameStateModel() {
		return gameStateModel;
	}

	protected Effect getCurrentEffect() {
		return currentEffect;
	}

	protected void setCurrentEffect(Effect currentEffect) {
		this.currentEffect = currentEffect;
	}

	protected boolean isHasReacted() {
		return hasReacted;
	}

	protected void setHasReacted(boolean hasReacted) {
		this.hasReacted = hasReacted;
	}

	protected Map<UUID, Boolean> getPassed() {
		return passed;
	}

	protected PlayerService getPlayerListService() {
		return playerListService;
	}

	protected UUID getActivePlayer() {
		return activePlayer;
	}

	protected Reaction getCurrentReaction() {
		return currentReaction;
	}

	protected void setCurrentReaction(Reaction currentReaction) {
		this.currentReaction = currentReaction;
	}

	protected boolean isEndOfTurn() {
		return endOfTurn;
	}

	protected void setEndOfTurn(boolean endOfTurn) {
		this.endOfTurn = endOfTurn;
	}

	protected GameEventHandler getGameEventHandler() {
		return gameEventHandler;
	}

	protected List<Character> getStunnedCharacters() {
		return stunnedCharacters;
	}

	protected EffectExecutionService getEffectExecutionService() {
		return effectExecutionService;
	}

}
