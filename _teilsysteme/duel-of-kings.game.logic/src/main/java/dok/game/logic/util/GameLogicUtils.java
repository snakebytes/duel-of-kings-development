package dok.game.logic.util;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.logic.services.impl.EffectExecutionProvider;
import dok.game.logic.services.impl.GameStateModelProvider;
import dok.game.model.Ability;
import dok.game.model.AbilityClass;
import dok.game.model.ActionEffect;
import dok.game.model.Character;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.util.CharacterStateUtils;
import dok.game.services.EffectExecutionService;
import dok.game.services.GameStateModelService;

public abstract class GameLogicUtils {

    // Class Constants

    private static final Logger logger = LogManager.getLogger();
    private static final EffectExecutionService effectResolver = new EffectExecutionProvider();

    // Methods

    public static int getSkillDamageWithTarget(Ability ability, Position skilltarget, Position enemy, GameStateModelService gameStateModelService, AbilityDataService abilityClassService, CharacterDataService characterClassService) {
        logger.entry(ability, skilltarget, enemy, abilityClassService, characterClassService, gameStateModelService);
        // TODO: #1: Find a more durable solution instead of directly referencing GameStateModelImpl
        gameStateModelService = createDummyModelService((GameStateModelProvider) gameStateModelService);
        AbilityClass ac = abilityClassService.getAbilityClass(ability.getAbilityClassId());
        HashSet<Position> setp = new HashSet<Position>();
        setp.add(skilltarget);
        Map<Position, TargetType> hm = ac.getHitFilter().generateTargets(ability, setp, gameStateModelService, characterClassService, abilityClassService);
        if(hm.containsKey(enemy)) {
            try {
                ActionEffect ae = ac.getAbilityEffectClass().getConstructor(Ability.class, Map.class, Map.class).newInstance(ability, hm, ac.getProperties());
                int dmgBefore = gameStateModelService.getCharacterAt(enemy).getDamageTaken();
                getEffectResolver().execute(ae, gameStateModelService, characterClassService, abilityClassService);
                int dmgAfter = gameStateModelService.getCharacterAt(enemy).getDamageTaken();
                return logger.exit(dmgAfter - dmgBefore);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                logger.catching(e);
            }
        }
        logger.debug("not contained");
        return logger.exit(0);
    }

    public static boolean canAct(Character c, GameStateModel gameStateModel, CharacterDataService characterClassService) {
    	logger.entry(c, gameStateModel, characterClassService);
        return logger.exit(!(CharacterStateUtils.isCharacterDead(c, characterClassService) || c.isStunned() || c.getTimeCounters() > 0));
    }

    public static boolean canUseSkills(Character c, GameStateModel gameStateModel, CharacterDataService characterClassService) {
    	logger.entry(c, gameStateModel, characterClassService);
        return logger.exit(canAct(c, gameStateModel, characterClassService) && !c.isSilenced());
    }

    public static Character getEnemyKing(GameStateModel gameStateModel, UUID self) {
    	logger.entry(gameStateModel, self);
        for(Character c : gameStateModel.getCharacters()) {
            if(c.isKing() && !c.getOwningPlayerId().equals(gameStateModel.getActivePlayer())) {
                return logger.exit(c);
            }
        }
        return logger.exit(null);
    }

    public static Character getFirstAliveOfCol(int col, UUID player, GameStateModel gameStateModel, CharacterDataService characterClassService) {
    	logger.entry(col, player, gameStateModel, characterClassService);
        for(int i = 0; i < 3; i++) {
        	Character c = gameStateModel.getCharacterAt(new Position(player, i, col));
        	if(CharacterStateUtils.isCharacterAlive(c, characterClassService)) {
        		return logger.exit(c);
            }
        }
        return logger.exit(null);
    }

    // TODO: #1: Find a more durable solution instead of directly referencing GameStateModelImpl
    private static GameStateModelService createDummyModelService(GameStateModelProvider gameStateModel) {
    	logger.entry(gameStateModel);
        return logger.exit(gameStateModel.clone());
    }

	protected static EffectExecutionService getEffectResolver() {
		return effectResolver;
	}
}
