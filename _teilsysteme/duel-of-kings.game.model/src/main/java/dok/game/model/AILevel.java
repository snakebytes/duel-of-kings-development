package dok.game.model;

public enum AILevel {

	BEGINNER,
	EASY,
	NORMAL,
	HARD,
	EXPERT,
	MASTER

}
