package dok.game.model;

import java.io.Serializable;
import java.util.UUID;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import dok.game.model.constants.AbilityClassIds;

/**
 * Model representation of a single ability of a character within
 * a running game.
 *
 * @author Konstantin Schaper
 * @since 0.1.0
 */
public final class Ability implements Cloneable, Serializable {

    // Class Constants

    private static final long serialVersionUID = 324099170442533374L;

    // Attributes

    private UUID owner;
    private int abilityClassId;
    private int timeCounters;

    // Constructor(s)

    public Ability() {}

    public Ability(UUID owner, int upgradeAbilityClassId) {
        super();
        this.owner = owner;
        this.abilityClassId = upgradeAbilityClassId;
    }

    Ability(Ability other) {
        this.owner = other.getOwningCharacterId();
        this.abilityClassId = other.getAbilityClassId();
        this.timeCounters = other.getTimeCounters();
    }

    // Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "owner: " + getOwningCharacterId()
				+ ", abilityClassId: " + getAbilityClassId()
				+ ", timeCounters: " + getTimeCounters()
				+ "}";
	}

    public Ability clone() {
        return new Ability(this);
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Ability)) return false;
        else {
            Ability other = (Ability)obj;
            return other.getAbilityClassId() == getAbilityClassId()
                    && other.getOwningCharacterId().equals(getOwningCharacterId());
        }
    }

    public int hashCode() {
        return new HashCodeBuilder(19,27)
                .append(getAbilityClassId())
                .append(getOwningCharacterId())
                .toHashCode();
    }

    public boolean isBasic() {
		return getAbilityClassId() == AbilityClassIds.ATTACK_ABILITY_CLASS_ID
			   || getAbilityClassId() == AbilityClassIds.DEFEND_ABILITY_CLASS_ID
			   || getAbilityClassId() == AbilityClassIds.SWAP_ABILITY_CLASS_ID;
	}

    // Getters & Setters

    public final UUID getOwningCharacterId() {
        return owner;
    }

    public final int getAbilityClassId() {
        return abilityClassId;
    }

    public final int getTimeCounters() {
        return timeCounters;
    }

    protected final void setOwner(UUID owner) {
        this.owner = owner;
    }

    public final void setAbilityClassId(int abilityClassId) {
        this.abilityClassId = abilityClassId;
    }

    public final void setTimeCounters(int timeCounters) {
        this.timeCounters = timeCounters;
    }

}
