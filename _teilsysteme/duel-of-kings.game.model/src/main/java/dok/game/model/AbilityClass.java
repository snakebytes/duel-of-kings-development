package dok.game.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import dok.game.model.constants.AbilityClassIds;

/**
 * Persistent model representation of a specific type of ability.<br>
 * One ability class's {@linkplain #id} is shared among many {@linkplain Ability}s.<br>
 * <br>
 * This class is annotated to be used with the JPA.
 *
 * @author Konstantin Schaper
 * @since 0.1.0
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class AbilityClass implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 3012193427030188912L;

	// Attributes

	@Id
	private int id;

	private Class<? extends ActionEffect> abilityEffectClass;
	private int timeCountersCost;

	@ElementCollection(fetch=FetchType.EAGER)
	@JoinTable(
			name="ABILITY_CLASS_PROPERTIES",
			joinColumns = @JoinColumn(name="mapOwner"))
	@Column(name="property_value")
	private Map<String, Double> properties = new LinkedHashMap<>();

	@OneToOne(cascade = {CascadeType.ALL})
	private TargetFilterList targetFilter;

	@OneToOne(cascade = {CascadeType.ALL})
	private HitFilterList hitFilter;

	// Constructor(s)

	public AbilityClass() {}

	public AbilityClass(int id, int timeCost, Class<? extends ActionEffect> abilityEffectClass, TargetFilterList targetFilter, HitFilterList hitFilter, HashMap<String, Double> properties) {
		this();
		setId(id);
		setTimeCountersCost(timeCost);
		setTargetFilter(targetFilter);
		setHitFilter(hitFilter);
		if (properties != null) getProperties().putAll(properties);
		setAbilityEffectClass(abilityEffectClass);
	}

	// Methods

	@Override
	public String toString() {
		return "" + getId();
	}

	public boolean isPassive() {
		return getTargetFilter() == null && getHitFilter() == null;
	}

	public boolean isBasic() {
		return getId() == AbilityClassIds.ATTACK_ABILITY_CLASS_ID
				|| getId() == AbilityClassIds.SWAP_ABILITY_CLASS_ID
				|| getId() == AbilityClassIds.DEFEND_ABILITY_CLASS_ID;
	}

	// Getters & Setters

	public int getId() {
		return id;
	}

	public int getTimeCountersCost() {
		return timeCountersCost;
	}

	public Double getPropertyValue(String propertyName) {
		return properties.get(propertyName);
	}

	public Map<String, Double> getProperties() {
		return properties;
	}

	protected void setProperties(Map<String, Double> newProperties) {
		this.properties.clear();
		this.properties.putAll(newProperties);
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTimeCountersCost(int timeCountersCost) {
		this.timeCountersCost = timeCountersCost;
	}

	public Class<? extends ActionEffect> getAbilityEffectClass() {
		return abilityEffectClass;
	}

	public void setAbilityEffectClass(Class<? extends ActionEffect> abilityEffectClass) {
		this.abilityEffectClass = abilityEffectClass;
	}

	public TargetFilterList getTargetFilter() {
		return targetFilter;
	}

	public void setTargetFilter(TargetFilterList targetFilter) {
		this.targetFilter = targetFilter;
	}

	public HitFilterList getHitFilter() {
		return hitFilter;
	}

	public void setHitFilter(HitFilterList hitFilter) {
		this.hitFilter = hitFilter;
	}

}
