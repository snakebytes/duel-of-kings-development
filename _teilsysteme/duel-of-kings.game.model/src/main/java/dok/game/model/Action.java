package dok.game.model;

import java.io.Serializable;
import java.util.UUID;

public class Action implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final UUID source;
	private final Position target;
	private final int abilityClassId;

	// Constructor(s)

	public Action(UUID source, Position target, int abilityclassId) {
		this.source = source;
		this.target = target;
		this.abilityClassId = abilityclassId;
	}

	// Getter & Setter

	public UUID getSourceCharacterId() {
		return source;
	}

	public Position getTargetPosition() {
		return target;
	}

	public int getAbilityClassId() {
		return abilityClassId;
	}

}
