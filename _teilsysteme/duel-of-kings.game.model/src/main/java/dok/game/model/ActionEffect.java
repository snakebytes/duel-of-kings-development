package dok.game.model;

import java.util.Map;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class ActionEffect extends Effect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public ActionEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

    public ActionEffect(ActionEffect other) {
		super(other);
	}

    // Overridden Methods


    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ActionEffect)) return false;
        else {
        	ActionEffect other = (ActionEffect)obj;
        	return other.getClass().equals(getClass())
        			&& other.getSourceAbility().equals(getSourceAbility())
        			&& other.getTargetedPositions().equals(getTargetedPositions())
        			&& other.getProperties().equals(getProperties());
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(3,31)
        		.append(getClass())
                .append(getSourceAbility())
                .append(getTargetedPositions())
                .append(getProperties())
                .toHashCode();
    }

}
