package dok.game.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class Buff implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final Map<String, Number> properties = new HashMap<>();
	private UUID 			activePlayerOnCreation;
	private Effect 	source;
	private UUID 			target;
	private int 			duration 						= 1;
	private int 			durationLeft 					= 1;
	private int 			maxStacks 						= 0;
	private int 			stacks 							= 1;
	private boolean 		stackCompensationInsteadRemoval = false;

	// Constructor(s)

	public Buff(UUID activePlayerOnCreation, Effect source, UUID target) {
		super();
		this.activePlayerOnCreation = activePlayerOnCreation;
		this.source = source;
		this.target = target;
	}

	public Buff(UUID activePlayerOnCreation, Effect source, UUID target, int duration) {
		this(activePlayerOnCreation, source, target);
		this.duration = duration;
		this.durationLeft = duration;
	}

	public Buff(UUID activePlayerOnCreation, Effect source, UUID target, int duration, int maxStacks) {
		this(activePlayerOnCreation, source, target, duration);
		this.maxStacks = maxStacks;
	}

	public Buff(UUID activePlayerOnCreation, Effect source, UUID target, int duration, int maxStacks, int initialStacks) {
		this(activePlayerOnCreation, source, target, duration, maxStacks);
		this.stacks = initialStacks;
	}

	public Buff(UUID activePlayerOnCreation, Effect source, UUID target, int duration, int maxStacks, int initialStacks, boolean stackCompensationInsteadRemoval) {
		this(activePlayerOnCreation, source, target, duration, maxStacks, initialStacks);
		this.stackCompensationInsteadRemoval = stackCompensationInsteadRemoval;
	}

	protected Buff(Buff other) {
		this.target = other.getTargetCharacterId();
		this.duration = other.getDuration();
		this.durationLeft = other.getDurationLeft();
		this.maxStacks = other.getMaxStacks();
		this.stacks = other.getStacks();
		this.stackCompensationInsteadRemoval = other.isStackCompensationInsteadRemoval();
		this.getProperties().putAll(other.getProperties());
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "sourceEffect: " + getSourceEffect()
				+ ", activePlayerOnCreation: " + getActivePlayerOnCreation()
				+ ", target: " + getTargetCharacterId()
				+ ", duration: " + getDuration()
				+ ", durationLeft: " + getDurationLeft()
				+ ", maxStacks: " + getMaxStacks()
				+ ", stacks: " + getStacks()
				+ ", stackCompensation: " + isStackCompensationInsteadRemoval()
				+ ", properties: " + getProperties()
				+ "}";
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null
			&& obj instanceof Buff
			&& getClass().equals(obj.getClass())
			&& getTargetCharacterId().equals(((Buff) obj).getTargetCharacterId());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(13,29)
				.append(getClass())
				.append(getTargetCharacterId())
				.toHashCode();
	}

	public final void refreshDuration() {
		setDurationLeft(getDuration());
	}

	public boolean hasSameValuesAs(Buff other) {
		return super.equals(other);
	}

	// Getters & Setters

	public abstract boolean isPositive();

	public final Effect getSourceEffect() {
		return source;
	}

	public final UUID getTargetCharacterId() {
		return target;
	}

	public int getDuration() {
		return duration;
	}

	public final int getDurationLeft() {
		return durationLeft;
	}

	public final void setDuration(Integer duration) {
		this.duration = duration;
	}

	public final void setDurationLeft(Integer durationLeft) {
		this.durationLeft = durationLeft;
	}

	public UUID getActivePlayerOnCreation() {
		return activePlayerOnCreation;
	}

	public void setActivePlayerOnCreation(UUID activePlayerOnCreation) {
		this.activePlayerOnCreation = activePlayerOnCreation;
	}

	public int getMaxStacks() {
		return maxStacks;
	}

	public void setMaxStacks(int maxStacks) {
		this.maxStacks = maxStacks;
	}

	public int getStacks() {
		return stacks;
	}

	public void setStacks(int stacks) {
		this.stacks = stacks;
	}

	public boolean isStackCompensationInsteadRemoval() {
		return stackCompensationInsteadRemoval;
	}

	public void setStackCompensationInsteadRemoval(boolean stackCompensationInsteadRemoval) {
		this.stackCompensationInsteadRemoval = stackCompensationInsteadRemoval;
	}

	public Map<String, Number> getProperties() {
		return properties;
	}

}
