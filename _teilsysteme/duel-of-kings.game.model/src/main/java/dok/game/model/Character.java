package dok.game.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import dok.commons.model.EnhancementPlan;
import dok.commons.model.constants.ModelConstants;
import dok.game.model.api.IHitFilter;
import dok.game.model.api.ITargetFilter;
import dok.game.model.buffs.SilenceBuff;
import dok.game.model.buffs.StunBuff;
import dok.game.model.buffs.StunCountersBuff;
import dok.game.model.util.GameModelUtils;

public final class Character implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 4253530675517031848L;

	// Attributes

	private final List<Buff> buffs = new ArrayList<>();
	private final List<Ability> abilities = new ArrayList<>();
	private final Map<CharacterAttribute, Integer> bonusAttributes = new HashMap<>();
	private final Set<Integer> enhancements = new HashSet<>();
	private EnhancementPlan enhancementPlan;
	private UUID id;
	private int timeCounters;
	private int characterClassId = ModelConstants.UNDEFINED_ID;
	private ITargetFilter targetFilter;
	private IHitFilter hitFilter;
	private DamageType damageType;
	private boolean tookDamageThisTurn;
	private boolean king;
	private int damageTaken;
	private Position position;
	private int rank;
	private boolean availableAtTurnStart;
	private boolean performedAction;

	// Constructor(s)

	public Character() {
		for (CharacterAttribute attribute : CharacterAttribute.values()) {
			getBonusAttributes().put(attribute, 0);
		}
	}

	public Character(int characterClassId, Position position, ITargetFilter baseTargetFilter, IHitFilter baseHitFilter, DamageType baseDamageType, boolean isKing) {
		this();
		this.id = UUID.randomUUID();
		this.setKing(isKing);
		this.targetFilter = baseTargetFilter;
		this.hitFilter = baseHitFilter;
		this.damageType = baseDamageType;
		this.position = position;
		this.characterClassId = characterClassId;
	}

	public Character(Position position, ITargetFilter baseTargetFilter, IHitFilter baseHitFilter, DamageType baseDamageType, boolean isKing, EnhancementPlan enhancementPlan, int characterClassId) {
		this();
		this.id = UUID.randomUUID();
		this.setKing(isKing);
		this.targetFilter = baseTargetFilter;
		this.hitFilter = baseHitFilter;
		this.damageType = baseDamageType;
		this.position = position;
		this.enhancementPlan = enhancementPlan;
		this.characterClassId = characterClassId;
	}

	public Character(Character other) {
		this();

		// Copy buffs
		for (Buff b : new ArrayList<>(other.getBuffs())) {
			try {
				getBuffs().add(GameModelUtils.cloneBuff(b));
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}

		// Copy abilities
		for (Ability a : new ArrayList<>(other.getAbilities())) {
			getAbilities().add(new Ability(a));
		}

		// Copy other fields
		bonusAttributes.putAll(((Character) other).getBonusAttributes());
		id = other.getId();
		position = other.getPosition();
		timeCounters = other.getTimeCounters();
		targetFilter = other.getTargetFilter();
		hitFilter = other.getHitFilter();
		damageType = other.getDamageType();
		if (other.getStunCounters() > 0) {
			setStunCounters(other.getBuff(StunCountersBuff.class).getSourceEffect(), other.getStunCounters());
		}
		tookDamageThisTurn = other.tookDamageThisTurn();
		king = other.isKing();
		rank = other.getRank();
		damageTaken = other.getDamageTaken();
		availableAtTurnStart = other.wasAvailableAtTurnStart();
		performedAction = other.hasPerformedAction();
		characterClassId = other.getCharacterClassId();
	}

	// Methods

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Character(this);
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null
			&& obj instanceof Character
			&& ((Character)obj).getId().equals(getId());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(3,5)
				.append(getId())
				.toHashCode();
	}

	// Getters & Setters

	public boolean wasAvailableAtTurnStart() {
		return availableAtTurnStart;
	}

	public void setAvailableAtTurnStart(boolean flag) {
		availableAtTurnStart = flag;
	}

	public boolean isSilenced() {
		return hasBuff(SilenceBuff.class);
	}

	public boolean isStunned() {
		return hasBuff(StunBuff.class);
	}

	public boolean isReady() {
		return getTimeCounters() == 0;
	}

	public final void addTimeCounters(Integer amount) {
		timeCounters = Math.abs(getTimeCounters() + Math.abs(amount));
	}

	public final void removeTimeCounters(Integer amount) {
		timeCounters = Math.abs(getTimeCounters() - Math.abs(amount));
	}

	public final void setTimeCounters(Integer amount) {
		timeCounters = Math.max(0, amount);
	}

	public final int getTimeCounters() {
		return timeCounters;
	}

	public final UUID getId() {
		return id;
	}

	public final int getCharacterClassId() {
		return characterClassId;
	}

	public final int getBonusAttributeValue(CharacterAttribute attribute) {
		return bonusAttributes.get(attribute) == null ? 0 : bonusAttributes.get(attribute);
	}

	public final void setBonusAttributeValue(CharacterAttribute attribute, int value) {
		bonusAttributes.put(attribute, value);
	}

	public final void addBonusAttributeAmount(CharacterAttribute attribute, int amount) {
		bonusAttributes.put(attribute, bonusAttributes.get(attribute) + Math.abs(amount));
	}

	public final void removeBonusAttributeAmount(CharacterAttribute attribute, int amount) {
		bonusAttributes.put(attribute, bonusAttributes.get(attribute) - Math.abs(amount));
	}

	public final List<Ability> getAbilities() {
		return abilities;
	}

	public final Ability getAbility(int abilityClassId) {
		for (Ability abil : getAbilities())
			if (abil.getAbilityClassId() == abilityClassId) return abil;
		return null;
	}

	public final List<Buff> getBuffs() {
		return buffs;
	}

	public final boolean addBuff(Buff buff) {
		return getBuffs().add(buff);
	}

	public void removeBuff(Buff buff) {
		getBuffs().remove(buff);
	}

	public final Map<CharacterAttribute, Integer> getBonusAttributes() {
		return bonusAttributes;
	}

	public final ITargetFilter getTargetFilter() {
		return targetFilter;
	}

	public final void setTargetType(TargetFilter targetFilter) {
		this.targetFilter = targetFilter;
	}

	public final DamageType getDamageType() {
		return damageType;
	}

	public final void setDamageType(DamageType damageType) {
		this.damageType = damageType;
	}

	public IHitFilter getHitFilter() {
		return hitFilter;
	}

	public void setHitFilter(HitFilter hitFilter) {
		this.hitFilter = hitFilter;
	}

	public UUID getOwningPlayerId() {
		return getPosition().getPlayer();
	}

	public int getStunCounters() {
		return getBuff(StunCountersBuff.class) == null ? 0 : getBuff(StunCountersBuff.class).getStacks();
	}

	public void setStunCounters(Effect sourceEffect, int stunCounters) {
		StunCountersBuff buff = getBuff(StunCountersBuff.class);
		if (stunCounters <= 0 && buff != null) {
			removeBuff(buff);
		} else if (stunCounters > 0 && buff != null){
			buff.setStacks(stunCounters);
		} else if (stunCounters > 0 && buff == null){
			addBuff(new StunCountersBuff(getOwningPlayerId(), sourceEffect, getId(), ModelConstants.INDEFINITE, stunCounters));
		}
	}

	public boolean tookDamageThisTurn() {
		return tookDamageThisTurn;
	}

	public void setTookDamageThisTurn(boolean tookDamageThisTurn) {
		this.tookDamageThisTurn = tookDamageThisTurn;
	}

	public boolean hasBuff(Class<? extends Buff> buffClass) {
		for (Buff b : getBuffs()) if (b.getClass() == buffClass) return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	public <T extends Buff> T getBuff(Class<T> buffClass) {
		for (Buff b : getBuffs())
			if (b.getClass().isAssignableFrom(buffClass)) {
				try {
					return (T) b;
				} catch (ClassCastException e) {
					return null;
				}
		}
		return null;
	}

	public boolean isKing() {
		return king;
	}

	public void setKing(boolean king) {
		this.king = king;
	}

	public int getDamageTaken() {
		return damageTaken;
	}

	public void setDamageTaken(int damageTaken) {
		this.damageTaken = damageTaken;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public boolean hasPerformedAction() {
		return performedAction;
	}

	public void setPerformedAction(boolean performedAction) {
		this.performedAction = performedAction;
	}

	public boolean hasEqualValues(Character other) {
		return super.equals(other);
	}

	public Set<Integer> getActiveEnhancements() {
		return enhancements;
	}

	public EnhancementPlan getEnhancementPlan() {
		return enhancementPlan;
	}

}
