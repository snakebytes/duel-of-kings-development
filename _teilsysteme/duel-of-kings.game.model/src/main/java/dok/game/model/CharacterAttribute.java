package dok.game.model;

public enum CharacterAttribute {

	ATTACK_DAMAGE,
	HEALTH,
	ARMOR,
	MAGIC_RESISTANCE

}
