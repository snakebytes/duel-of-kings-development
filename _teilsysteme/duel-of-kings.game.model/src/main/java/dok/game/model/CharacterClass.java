package dok.game.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import dok.commons.model.EnhancementSlot;

@Entity
@Table(name="CHARACTER_CLASS")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CharacterClass implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -5313432964890509239L;

	// Attributes

	@Id
	private int id;

	@OneToOne(cascade = {CascadeType.ALL})
	private TargetFilterList baseTargetFilter;

	@OneToOne(cascade = {CascadeType.ALL})
	private HitFilterList baseHitFilter;

	@Enumerated
	private DamageType baseDamageType;

	@ElementCollection(fetch=FetchType.EAGER)
	@JoinTable(
			name="BASE_ATTRIBUTES_MAP",
			joinColumns = @JoinColumn(name="mapOwner"))
	@MapKeyEnumerated(EnumType.STRING)
	@Column(name="attribute_value")
	private final Map<CharacterAttribute, Integer> baseAttributes = new LinkedHashMap<>();

	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(
			name="ABILITY_CLASS_IDS",
			joinColumns = @JoinColumn(name="ability_class_id"))
	private final Set<Integer> abilityClassIds = new HashSet<>();

	@ElementCollection
	private Set<EnhancementSlot> enhancementSlots = new HashSet<>();

	@ElementCollection
	private Set<Integer> allowedEnhancementClassIds = new HashSet<>();

	// Constructor(s)

	public CharacterClass() {}

	public CharacterClass(int id, TargetFilterList baseTargetFilter, HitFilterList baseHitFilter, DamageType baseDamageType, Map<CharacterAttribute, Integer> baseAttributes, Set<Integer> abilityClassIds) {
		super();

		this.id = id;
		this.baseTargetFilter = baseTargetFilter;
		this.baseHitFilter = baseHitFilter;
		this.baseDamageType = baseDamageType;
		if(baseAttributes != null) {
			getBaseAttributes().putAll(baseAttributes);
		}
		if(abilityClassIds != null) {
			getAbilityClassIds().addAll(abilityClassIds);
		}
	}

	// Methods

	@Override
	public boolean equals(Object obj) {
		return obj != null
			&& obj instanceof CharacterClass
			&& ((CharacterClass)obj).getId() == getId();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(1,97)
				.append(getId())
				.toHashCode();
	}

	// Getter & Setter

	public int getId() {
		return id;
	}

	public TargetFilterList getBaseTargetFilter() {
		return baseTargetFilter;
	}

	public int getBaseAttributeValue(CharacterAttribute attribute) {
		return Math.abs(baseAttributes.get(attribute));
	}

	public void setBaseAttributeValue(CharacterAttribute attribute, int value) {
		baseAttributes.put(attribute, value);
	}

	public void addBaseAttributeAmount(CharacterAttribute attribute, int amount) {
		baseAttributes.put(attribute, baseAttributes.get(attribute) + Math.abs(amount));
	}

	public void removeBaseAttributeAmount(CharacterAttribute attribute, int amount) {
		baseAttributes.put(attribute, baseAttributes.get(attribute) - Math.abs(amount));
	}

	public Map<CharacterAttribute, Integer> getBaseAttributes() {
		return baseAttributes;
	}

	protected void setBaseAttributes(Map<CharacterAttribute, Integer> newAttributes) {
		baseAttributes.clear();
		baseAttributes.putAll(newAttributes);
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setBaseTargetType(TargetFilterList baseTargetFilter) {
		this.baseTargetFilter = baseTargetFilter;
	}

	public DamageType getBaseDamageType() {
		return baseDamageType;
	}

	public void setBaseDamageType(DamageType baseDamageType) {
		this.baseDamageType = baseDamageType;
	}

	public HitFilterList getBaseHitFilter() {
		return baseHitFilter;
	}

	public void setBaseHitFilter(HitFilterList baseHitFilter) {
		this.baseHitFilter = baseHitFilter;
	}

	public Set<Integer> getAbilityClassIds() {
		return abilityClassIds;
	}

	protected void setAbilityClassIds(Set<Integer> newIds) {
		abilityClassIds.clear();
		abilityClassIds.addAll(newIds);
	}

	public Set<EnhancementSlot> getEnhancementSlots() {
		return enhancementSlots;
	}

	protected void setEnhancementSlots(Set<EnhancementSlot> enhancementSlots) {
		this.enhancementSlots = enhancementSlots;
	}

	public Set<Integer> getAllowedEnhancementClassIds() {
		return allowedEnhancementClassIds;
	}

	protected void setAllowedEnhancementClassIds(Set<Integer> allowedEnhancementClassIds) {
		this.allowedEnhancementClassIds = allowedEnhancementClassIds;
	}

}
