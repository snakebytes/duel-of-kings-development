package dok.game.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import dok.commons.model.EnhancementSlot;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyEnumerated;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import dok.commons.model.EnhancementType;

/**
 * Each instance of this class describes a single type of character enhancement.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CharacterEnhancementClass implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 3336742223433212504L;

	// Attributes

	@Id
	private int id;

	private int rank;

	@Enumerated
	private EnhancementType type;

	@ElementCollection(fetch=FetchType.EAGER)
	@JoinTable(
			name="ENHANCEMENT_CLASS_ATTRIBUTES_MAP",
			joinColumns = @JoinColumn(name="mapOwner"))
	@MapKeyEnumerated(EnumType.STRING)
	@Column(name="attribute_value")
	private final Map<CharacterAttribute, Integer> attributes = new LinkedHashMap<>();

	@ElementCollection(fetch=FetchType.EAGER)
	private final Set<Integer> abilityClassIds = new HashSet<>();

	// Constructor(s)

	/**
	 * Default constructor for jpa entity specification.
	 */
	public CharacterEnhancementClass() {
		// Do nothing
	}

	/**
	 * Initializes the fields of the created object with the given values.
	 *
	 * @param id See {@linkplain #getId()}
	 * @param type See {@linkplain #getType()}
	 * @param rank See {@linkplain #getRank()}
	 */
	public CharacterEnhancementClass(int id, EnhancementType type, int rank) {
		super();
		this.setId(id);
		this.type = type;
		this.rank = rank;
	}

	// Methods

	/**
	 * @param slot The slot to check
	 * @return Whether the enhancement class can be placed in the given enhancement slot
	 */
	public boolean isValidFor(EnhancementSlot slot) {
		return slot.getRank() >= this.getRank() && (slot.getType() == this.getType() ||
				slot.getType() == EnhancementType.GENERIC);
	}

	// Getter & Setter

	/**
	 * Returns the enhancement type of this enhancement class.
	 * Determines in which enhancement slots it can be placed.
	 *
	 * @return The type of this enhancement class
	 */
	public EnhancementType getType() {
		return type;
	}

	/**
	 * Returns the power rank of this enhancement class.
	 * Determines in which enhancement slots it can be placed.
	 *
	 * @return The rank of this enhancement class
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @return The ids of the abilities which will be added to the character
	 * when an enhancement of this class is being activated.
	 */
	public Set<Integer> getAbilityClassIds() {
		return abilityClassIds;
	}

	/**
	 * @return A mapping of character attribute values which will be added to the character when
	 * an enhancement of this class is being activated.
	 */
	public Map<CharacterAttribute, Integer> getAttributes() {
		return attributes;
	}

	/**
	 * @return The unique identifier of this object
	 */
	public int getId() {
		return id;
	}

	/**
	 * Overrides the unique identifier of this object with the given value
	 *
	 * @param id The unique identifier to associate with the object
	 */
	public void setId(int id) {
		this.id = id;
	}

}
