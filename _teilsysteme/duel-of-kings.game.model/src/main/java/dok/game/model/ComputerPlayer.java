package dok.game.model;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import dok.commons.model.Lineup;

public class ComputerPlayer extends Player {

	// Class Constants

	private static final long serialVersionUID = -6528871043327434934L;

	// Attributes

	private final AILevel strength;

	// Constructor(s)

	public ComputerPlayer(AILevel strength, Lineup lineup) {
		super(lineup);
		this.strength = strength;
	}

	// Methods

	@Override
	public String toString() {
		return getStrength().toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(5,17)
				.append(getPlayerId())
				.toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		return other != null
				&& other instanceof Player
				&& getPlayerId().equals(((Player)other).getPlayerId());
	}

	// Getter & Setter

	public AILevel getStrength() {
		return strength;
	}

	@Override
	public String getDisplayName() {
		return getStrength().toString();
	}

}
