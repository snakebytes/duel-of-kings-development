package dok.game.model;

public class Damage extends HealthModification {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private DamageType damageType;
	private int blockedAmount;
	private int resistedAmount;

	// Constructor(s)

	public Damage(int baseAmount, Effect source, Position target, TargetType targetType, DamageType damageType) {
		super(baseAmount, source, target, targetType);
		this.damageType = damageType;
	}

	// Getter & Setter

	public DamageType getDamageType() {
		return damageType;
	}

	/**
	 * @return amount of damage absorbed by shields
	 */
	public int getBlockedAmount() {
		return blockedAmount;
	}

	/**
	 * <i>WARNING: Internal method, do not call unless you know what you are doing!</i>
	 */
	public void setBlockedAmount(int blockedAmount) {
		this.blockedAmount = blockedAmount;
	}

	/**
	 * @return amount of damage absorbed by resistances
	 */
	public int getResistedAmount() {
		return resistedAmount;
	}

	public void setResistedAmount(int resistedAmount) {
		this.resistedAmount = resistedAmount;
	}

//	@Override
//	public String toString() {
//		return new Gson().toJson(this);
//	}

}
