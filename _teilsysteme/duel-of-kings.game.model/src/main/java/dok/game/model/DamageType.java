package dok.game.model;

public enum DamageType {

	PHYSICAL {
		@Override
		public String toString() {
			return "Physical";
		}
	},
	MAGICAL{
		@Override
		public String toString() {
			return "Magical";
		}
	};

}
