package dok.game.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public abstract class Effect implements Cloneable, Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

    private final Ability source;
    private final Map<Position, TargetType> targets = new LinkedHashMap<>();
    private final Map<String, Double> properties;

    // Constructor(s)

    public Effect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
    	this.source = source;
        if (targets != null) getTargetedPositions().putAll(targets);
        this.properties = properties;
    }

    public Effect(Effect other) {
        this.source = new Ability(other.getSourceAbility());
        if (other.getTargetedPositions() != null) getTargetedPositions().putAll(other.getTargetedPositions());
        this.properties = new LinkedHashMap<>(other.getProperties());
    }

    // Methods

    public String toString() {
    	return getClass().getSimpleName() + "{"
    			+ "sourceAbility: " + getSourceAbility()
    			+ ", targets: " + getTargetedPositions()
    			+ ", properties: " + getProperties()
    			+ "}";
    }

    // Getter & Setter

	public final UUID getSourceCharacterId() {
		return getSourceAbility().getOwningCharacterId();
	}

	public final Map<String, Double> getProperties() {
		return properties;
	}

	public final Map<Position, TargetType> getTargetedPositions() {
		return targets;
	}

	public final Ability getSourceAbility() {
		return source;
	}

}
