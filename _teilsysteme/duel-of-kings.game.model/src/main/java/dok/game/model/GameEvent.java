package dok.game.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Immutable
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 *
 */
public class GameEvent implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final GameEventType eventType;
	private final Object[] parameters;
	private final Effect sourceEffect;

	// Constructor(s)

	/**
	 * TODO: Describe constructor

	 * @param eventType of this event
	 * @param parameters of this event
	 * @throws IllegalArgumentException When the list of given parameters does not match the parameters requested by the given eventType
	 */
	public GameEvent(GameEventType eventType, Effect sourceEffect, Object... parameters) throws IllegalArgumentException {
		// Validate input parameters
		if (eventType.getParameterClasses().size() != parameters.length)
			throw new IllegalArgumentException("Number of parameters do not match the requested number of parameters by the given game event type.");

		for (int i = 0; i < parameters.length; i++) {
			if (!eventType.getParameterClasses().get(i).isAssignableFrom(parameters[i].getClass()))
				throw new IllegalArgumentException("Given parameter types do not match the requested parameter types of the given game event type.");
		}

		// Initialize variables with parameters
		this.sourceEffect = sourceEffect;
		this.eventType = eventType;
		this.parameters = parameters;
	}

	// Methods

	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "eventType: " + getEventType()
				+ ", sourceEffect: " + getSourceEffect()
				+ ", parameters: " + Arrays.asList(getParameters())
				+ "}";
	};

	// Getter & Setter

	public GameEventType getEventType() {
		return eventType;
	}

	public Object[] getParameters() {
		return parameters;
	}

	public Effect getSourceEffect() {
		return sourceEffect;
	}

}
