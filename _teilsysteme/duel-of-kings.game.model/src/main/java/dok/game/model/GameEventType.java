package dok.game.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum GameEventType {

    /**
     * No parameters.
     */
    TURN_STARTED(new Class<?>[0]),

    /**
     * No parameters.
     */
    TURN_ENDED(new Class<?>[0]),

    /**
     * No parameters.
     */
    ABILITY_CAST(new Class<?>[0]),

    /**
     * @param Character The source character of the swap
     * @param Character The targeted character of the swap
     */
    CHARACTERS_SWAPPED(new Class<?>[]{Character.class, Character.class}),

    /**
     * @param Buff The buff which was added
     */
    BUFF_ADDED(new Class<?>[]{Buff.class}),

    /**
     * @param Buff The buff which was removed
     */
    BUFF_REMOVED(new Class<?>[]{Buff.class}),

    /**
     * @param Buff The buff which refreshed
     */
    BUFF_REFRESHED(new Class<?>[]{Buff.class}),

    /**
     * @param Damage The damage which has been applied
     */
    DAMAGE_APPLIED(new Class<?>[]{Damage.class}),

    /**
     * @param Healing The healing which has been applied
     */
    HEALING_APPLIED(new Class<?>[]{Healing.class}),

    /**
     * @param Character The character who upgraded
     * @param Integer The rank to which the character has been upgraded to
     */
    CHARACTER_UPGRADED(new Class<?>[]{Character.class, Integer.class}),


    /**
     * @param Character The character whose enhancement has been activated
     * @param Integer The rank which caused the activation (-1 indicates an activation which has not been caused by an upgrade)
     * @param Integer The id of the {@linkplain CharacterEnhancementClass} who has been activated
     */
    ENHANCEMENT_ACTIVATED(new Class<?>[]{Character.class, Integer.class, Integer.class}),

    /**
     * @param Character The character whose enhancement has been deactivated
     * @param Integer The id of the {@linkplain CharacterEnhancementClass} who has been deactivated
     */
    ENHANCEMENT_DEACTIVATED(new Class<?>[]{Character.class, Integer.class}),

    /**
     * @param Character The character who died
     */
    CHARACTER_DIED(new Class<?>[]{Character.class}),

    /**
     * @param Character The character whose stun counters where manipulated
     * @param Integer The amount of stun counters added
     */
    STUN_COUNTERS_ADDED(new Class<?>[]{Character.class, Integer.class}),

    /**
     * @param Character The character whose stun counters where manipulated
     * @param Integer The amount of stun counters removed
     */
    STUN_COUNTERS_REMOVED(new Class<?>[]{Character.class, Integer.class}),

    /**
     * @param Character The character who has just been stunned
     */
    STUNNED(new Class<?>[]{Character.class});

    // Attributes

    private final Class<?>[] parameterClasses;

    // Constructor(s)

    private GameEventType(Class<?>[] parameterTypes) {
        this.parameterClasses = parameterTypes;
    }

    // Getters & Setters

    /**
     * Returns an unmodifiable list of class-objects each describing
     * the type of a {@linkplain GameEvent} parameter. Both lists
     * are in the same order.
     *
     * @return List of parameter classes
     */
    public List<Class<?>> getParameterClasses() {
        return Collections.unmodifiableList(Arrays.asList(parameterClasses));
    }

}
