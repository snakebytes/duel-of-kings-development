package dok.game.model;

import java.io.Serializable;

public class GameResult implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 7603698701438722173L;

	// Attributes

	private final GameResultType resultType;

	// Constructor(s)

	public GameResult(GameResultType resultType) {
		this.resultType = resultType;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "resultType: " + getResultType()
				+ "}";
	}

	// Getter & Setter

	public GameResult(GameResult other) {
		this.resultType = other.resultType;
	}

	public GameResultType getResultType() {
		return resultType;
	}

}
