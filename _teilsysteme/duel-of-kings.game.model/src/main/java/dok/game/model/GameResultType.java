package dok.game.model;

public enum GameResultType {

	WIN,
	LOSS,
	DRAW,
	ERROR

}
