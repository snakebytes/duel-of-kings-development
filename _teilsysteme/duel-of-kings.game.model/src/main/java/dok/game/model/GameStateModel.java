package dok.game.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import java.util.UUID;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import dok.commons.impl.CustomTimer;
import dok.game.model.util.GameModelUtils;

public class GameStateModel implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -7834950918206137251L;

	// Attributes

	protected final HashMap<UUID, Character> characterMap = new HashMap<>();
	protected final HashMap<UUID, HashMap<Pair<Integer, Integer>, Character>> fields = new HashMap<>();
	protected UUID startingPlayer;
	protected UUID activePlayer;
	protected UUID priorityPlayer;
	protected int roundCount = 0;
	protected int performedActionsCount;
	protected GamePhase currentGamePhase;
	protected int remainingFreeSwaps = 0;

	protected final HashMap<UUID, List<Trigger>> possibleReactionsList = new HashMap<>();
	protected final Stack<Effect> effectStack = new Stack<>();
	protected final ArrayList<GameEvent> currentGameEvents = new ArrayList<>();
	protected final List<Effect> effectHistory = new ArrayList<>(10);

	protected CustomTimer turnTimer;
	protected CustomTimer chooseTimer;

	// Constructors

	public GameStateModel() {
		this.turnTimer = new CustomTimer();
		this.chooseTimer = new CustomTimer();
	}

	@SuppressWarnings("unchecked")
	protected GameStateModel(GameStateModel other) {

		// Create Fields
		for (UUID player : new HashSet<>(other.getFields().keySet())) {
			getFields().put(player, new HashMap<Pair<Integer, Integer>, Character>());
		}

		// Copy Characters
		for (UUID characterId : new HashSet<>(other.getCharacterMap().keySet())) {
			Character chr = new Character(other.getCharacterMap().get(characterId));
			getCharacterMap().put(characterId, chr);
			getFields().get(chr.getPosition().getPlayer()).put(chr.getPosition().getCoordinates(), chr);
		}

		// Copy possible reactions
		for (UUID player : new HashSet<>(other.getPossibleReactionsList().keySet())) {
			getPossibleReactionsList().put(player, new ArrayList<Trigger>());
			for (Trigger trigger : other.getPossibleReactionsList().get(player)) {
				getPossibleReactionsList().get(player).add(new Trigger(trigger));
			}
		}

		// Copy effect stack
		for (Effect effect : (Stack<Effect>) other.getEffectStack().clone()) {
			try {
				getEffectStack().add(GameModelUtils.cloneEffect(effect));
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}

		// Copy effect history
		for (Effect effect : new ArrayList<>(other.getEffectHistory())) {
			try {
				getEffectHistory().add(GameModelUtils.cloneEffect(effect));
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}

		// Copy Starting Player
		setStartingPlayer(other.getStartingPlayer());

		// Copy Active Player
		setActivePlayer(other.getActivePlayer());

		// Copy Starting Player
		setPriorityPlayer(other.getPriorityPlayer());

		// Copy Round Count
		setRoundCount(other.getRoundCount());

		// Copy performed actions count
		setPerformedActionsCount(other.getPerformedActionsCount());

		// Copy free swaps
		remainingFreeSwaps = other.getRemainingFreeSwaps();

		// Copy current game phase
		setCurrentGamePhase(other.getCurrentGamePhase());

		// Copy game events
		getCurrentGameEvents().addAll(other.getCurrentGameEvents());

		// Copy Timers
		if (other.getTurnTimer() == null) this.turnTimer = null;
		else this.turnTimer = new CustomTimer(other.getTurnTimer());

		if (other.getChooseTimer() == null) this.chooseTimer = null;
		else this.chooseTimer = new CustomTimer(other.getChooseTimer());

	}

	// Methods

	public GameStateModel copy() {
		return new GameStateModel(this);
	}

	@Override
	public String toString() {
		return "GameStateModel {"
				+ "fields: " + getFields()
				+ ", effectStack: " +  getEffectStack()
				+ "}";
	}

	// Getters & Setters

	public int getRowCount(UUID player) {
		if (getField(player) == null) return -1;
		Integer result = -1;
		for (Pair<Integer, Integer> pos : getField(player).keySet())
			if (result < pos.getKey()) result = pos.getKey();
		return (Integer) (result + 1);
	}

	public int getColCount(UUID player) {
		Integer result = -1;
		for (Pair<Integer, Integer> pos : getField(player).keySet())
			if (result < pos.getValue()) result = pos.getValue();
		return (Integer) (result + 1);
	}

	public Position getPosition(UUID characterId) {
		Character c = getCharacter(characterId);
		UUID owner = c.getOwningPlayerId();
		Pair<Integer, Integer> pos = null;
		for (Pair<Integer, Integer> p : getField(owner).keySet()) {
			Character chr = getFields().get(owner).get(p);
			if (c.equals(chr)) {
				pos = p;
				break;
			}
		}
		return new Position(owner, pos);
	}

	public List<Character> getCharacters() {
		return new ArrayList<>(getCharacterMap().values());
	}

	public void setField(UUID player, HashMap<Pair<Integer, Integer>, Character> field) {
		for (Pair<Integer, Integer> pos : field.keySet()) getCharacterMap().putIfAbsent(field.get(pos).getId(), field.get(pos));
		getFields().put(player, field);
	}

	/**
	 * The {@linkplain Pair}'s key is the row of the character and
	 * it's value is the column of the character within the field.
	 */
	public HashMap<UUID, HashMap<Pair<Integer, Integer>, Character>> getFields() {
		return fields;
	}

	public HashMap<Pair<Integer, Integer>, Character> getField(UUID owner) {
		return getFields().get(owner);
	}

	public Character getCharacterAt(Position pos) {
		if (pos == null) return null;
		return getField(pos.getPlayer()).get(new MutablePair<Integer, Integer>(pos.getRow(), pos.getCol()));
	}

	public Character getCharacter(UUID id) {
		return getCharacterMap().get(id);
	}

	public Position getRandomPosition(UUID owner) {
		List<Pair<Integer, Integer>> poses = new ArrayList<>(getFields().get(owner).keySet());
		return new Position(owner, poses.get(new Random().nextInt(poses.size())));
	}

	public List<UUID> getCharacterIds(UUID owner) {
		List<UUID> result = new ArrayList<>();
		for (Character c : getFields().get(owner).values()) result.add(c.getId());
		return result;
	}

	public UUID getActivePlayer() {
		return activePlayer;
	}

	public int getRoundCount() {
		return roundCount;
	}

	public void setRoundCount(int roundCount) {
		this.roundCount = roundCount;
	}

	public int getPerformedActionsCount() {
		return performedActionsCount;
	}

    public Stack<Effect> getEffectStack() {
        return effectStack;
    }

	public HashMap<UUID, List<Trigger>> getPossibleReactionsList() {
		return possibleReactionsList;
	}

	public UUID getStartingPlayer() {
		return startingPlayer;
	}

	public List<GameEvent> getCurrentGameEvents() {
		return currentGameEvents;
	}

	public UUID getPriorityPlayer() {
		return priorityPlayer;
	}

	public GamePhase getCurrentGamePhase() {
		return currentGamePhase;
	}

	public HashMap<UUID, Character> getCharacterMap() {
		return characterMap;
	}

	public CustomTimer getTurnTimer() {
		return turnTimer;
	}

	public CustomTimer getChooseTimer() {
		return chooseTimer;
	}

	public int getRemainingFreeSwaps() {
		return remainingFreeSwaps;
	}

	public List<Effect> getEffectHistory() {
		return effectHistory;
	}

	public Position getCharacterPosition(UUID characterId) {
		return getCharacter(characterId).getPosition();
	}

	public UUID getCharacterIdFromPosition(Position pos) {
		for (Character chr : getCharacterMap().values())
			if (chr.getPosition().equals(pos)) return chr.getId();
		return null;
	}

	protected void setCurrentGamePhase(GamePhase gamePhase) {
		this.currentGamePhase = gamePhase;
	}

	protected void setPriorityPlayer(UUID player) {
		this.priorityPlayer = player;
	}

	protected void setActivePlayer(UUID activePlayer) {
		this.activePlayer = activePlayer;
	}

	protected void setPerformedActionsCount(int performedActions) {
		this.performedActionsCount = performedActions;
	}

	protected void setStartingPlayer(UUID startingPlayer) {
		this.startingPlayer = startingPlayer;
	}

	protected void setTurnTimer(CustomTimer turnTimer) {
		this.turnTimer = turnTimer;
	}

	protected void setChooseTimer(CustomTimer chooseTimer) {
		this.chooseTimer = chooseTimer;
	}

}
