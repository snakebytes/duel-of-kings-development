package dok.game.model;

public class Healing extends HealthModification {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public Healing(int baseAmount, Effect source, Position target, TargetType targetType) {
		super(baseAmount, source, target, targetType);
	}

}
