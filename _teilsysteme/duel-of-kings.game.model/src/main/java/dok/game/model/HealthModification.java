package dok.game.model;

import java.io.Serializable;

public abstract class HealthModification implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final int baseAmount;
	private final Effect source;
	private final Position target;
	private final TargetType targetType;

	// Constructor(s)

	public HealthModification(int baseAmount, Effect source, Position target, TargetType targetType) {
		super();
		this.baseAmount = baseAmount;
		this.source = source;
		this.target = target;
		this.targetType = targetType;
	}

	// Getter& setter

	public TargetType getTargetType() {
		return targetType;
	}

	public int getBaseAmount() {
		return baseAmount;
	}

	public Effect getSource() {
		return source;
	}

	public Position getTarget() {
		return target;
	}

}
