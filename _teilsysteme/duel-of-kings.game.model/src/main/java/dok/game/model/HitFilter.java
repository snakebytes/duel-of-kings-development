package dok.game.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.IHitFilter;

public enum HitFilter implements IHitFilter {

	PRIMARY {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = new HashMap<>();
			//
			for (Position target : targets)
				result.put(target, TargetType.PRIMARY);
			//
			return result;
		}

	},

	SELF {
		@Override
		public Map<Position, TargetType> generateTargets(Ability sourceAbility, Set<Position> targetPositions,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = new HashMap<>();
			//
			result.put(model.getPosition(sourceAbility.getOwningCharacterId()), TargetType.PRIMARY);
			//
			return result;
		}

	},

	ADJACENT_ROW {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = PRIMARY.generateTargets(source, targets, model, characterClassService, abilityClassService);
			//
			Position pos = (Position) targets.toArray()[0];

			// Upper Character
			if (pos.getCol() + 1 < model.getColCount(pos.getPlayer())) {
				result.putIfAbsent(new Position(pos.getPlayer(), pos.getRow(), pos.getCol() + 1), TargetType.SECONDARY);
			}

			// Lower Character
			if (pos.getCol() - 1 >= 0) {
				result.putIfAbsent(new Position(pos.getPlayer(), pos.getRow(), pos.getCol() - 1), TargetType.SECONDARY);
			}
			//
			return result;
		}

	},

	ROW {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = PRIMARY.generateTargets(source, targets, model, characterClassService, abilityClassService);

			Position pos = (Position) targets.toArray()[0];

			for(int i = 0; i < model.getColCount(pos.getPlayer()); i++) {
				result.putIfAbsent(model.getCharacterAt(new Position(pos.getPlayer(), pos.getRow(), i)).getPosition(), TargetType.SECONDARY);
			}
			return result;
		}

	},

	ADJACENT_COLUMN {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = PRIMARY.generateTargets(source, targets, model, characterClassService, abilityClassService);
			//
			Position pos = (Position) targets.toArray()[0];

			// Upper Character
			if (pos.getRow() + 1 < model.getRowCount(pos.getPlayer())) {
				result.putIfAbsent(new Position(pos.getPlayer(), pos.getRow() + 1, pos.getCol()), TargetType.SECONDARY);
			}

			// Lower Character
			if (pos.getRow() - 1 >= 0) {
				result.putIfAbsent(new Position(pos.getPlayer(), pos.getRow() - 1, pos.getCol()), TargetType.SECONDARY);
			}
			//
			return result;
		}

	},

	COLUMN {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = PRIMARY.generateTargets(source, targets, model, characterClassService, abilityClassService);

			Position pos = (Position) targets.toArray()[0];

			if (pos != null)
				for(int i = 0; i < model.getColCount(pos.getPlayer()); i++) {
					result.putIfAbsent(model.getCharacterAt(new Position(pos.getPlayer(), i, pos.getCol())).getPosition(), TargetType.SECONDARY);
				}
			return result;
		}

	},

	PIERCING {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = PRIMARY.generateTargets(source, targets, model, characterClassService, abilityClassService);
			//
			Position pos = (Position) targets.toArray()[0];

			// Character behind
			if (pos != null && pos.getRow() + 1 < model.getRowCount(pos.getPlayer())) {
				result.putIfAbsent(new Position(pos.getPlayer(), pos.getRow() + 1, pos.getCol()), TargetType.SECONDARY);
			}
			//
			return result;
		}

	},

	ATTACK {
		@Override
		public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return model.getCharacter(source.getOwningCharacterId()).getHitFilter()
					.generateTargets(source, targets, model, characterClassService, abilityClassService);
		}

	},

	ALL_HOSTILE {
		@Override
		public Map<Position, TargetType> generateTargets(Ability sourceAbility, Set<Position> targetPositions,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			Map<Position, TargetType> result = new HashMap<>();
			//
			UUID playerOwningSource = model.getCharacter(sourceAbility.getOwningCharacterId()).getOwningPlayerId();
			//
			for (Character chr : model.getCharacters()) {
				if (!chr.getOwningPlayerId().equals(playerOwningSource)) result.putIfAbsent(chr.getPosition(), TargetType.SECONDARY);
			}
			//
			return result;
		}
	};

	// Methods

	public int getId() {
		return ordinal();
	}

	// Class Methods

	public static HitFilterList combine(HitFilter... filters) {
		return new HitFilterList(filters);
	}

	public static HitFilterList single(HitFilter filter) {
		return new HitFilterList(filter);
	}

}
