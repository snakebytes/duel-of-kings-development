package dok.game.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.IHitFilter;

/**
 * Combines a set of hit filters which returns a combined result of all given filters.<br>
 * TargetTypes are merged following a certain priority. A target's TargetType will not
 * be overridden by a {@linkplain TargetType} with a lower ordinal.
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class HitFilterList implements IHitFilter {

	// Class Constants

	private static final long serialVersionUID = -7515404131341863080L;

	// Attributes

	private Set<HitFilter> filters = new HashSet<>();

	// Constructor(s)

	public HitFilterList() {
		// do nothing
	}

	public HitFilterList(HitFilter... filters) {
		getFilters().addAll(Arrays.asList(filters));
	}

	public HitFilterList(List<HitFilter> filters) {
		getFilters().addAll(filters);
	}

	// Overridden Methods

	@Override
	public boolean equals(Object obj) {
		return obj != null
				&& obj instanceof HitFilterList
				&& ((HitFilterList)obj).getFilters()
					.equals(getFilters());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(getFilters())
				.build();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "filters: " + getFilters()
				+ "}";
	}

	// Implemented Methods

	@Override
	public Map<Position, TargetType> generateTargets(Ability source, Set<Position> targets, GameStateModel model,
			CharacterDataService characterClassService, AbilityDataService abilityClassService) {

		// Initialize result value
		Map<Position, TargetType> result = new HashMap<>();

		for (IHitFilter filter : getFilters()) {
			// Load the targets from the filter
			Map<Position, TargetType> filterResult = filter.generateTargets(source, targets, model, characterClassService, abilityClassService);

			//Add all targets from filterResult
			for (Position id : filterResult.keySet())
				if (!result.containsKey(id) || result.get(id).compareTo(filterResult.get(id)) < 0)
					result.put(id, filterResult.get(id));
		}

		// Return the result
		return result;
	}

	// Methods

	public boolean contains(HitFilter filter) {
		return getFilters().contains(filter);
	}

	// Getter & Setter

	@Access(AccessType.PROPERTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="HIT_FILTER_LIST_FILTERS",
			joinColumns=@JoinColumn(name="filter"))
	private Set<HitFilter> getFilters() {
		return filters;
	}

	protected void setFilters(Set<HitFilter> newFilters) {
		filters = newFilters;
	}

	@Id
	@Column(name="hitfilter_id")
	public long getId() {
		long id = HitFilter.values().length;
		for(HitFilter filter : getFilters()) {
			id += (long) Math.pow(2, filter.getId());
		}
		return id;
	}

	public void setId(long newId) {
		// do nothing
	}

}
