package dok.game.model;

import java.io.Serializable;
import java.util.UUID;

import dok.commons.model.Lineup;

/**
 *
 * @author Konstantin Schaper
 * @immutable
 */
public abstract class Player implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -4100687905682457575L;

	// Attributes

	private final Lineup lineup;
	private final UUID playerId;

	// Constructor(s)

	public Player(Lineup lineup) {
		super();
		this.lineup = lineup;
		this.playerId = UUID.randomUUID();
	}

	// Abstract Methods

	public abstract String getDisplayName();

	// Getter & Setter

	public Lineup getLineup() {
		return lineup;
	}

	public UUID getPlayerId() {
		return playerId;
	}

}
