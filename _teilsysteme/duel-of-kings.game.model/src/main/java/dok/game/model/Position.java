package dok.game.model;

import java.io.Serializable;
import java.util.UUID;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;


/**
 * Immutable.
 *
 * @author Konstantin Schaper
 */
public class Position implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -3597324285799527972L;

	// Attributes

	private final UUID player;
	private final Pair<Integer,Integer> coordinates;

	// Constructor(s)

	public Position(UUID player, Pair<Integer, Integer> coordinates) {
		super();
		this.player = player;
		this.coordinates = coordinates;
	}

	public Position(UUID player, int row, int col) {
		super();
		this.player = player;
		this.coordinates = new MutablePair<Integer,Integer>(row,col);
	}

	public Position(Position other) {
		this.player = other.player;
		this.coordinates = other.getCoordinates();
	}

	// Methods

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getPlayer().hashCode())
				.append(getCoordinates())
				.toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Position)) return false;
		else return getPlayer().equals(((Position)other).getPlayer())
					&& getCoordinates().equals(((Position)other).getCoordinates());
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "row: " + getRow()
				+ ", col: " + getCol()
				+ "}";
	}

	// Getters & Setters

	public Pair<Integer, Integer> getCoordinates() {
		return coordinates;
	}

	public final UUID getPlayer() {
		return player;
	}

	public int getRow() {
		return getCoordinates().getKey();
	}

	public int getCol() {
		return getCoordinates().getValue();
	}

}
