package dok.game.model;

import java.io.Serializable;

public class Reaction implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final Position target;
	private final Trigger reactionTrigger;

	// Constructor(s)

	public Reaction(Trigger reactionTrigger, Position target) {
		this.target = target;
		this.reactionTrigger = reactionTrigger;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "target: " + getTarget()
				+ ", reactionTrigger: " + getReactionTrigger()
				+ "}";
	}

	// Getter & Setter

	public Position getTarget() {
		return target;
	}

	public Trigger getReactionTrigger() {
		return reactionTrigger;
	}

}
