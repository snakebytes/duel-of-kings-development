package dok.game.model;

import java.util.Map;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class ReactionEffect extends Effect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final Trigger trigger;

	// Constructor(s)

	public ReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource.getSourceAbility(), targets, properties);
		this.trigger = triggerSource;
	}

	public ReactionEffect(ReactionEffect other) {
		super(other);
		this.trigger = other.getTrigger();
	}

    // Overridden Methods

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ReactionEffect)) return false;
        else {
        	ReactionEffect other = (ReactionEffect)obj;
        	return other.getClass().equals(getClass())
        			&& other.getSourceAbility().equals(getSourceAbility())
        			&& other.getTargetedPositions().equals(getTargetedPositions())
        			&& other.getProperties().equals(getProperties())
        			&& other.getTrigger().equals(getTrigger());
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,17)
        		.append(getClass())
                .append(getSourceAbility())
                .append(getTargetedPositions())
                .append(getProperties())
                .append(getTrigger())
                .toHashCode();
    }

	// Getters & Setters
	public Trigger getTrigger() {
		return trigger;
	}

}
