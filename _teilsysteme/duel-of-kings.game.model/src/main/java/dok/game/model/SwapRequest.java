package dok.game.model;

import java.io.Serializable;

public class SwapRequest implements Serializable {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attributes

	private final Position source;
	private final Position target;

	// Constructor(s)

	public SwapRequest(Position source, Position target) {
		this.source = source;
		this.target = target;
	}

	// Getter & Setter

	public Position getSource() {
		return source;
	}

	public Position getTarget() {
		return target;
	}

}
