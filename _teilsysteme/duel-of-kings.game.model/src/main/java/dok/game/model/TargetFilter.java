package dok.game.model;

import java.util.UUID;

import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import dok.game.model.util.CharacterStateUtils;

public enum TargetFilter implements ITargetFilter {

	OWN {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return chooser.equals(target.getPlayer());
		}

		@Override
		public String toString() {
			return "eigenen";
		}

	},

	HOSTILE {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return !chooser.equals(target.getPlayer());
		}

		@Override
		public String toString() {
			return "feindlichen";
		}

	},

	ATTACK {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			try {
				return model.getCharacterAt(source).getTargetFilter()
						.test(chooser, sourceAbilityClassId, source, target, targetCount, model, characterClassService, abilityClassService);
			} catch (Exception e) {
				return false;
			}
		}

	},

	MEELE {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			for (int r = target.getRow() - 1; r >= 0; r--) {
				if (CharacterStateUtils.isCharacterAlive(model.getCharacterAt(new Position(target.getPlayer(), r, target.getCol())), characterClassService))
					return false;
			}
			return true;
		}

	},

	SEMI_RANGED {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount, GameStateModel model,
				CharacterDataService characterClassService, AbilityDataService abilityClassService) {
			int counter = 0;
			for (int r = target.getRow() - 1; r >= 0; r--) {
				if (CharacterStateUtils.isCharacterAlive(model.getCharacterAt(new Position(target.getPlayer(), r, target.getCol())), characterClassService)
						 && ++counter >= 2) return false;
			}
			return true;
		}

	},

	READY {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target,
				int targetCount, GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return model.getCharacterAt(target).getTimeCounters() == 0;
		}

	},

	ALIVE {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target,
				int targetCount, GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return CharacterStateUtils.isCharacterAlive(model.getCharacterAt(target), characterClassService);
		}

	},

	DEAD {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return CharacterStateUtils.isCharacterDead(model.getCharacterAt(target), characterClassService);
		}

	},

	OTHER {

		@Override
		public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
				GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return !source.equals(target);
		}

	},

	AVAILABLE {

		@Override
		public boolean test(UUID choosingPlayer, int sourceAbilityClassId, Position source, Position target,
				int targetCount, GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return CharacterStateUtils.isCharacterAvailable(model.getCharacterAt(target), characterClassService);
		}

	},

	AVAILABLE_AT_TURN_START {

		@Override
		public boolean test(UUID choosingPlayer, int sourceAbilityClassId, Position source, Position target,
				int targetCount, GameStateModel model, CharacterDataService characterClassService,
				AbilityDataService abilityClassService) {
			return model.getCharacterAt(target).wasAvailableAtTurnStart();
		}

	};

	// Methods

	public int getId() {
		return ordinal();
	}

	// Class Methods

	public static TargetFilterList and(TargetFilter... filters) {
		return new TargetFilterList(filters);
	}

}
