package dok.game.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;

/**
 * Represents a logical AND operation between any number of target filters.
 *
 * @author Konstantin Schaper
 * @since 0.1.0
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class TargetFilterList implements ITargetFilter {

	// Class Constants

	private static final long serialVersionUID = -1754229149719773669L;

	// Attributes

	private Set<TargetFilter> filters = new HashSet<>();

	// Constructor(s)

	public TargetFilterList() {
		// do nothing
	}

	public TargetFilterList(TargetFilter... filters) {
		getFilters().addAll(Arrays.asList(filters));
	}

	public TargetFilterList(List<TargetFilter> filters) {
		getFilters().addAll(filters);
	}

	// Overridden Methods

	@Override
	public boolean equals(Object obj) {
		return obj != null
				&& obj instanceof TargetFilterList
				&& ((TargetFilterList)obj).getFilters()
					.equals(getFilters());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(getFilters())
				.build();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "filters: " + getFilters()
				+ "}";
	}

	// Implemented Methods

	@Override
	public boolean test(UUID chooser, int sourceAbilityClassId, Position source, Position target, int targetCount,
			GameStateModel model, CharacterDataService characterClassService,
			AbilityDataService abilityClassService) {
		boolean result = true;
		for (ITargetFilter filter : getFilters())
			result = result && filter.test(chooser, sourceAbilityClassId, source, target,
											targetCount, model, characterClassService, abilityClassService);
		return result;
	}

	// Methods

	public boolean contains(ITargetFilter filter) {
		return getFilters().contains(filter);
	}

	// Getter & Setter

	@Access(AccessType.PROPERTY)
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="TARGET_FILTER_LIST_FILTERS",
			joinColumns=@JoinColumn(name="filter"))
	public Set<TargetFilter> getFilters() {
		return filters;
	}

	protected void setFilters(Set<TargetFilter> newFilters) {
		filters = newFilters;
	}

	@Id
	public long getId() {
		long id = TargetFilter.values().length;
		for(TargetFilter filter : getFilters()) {
			id += (long) Math.pow(2, filter.getId());
		}
		return id;
	}

	public void setId(long newId) {
		// do nothing
	}

}
