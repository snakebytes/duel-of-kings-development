package dok.game.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import dok.game.model.util.GameModelUtils;

/**
 * Immutable
 *
 * @author Konstantin Schaper
 */
public class Trigger implements Serializable {

    // Class Constants

    private static final long serialVersionUID = 324099170442533374L;

    // Attributes

    private final int triggerClassId;
    private Ability sourceAbility;
    private Buff sourceBuff;
    private GameEvent sourceEvent;

    // Constructor(s)

    public Trigger(GameEvent sourceEvent, int triggerClassId, Ability sourceAbility) {
        this.sourceEvent = sourceEvent;
        this.triggerClassId = triggerClassId;
        this.sourceAbility = sourceAbility;
    }

    public Trigger(GameEvent sourceEvent, int triggerClassId, Buff sourceBuff) {
        this.sourceEvent = sourceEvent;
        this.triggerClassId = triggerClassId;
        this.sourceBuff = sourceBuff;
    }

    public Trigger(Trigger other) {
        triggerClassId = other.getTriggerClassId();

        sourceAbility = new Ability(other.getSourceAbility());

        try {
            sourceBuff = GameModelUtils.cloneBuff(other.getSourceBuff());
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    // Methods

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Trigger)) return false;
        else {
            Trigger other = (Trigger)obj;
            return other.getTriggerClassId() == getTriggerClassId()
                    && (((other.getSourceAbility() == null && getSourceAbility() == null) || other.getSourceAbility().equals(getSourceAbility()))
                    		&& ((other.getSourceBuff() == null && getSourceBuff() == null) || other.getSourceBuff().equals(getSourceBuff())));
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(7,13)
                .append(getTriggerClassId())
                .append(getSourceAbility())
                .append(getSourceBuff())
                .toHashCode();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{"
                + "triggerClassId: " + getTriggerClassId()
                + ", sourceAbility: " + getSourceAbility()
                + ", sourceBuff: " + getSourceBuff()
                + ", sourceEvent: " + getSourceEvent()
                + "}";
    }

    // Getter & Setter

    public final int getTriggerClassId() {
        return triggerClassId;
    }

    public final Ability getSourceAbility() {
    	if (sourceAbility == null && sourceBuff == null) return null;
    	else if (sourceAbility == null) return getSourceBuff().getSourceEffect().getSourceAbility();
        else return sourceAbility;
    }

    public final Buff getSourceBuff() {
        return sourceBuff;
    }

    public GameEvent getSourceEvent() {
        return sourceEvent;
    }

}
