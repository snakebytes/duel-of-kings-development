package dok.game.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class TriggerClass implements Serializable {

	// Class Constants

	private static final long serialVersionUID = -8717458564107239737L;

	// Attributes

	@Id
	private int id;

	@ElementCollection(fetch=FetchType.EAGER)
	@JoinTable(
			name="TRIGGER_CLASS_PROPERTIES",
			joinColumns = @JoinColumn(name="mapOwner"))
	@Column(name="property_value")
	private Map<String, Number> properties = new LinkedHashMap<>();
	private Class<? extends ReactionEffect> effectClass;

	@Column(length=65535)
	private TargetFilterList targetFilter;

	@Column(length=65535)
	private HitFilterList hitFilter;
	private boolean reaction;

	// Constructor(s)

	public TriggerClass() {
		// Do nothing
	}

	public TriggerClass(int triggerClassId, TargetFilterList targetFilter, HitFilterList hitFilter, Class<? extends ReactionEffect> effectClass, boolean isReaction) {
		this.id = triggerClassId;
		this.targetFilter = targetFilter;
		this.hitFilter = hitFilter;
		this.effectClass = effectClass;
		this.reaction = isReaction;
	}

	public TriggerClass(int triggerClassId, TargetFilterList targetFilter, HitFilterList hitFilter, Class<? extends ReactionEffect> effectClass, boolean isReaction, Map<String, Double> properties) {
		this(triggerClassId, targetFilter, hitFilter, effectClass, isReaction);
		if (properties != null) getProperties().putAll(properties);
	}

	// Methods

//	@Override
//	public String toString() {
//		return new Gson().toJson(this);
//	}

	// Getter & Setter

	public int getId() {
		return id;
	}

	protected void setId(int newId) {
		this.id = newId;
	}

	public Map<String, Number> getProperties() {
		return properties;
	}

	protected void setProperties(Map<String, Number> newProperties) {
		properties.clear();
		properties.putAll(newProperties);
	}

	public Number getPropertyValue(String propertyName) {
		return getProperties().get(propertyName);
	}

	public Class<? extends ReactionEffect> getEffectClass() {
		return effectClass;
	}

	protected void setEffectClass(Class<? extends ReactionEffect> newEffectClass) {
		effectClass = newEffectClass;
	}

	public TargetFilterList getTargetFilter() {
		return targetFilter;
	}

	protected void setTargetFilter(TargetFilterList newTargetFilter) {
		targetFilter = newTargetFilter;
	}

	public HitFilterList getHitFilter() {
		return hitFilter;
	}

	protected void setHitFilter(HitFilterList newHitFilter) {
		hitFilter = newHitFilter;
	}

	public boolean isReaction() {
		return reaction;
	}

	protected void setReaction(boolean newReaction) {
		reaction = newReaction;
	}

}
