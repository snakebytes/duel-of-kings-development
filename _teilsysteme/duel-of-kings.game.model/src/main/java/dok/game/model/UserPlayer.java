package dok.game.model;

import java.util.UUID;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import dok.commons.model.Lineup;

public class UserPlayer extends Player {

	// Class Constants

	private static final long serialVersionUID = -9212272956862722219L;

	// Attributes

	private final String username;
	private final UUID accountId;

	// Constructor(s)

	public UserPlayer(UUID accountId, String username, Lineup lineup) {
		super(lineup);
		this.accountId = accountId;
		this.username = username;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "id: " + getPlayerId()
				+ ", user: " + getAccountId()
				+ ", lineup: " + getLineup()
				+ "}";
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(5,17)
				.append(getPlayerId())
				.toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		return other != null
				&& other instanceof Player
				&& getPlayerId().equals(((Player)other).getPlayerId());
	}

	// Getter & Setter

	public UUID getAccountId() {
		return accountId;
	}

	@Override
	public String getDisplayName() {
		return username;
	}

}
