package dok.game.model.api;

import dok.game.model.AbilityClass;
import dok.game.model.TriggerClass;

public interface AbilityDataService {

	public AbilityClass getAbilityClass(int abilityClassId);
	public TriggerClass getTriggerClass(int triggerClassId);

}
