package dok.game.model.api;

import java.util.List;

import dok.commons.model.EnhancementSlot;
import dok.game.model.CharacterClass;
import dok.game.model.CharacterEnhancementClass;

public interface CharacterDataService {

	public CharacterClass getCharacterClass(int id);
	public CharacterEnhancementClass getCharacterEnhancementClass(int id);
	List<CharacterClass> getAllCharacterClasses();
	List<Integer> getValidEnhancementClassIds(EnhancementSlot slot, int characterClassId);

}
