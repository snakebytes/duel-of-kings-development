package dok.game.model.api;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import dok.game.model.Ability;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.TargetType;

/**
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface IHitFilter extends Serializable {

	public Map<Position, TargetType> generateTargets(
			Ability sourceAbility,
			Set<Position> targetPositions,
			GameStateModel model,
			CharacterDataService characterClassService,
			AbilityDataService abilityClassService);

}
