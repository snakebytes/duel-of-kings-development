package dok.game.model.api;

import java.io.Serializable;
import java.util.UUID;
import java.util.function.Predicate;

import dok.game.model.GameStateModel;
import dok.game.model.Position;

/**
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 *
 * @see {@linkplain Predicate}
 */
public interface ITargetFilter extends Serializable {

	public boolean test(UUID choosingPlayer,
			int sourceAbilityClassId,
			Position source,
			Position target,
			int targetCount,
			GameStateModel model,
			CharacterDataService characterClassService,
			AbilityDataService abilityClassService);

}
