package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.Buff;
import dok.game.model.Effect;

public class BlockBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public BlockBuff(UUID activePlayerOnCreation, Effect source, UUID target, int duration, int blockValue) {
		super(activePlayerOnCreation, source, target, duration);
		setStacks(blockValue);
		setMaxStacks(blockValue);
	}

	public BlockBuff(BlockBuff other) {
		super(other);
		setBlockValue(other.getBlockValue());
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return true;
	}

	public int getBlockValue() {
		return getStacks();
	}

	public void setBlockValue(int blockValue) {
		setStacks(blockValue);
	}

}
