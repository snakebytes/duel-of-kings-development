package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.ActionEffect;
import dok.game.model.Buff;

public class CurseBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Attribute

	private final int attackDamageMalus;

	// Constructor(s)

	public CurseBuff(UUID activePlayerOnCreation, ActionEffect source, UUID target, int duration, int attackDamageMalus) {
		super(activePlayerOnCreation, source, target, duration);
		this.attackDamageMalus = attackDamageMalus;
		getProperties().put("attackDamageMalus", attackDamageMalus);
	}

	public CurseBuff(CurseBuff other) {
		super(other);
		this.attackDamageMalus = other.attackDamageMalus;
		getProperties().put("attackDamageMalus", attackDamageMalus);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return false;
	}

	public int getAttackDamageMalus() {
		return attackDamageMalus;
	}

}
