package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.Buff;
import dok.game.model.Effect;

public class DauntingDefenderBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Constructor(s)

	public DauntingDefenderBuff(UUID activePlayerOnCreation, Effect source, UUID target, int duration) {
		super(activePlayerOnCreation, source, target, duration);
	}

	public DauntingDefenderBuff(DauntingDefenderBuff other) {
		super(other);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return true;
	}

}
