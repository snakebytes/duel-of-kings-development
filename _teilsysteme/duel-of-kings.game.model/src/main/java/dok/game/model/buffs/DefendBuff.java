package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.ActionEffect;
import dok.game.model.Buff;

public class DefendBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constants

	private final int armorBonus;
	private final int magicResistanceBonus;

	// Constructor(s)

	public DefendBuff(UUID activePlayerOnCreation, ActionEffect source, UUID target, int duration, int armorBonus, int magicResistanceBonus) {
		super(activePlayerOnCreation, source, target, duration);
		this.armorBonus = armorBonus;
		this.magicResistanceBonus = magicResistanceBonus;
	}

	public DefendBuff(DefendBuff other) {
		super(other);
		this.armorBonus = other.getArmorBonus();
		this.magicResistanceBonus = other.getMagicResistanceBonus();
	}

	// Methods

	@Override
	public boolean isPositive() {
		return true;
	}

	// Getter & Setter

	public int getArmorBonus() {
		return armorBonus;
	}

	public int getMagicResistanceBonus() {
		return magicResistanceBonus;
	}

}
