package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.Buff;
import dok.game.model.Effect;

public class MagicHuntBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Constructor(s)

	public MagicHuntBuff(UUID activePlayerOnCreation, Effect source, UUID target, int duration, int attackDamageBonus) {
		super(activePlayerOnCreation, source, target, duration);
		getProperties().put("attackDamageBonus", attackDamageBonus);
	}

	public MagicHuntBuff(TransferBuff other) {
		super(other);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return true;
	}

	public int getAttackDamageBonus() {
		return getProperties().get("attackDamageBonus").intValue();
	}

}
