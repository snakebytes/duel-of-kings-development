package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.ActionEffect;
import dok.game.model.Buff;

public class SilenceBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public SilenceBuff(UUID activePlayerOnCreation, ActionEffect source, UUID target, int duration) {
		super(activePlayerOnCreation, source, target, duration);
	}

	public SilenceBuff(SilenceBuff other) {
		super(other);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return false;
	}

}
