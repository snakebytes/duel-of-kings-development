package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.Buff;
import dok.game.model.Effect;

public class StunBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public StunBuff(UUID activePlayerOnCreation, Effect source, UUID target, int duration) {
		super(activePlayerOnCreation, source, target, duration);
	}

	public StunBuff(StunBuff other) {
		super(other);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return false;
	}

}
