package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.Buff;
import dok.game.model.Effect;

public class StunCountersBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = -858486655844843909L;

	// Constructor(s)

	public StunCountersBuff(UUID activePlayerOnCreation, Effect source, UUID target, int duration, int initialStacks) {
		super(activePlayerOnCreation, source, target, duration);
		setDuration(0);
	}

	public StunCountersBuff(StunCountersBuff other) {
		super(other);
		setDuration(0);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return false;
	}

	@Override
	public int getMaxStacks() {
		return 9;
	}

	@Override
	public int getDuration() {
		return 0;
	}

}
