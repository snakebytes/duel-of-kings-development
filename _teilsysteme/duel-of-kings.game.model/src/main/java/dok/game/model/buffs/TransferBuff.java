package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.ActionEffect;
import dok.game.model.Buff;

public class TransferBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public TransferBuff(UUID activePlayerOnCreation, ActionEffect source, UUID target, int duration, int attackDamageBonus) {
		super(activePlayerOnCreation, source, target, duration);
		getProperties().put("attackDamageBonus", attackDamageBonus);
	}

	public TransferBuff(TransferBuff other) {
		super(other);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return true;
	}

	public int getAttackDamageBonus() {
		return getProperties().get("attackDamageBonus").intValue();
	}

}
