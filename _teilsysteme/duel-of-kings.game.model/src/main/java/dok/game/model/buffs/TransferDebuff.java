package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.ActionEffect;
import dok.game.model.Buff;

public class TransferDebuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Constructor(s)

	public TransferDebuff(UUID activePlayerOnCreation, ActionEffect source, UUID target, int duration, int attackDamageMalus) {
		super(activePlayerOnCreation, source, target, duration);
		getProperties().put("attackDamageMalus", attackDamageMalus);
	}

	public TransferDebuff(TransferDebuff other) {
		super(other);
	}

	// Getter & Setter

	@Override
	public boolean isPositive() {
		return false;
	}

	public int getAttackDamageMalus() {
		return getProperties().get("attackDamageMalus").intValue();
	}

}
