package dok.game.model.buffs;

import java.util.UUID;

import dok.game.model.ActionEffect;
import dok.game.model.Buff;

public class WardensCodexBuff extends Buff {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Constructor(s)

	public WardensCodexBuff(UUID activePlayerOnCreation, ActionEffect source, UUID target, int duration) {
		super(activePlayerOnCreation, source, target, duration);
	}

	public WardensCodexBuff(WardensCodexBuff other) {
		super(other);
	}

	// Methods

	@Override
	public boolean isPositive() {
		return true;
	}

}
