package dok.game.model.constants;

public abstract class AbilityClassIds {

	// Base Abilities

	public static final int ATTACK_ABILITY_CLASS_ID = 101;
	public static final int SWAP_ABILITY_CLASS_ID = 102;
	public static final int DEFEND_ABILITY_CLASS_ID = 103;

	/**
	 * @deprecated Removed from rules set.
	 */
	public static final int WAIT_ABILITY_CLASS_ID = 104;

	// Advanced Abilities

	public static final int WARDENS_CODE_ABILITY_CLASS_ID = 105;
	public static final int VETERAN_OF_BATTLES_ABILITY_CLASS_ID = 106;
	//public static final int MOTIVATE_ABILITY_CLASS_ID = 107;
	public static final int AXE_BASH_ABILITY_CLASS_ID = 108;
	public static final int CALLING_THE_PACK_ABILITY_CLASS_ID = 109;
	//public static final int SUMMON_THE_BEAST_ABILITY_CLASS_ID = 110;
	public static final int CURSE_ABILITY_CLASS_ID = 111;
	public static final int SILENCE_ABILITY_CLASS_ID = 112;
	public static final int BREATHTAKING_SILENCE_ABILITY_CLASS_ID = 113;
	public static final int HEAL_ABILITY_CLASS_ID = 114;
	public static final int TRANQUILITY_ABILITY_CLASS_ID = 115;
	public static final int SPEAR_BLOW_ABILITY_CLASS_ID = 116;
	public static final int DAUNTING_DEFENDER_ABILITY_CLASS_ID = 117;
	public static final int STORM_SPRINT_ABILITY_CLASS_ID = 118;
	public static final int AROUSE_ABILITY_CLASS_ID = 119;
	public static final int POWER_TRANSFER_ABILITY_CLASS_ID = 120;
	//public static final int MAGICAL_HUNT_ABILITY_CLASS_ID = 121;
	public static final int BOMBARDMENT_ABILITY_CLASS_ID = 122;
	public static final int ADAPTIVE_BOMB_ABILITY_CLASS_ID = 123;
	public static final int THROW_BOMBS_ABILITY_CLASS_ID = 124;
	public static final int MAGIC_HUNT_ABILITY_CLASS_ID = 125;
	public static final int ENCOURAGE_ABILITY_CLASS_ID = 127;
	public static final int ADV_TRANQUILITY_ABILITY_CLASS_ID = 128;

}
