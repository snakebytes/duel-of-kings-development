package dok.game.model.constants;

public abstract class CharacterClassIds {

	public static final int SWORD_CHARACTER_CLASS_ID = 0;
	public static final int AXE_CHARACTER_CLASS_ID = 1;
	public static final int SPEAR_CHARACTER_CLASS_ID = 2;
	public static final int STAFF_CHARACTER_CLASS_ID = 3;
	public static final int BOW_CHARACTER_CLASS_ID = 4;
	public static final int CROSSBOW_CHARACTER_CLASS_ID = 5;
	public static final int HANDCANNON_CHARACTER_CLASS_ID = 6;

}
