package dok.game.model.constants;

public class EnhancementClassIds {

	public static final int WARDENS_CODEX_CRYSTAL_ID = 1;
	public static final int AXE_BASH_CRYSTAL_ID = 2;
	public static final int SPEAR_BLOW_CRYSTAL_ID = 3;
	public static final int HEAL_CRYSTAL_ID = 4;
	public static final int CURSE_CRYSTAL_ID = 5;
	public static final int BOMBARDMENT_CRYSTAL_ID = 6;
	public static final int AROUSE_CRYSTAL_ID = 7;

	public static final int TRANQUILITY_CRYSTAL_ID = 101;
	public static final int VETERAN_OF_BATTLES_CRYSTAL_ID = 102;
	public static final int SILENCE_CRYSTAL_ID = 103;
	public static final int CALLING_THE_PACK_CRYSTAL_ID = 104;
	public static final int DAUNTING_DEFENDER_CRYSTAL_ID = 105;
	public static final int ADAPTIVE_BOMB_CRYSTAL_ID = 106;
	public static final int POWER_TRANSFER_CRYSTAL_ID = 107;

	public static final int ADV_TRANQUILITY_CRYSTAL_ID = 201;
	public static final int BREATHTAKING_SILENCE_CRYSTAL_ID = 202;
	public static final int STORM_SPRINT_CRYSTAL_ID = 203;
	public static final int THROW_BOMBS_CRYSTAL_ID = 204;
	public static final int MAGIC_HUNT_CRYSTAL_ID = 205;
	public static final int ENCOURAGE_CRYSTAL_ID = 206;

}
