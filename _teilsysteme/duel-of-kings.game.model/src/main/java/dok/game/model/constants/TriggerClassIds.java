package dok.game.model.constants;

public class TriggerClassIds {

	public static final int WARDENS_CODE_TRIGGER_CLASS_ID = 201;
	public static final int TRANQUILITY_TRIGGER_CLASS_ID = 202;
	public static final int VETERAN_OF_BATTLES_TRIGGER_CLASS_ID = 203;
	public static final int DAUNTING_DEFENDER_TRIGGER_CLASS_ID = 204;
	public static final int MAGIC_HUNT_ADD_TRIGGER_CLASS_ID = 205;
	public static final int MAGIC_HUNT_REMOVE_TRIGGER_CLASS_ID = 206;
	public static final int STORM_SPRINT_TRIGGER_CLASS_ID = 207;
	public static final int ENCOURAGE_TRIGGER_CLASS_ID = 208;
	public static final int ADV_TRANQUILITY_TRIGGER_CLASS_ID = 209;
	public static final int DAUNTING_DEFENDER_REAPPLY_TRIGGER_CLASS_ID = 210;

}
