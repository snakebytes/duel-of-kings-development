package dok.game.model.effects.actions;

import java.util.Map;

import dok.game.model.Ability;
import dok.game.model.ActionEffect;
import dok.game.model.Position;
import dok.game.model.TargetType;

public class PowerTransferEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Constructor(s)

	public PowerTransferEffect(PowerTransferEffect other) {
		super(other);
	}

	public PowerTransferEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}
}