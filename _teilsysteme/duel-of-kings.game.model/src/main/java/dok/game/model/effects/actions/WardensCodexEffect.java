package dok.game.model.effects.actions;

import java.util.Map;

import dok.game.model.Ability;
import dok.game.model.ActionEffect;
import dok.game.model.Position;
import dok.game.model.TargetType;

public class WardensCodexEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Constructor(s)

	public WardensCodexEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	public WardensCodexEffect(WardensCodexEffect other) {
		super(other);
	}

}
