package dok.game.model.effects.reactions;

import java.util.Map;

import dok.game.model.Position;
import dok.game.model.ReactionEffect;
import dok.game.model.TargetType;
import dok.game.model.Trigger;

public class DauntingDefenderReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Constructor(s)

	public DauntingDefenderReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
	}

	public DauntingDefenderReactionEffect(DauntingDefenderReactionEffect other) {
		super(other);
	}

}
