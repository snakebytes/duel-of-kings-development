package dok.game.model.effects.reactions;

import java.util.Map;

import dok.game.model.Position;
import dok.game.model.ReactionEffect;
import dok.game.model.TargetType;
import dok.game.model.Trigger;

public class MagicHuntRemoveReactionEffect extends ReactionEffect {

    // Class Constants

    private static final long serialVersionUID = 1L;

    // Constructor(s)

    public MagicHuntRemoveReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
        super(triggerSource, targets, properties);
    }

    public MagicHuntRemoveReactionEffect(MagicHuntRemoveReactionEffect other) {
        super(other);
    }

}
