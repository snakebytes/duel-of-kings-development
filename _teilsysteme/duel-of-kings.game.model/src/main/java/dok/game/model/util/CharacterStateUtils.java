package dok.game.model.util;

import dok.game.model.CharacterAttribute;
import dok.game.model.api.CharacterDataService;
import dok.game.model.Character;

/**
 * Contains useful convenience methods regarding the current
 * state of a given character.
 *
 * @author Konstantin Schaper
 * @since 0.2.4.1
 */
public abstract class CharacterStateUtils {

	public static final boolean isCharacterDead(Character character, CharacterDataService characterClassService) {
		return character.getDamageTaken() >= getCharacterAttributeValue(character, CharacterAttribute.HEALTH, characterClassService);
	}

	public static final boolean isCharacterAlive(Character character, CharacterDataService characterClassService) {
		return !isCharacterDead(character, characterClassService);
	}

	public static final boolean isCharacterAvailable(Character character, CharacterDataService characterClassService) {
		return isCharacterAlive(character, characterClassService)
				&& character.isReady()
				&& !character.isStunned();
	}

	public static final boolean isCharacterReady(Character character) {
		return character.isReady();
	}

	public static final boolean isCharacterStunned(Character character) {
		return character.isStunned();
	}

	public static final boolean isCharacterSilenced(Character character) {
		return character.isSilenced();
	}

	public static final int getCharacterAttributeValue(Character character, CharacterAttribute attributeType, CharacterDataService characterClassService) {
		return Math.abs(character.getBonusAttributeValue(attributeType)
				+ characterClassService.getCharacterClass(character.getCharacterClassId()).getBaseAttributeValue(attributeType));
	}

	public static final int getCharacterAttackDamage(Character character, CharacterDataService characterClassService) {
		return getCharacterAttributeValue(character, CharacterAttribute.ATTACK_DAMAGE, characterClassService);
	}

}
