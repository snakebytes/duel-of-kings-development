package dok.game.model.util;

import java.lang.reflect.InvocationTargetException;

import dok.game.model.Buff;
import dok.game.model.Effect;

public abstract class GameModelUtils {

    // Copying

    public static Buff cloneBuff(Buff other) throws CloneNotSupportedException {
        try {
            return other
                    .getClass()
                    .getConstructor(other.getClass())
                    .newInstance(other);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            throw new CloneNotSupportedException("Buff " + other + " could not be copied");
        }
    }

    public static Effect cloneEffect(Effect effect) throws CloneNotSupportedException {
        try {
            return effect
            .getClass()
            .getConstructor(effect.getClass())
            .newInstance(effect);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            throw new CloneNotSupportedException("Effect " + effect + " could not be copied");
        }
    }

}
