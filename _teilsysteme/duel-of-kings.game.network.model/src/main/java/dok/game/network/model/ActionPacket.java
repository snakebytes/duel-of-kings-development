package dok.game.network.model;

import dok.commons.network.core.PacketImpl;
import dok.game.model.Action;

public class ActionPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -5437388032024661695L;

	// Attributes

	private final Action action;

	// Constructor(s)

	public ActionPacket(Action action) {
		super();
		this.action = action;

	}

	// Methods

	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "action: " + getAction()
				+ "}";
	}

	// Getter & Setter

	public Action getAction() {
		return action;
	}

}
