package dok.game.network.model;

import java.util.Set;

import dok.commons.network.core.PacketImpl;
import dok.game.model.Position;
import dok.game.model.api.ITargetFilter;

public class CharacterSelectionPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -5437388032024661695L;

	// Attributes

	private final Position selectedPosition;
	private final ITargetFilter filter;
	private final Set<Position> availablePositions;

	// Constructor(s)

	public CharacterSelectionPacket(Position selectedPosition) {
		super();
		this.selectedPosition = selectedPosition;
		this.filter = null;
		this.availablePositions = null;
	}

	public CharacterSelectionPacket(ITargetFilter filter, Set<Position> availablePositions) {
		super();
		this.selectedPosition = null;
		this.filter = filter;
		this.availablePositions = availablePositions;
	}

	// Method

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "selectedPosition: " + getSelectedPosition()
				+ ", filter: " + getFilter()
				+ ", availablePositions: " + getAvailablePositions()
				+ "}";
	}

	// Getter & Setter

	public Position getSelectedPosition() {
		return selectedPosition;
	}

	public ITargetFilter getFilter() {
		return filter;
	}

	public Set<Position> getAvailablePositions() {
		return availablePositions;
	}

}
