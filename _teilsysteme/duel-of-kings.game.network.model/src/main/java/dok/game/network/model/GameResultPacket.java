package dok.game.network.model;

import dok.commons.network.core.PacketImpl;
import dok.game.model.GameResult;


public class GameResultPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = 4113994676740851773L;

	// Attributes

	private GameResult gameResult;

	// Constructor(s)

	public GameResultPacket(GameResult gameResult) {
		super();
		this.gameResult = gameResult;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "gameResult: " + getGameResult()
				+ "}";
	}

	// Getter & Setter

	public GameResult getGameResult() {
		return gameResult;
	}

}
