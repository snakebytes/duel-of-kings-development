package dok.game.network.model;

import java.util.List;

import dok.commons.model.Lineup;
import dok.commons.model.StatusCode;
import dok.commons.network.core.PacketImpl;

public class LineupUpdatePacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -722009548920270367L;

	// Attributes

	private List<Lineup> result;
	private StatusCode statusCode;

	// Constructor(s)

	public LineupUpdatePacket() {
		super();
	}

	public LineupUpdatePacket(StatusCode statusCode, List<Lineup> result) {
		super();
		setResult(result);
		setStatusCode(statusCode);
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "statusCode: " + getStatusCode()
				+ ", result: " + getResult()
				+ "}";
	}

	// Getter & Setter

	public List<Lineup> getResult() {
		return result;
	}

	public void setResult(List<Lineup> result) {
		this.result = result;
	}

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(StatusCode statusCode) {
		this.statusCode = statusCode;
	}

}
