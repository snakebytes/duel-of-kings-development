package dok.game.network.model;

import dok.commons.network.core.PacketImpl;
import dok.game.model.GameStateModel;


public class ModelPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = 5728788066960533415L;

	// Attributes

	private GameStateModel model;

	// Constructor(s)

	public ModelPacket(GameStateModel model) {
		super();
		this.model = model;
	}

	// Methods

	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "model: " + getModel()
				+ "}";
	}

	// Getter & Setter

	public GameStateModel getModel() {
		return model;
	}

}
