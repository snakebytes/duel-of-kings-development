package dok.game.network.model;

import dok.commons.model.constants.ModelConstants;
import dok.commons.network.core.PacketImpl;
import dok.game.model.GameType;


public class QueuingPacket extends PacketImpl {

	// Result Types

	public enum QueuingResult {

		/**
		 * Occurs when the request has been successfully processed.
		 */
		SUCCESSFULLY_LEFT,

		/**
		 * Occurs when the request has been successfully processed.
		 */
		SUCCESSFULLY_ENTERED,

		/**
		 * Occurs when a user, who is not logging in, requests queuing up.
		 */
		UNAUTHORIZED,

		/**
		 * Occurs when an error occurred while processing the request.
		 */
		ERROR,

		/**
		 * Occurs when the desired queue is disabled.
		 */
		DISABLED,

		/**
		 * Occurs when the desired queue's capacity is reached.
		 */
		FULL,

		/**
		 * Occurs when a user may not be queued up, or if a user tries to leave queue when he/she has not been queued up yet
		 */
		REJECTED

	}

	public enum QueuingPacketType {
		JOIN,
		LEAVE,
		RESULT
	}

	// Class Constants

	private static final long serialVersionUID = -1035453777064789745L;

	// Attributes

	private final QueuingPacketType packetType;
	private final QueuingResult result;
	private final GameType gameType;
	private final long lineupId;

	// Constructor(s)

	public QueuingPacket(QueuingPacketType packetType) {
		super();
		this.packetType = packetType;
		this.gameType = null;
		this.result = null;
		this.lineupId = ModelConstants.UNDEFINED_ID;
	}

	public QueuingPacket(QueuingPacketType packetType, QueuingResult result) {
		super();
		this.packetType = packetType;
		this.gameType = null;
		this.result = result;
		this.lineupId = ModelConstants.UNDEFINED_ID;
	}

	public QueuingPacket(QueuingPacketType packetType, GameType gameType, long lineupId) {
		super();
		this.packetType = packetType;
		this.gameType = gameType;
		this.result = null;
		this.lineupId = lineupId;
	}

	public QueuingPacket(QueuingPacketType packetType, GameType gameType, QueuingResult result) {
		super();
		this.packetType = packetType;
		this.gameType = gameType;
		this.result = result;
		this.lineupId = ModelConstants.UNDEFINED_ID;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "packetType: " + getPacketType()
				+ ", gameType: " + getGameType()
				+ ", result: " + getQueuingResult()
				+ ", lineupid: " + getLineupId()
				+ "}";
	}

	// Getter & Setter

	public QueuingResult getQueuingResult() {
		return result;
	}

	public GameType getGameType() {
		return gameType;
	}

	public QueuingPacketType getPacketType() {
		return packetType;
	}

	public long getLineupId() {
		return lineupId;
	}

}
