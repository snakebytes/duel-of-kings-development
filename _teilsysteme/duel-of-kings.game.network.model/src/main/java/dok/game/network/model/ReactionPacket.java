package dok.game.network.model;

import java.util.List;

import dok.commons.network.core.PacketImpl;
import dok.game.model.GameEvent;
import dok.game.model.Reaction;
import dok.game.model.Trigger;

public class ReactionPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -5437388032024661695L;

	// Attributes

	private final Reaction reaction;
	private final List<Trigger> list;
	private final List<GameEvent> gameEvents;

	// Constructor(s)

	public ReactionPacket(Reaction reaction) {
		super();
		this.reaction = reaction;
		this.list = null;
		this.gameEvents = null;
	}


	public ReactionPacket(List<Trigger> list, List<GameEvent> gameEvents) {
		super();
		this.reaction = null;
		this.list = list;
		this.gameEvents = gameEvents;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "reaction: " + getReaction()
				+ ", list: " + getList()
				+ ", gameEvents: " + getGameEvents()
				+ "}";
	}

	// Getter & Setter

	public Reaction getReaction() {
		return reaction;
	}

	public List<Trigger> getList() {
		return list;
	}

	public List<GameEvent> getGameEvents() {
		return gameEvents;
	}

}
