package dok.game.network.model;

import dok.commons.model.Lineup;
import dok.commons.network.core.PacketImpl;


public class SaveLineupPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = 8771771689041746528L;

	// Attributes

	private Lineup lineup;

	// Constructor(s)

	public SaveLineupPacket() {
		super();
	}

	public SaveLineupPacket(Lineup lineup) {
		super();
		setLineup(lineup);
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "lineup: " + getLineup().toString()
				+ "}";
	}

	// Getter & Setter

	public Lineup getLineup() {
		return lineup;
	}

	public void setLineup(Lineup lineup) {
		this.lineup = lineup;
	}

}
