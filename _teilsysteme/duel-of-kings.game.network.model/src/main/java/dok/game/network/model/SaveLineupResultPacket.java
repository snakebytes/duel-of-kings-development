package dok.game.network.model;

import dok.commons.model.Lineup;
import dok.commons.model.StatusCode;
import dok.commons.network.core.PacketImpl;

public class SaveLineupResultPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -4927652668229856572L;

	// Attributes

	private final StatusCode statusCode;
	private final Lineup lineup;

	// Constructor(s)

	public SaveLineupResultPacket(StatusCode statusCode) {
		super();
		this.statusCode = statusCode;
		this.lineup = null;
	}

	public SaveLineupResultPacket(StatusCode statusCode, Lineup lineup) {
		super();
		this.statusCode = statusCode;
		this.lineup = lineup;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "statusCode: " + getStatusCode()
				+ ", lineup: " + getLineup()
				+ "}";
	}

	// Getter & Setter

	public StatusCode getStatusCode() {
		return statusCode;
	}

	public Lineup getLineup() {
		return lineup;
	}

}
