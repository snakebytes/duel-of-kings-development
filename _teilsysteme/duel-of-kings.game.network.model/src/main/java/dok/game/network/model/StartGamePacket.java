package dok.game.network.model;

import java.util.List;

import dok.commons.network.core.PacketImpl;
import dok.game.model.GameType;
import dok.game.model.Player;

public class StartGamePacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -3417593553392858624L;

	// Attributes

	private final GameType gameType;
	private final List<Player> players;
	private final Player player;

	// Constructor(s)

	public StartGamePacket(GameType gameType, List<Player> players, Player player) {
		super();
		this.gameType = gameType;
		this.players = players;
		this.player = player;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "gameType: " + getGameType()
				+ ", players: " + getPlayers()
				+ ", player: " + getPlayer()
				+ "}";
	}

	// Getter & Setter

	public GameType getGameType() {
		return gameType;
	}

	public Player getPlayer() {
		return player;
	}

	public List<Player> getPlayers() {
		return players;
	}

}
