package dok.game.network.model;


import dok.commons.network.core.PacketImpl;
import dok.game.model.SwapRequest;

public class SwapRequestPacket extends PacketImpl {

	// Class Constants

	private static final long serialVersionUID = -5437388032024661695L;

	// Attributes

	private final SwapRequest swapRequest;

	// Constructor(s)

	public SwapRequestPacket(SwapRequest swapRequest) {
		super();
		this.swapRequest = swapRequest;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "swapRequest: " + getSwapRequest()
				+ "}";
	}

	// Getter & Setter

	public SwapRequest getSwapRequest() {
		return swapRequest;
	}

}
