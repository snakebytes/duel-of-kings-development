package dok.game.services;

import dok.game.model.Buff;

public interface BuffLogicService {

	boolean onBuffAdd(Buff buff, GameStateModelService gameStateModelService);
	boolean onBuffRemove(Buff buff, GameStateModelService gameStateModelService);

}