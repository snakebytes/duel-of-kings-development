package dok.game.services;

import dok.game.model.Effect;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;

/**
 * Responsible for apply changes made by effects to the underlying game model.
 *
 * @author Konstantin Schaper
 * @since 0.2.4.1
 */
public interface EffectExecutionService {

	/**
	 * Resolves the given effect against the given game state model.
	 *
	 * @param effect The effect to resolve
	 * @param gameStateModelService The service to approach for model modifications
	 * @param characterDataService The service to query for character data
	 * @param abilityDataService The service to query for ability and trigger data
	 */
	public void execute(Effect effect, GameStateModelService gameStateModelService, CharacterDataService characterDataService, AbilityDataService abilityDataService);

}
