package dok.game.services;

import dok.game.model.GameEvent;

public interface GameEventHandler {

	public void handleEvent(GameEvent sourceEvent);

}
