package dok.game.services;

import dok.game.model.Ability;
import dok.game.model.Buff;
import dok.game.model.GameEvent;
import dok.game.model.GameStateModel;
import dok.game.model.Trigger;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;

public interface GameEventTriggerService {

	public default Trigger generateEventTriggers(Ability ability, GameEvent sourceEvent, GameStateModel model,
												 CharacterDataService characterClassService,
												 AbilityDataService abilityClassService) {return null;}

	public default Trigger generateEventTriggers(Buff buff, GameEvent sourceEvent, GameStateModel model,
												 CharacterDataService characterClassService,
												 AbilityDataService abilityClassService) {return null;}

}
