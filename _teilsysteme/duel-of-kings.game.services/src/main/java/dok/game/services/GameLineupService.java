package dok.game.services;

import java.util.UUID;

import dok.commons.model.Lineup;

public interface GameLineupService {

	public Lineup getLineup(UUID player);

}
