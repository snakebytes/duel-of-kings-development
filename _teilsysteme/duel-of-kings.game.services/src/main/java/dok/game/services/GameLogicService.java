package dok.game.services;

import java.util.UUID;

import dok.game.model.Action;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;

/**
 * Allows the Player to interact with the GameLogic and vice-versa.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface GameLogicService {

	public void handleReaction(UUID sourcePlayerId, Reaction reaction);

	public void handleAction(UUID sourcePlayerId, Action action);

	public void handleCharacterChoosen(UUID sourcePlayerId, Position pos);

	public void handleSwapRequest(UUID sourcePlayerId, SwapRequest swapRequest);

	public void handleSurrenderRequest(UUID sourcePlayerId);

}
