package dok.game.services;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import dok.game.model.GameEvent;
import dok.game.model.GameEventType;
import dok.game.model.GameResult;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.SwapRequest;
import dok.game.model.Trigger;
import dok.game.model.api.ITargetFilter;

/**
 * The GameLogic must be able to contact the {@linkplain PlayerInteractionService}s
 * about tasks they should perform or input they should handle.<br>
 * <br>
 * This interface realizes this necessity. The implementing class, directly or indirectly,
 * forwards the request to the {@linkplain PlayerInteractionWrapper}
 * which itself forwards it directly to the actual {@linkplain PlayerInteractionService}
 * provider.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface GameLogicWrapper {

	// Tasks

	/**
	 * Called by {@linkplain GameLogic_Initialization} and should be
	 * forwarded to the specified player's PlayerStateMachineService provider's
	 * {@linkplain #placeCharacters(long)} method.<br>
	 *
	 * @param player which's PlayerStateMachineSerivce provider to forward the task to
	 * @param timeLeft given the player to finish the task (in milliseconds)
	 *
	 * @see PlayerService
	 */
	public void placeCharacters(UUID player);

	/**
	 * Called by {@linkplain GameLogic_TimeMarkerPlacing} and should be
	 * forwarded to the specified player's PlayerStateMachineService provider's
	 * {@linkplain #chooseCharacter(ITargetFilter, long)} method.<br>
	 *
	 * @param filter to restrict the player in terms of selection
	 * @param player which's PlayerStateMachineSerivce provider to forward the task to
	 * @param availablePositions Positions to choose from
	 * @param timeLeft given the player to finish the task (in milliseconds)
	 *
	 * @see PlayerService
	 */
	public void chooseCharacter(UUID player, ITargetFilter filter, Set<Position> availablePositions);

	/**
	 * Called by {@linkplain GameLogic_Turn} and should be
	 * forwarded to the specified player's PlayerStateMachineService provider's
	 * {@linkplain #performCharacterAction(long)} method.<br>
	 *
	 * @param player which's PlayerStateMachineSerivce provider to forward the task to
	 * @see PlayerService
	 */
	public void performCharacterAction(UUID player);

	/**
	 * Called by {@linkplain GameLogic_Turn} and should be
	 * forwarded to the specified player's
	 * {@linkplain PlayerInteractionService#performReaction(long, List)}
	 * method.<br>
	 *
	 * @param player which's PlayerStateMachineSerivce provider to forward the task to
	 * @param list to choose from
	 * @param eventType The {@link GameEventType} the reaction refers to
	 * @param parameters of the event
	 *
	 * @see PlayerService
	 */
	public void performReaction(UUID player, List<Trigger> list, List<GameEvent> gameEvents);

	// GameStateModel operations

	/**
	 * Called by the GameLogic and should be forwarded to the
	 * specified player's PlayerStateMachineService provider's
	 * {@link #performModelUpdate(GameStateModelAPI)} method.<br>
	 *
	 * @param player which's PlayerStateMachineSerivce provider to forward the task to
	 * @param gameStateModelService to replace the {@linkplain GameStateModelAPI} of the corresponding {@linkplain GameStateModelService} with
	 *
	 * @see PlayerService
	 * @deprecated Replaced by {@link #handleModelUpdate(GameStateModelAPI)}
	 */
	public void handleModelUpdate(UUID player, GameStateModel gameStateModelService);

	/**
	 * Called by the GameLogic to update all players' models
	 * simultaneously. Does block until all models have been updated.
	 * @param gameStateModel
	 */
	public boolean handleModelUpdate(GameStateModel gameStateModel);

	/**
	 * Called by the GameLogic and should be forwarded to the
	 * specified player's PlayerStateMachineService provider's
	 * {@link #performSwapRequest(SwapRequest)} method.<br>
	 *
	 * @param player which's PlayerStateMachineSerivce provider to forward the task to
	 * @param swapRequest to perform
	 *
	 * @see PlayerService
	 */
	public void handleSwapRequest(UUID player, SwapRequest swapRequest);

	/**
	 * Called by the GameLogic and should be forwarded to the
	 * specified player's PlayerStateMachineService provider's
	 * {@link #handleGameResult(GameResult)} method.<br>
	 */
	public void handleGameResult(UUID player, GameResult result);

}
