package dok.game.services;

import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang3.tuple.Pair;

import dok.commons.impl.CustomTimer;
import dok.game.model.Ability;
import dok.game.model.Buff;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterEnhancementClass;
import dok.game.model.Damage;
import dok.game.model.Effect;
import dok.game.model.GameEvent;
import dok.game.model.GamePhase;
import dok.game.model.GameStateModel;
import dok.game.model.Healing;
import dok.game.model.Position;
import dok.game.model.api.CharacterDataService;

public abstract class GameStateModelService extends GameStateModel {

	// Class Constants

	private static final long serialVersionUID = 5320158523432978782L;

	// Constructor(s)

	public GameStateModelService() {
		super();
	}

	protected GameStateModelService(GameStateModel other) {
		super(other);
	}

	// Fields

	@Override
	public void setField(UUID player, HashMap<Pair<Integer, Integer>, Character> field) {
		super.setField(player, field);
	}

	// Effects

	public abstract void addRound();
	public abstract void applyDamage(Damage damage, CharacterDataService characterClassService);
	public abstract void applyHealing(Healing healing, CharacterDataService characterClassService);
	public abstract void addBuff(Buff buff);
	public abstract void reduceBuffDuration(Buff buff);
	public abstract void modifyStunCounters(Effect source, UUID characterid, int amount);
	public abstract void modifyStunCounters(Effect source, Position pos, int amount);
	public abstract void removeStunCounter(Effect source, UUID characterid);
	public abstract void toggleDamageTaken(UUID characterId);
	public abstract void removeBuff(UUID characterTarget, Class<? extends Buff> buffClass);
	public abstract void swap(Effect sourceEffect, UUID source, Position target);
	public abstract void swap(Effect sourceEffect, Position p1, Position p2);
	public abstract void modifyBonusAttribute(UUID character, CharacterAttribute attribute, int amount);
	public abstract void modifyTimeCounters(UUID id, int amount);
	public abstract void modifyTimeCounters(Position pos, int amount);
	public abstract void modifyTimeCounters(Ability abil, int amount);
	public abstract void reduceTimeCounters(Character character);
	public abstract void reduceTimeCounters(Ability ability);
	public abstract void addToEffectHistory(Effect effect);

	// Players

	@Override
	public void setStartingPlayer(UUID playerId) {
		super.setStartingPlayer(playerId);
	}

	@Override
	public void setActivePlayer(UUID playerId) {
		super.setActivePlayer(playerId);
	}

	@Override
	public void setPriorityPlayer(UUID playerId) {
		super.setPriorityPlayer(playerId);
	}

	// General game manipulation

	@Override
	public void setPerformedActionsCount(int performedActions) {
		super.setPerformedActionsCount(performedActions);
	}

	@Override
	public void setCurrentGamePhase(GamePhase gamePhase) {
		super.setCurrentGamePhase(gamePhase);
	}

	public abstract void overrideWith(GameStateModel other);
	public abstract void setCharacterWasAvailableAtTurnStart(UUID characterId, boolean flag);
	public abstract void setCharacterPerformedAction(UUID characterId, boolean b);
	public abstract void reduceRemainingFreeSwaps();
	public abstract void resetRemainingFreeSwaps();
	public abstract void fireEvent(GameEvent event);

	/**
	 * Activates the given enhancement on the given character by
	 * applying the enhancement's bonuses and mali as well as
	 * adding the enhancement's abilities.
	 *
	 * @param sourceEffect The source of the enhancement activation, may be null
	 * @param activatingUpgradeRank The rank of the character on activation
	 * @param characterId The character who to activate the enhancement on
	 * @param enhancement The type of the enhancement to activate
	 */
	public abstract void activateEnhancement(Effect sourceEffect, int activatingUpgradeRank, UUID characterId, CharacterEnhancementClass enhancement);

	/**
	 *
	 * @param sourceEffect
	 * @param characterId
	 * @param enhancement
	 */
	public abstract void deactivateEnhancement(Effect sourceEffect, UUID characterId, CharacterEnhancementClass enhancement);

	// Event Handling

	public abstract boolean addEventHandler(GameEventHandler handler);
	public abstract boolean removeEventHandler(GameEventHandler handler);

	// Timers

	@Override
	public void setChooseTimer(CustomTimer chooseTimer) {
		super.setChooseTimer(chooseTimer);
	}

	@Override
	public void setTurnTimer(CustomTimer turnTimer) {
		super.setTurnTimer(turnTimer);
	}

}
