package dok.game.services;

import dok.game.model.GameStateModel;
import dok.game.model.HealthModification;
import dok.game.model.api.CharacterDataService;

public interface HealthModificationCalculationService<T extends HealthModification> {

	public int getCalculatedAmount(T modification, GameStateModel model, CharacterDataService characterClassService);

}
