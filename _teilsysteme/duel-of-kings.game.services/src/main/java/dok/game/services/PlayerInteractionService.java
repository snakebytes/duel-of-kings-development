package dok.game.services;

import java.util.List;
import java.util.Set;

import dok.game.model.Action;
import dok.game.model.ActionEffect;
import dok.game.model.GameEvent;
import dok.game.model.GameResult;
import dok.game.model.GameStateModel;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;
import dok.game.model.Trigger;
import dok.game.model.api.ITargetFilter;

/**
 * Allows the GameLogic to interact with the players
 * within a game and vice-versa.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 *
 * @see GameStateModelService
 * @see GameLogicService
 */
public interface PlayerInteractionService {

	// Tasks

	/**
	 * Should allow the specified player to perform any number of free swaps
	 * before the given time runs out or no more swaps are desired.<br>
	 * <br>
	 * The resulting {@linkplain SwapRequest}s, or null if no swaps are desired,
	 * should be sent back to the GameLogic by calling the responsible
	 * {@linkplain GameLogicService}'s
	 * {@linkplain GameLogicService#handleSwapRequest(SwapRequest)}
	 * method.<br>
	 *
	 * @param timeLeft given the player to finish the task (in milliseconds)
	 * @throws IllegalStateException If the service provider is not in a state that can handle the request
	 */
	public void placeCharacters() throws IllegalStateException;

	/**
	 * Should request the player to perform a valid character selection concerning
	 * the given filter or let him refuse making a selection.<br>
	 * <br>
	 * The resulting {@linkplain Position}, or null if no selection is desired,
	 * should be sent back to the GameLogic by calling the responsible
	 * {@linkplain PlayerInteractionWrapper#handleCharacterChoosen(Position)}
	 * method.<br>
	 * <br>
	 * This method will only be invoked within the TimeMarkerPlacingPhase during initialization of the game.
	 *
	 * @param filter to restrict the player in terms of selection
	 * @param availablePositions positions to choose from
	 * @throws IllegalStateException If the service provider is not in a state that can handle the request
	 */
	public void performCharacterMarkerPlacement(ITargetFilter filter, Set<Position> availablePositions) throws IllegalStateException;

	/**
	 * Should request the player to select a single character's ability as one of
	 * the actions he wants to perform during his turn.<br>
	 * <br>
	 * The resulting {@linkplain Action} should be sent back to the GameLogic by
	 * calling the responsible {@linkplain GameLogicService}'s
	 * {@linkplain GameLogicService#handleAction(Action)}
	 * method.<br>
	 * <br>
	 * If no action is wanted, null should be sent back and the game logic
	 * will interpret this as the desire to end the turn without any further actions.<br>
	 *
	 * @throws IllegalStateException If the service provider is not in a state that can handle the request
	 */
	public void performAction() throws IllegalStateException;

	/**
	 * Should allow the player to generate any number of {@linkplain Reaction}s, one at a time,
	 * by first choosing one of the triggers given by the possibleReactions parameter
	 * and then choosing a valid target for that ability afterwards (if it requires any).<br>
	 * <br>
	 * The resulting {@linkplain Reaction}s should be sent back to the GameLogic by
	 * calling the responsible {@linkplain GameLogicService}'s
	 * {@linkplain GameLogicService#handleReaction(Reaction)}
	 * method.<br>
	 * <br>
	 * If no (further) reaction is wanted, null should be sent back and the game logic
	 * will interpret this as passing priority.<br>
	 * <br>
	 * The game logic will put all valid Reactions' {@linkplain ActionEffect}s on the EffectStack
	 * as they arrive so no Reaction is lost when sending null after first sending some Reactions.
	 * Invalid Reactions will be discarded and the player is not going to be informed.<br>
	 *
	 * @param timeLeft given the player to finish the task (in milliseconds)
	 * @param possibleReactions to choose from
	 * @param gameEvents a list of all game events to which to react
	 * @throws IllegalStateException If the service provider is not in a state that can handle the request
	 */
	public void performReaction(List<Trigger> possibleReactions, List<GameEvent> gameEvents) throws IllegalStateException;

	// GameStateModel operations

	/**
	 * Should override the {@linkplain GameStateModelAPI} of the corresponding
	 * {@linkplain GameStateModelService} with the given newModel.<br>
	 * <br>
	 * This method should be able to be called in any state and must be immediately
	 * dealt with.<br>
	 *
	 * @param newModel to replace the {@linkplain GameStateModelAPI} of the corresponding {@linkplain GameStateModelService} with
	 */
	public void handleModelUpdate(GameStateModel newModel);

	/**
	 * Should perform the given SwapRequest's swap against the {@linkplain GameStateModelAPI}
	 * of the corresponding {@linkplain GameStateModelService}.<br>
	 *
	 * @param swapRequest to perform
	 * @see GameStateModelService
	 */
	public void handleSwapRequest(SwapRequest swapRequest);

	/**
	 * Called when the Game has ended and the result has to be
	 * computed.<br>
	 * <br>
	 * This method should be able to be called in any state and must be immediately
	 * dealt with.<br>
	 *
	 * @param result
	 */
	public void handleGameResult(GameResult result);

}
