package dok.game.services;

import dok.game.model.Action;
import dok.game.model.Position;
import dok.game.model.Reaction;
import dok.game.model.SwapRequest;

/**
 * The Player must be able to contact the {@linkplain GameLogicService}s
 * about the result of tasks he/she performed.<br>
 * <br>
 * This interface realizes this necessity. The implementing class, directly or indirectly,
 * forwards the request to the {@linkplain GameLogicWrapper}
 * which itself forwards it directly to the actual {@linkplain GameLogicService}
 * provider.
 *
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public interface PlayerInteractionWrapper {

	public void handleReaction(Reaction reaction);

	public void handleAction(Action action);

	public void handleCharacterChoosen(Position pos);

	public void handleSwapRequest(SwapRequest swapRequest);

	public void handleSurrenderRequest();

	public void handleWorkDone();

}
