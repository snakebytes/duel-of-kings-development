package dok.game.services;

import java.util.List;
import java.util.UUID;

import dok.game.model.Player;

public interface PlayerService {

	/**
	 * @return A read-only list of all players in the corresponding game.
	 */
	public List<Player> getPlayers();
	public List<UUID> getPlayerIds();
	public Player getPlayer(UUID id);
	public void setPlayerPosition(UUID player, int position);
	public void shufflePlayers();

	public default Player getFollowingPlayer(UUID playerid) {
		int index = getPlayerIds().indexOf(playerid);
		if (index + 1 >= getPlayers().size()) return getPlayers().get(0);
		else return getPlayers().get(index + 1);
	}

	public default UUID getFollowingPlayerId(UUID playerid) {
		return getFollowingPlayer(playerid).getPlayerId();
	}

	public default Player getNextPlayer(UUID playerid) {
		int index = getPlayerIds().indexOf(playerid);
		if (index + 1 >= getPlayers().size()) return null;
		else return getPlayers().get(index + 1);
	}

	public default UUID getNextPlayerId(UUID playerid) {
		try {
			return getNextPlayer(playerid).getPlayerId();
		} catch (NullPointerException e) {
			return null;
		}
	}

}
