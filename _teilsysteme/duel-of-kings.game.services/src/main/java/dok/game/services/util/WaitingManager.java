package dok.game.services.util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * Utility class for waiting on players while they perform an asynchronous task.
 *
 * @author Konstantin Schaper
 * @since 0.1.0.8
 */
public class WaitingManager {

	// Attributes

	private final Map<UUID, Boolean> waitingFlags = new HashMap<>();
	private volatile boolean interrupted = false;

	// Methods

	public void startWaitingOn(UUID playerid) {
		getWaitingFlags().put(playerid, true);
	}

	public void stopWaitingOn(UUID playerid) {
		getWaitingFlags().put(playerid, false);
	}

	public int getWaitingCount() {
		int result = 0;
		for (boolean flag : waitingFlags.values()) if (flag) result++;
		return result;
	}

	/**
	 * Blocks until there is no player to wait on anymore.
	 *
	 * @return Whether the waiting manager has not been interrupted during the wait
	 */
	public boolean waitForAll() {
		try {
			return waitForAll(-1);
		} catch (TimeoutException e) {
			return false;
		}
	}

	/**
	 * Blocks until there is no player to wait on anymore
	 * or until the given timeout expired.
	 *
	 * @param timeout The time (in ms) to wait at max, values less or equal to zero result in no timeout at all
	 * @throws TimeoutException Thrown when the waiting-process timed out
	 */
	public boolean waitForAll(double timeout) throws TimeoutException {
		final long timestamp = System.nanoTime();

		// Wait for work to be finished
    	while (isWaiting())
    		if (Thread.interrupted() || isInterrupted())
    			return false;
    		else if (timeout > 0 && (timestamp - System.nanoTime()) > (timeout * 1000000))
    			throw new TimeoutException();

    	// Waiting finished without problem
    	return true;
	}

	// Getter & Setter

	protected final Map<UUID, Boolean> getWaitingFlags() {
		return waitingFlags;
	}

	public boolean isWaiting() {
		for (boolean waiting : getWaitingFlags().values()) if (waiting) return true;
		return false;
	}

	public boolean isInterrupted() {
		return interrupted;
	}

	public void interrupt() {
		interrupted = true;
	}

}
