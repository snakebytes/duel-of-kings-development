package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.CharacterAttribute;
import dok.game.model.DamageType;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.model.impl.DamageImpl;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class AdaptiveBombEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public AdaptiveBombEffect(AdaptiveBombEffect other) {
		super(other);
	}

	public AdaptiveBombEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);
		int dmg = 0;
		// Find Primary target and calculate damage from its armor
		for (Position pos : getTargetedPositions().keySet()) {
			if(getTargetedPositions().get(pos).equals(TargetType.PRIMARY)) {
				dmg = gameStateModelService.getCharacterAt(pos).getAttributeValue(CharacterAttribute.ARMOR, characterClassService) + getProperties().get("dmgBonus").intValue();
			}
		}
		// Apply modified damage to all targets
		for (Position pos : getTargetedPositions().keySet()) {
			int dmgt = dmg;
			if (getTargetedPositions().get(pos) == TargetType.SECONDARY) {
				logger.debug("TARGET IS SECONDARY, damage was: " + dmg);
				dmgt = (int) Math.round((double) dmg * getProperties().get("secondaryDamageMultiplier").doubleValue());
				logger.debug("TARGET IS SECONDARY, damage is: " + dmgt);
			}
			gameStateModelService.applyDamage(new DamageImpl(dmgt, this, pos,
					 getTargetedPositions().get(pos), DamageType.PHYSICAL), characterClassService);
		}
		logger.exit();
	}
}
