package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.GameConstants;
import dok.game.model.Character;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class ArouseEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public ArouseEffect(ArouseEffect other) {
		super(other);
	}

	public ArouseEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);
		for (Position pos : getTargetedPositions().keySet()) {
			Character target = gameStateModelService.getCharacterAt(pos);
			if (target.isStunned()) {
				for(int i = 0; i < getProperties().get("stunShortenAmount").intValue(); i++) {
					gameStateModelService.reduceBuffDuration(target.getBuff(GameConstants.STUN_BUFF_CLASS));
				}
			} else {
				gameStateModelService.modifyStunCounters(this, target.getId(), -getProperties().get("stunCountersAmount").intValue());
			}
		}
		logger.exit();
	}

}
