package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class AxeBashEffect extends AttackEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public AxeBashEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	public AxeBashEffect(AxeBashEffect other) {
		super(other);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);

		// Iterate over targets
		for (Position target : getTargetedPositions().keySet()) {

			// Add stun counter to all targets
			gameStateModelService.modifyStunCounters(this, gameStateModelService.getCharacterIdFromPosition(target), getProperties().get("stunCountersAmount").intValue());

		}

		logger.exit();
	}

}
