package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.DamageType;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.model.impl.DamageImpl;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class BombardmentEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public BombardmentEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	public BombardmentEffect(BombardmentEffect other) {
		super(other);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);
		for (Position target : getTargetedPositions().keySet())
			gameStateModelService.applyDamage(new DamageImpl(getProperties().get("damageAmount").intValue(), this, target,
															 getTargetedPositions().get(target), DamageType.PHYSICAL), characterClassService);
		logger.exit();
	}

}
