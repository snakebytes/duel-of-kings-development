package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.DamageType;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.model.impl.DamageImpl;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class CallingPackEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public CallingPackEffect(CallingPackEffect other) {
		super(other);
	}

	public CallingPackEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);
		for (Position pos : getTargetedPositions().keySet()) {
			int d = getProperties().get("dmgToUndamaged").intValue();
			if(gameStateModelService.getCharacterAt(pos).getDamageTaken() > 0) {
				d = getProperties().get("dmgToDamaged").intValue();
			}
			gameStateModelService.applyDamage(new DamageImpl(d, this, pos,
					 getTargetedPositions().get(pos), DamageType.PHYSICAL), characterClassService);
		}
		logger.exit();
	}
}
