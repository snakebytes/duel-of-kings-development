package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.CurseBuff;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class CurseEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public CurseEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	public CurseEffect(CurseEffect other) {
		super(other);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);
		for (Position target : getTargetedPositions().keySet()) {
				gameStateModelService.addBuff(new CurseBuff(
						gameStateModelService.getActivePlayer(),
						this,
						gameStateModelService.getCharacterIdFromPosition(target),
						getProperties().get("duration").intValue(),
						getProperties().get("attackDamageMalus").intValue()));
		}
		logger.exit();
	}
}
