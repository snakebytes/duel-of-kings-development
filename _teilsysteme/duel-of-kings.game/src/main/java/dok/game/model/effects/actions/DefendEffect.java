package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.DefendBuff;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class DefendEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public DefendEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	public DefendEffect(DefendEffect other) {
		super(other);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		gameStateModelService.modifyTimeCounters(getSourceAbility().getOwningCharacterId(), 1);
        gameStateModelService.modifyTimeCounters(getSourceAbility(), abilityClassService.getAbilityClass(getSourceAbility().getAbilityClassId()).getTimeCountersCost());

		gameStateModelService.addBuff(new DefendBuff(
				gameStateModelService.getActivePlayer(),
				this,
				getSourceAbility().getOwningCharacterId(),
				getProperties().get("duration").intValue(),
				getProperties().get("armorBonus").intValue(),
				getProperties().get("magicResistanceBonus").intValue()));

		logger.exit();
	}

}
