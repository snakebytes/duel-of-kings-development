package dok.game.model.effects.actions;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.TransferBuff;
import dok.game.model.buffs.TransferDebuff;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class PowerTransferEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public PowerTransferEffect(PowerTransferEffect other) {
		super(other);
	}

	public PowerTransferEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);
		for(Position pos : getTargetedPositions().keySet()) {
			if(gameStateModelService.getCharacterAt(pos).isAlive(characterClassService)) {
				gameStateModelService.addBuff(new TransferDebuff(gameStateModelService.getActivePlayer(), this, gameStateModelService.getCharacterIdFromPosition(pos), getProperties().get("duration").intValue(), getProperties().get("atkValue").intValue()));
				Position buffpos = new Position(pos.getPlayer(), pos.getRow() + 1, pos.getCol());
				UUID charaId = gameStateModelService.getCharacterIdFromPosition(buffpos);
				if(gameStateModelService.getCharacter(charaId).isAlive(characterClassService)) {
					gameStateModelService.addBuff(new TransferBuff(gameStateModelService.getActivePlayer(), this, gameStateModelService.getCharacterIdFromPosition(buffpos), getProperties().get("duration").intValue(), getProperties().get("atkValue").intValue()));
				}
			}
		}
		logger.exit();
	}
}