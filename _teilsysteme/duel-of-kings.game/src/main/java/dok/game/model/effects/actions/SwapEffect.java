package dok.game.model.effects.actions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ActionEffect;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class SwapEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public SwapEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	public SwapEffect(SwapEffect other) {
		super(other);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);

		// Handle the "Swap"-Ruling
		if (gameStateModelService.getRemainingFreeSwaps() <= 0)
			gameStateModelService.modifyTimeCounters(getSourceAbility().getOwningCharacterId(), 1);
		else
			gameStateModelService.reduceRemainingFreeSwaps();

		// Perform the swap
		gameStateModelService.swap(this, getSourceAbility().getOwningCharacterId(), getTargetedPositions().keySet().iterator().next());
		logger.exit();
	}

}
