package dok.game.model.effects.actions;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.GameConstants;
import dok.game.model.Character;
import dok.game.model.DamageType;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.AbilityClass;
import dok.game.model.impl.ActionEffect;
import dok.game.model.impl.DamageImpl;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class ThrowBombsEffect extends ActionEffect {

	// Class Constants

	private static final long serialVersionUID = 1L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public ThrowBombsEffect(ThrowBombsEffect other) {
		super(other);
	}

	public ThrowBombsEffect(Ability source, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(source, targets, properties);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		super.resolve(gameStateModelService, characterClassService, abilityClassService);

		List<Character> list = gameStateModelService.getCharacters().stream()
				.filter((character) -> character.isAlive(characterClassService) &&
						!character.getOwningPlayerId().equals(gameStateModelService.getCharacter(getSourceCharacterId()).getOwningPlayerId()))
				.collect(Collectors.toList());
		Collections.shuffle(list);

		for(int i = 0; i < getProperties().get("bombAmount").intValue(); i++) {
			if (list.size() == 0) break;
			Character target = list.remove(0);

			// Simulate Attack against target
			Character self = gameStateModelService.getCharacter(getSourceCharacterId());
			Ability ab = self.getAbility(GameConstants.ATTACK_ABILITY_CLASS_ID);
			AbilityClass ac = abilityClassService.getAbilityClass(GameConstants.ATTACK_ABILITY_CLASS_ID);
	        HashSet<Position> targets = new HashSet<Position>();
	        targets.add(target.getPosition());
	        Map<Position, TargetType> hm = ac.getHitFilter().generateTargets(ab, targets, gameStateModelService, characterClassService, abilityClassService);
	        abilityClassService.getAbilityClass(GameConstants.ATTACK_ABILITY_CLASS_ID).getProperties();
			AttackEffect ae = new AttackEffect(ab, hm, abilityClassService.getAbilityClass(GameConstants.ATTACK_ABILITY_CLASS_ID).getProperties());

			// Apply damage manually to all targets that would be hit by an attack
			for (Position pos : ae.getTargetedPositions().keySet()) {
				int dmg = getProperties().get("bombDamage").intValue();
				if (getTargetedPositions().get(pos) == TargetType.SECONDARY) {
					logger.debug("TARGET IS SECONDARY, damage was: " + dmg);
					dmg = (int) Math.round((double) dmg * getProperties().get("secondaryDamageMultiplier").doubleValue());
					logger.debug("TARGET IS SECONDARY, damage is: " + dmg);
				}
				gameStateModelService.applyDamage(new DamageImpl(dmg, this, pos, getTargetedPositions().get(pos), DamageType.PHYSICAL), characterClassService);
			}
		}
		logger.exit();
	}
}
