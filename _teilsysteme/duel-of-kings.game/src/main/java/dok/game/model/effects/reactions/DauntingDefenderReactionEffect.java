package dok.game.model.effects.reactions;

import java.util.HashSet;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.GameConstants;
import dok.game.GameLogicUtils;
import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.DauntingDefenderBuff;
import dok.game.model.impl.Ability;
import dok.game.model.impl.AbilityClass;
import dok.game.model.impl.DamageImpl;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class DauntingDefenderReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public DauntingDefenderReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
		logger.entry(targets,properties);
		logger.exit(this);
	}

	public DauntingDefenderReactionEffect(DauntingDefenderReactionEffect other) {
		super(other);
		logger.entry(other);
		logger.exit(this);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		if(gameStateModelService.getCharacter(getSourceCharacterId()).hasBuff(DauntingDefenderBuff.class)) {
			gameStateModelService.removeBuff(getSourceCharacterId(), DauntingDefenderBuff.class);

			Character target = gameStateModelService.getCharacter(getTrigger().getSourceEvent().getSourceEffect().getSourceCharacterId());
			target = GameLogicUtils.getFirstAliveOfCol(target.getPosition().getCol(), target.getOwningPlayerId(), gameStateModelService, characterClassService);

			Character self = gameStateModelService.getCharacter(getSourceCharacterId());
			Ability ab = self.getAbility(GameConstants.ATTACK_ABILITY_CLASS_ID);
			AbilityClass ac = abilityClassService.getAbilityClass(GameConstants.ATTACK_ABILITY_CLASS_ID);
	        HashSet<Position> targets = new HashSet<Position>();
	        targets.add(target.getPosition());
	        Map<Position, TargetType> hm = ac.getHitFilter().generateTargets(ab, targets, gameStateModelService, characterClassService, abilityClassService);

	        for (Position t : hm.keySet()) {

				// Get the damage to be dealt
				int amount = self.getAttributeValue(CharacterAttribute.ATTACK_DAMAGE, characterClassService);

				// Manipulate the damage if the target is secondary
				if (hm.get(t) == TargetType.SECONDARY) {
					logger.debug("TARGET IS SECONDARY, damage was: " + amount);
					amount = (int) Math.round((double) amount * getProperties().get("secondaryDamageMultiplier").doubleValue());
					logger.debug("TARGET IS SECONDARY, damage is: " + amount);
				}

				// Let the model service apply the damage
				gameStateModelService.applyDamage(new DamageImpl(amount,this,t,getTargetedPositions().get(t),
						gameStateModelService.getCharacter(getSourceAbility().getOwningCharacterId()).getDamageType()), characterClassService);
			}
		}
		logger.exit();
	}

}
