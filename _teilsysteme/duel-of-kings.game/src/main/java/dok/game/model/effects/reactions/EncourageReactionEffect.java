package dok.game.model.effects.reactions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.Character;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class EncourageReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public EncourageReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
		logger.entry(targets,properties);
		logger.exit(this);
	}

	public EncourageReactionEffect(EncourageReactionEffect other) {
		super(other);
		logger.entry(other);
		logger.exit(this);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		Character source = gameStateModelService.getCharacter(getSourceCharacterId());
		if(source.getPosition().getCol() != 0) {
			Character c = gameStateModelService.getCharacterAt(new Position(source.getOwningPlayerId(), source.getPosition().getRow(), source.getPosition().getCol()-1));
			if(c.isAlive(characterClassService)) {
				for(int i = 0; i < getProperties().get("timeMarkersAmt").intValue(); i++) {
					gameStateModelService.reduceTimeCounters(c);
				}
			}
		}
		if(source.getPosition().getCol() != 2) {
			Character c = gameStateModelService.getCharacterAt(new Position(source.getOwningPlayerId(), source.getPosition().getRow(), source.getPosition().getCol()+1));
			if(c.isAlive(characterClassService)) {
				for(int i = 0; i < getProperties().get("timeMarkersAmt").intValue(); i++) {
					gameStateModelService.reduceTimeCounters(c);
				}
			}
		}
		logger.exit();
	}

}
