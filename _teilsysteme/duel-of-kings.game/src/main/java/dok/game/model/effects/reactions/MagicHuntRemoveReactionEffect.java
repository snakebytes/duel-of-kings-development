package dok.game.model.effects.reactions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.MagicHuntBuff;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class MagicHuntRemoveReactionEffect extends ReactionEffect {

    // Class Constants

    private static final long serialVersionUID = 1L;

    // Class Constants

    private static final Logger logger = LogManager.getLogger();

    // Constructor(s)

    public MagicHuntRemoveReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
        super(triggerSource, targets, properties);
        logger.entry(targets,properties);
        logger.exit(this);
    }

    public MagicHuntRemoveReactionEffect(MagicHuntRemoveReactionEffect other) {
        super(other);
        logger.entry(other);
        logger.exit(this);
    }

    // Methods
    @Override
    public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
        logger.entry(gameStateModelService, characterClassService, abilityClassService);
        gameStateModelService.removeBuff(getSourceCharacterId(), MagicHuntBuff.class);
        logger.exit();
    }

}
