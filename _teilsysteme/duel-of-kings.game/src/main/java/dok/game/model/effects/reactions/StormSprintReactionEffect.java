package dok.game.model.effects.reactions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.Ability;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class StormSprintReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public StormSprintReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
		logger.entry(targets,properties);
		logger.exit(this);
	}

	public StormSprintReactionEffect(StormSprintReactionEffect other) {
		super(other);
		logger.entry(other);
		logger.exit(this);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		gameStateModelService.modifyTimeCounters(getSourceCharacterId(), -1 * getProperties().get("timeMarkersChar").intValue());
		for(Ability ab : gameStateModelService.getCharacter(getSourceCharacterId()).getAbilities()) {
			gameStateModelService.modifyTimeCounters(ab, getProperties().get("timeMarkersAbil").intValue());
		}
		logger.exit();

	}

}
