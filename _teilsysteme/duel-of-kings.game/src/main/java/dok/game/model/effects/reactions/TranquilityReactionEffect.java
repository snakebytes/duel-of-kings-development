package dok.game.model.effects.reactions;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.Character;
import dok.game.model.CharacterAttribute;
import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.impl.HealingImpl;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class TranquilityReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public TranquilityReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
		logger.entry(targets,properties);
		logger.exit(this);
	}

	public TranquilityReactionEffect(TranquilityReactionEffect other) {
		super(other);
		logger.entry(other);
		logger.exit(this);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		int lowestHP = 99;
		Position target = null;
		for(UUID id : gameStateModelService.getCharacterIds(gameStateModelService.getActivePlayer())) {
			Character c = gameStateModelService.getCharacter(id);
			if(c.isAlive(characterClassService)) {
				int hp = c.getAttributeValue(CharacterAttribute.HEALTH, characterClassService) - c.getDamageTaken();
				if(hp < lowestHP && c.getDamageTaken() > 0) {
					lowestHP = hp;
					target = c.getPosition();
				}
			}
		}
		gameStateModelService.applyHealing(new HealingImpl(getProperties().get("healingAmount").intValue(), this, target, TargetType.PRIMARY), characterClassService);
		logger.exit();
	}

}
