package dok.game.model.effects.reactions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.BlockBuff;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class VeteranOfBattlesReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public VeteranOfBattlesReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
		logger.entry(targets,properties);
		logger.exit(this);
	}

	public VeteranOfBattlesReactionEffect(VeteranOfBattlesReactionEffect other) {
		super(other);
		logger.entry(other);
		logger.exit(this);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		gameStateModelService.addBuff(new BlockBuff(gameStateModelService.getActivePlayer(), this, getSourceCharacterId(), getProperties().get("duration").intValue(), getProperties().get("blockValue").intValue()));
		logger.exit();
	}

}
