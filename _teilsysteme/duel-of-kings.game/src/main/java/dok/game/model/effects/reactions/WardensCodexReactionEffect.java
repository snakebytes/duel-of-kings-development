package dok.game.model.effects.reactions;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.game.model.GameStateModelService;
import dok.game.model.Position;
import dok.game.model.TargetType;
import dok.game.model.buffs.WardensCodexBuff;
import dok.game.model.impl.ReactionEffect;
import dok.game.model.impl.Trigger;
import dok.game.services.AbilityDataService;
import dok.game.services.CharacterDataService;

public class WardensCodexReactionEffect extends ReactionEffect {

	// Class Constants

	private static final long serialVersionUID = 324099170442533374L;

	// Class Constants

	private static final Logger logger = LogManager.getLogger();

	// Constructor(s)

	public WardensCodexReactionEffect(Trigger triggerSource, Map<Position, TargetType> targets, Map<String, Double> properties) {
		super(triggerSource, targets, properties);
		logger.entry(targets,properties);
		logger.exit(this);
	}

	public WardensCodexReactionEffect(WardensCodexReactionEffect other) {
		super(other);
		logger.entry(other);
		logger.exit(this);
	}

	// Methods

	@Override
	public void resolve(GameStateModelService gameStateModelService, CharacterDataService characterClassService, AbilityDataService abilityClassService) {
		logger.entry(gameStateModelService, characterClassService, abilityClassService);
		Position target = null;
		Map<Position, TargetType> targets = getTrigger().getSourceEvent().getSourceEffect().getTargetedPositions();
		target = getTrigger().getSourceEvent().getSourceEffect().getTargetedPositions().keySet()
				.stream().filter((pos) -> targets.get(pos) == TargetType.PRIMARY).findFirst().get();
		gameStateModelService.swap(this, getSourceAbility().getOwningCharacterId(), target);
		gameStateModelService.removeBuff(getSourceAbility().getOwningCharacterId(), WardensCodexBuff.class);
		logger.exit();
	}

}
