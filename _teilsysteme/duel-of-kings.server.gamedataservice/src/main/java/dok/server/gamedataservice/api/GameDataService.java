package dok.server.gamedataservice.api;

import javax.ejb.Local;

import dok.commons.model.EnhancementSlot;
import dok.commons.model.EnhancementType;
import dok.commons.services.DataPersistenceService;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;

/**
 * Main DAO (Data Access Object) service for the
 * <i>com.duel-of-kings.gamedata</i> persistence unit.<br>
 * <br>
 * Only accessible in local context (i.e. the application server).<br>
 * <br>
 * <code>Note:</code> The persistence context is only present in the same enterprise application,
 * where the corresponding persistence archive is placed. In this case this is <i>duel-of-kings.game.model.jar</i>.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
@Local
public interface GameDataService extends CharacterDataService, AbilityDataService, DataPersistenceService {

	public EnhancementSlot getEnhancementSlot(EnhancementType type, int rank);

}
