package dok.server.gamedataservice.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.jboss.logging.Logger;

/**
 * Migrates the database schema for the gameData datasource on startup.
 */
@Singleton
@LocalBean
@Startup
@Lock(LockType.READ)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@TransactionManagement(TransactionManagementType.BEAN)
public class GameDataMigrationProvider {

	// Class Constants

	private static final Logger logger = Logger.getLogger(GameDataMigrationProvider.class);
	private static final String DATA_SOURCE = "java:/dok/datasources/gameData";

	// Injection

	@Resource(lookup = DATA_SOURCE)
	private DataSource dataSource;

	// Constructor(s)

    /**
     * Default constructor for java bean specification.
     */
    public GameDataMigrationProvider() {
        // Do nothing
    }

    // Methods

    @PostConstruct
    public void onStartup() {
    	logger.info("Attempting to migrate Data Source: " + DATA_SOURCE);
    	if (dataSource == null) {
    		logger.error("no datasource found to execute the db migrations!");
			throw new EJBException("no datasource found to execute the db migrations!");
		}

		Flyway flyway = new Flyway();
		flyway.setValidateOnMigrate(true);
		flyway.setLocations("/migration/game_data");
		flyway.setDataSource(dataSource);
		for (MigrationInfo i : flyway.info().all()) {
			logger.info("migrate task: " + i.getVersion() + " : " + i.getDescription() + " from file: " + i.getScript());
		}
		flyway.migrate();
		logger.info(DATA_SOURCE + " has been successfully migrated");
    }

}
