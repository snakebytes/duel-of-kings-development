package dok.server.gamedataservice.ejb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.jboss.logging.Logger;

import dok.commons.model.EnhancementSlot;
import dok.commons.model.EnhancementType;
import dok.game.model.AbilityClass;
import dok.game.model.CharacterAttribute;
import dok.game.model.CharacterClass;
import dok.game.model.CharacterEnhancementClass;
import dok.game.model.DamageType;
import dok.game.model.HitFilter;
import dok.game.model.HitFilterList;
import dok.game.model.TargetFilter;
import dok.game.model.TargetFilterList;
import dok.game.model.TriggerClass;
import dok.game.model.constants.AbilityClassIds;
import dok.game.model.constants.CharacterClassIds;
import dok.game.model.constants.EnhancementClassIds;
import dok.game.model.constants.TriggerClassIds;
import dok.game.model.effects.actions.AdaptiveBombEffect;
import dok.game.model.effects.actions.ArouseEffect;
import dok.game.model.effects.actions.AttackEffect;
import dok.game.model.effects.actions.AxeBashEffect;
import dok.game.model.effects.actions.BombardmentEffect;
import dok.game.model.effects.actions.BreathtakingSilenceEffect;
import dok.game.model.effects.actions.CallingPackEffect;
import dok.game.model.effects.actions.CurseEffect;
import dok.game.model.effects.actions.DefendEffect;
import dok.game.model.effects.actions.HealEffect;
import dok.game.model.effects.actions.PowerTransferEffect;
import dok.game.model.effects.actions.SilenceEffect;
import dok.game.model.effects.actions.SpearBlowEffect;
import dok.game.model.effects.actions.SwapEffect;
import dok.game.model.effects.actions.ThrowBombsEffect;
import dok.game.model.effects.actions.WardensCodexEffect;
import dok.game.model.effects.reactions.AdvTranquilityReactionEffect;
import dok.game.model.effects.reactions.DauntingDefenderReactionEffect;
import dok.game.model.effects.reactions.DauntingDefenderReapplyReactionEffect;
import dok.game.model.effects.reactions.EncourageReactionEffect;
import dok.game.model.effects.reactions.MagicHuntAddReactionEffect;
import dok.game.model.effects.reactions.MagicHuntRemoveReactionEffect;
import dok.game.model.effects.reactions.StormSprintReactionEffect;
import dok.game.model.effects.reactions.TranquilityReactionEffect;
import dok.game.model.effects.reactions.VeteranOfBattlesReactionEffect;
import dok.game.model.effects.reactions.WardensCodexReactionEffect;
import dok.server.gamedataservice.api.GameDataService;

/**
 * Implementation of the {@linkplain GameDataService} interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
@DependsOn(value = { "GameDataMigrationProvider" })
public class GameDataProvider implements GameDataService {

	// Class Constants

	private static final Logger logger = Logger.getLogger(GameDataProvider.class);

	// Injection

	@PersistenceContext(unitName = "com.duel-of-kings.gamedata")
	private EntityManager entityManager;

	// Constructor(s)

	/**
	 * Default constructor.
	 */
	public GameDataProvider() {
		// Do nothing
	}

	// <--- GameDataService --->

	@Override
	public EnhancementSlot getEnhancementSlot(EnhancementType type, int rank) {
		try {

			if (type == null)
				throw new IllegalArgumentException("type must not be null");

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<EnhancementSlot> query = cb.createQuery(EnhancementSlot.class);
			Root<EnhancementSlot> from = query.from(EnhancementSlot.class);

			ParameterExpression<EnhancementType> typeParamter = cb.parameter(EnhancementType.class);
			Expression<EnhancementType> slotType = from.get("type");

			ParameterExpression<Integer> rankParameter = cb.parameter(Integer.class);
			Expression<Integer> slotRank = from.get("rank");

			query.select(from).where(cb.and(cb.equal(slotType, typeParamter), cb.equal(slotRank, rankParameter)));

			// Execute the query

			return getEntityManager().createQuery(query).setParameter(typeParamter, type)
					.setParameter(rankParameter, rank).getResultList().get(0);

		} catch (Exception e) {

			logger.debug("An error occurred while retrieving the list of objects of the given type", e);
			return null;

		}
	}

	@Override
	public List<Integer> getValidEnhancementClassIds(EnhancementSlot slot, int characterClassId) {
		List<Integer> results = new ArrayList<>();
		CharacterClass cc = getCharacterClass(characterClassId);
		for (int id : cc.getAllowedEnhancementClassIds()) {
			CharacterEnhancementClass enh = getCharacterEnhancementClass(id);
			if (enh.isValidFor(slot))
				results.add(id);
		}
		return results;
	}

	private final Map<Integer, CharacterEnhancementClass> enhancementClasses = new ConcurrentHashMap<>();

	@Override
	public CharacterEnhancementClass getCharacterEnhancementClass(int id) {
		CharacterEnhancementClass result = enhancementClasses.get(id);
		if (result == null) {
			if ((result = getObjectById(CharacterEnhancementClass.class, id)) != null) {
				enhancementClasses.put(id, result);
			}
		}
		return result;
	}

	private final Map<Integer, AbilityClass> abilityClasses = new ConcurrentHashMap<>();

	@Override
	public AbilityClass getAbilityClass(int id) {
		AbilityClass result = abilityClasses.get(id);
		if (result == null) {
			if ((result = getObjectById(AbilityClass.class, id)) != null) {
				abilityClasses.put(id, result);
			}
		}
		return result;
	}

	@Override
	public List<CharacterClass> getAllCharacterClasses() {
		return this.getAll(CharacterClass.class);
	}

	private final Map<Integer, CharacterClass> characterClasses = new ConcurrentHashMap<>();

	@Override
	public CharacterClass getCharacterClass(int id) {
		CharacterClass result = characterClasses.get(id);
		if (result == null) {
			if ((result = getObjectById(CharacterClass.class, id)) != null) {
				characterClasses.put(id, result);
			}
		}
		return result;
	}

	private final Map<Integer, TriggerClass> triggerClasses = new ConcurrentHashMap<>();

	@Override
	public TriggerClass getTriggerClass(int id) {
		TriggerClass result = triggerClasses.get(id);
		if (result == null) {
			if ((result = getObjectById(TriggerClass.class, id)) != null) {
				triggerClasses.put(id, result);
			}
		}
		return result;
	}

	// <-- DataPersistenceService --->

	@Override
	public Object save(Serializable object) {
		try {

			getEntityManager().persist(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be persisted", e);
			return false;

		}
	}

	@Override
	public boolean update(Serializable object) {
		try {

			getEntityManager().merge(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be updated", e);
			return false;

		}
	}

	@Override
	public boolean delete(Serializable object) {
		try {

			getEntityManager().remove(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be deleted", e);
			return false;

		}
	}

	@Override
	public <T> List<T> getAll(Class<T> type) {
		try {

			if (type == null)
				throw new IllegalArgumentException("type must not be null");

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<T> query = cb.createQuery(type);
			Root<T> from = query.from(type);
			CriteriaQuery<T> all = query.select(from);

			// Execute the query

			return getEntityManager().createQuery(all).getResultList();

		} catch (Exception e) {

			logger.debug("An error occurred while retrieving the list of objects of the given type", e);
			return new ArrayList<>();

		}
	}

	@Override
	public <T> T getObjectById(Class<T> type, Serializable id) {
		try {

			return getEntityManager().find(type, id);

		} catch (Exception e) {

			logger.debug("Requested entity could not be retrieved", e);
			return null;

		}
	}

	@Lock(LockType.WRITE)
	public boolean saveOrUpdate(Serializable object) {
		try {

			getEntityManager().merge(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be merged", e);
			return false;

		}
	}

	@Lock(LockType.WRITE)
	public TargetFilterList saveOrUpdate(TargetFilterList filterList) {
		TargetFilterList filterList2;
		if ((filterList2 = getEntityManager().find(TargetFilterList.class, filterList.getId())) != null) {
			return filterList2;
		} else {
			getEntityManager().persist(filterList);
			return filterList;
		}
	}

	@Lock(LockType.WRITE)
	public HitFilterList saveOrUpdate(HitFilterList filterList) {
		HitFilterList filterList2;
		if ((filterList2 = getEntityManager().find(HitFilterList.class, filterList.getId())) != null) {
			return filterList2;
		} else {
			getEntityManager().persist(filterList);
			return filterList;
		}
	}

	@Lock(LockType.WRITE)
	public EnhancementSlot saveOrUpdate(EnhancementSlot enhancementSlot) {
		EnhancementSlot enhancementSlot2;
		if ((enhancementSlot2 = getEntityManager().find(EnhancementSlot.class, enhancementSlot.getId())) != null) {
			return enhancementSlot2;
		} else {
			getEntityManager().persist(enhancementSlot);
			return enhancementSlot;
		}
	}

	// Test

	@PostConstruct
	public void initialize() {
		createTestCharacterAndAbilityClasses();
		preload();
	}

	private void preload() {

		for (CharacterClass characterClass : getAll(CharacterClass.class)) getCharacterClass(characterClass.getId());
		for (CharacterEnhancementClass characterEnhancementClass : getAll(CharacterEnhancementClass.class)) getCharacterEnhancementClass(characterEnhancementClass.getId());
		for (AbilityClass abilityClass : getAll(AbilityClass.class)) getCharacterClass(abilityClass.getId());
		for (TriggerClass triggerClass : getAll(TriggerClass.class)) getTriggerClass(triggerClass.getId());

	}

	private void createTestCharacterAndAbilityClasses() {
		try {

			// Meele Filter

			TargetFilterList meeleFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.HOSTILE, TargetFilter.ALIVE, TargetFilter.MEELE));
			TargetFilterList semiRangedAttackFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.HOSTILE, TargetFilter.ALIVE, TargetFilter.SEMI_RANGED));
			TargetFilterList semiRangedTargetFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.ALIVE, TargetFilter.SEMI_RANGED));
			TargetFilterList rangedFilter = saveOrUpdate(TargetFilter.and(TargetFilter.HOSTILE, TargetFilter.ALIVE));
			TargetFilterList attackTargetFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.ALIVE, TargetFilter.HOSTILE, TargetFilter.ATTACK));
			TargetFilterList otherAllyFilter = saveOrUpdate(
					TargetFilter.and(TargetFilter.OWN, TargetFilter.ALIVE, TargetFilter.OTHER));
			TargetFilterList ownAliveTargetFilter = saveOrUpdate(TargetFilter.and(TargetFilter.ALIVE, TargetFilter.OWN));
			TargetFilterList aliveHostileTargetFilter = saveOrUpdate(TargetFilter.and(TargetFilter.ALIVE, TargetFilter.HOSTILE));
			HitFilterList primaryFilter = saveOrUpdate(HitFilter.single(HitFilter.PRIMARY));
			HitFilterList piercingFilter = saveOrUpdate(HitFilter.single(HitFilter.PIERCING));
			HitFilterList adjacentRowFilter = saveOrUpdate(HitFilter.single(HitFilter.ADJACENT_ROW));
			HitFilterList crossingFilter = saveOrUpdate(
					HitFilter.combine(HitFilter.ADJACENT_ROW, HitFilter.ADJACENT_COLUMN));
			HitFilterList attackHitFilter = saveOrUpdate(HitFilter.single(HitFilter.ATTACK));
			HitFilterList rowHitFilter = saveOrUpdate(HitFilter.single(HitFilter.ROW));
			HitFilterList allHostileHitFilter = saveOrUpdate(HitFilter.single(HitFilter.ALL_HOSTILE));


			EnhancementSlot rankOneSlotA = saveOrUpdate(new EnhancementSlot(EnhancementType.CRYSTAL, 1));
			EnhancementSlot rankOneSlotB = saveOrUpdate(new EnhancementSlot(EnhancementType.RUNE, 1));
			EnhancementSlot rankTwoSlot = saveOrUpdate(new EnhancementSlot(EnhancementType.GENERIC, 2));
			EnhancementSlot rankThreeSlot = saveOrUpdate(new EnhancementSlot(EnhancementType.GENERIC, 3));

			Set<Integer> abilityIds;
			Map<CharacterAttribute, Integer> attributes;
			CharacterClass clazz;

			// Ability Crystals

			CharacterEnhancementClass wardensCodexCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.WARDENS_CODEX_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			wardensCodexCrystal.getAbilityClassIds().add(AbilityClassIds.WARDENS_CODE_ABILITY_CLASS_ID);
			saveOrUpdate(wardensCodexCrystal);

			CharacterEnhancementClass axeBashCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.AXE_BASH_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			axeBashCrystal.getAbilityClassIds().add(AbilityClassIds.AXE_BASH_ABILITY_CLASS_ID);
			saveOrUpdate(axeBashCrystal);

			CharacterEnhancementClass spearBlowCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.SPEAR_BLOW_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			spearBlowCrystal.getAbilityClassIds().add(AbilityClassIds.SPEAR_BLOW_ABILITY_CLASS_ID);
			saveOrUpdate(spearBlowCrystal);

			CharacterEnhancementClass healCrystal = new CharacterEnhancementClass(EnhancementClassIds.HEAL_CRYSTAL_ID,
					EnhancementType.CRYSTAL, 1);
			healCrystal.getAbilityClassIds().add(AbilityClassIds.HEAL_ABILITY_CLASS_ID);
			saveOrUpdate(healCrystal);

			CharacterEnhancementClass curseCrystal = new CharacterEnhancementClass(EnhancementClassIds.CURSE_CRYSTAL_ID,
					EnhancementType.CRYSTAL, 1);
			curseCrystal.getAbilityClassIds().add(AbilityClassIds.CURSE_ABILITY_CLASS_ID);
			saveOrUpdate(curseCrystal);

			CharacterEnhancementClass bombardmentCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.BOMBARDMENT_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			bombardmentCrystal.getAbilityClassIds().add(AbilityClassIds.BOMBARDMENT_ABILITY_CLASS_ID);
			saveOrUpdate(bombardmentCrystal);

			CharacterEnhancementClass arouseCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.AROUSE_CRYSTAL_ID, EnhancementType.CRYSTAL, 1);
			arouseCrystal.getAbilityClassIds().add(AbilityClassIds.AROUSE_ABILITY_CLASS_ID);
			saveOrUpdate(arouseCrystal);

			// Rank 2

			CharacterEnhancementClass tranquilityCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.TRANQUILITY_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			tranquilityCrystal.getAbilityClassIds().add(AbilityClassIds.TRANQUILITY_ABILITY_CLASS_ID);
			saveOrUpdate(tranquilityCrystal);

			CharacterEnhancementClass veteranOfBattlesCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.VETERAN_OF_BATTLES_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			veteranOfBattlesCrystal.getAbilityClassIds().add(AbilityClassIds.VETERAN_OF_BATTLES_ABILITY_CLASS_ID);
			saveOrUpdate(veteranOfBattlesCrystal);

			CharacterEnhancementClass silenceCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.SILENCE_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			silenceCrystal.getAbilityClassIds().add(AbilityClassIds.SILENCE_ABILITY_CLASS_ID);
			saveOrUpdate(silenceCrystal);

			CharacterEnhancementClass callThePackCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.CALLING_THE_PACK_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			callThePackCrystal.getAbilityClassIds().add(AbilityClassIds.CALLING_THE_PACK_ABILITY_CLASS_ID);
			saveOrUpdate(callThePackCrystal);

			CharacterEnhancementClass dauntingDefenderCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.DAUNTING_DEFENDER_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			dauntingDefenderCrystal.getAbilityClassIds().add(AbilityClassIds.DAUNTING_DEFENDER_ABILITY_CLASS_ID);
			saveOrUpdate(dauntingDefenderCrystal);

			CharacterEnhancementClass adaptiveBombCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.ADAPTIVE_BOMB_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			adaptiveBombCrystal.getAbilityClassIds().add(AbilityClassIds.ADAPTIVE_BOMB_ABILITY_CLASS_ID);
			saveOrUpdate(adaptiveBombCrystal);

			CharacterEnhancementClass powerTransferCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.POWER_TRANSFER_CRYSTAL_ID, EnhancementType.CRYSTAL, 2);
			powerTransferCrystal.getAbilityClassIds().add(AbilityClassIds.POWER_TRANSFER_ABILITY_CLASS_ID);
			saveOrUpdate(powerTransferCrystal);

			// Rank 3

			CharacterEnhancementClass advTranquilityCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.ADV_TRANQUILITY_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			advTranquilityCrystal.getAbilityClassIds().add(AbilityClassIds.ADV_TRANQUILITY_ABILITY_CLASS_ID);
			saveOrUpdate(advTranquilityCrystal);

			CharacterEnhancementClass btSilenceCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.BREATHTAKING_SILENCE_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			btSilenceCrystal.getAbilityClassIds().add(AbilityClassIds.BREATHTAKING_SILENCE_ABILITY_CLASS_ID);
			saveOrUpdate(btSilenceCrystal);

			CharacterEnhancementClass stormSprintCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.STORM_SPRINT_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			stormSprintCrystal.getAbilityClassIds().add(AbilityClassIds.STORM_SPRINT_ABILITY_CLASS_ID);
			saveOrUpdate(stormSprintCrystal);

			CharacterEnhancementClass throwBombsCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.THROW_BOMBS_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			throwBombsCrystal.getAbilityClassIds().add(AbilityClassIds.THROW_BOMBS_ABILITY_CLASS_ID);
			saveOrUpdate(throwBombsCrystal);

			CharacterEnhancementClass magicHuntCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.MAGIC_HUNT_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			magicHuntCrystal.getAbilityClassIds().add(AbilityClassIds.MAGIC_HUNT_ABILITY_CLASS_ID);
			saveOrUpdate(magicHuntCrystal);

			CharacterEnhancementClass encourageCrystal = new CharacterEnhancementClass(
					EnhancementClassIds.ENCOURAGE_CRYSTAL_ID, EnhancementType.CRYSTAL, 3);
			encourageCrystal.getAbilityClassIds().add(AbilityClassIds.ENCOURAGE_ABILITY_CLASS_ID);
			saveOrUpdate(encourageCrystal);

			// <Sword>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 20);
			attributes.put(CharacterAttribute.ARMOR, 2);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.SWORD_CHARACTER_CLASS_ID, meeleFilter, primaryFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Sword>

			// <Axe>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 15);
			attributes.put(CharacterAttribute.ARMOR, 1);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 3);

			// Add

			clazz = new CharacterClass(CharacterClassIds.AXE_CHARACTER_CLASS_ID, meeleFilter, adjacentRowFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Axe>

			// <Spear>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 1);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 4);

			// Add

			clazz = new CharacterClass(CharacterClassIds.SPEAR_CHARACTER_CLASS_ID, meeleFilter, piercingFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Spear>

			// <Staff>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 0);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 1);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 3);

			// Add

			clazz = new CharacterClass(CharacterClassIds.STAFF_CHARACTER_CLASS_ID, meeleFilter, primaryFilter,
					DamageType.MAGICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Staff>

			// <Bow>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 0);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.BOW_CHARACTER_CLASS_ID, rangedFilter, primaryFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Bow>

			// <Crossbow>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 1);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 2);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.CROSSBOW_CHARACTER_CLASS_ID, semiRangedAttackFilter, piercingFilter,
					DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Crossbow>

			// <Handcannon>

			// Abilities

			abilityIds = new HashSet<>();
			abilityIds.add(AbilityClassIds.ATTACK_ABILITY_CLASS_ID); // Attack
			abilityIds.add(AbilityClassIds.SWAP_ABILITY_CLASS_ID); // Swap
			abilityIds.add(AbilityClassIds.DEFEND_ABILITY_CLASS_ID); // Defend

			// Attributes

			attributes = new HashMap<>();
			attributes.put(CharacterAttribute.HEALTH, 10);
			attributes.put(CharacterAttribute.ARMOR, 0);
			attributes.put(CharacterAttribute.MAGIC_RESISTANCE, 0);
			attributes.put(CharacterAttribute.ATTACK_DAMAGE, 2);

			// Add

			clazz = new CharacterClass(CharacterClassIds.HANDCANNON_CHARACTER_CLASS_ID, semiRangedAttackFilter,
					crossingFilter, DamageType.PHYSICAL, attributes, abilityIds);
			clazz.getEnhancementSlots().add(rankOneSlotA);
			clazz.getEnhancementSlots().add(rankOneSlotB);
			clazz.getEnhancementSlots().add(rankTwoSlot);
			clazz.getEnhancementSlots().add(rankThreeSlot);
			saveOrUpdate(clazz);

			// </Handcannon>

			LinkedHashMap<String, Double> properties;

			// <Attack>

			properties = new LinkedHashMap<>();
			properties.put("cost", 2d);
			properties.put("secondaryDamageMultiplier", 0.5);

			AbilityClass attackAbilityClass = new AbilityClass(AbilityClassIds.ATTACK_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), AttackEffect.class, attackTargetFilter, attackHitFilter, properties);
			saveOrUpdate(attackAbilityClass);

			// </Attack>

			// <Swap>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);

			AbilityClass swapAbilityClass = new AbilityClass(AbilityClassIds.SWAP_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), SwapEffect.class, otherAllyFilter, primaryFilter, properties);
			saveOrUpdate(swapAbilityClass);

			// </Swap>

			// <Defend>

			properties = new LinkedHashMap<>();
			properties.put("cost", 1d);
			properties.put("duration", 1d);
			properties.put("armorBonus", 1d);
			properties.put("magicResistanceBonus", 1d);

			AbilityClass defendAbilityClass = new AbilityClass(AbilityClassIds.DEFEND_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), DefendEffect.class, null, primaryFilter, properties);
			saveOrUpdate(defendAbilityClass);

			// </Defend>

			// <Warden's Codex>

			properties = new LinkedHashMap<>();
			properties.put("cost", 1d);
			properties.put("duration", 1d);

			AbilityClass wardensCodexAbilityClass = new AbilityClass(AbilityClassIds.WARDENS_CODE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), WardensCodexEffect.class, null,
					primaryFilter, properties);
			saveOrUpdate(wardensCodexAbilityClass);

			// </Warden's Codex>

			// <Axe Bash>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("stunCountersAmount", 1d);
			properties.put("secondaryDamageMultiplier", 0.5);

			AbilityClass axeBashAbilityClass = new AbilityClass(AbilityClassIds.AXE_BASH_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), AxeBashEffect.class,
					attackTargetFilter,
					adjacentRowFilter, properties);
			saveOrUpdate(axeBashAbilityClass);

			// </Axe Bash>

			// <Curse>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("duration", 2d);
			properties.put("attackDamageMalus", 1d);

			AbilityClass curseAbilityClass = new AbilityClass(AbilityClassIds.CURSE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), CurseEffect.class,
					attackTargetFilter,
					adjacentRowFilter, properties);
			saveOrUpdate(curseAbilityClass);

			// </Curse>

			// <Heal>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("healingAmount", 2d);

			AbilityClass healAbilityClass = new AbilityClass(AbilityClassIds.HEAL_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), HealEffect.class,
					ownAliveTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(healAbilityClass);

			// </Heal>

			// <Spear Blow>

			properties = new LinkedHashMap<>();
			properties.put("cost", 3d);
			properties.put("timeCountersAmount", 1d);
			properties.put("secondaryDamageMultiplier", 0.5d);

			AbilityClass spearBlowAbilityClass = new AbilityClass(AbilityClassIds.SPEAR_BLOW_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), SpearBlowEffect.class,
					attackTargetFilter,
					piercingFilter, properties);
			saveOrUpdate(spearBlowAbilityClass);

			// </Spear Blow>

			// <Arouse>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("stunCountersAmount", 1d);
			properties.put("stunShortenAmount", 1d);

			AbilityClass arouseAbilityClass = new AbilityClass(AbilityClassIds.AROUSE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), ArouseEffect.class,
					ownAliveTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(arouseAbilityClass);

			// </Arouse>

			// <Bombardment>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("damageAmount", 1d);

			AbilityClass bombardmentAbilityClass = new AbilityClass(AbilityClassIds.BOMBARDMENT_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), BombardmentEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(bombardmentAbilityClass);

			// </Bombardment>

			// <Tranquility>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("healingAmount", 2d); // Relevant f�r den Tooltip,
													// NICHT f�r die
													// Funkionalit�t

			AbilityClass tranquilityAbilityClass = new AbilityClass(AbilityClassIds.TRANQUILITY_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(tranquilityAbilityClass);

			// </Tranquility>

			// <Silence>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("duration", 2d);

			AbilityClass silenceAbilityClass = new AbilityClass(AbilityClassIds.SILENCE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), SilenceEffect.class,
					aliveHostileTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(silenceAbilityClass);

			// </Silence>

			// <Veteran of Battles>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("blockValue", 3d); // Relevant f�r den Tooltip, NICHT
												// f�r die Funkionalit�t

			AbilityClass veteranAbilityClass = new AbilityClass(AbilityClassIds.VETERAN_OF_BATTLES_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(veteranAbilityClass);

			// </Veteran of Battles>

			// <Calling the Pack>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("dmgToUndamaged", 1d);
			properties.put("dmgToDamaged", 2d);

			AbilityClass packAbilityClass = new AbilityClass(AbilityClassIds.CALLING_THE_PACK_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), CallingPackEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(packAbilityClass);

			// </Calling the Pack>

			// <Daunting Defender>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);

			AbilityClass dauntingDefenderAbilityClass = new AbilityClass(
					AbilityClassIds.DAUNTING_DEFENDER_ABILITY_CLASS_ID, properties.get("cost").intValue(), null, null,
					null, properties);
			saveOrUpdate(dauntingDefenderAbilityClass);

			// </Daunting Defender>

			// <Adaptive Bomb>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("dmgBonus", 2d);
			properties.put("secondaryDamageMultiplier", 0.5d);

			AbilityClass adaptiveBombClass = new AbilityClass(AbilityClassIds.ADAPTIVE_BOMB_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), AdaptiveBombEffect.class,
					attackTargetFilter,
					attackHitFilter, properties);
			saveOrUpdate(adaptiveBombClass);

			// </Adaptive Bomb>

			// <Power Transfer>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("duration", 1d);
			properties.put("atkValue", 1d);

			AbilityClass powerTransferClass = new AbilityClass(AbilityClassIds.POWER_TRANSFER_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), PowerTransferEffect.class,
					semiRangedTargetFilter, rowHitFilter,
					properties);
			saveOrUpdate(powerTransferClass);

			// </Power Transfer>

			// <Throw Bombs>

			properties = new LinkedHashMap<>();
			properties.put("cost", 4d);
			properties.put("bombAmount", 3d);
			properties.put("bombDamage", 2d);
			properties.put("secondaryDamageMultiplier", 0.5);

			AbilityClass throwBombsClass = new AbilityClass(AbilityClassIds.THROW_BOMBS_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), ThrowBombsEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(throwBombsClass);

			// </Throw Bombs>

			// <Magic Hunt>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("atkValue", 1d); // Nur f�r den Tooltip relevant,
											// NICHT f�r die Funktionalit�t
			properties.put("duration", 0d);

			AbilityClass magicHuntClass = new AbilityClass(AbilityClassIds.MAGIC_HUNT_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(magicHuntClass);

			// </Magic Hunt>

			// <Storm Sprint>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);

			AbilityClass chargeRunnerClass = new AbilityClass(AbilityClassIds.STORM_SPRINT_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(chargeRunnerClass);

			// </Storm Sprint>

			// <Breathtaking Silence>

			properties = new LinkedHashMap<>();
			properties.put("cost", 5d);
			properties.put("duration", 1d);

			AbilityClass breathSilenceClass = new AbilityClass(AbilityClassIds.BREATHTAKING_SILENCE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), BreathtakingSilenceEffect.class, null,
					allHostileHitFilter, properties);
			saveOrUpdate(breathSilenceClass);

			// </Breathtaking Silence>

			// <Encourage>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("markerAmount", 2d); // Nur f�r den Skill-Tooltip
												// relevant

			AbilityClass encourageClass = new AbilityClass(AbilityClassIds.ENCOURAGE_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(encourageClass);

			// </Encourage>

			// <Advanced Tranquility>

			properties = new LinkedHashMap<>();
			properties.put("cost", 0d);
			properties.put("healingAmount", 4d); // Nur f�r den Tooltip
													// relevant, NICHT f�r die
													// Funktionalit�t
			properties.put("healCount", 2d); // Nur f�r den Tooltip relevant,
												// NICHT f�r die Funktionalit�t

			AbilityClass advTranquilityAbilityClass = new AbilityClass(AbilityClassIds.ADV_TRANQUILITY_ABILITY_CLASS_ID,
					properties.get("cost").intValue(), null, null, null, properties);
			saveOrUpdate(advTranquilityAbilityClass);

			// </Advanced Tranquility>

			// <--------- Triggers ------------>

			// <Warden's Codex Trigger>

			TriggerClass wardensCodexTriggerClass = new TriggerClass(TriggerClassIds.WARDENS_CODE_TRIGGER_CLASS_ID,
					null, primaryFilter, WardensCodexReactionEffect.class, true);
			saveOrUpdate(wardensCodexTriggerClass);

			// <Warden's Codex Trigger>

			// <Tranquility Trigger>

			properties = new LinkedHashMap<>();
			properties.put("healingAmount", 2d);

			TriggerClass tranquilityTriggerClass = new TriggerClass(TriggerClassIds.TRANQUILITY_TRIGGER_CLASS_ID, null,
					null, TranquilityReactionEffect.class, false, properties);
			saveOrUpdate(tranquilityTriggerClass);

			// </Tranquility Trigger>

			// <Veteran of Battles Trigger>

			properties = new LinkedHashMap<>();
			properties.put("duration", 0d);
			properties.put("blockValue", 3d); // Relevant f�r die
												// Funktionalit�t, NICHT den
												// Tooltip!

			TriggerClass veteranTriggerClass = new TriggerClass(TriggerClassIds.VETERAN_OF_BATTLES_TRIGGER_CLASS_ID,
					null, null, VeteranOfBattlesReactionEffect.class, false, properties);
			saveOrUpdate(veteranTriggerClass);

			// </Veteran of Battles Trigger>

			// <Daunting Defender Trigger>

			properties = new LinkedHashMap<>();
			properties.put("secondaryDamageMultiplier", 0.5);

			TriggerClass dauntingDefenderTriggerClass = new TriggerClass(
					TriggerClassIds.DAUNTING_DEFENDER_TRIGGER_CLASS_ID, null, null,
					DauntingDefenderReactionEffect.class, false, properties);
			saveOrUpdate(dauntingDefenderTriggerClass);

			// </Daunting Defender Trigger>

			// <Magic Hunt Add Trigger>

			properties = new LinkedHashMap<>();
			properties.put("atkValue", 1d);
			properties.put("duration", 0d);

			TriggerClass magicHuntAddTriggerClass = new TriggerClass(TriggerClassIds.MAGIC_HUNT_ADD_TRIGGER_CLASS_ID,
					null, null, MagicHuntAddReactionEffect.class, false, properties);
			saveOrUpdate(magicHuntAddTriggerClass);

			// </Magic Hunt Add Trigger>

			// <Magic Hunt Remove Trigger>

			properties = new LinkedHashMap<>();

			TriggerClass magicHuntRemoveTriggerClass = new TriggerClass(
					TriggerClassIds.MAGIC_HUNT_REMOVE_TRIGGER_CLASS_ID, null, null, MagicHuntRemoveReactionEffect.class,
					false, properties);
			saveOrUpdate(magicHuntRemoveTriggerClass);

			// </Magic Hunt Remove Trigger>

			// <Charge Runner Trigger>

			properties = new LinkedHashMap<>();
			properties.put("timeMarkersChar", 1d);
			properties.put("timeMarkersAbil", 1d);

			TriggerClass chargeRunnerTriggerClass = new TriggerClass(TriggerClassIds.STORM_SPRINT_TRIGGER_CLASS_ID,
					null, null, StormSprintReactionEffect.class, false, properties);
			saveOrUpdate(chargeRunnerTriggerClass);

			// </Charge Runner Trigger>

			// <Encourage Trigger>

			properties = new LinkedHashMap<>();
			properties.put("timeMarkersAmt", 2d);

			TriggerClass encourageTriggerClass = new TriggerClass(TriggerClassIds.ENCOURAGE_TRIGGER_CLASS_ID, null,
					null, EncourageReactionEffect.class, false, properties);
			saveOrUpdate(encourageTriggerClass);

			// </Encourage Trigger>

			// <Advanced Tranquility Trigger>

			properties = new LinkedHashMap<>();
			properties.put("healingAmount", 4d);
			properties.put("healCount", 2d);

			TriggerClass advTranquilityTriggerClass = new TriggerClass(TriggerClassIds.ADV_TRANQUILITY_TRIGGER_CLASS_ID,
					null, null, AdvTranquilityReactionEffect.class, false, properties);
			saveOrUpdate(advTranquilityTriggerClass);

			// </Advanced Tranquility Trigger>

			// <Daunting Defender Reapply Trigger>

			properties = new LinkedHashMap<>();

			TriggerClass dauntingDefenderReapplyTriggerClass = new TriggerClass(
					TriggerClassIds.DAUNTING_DEFENDER_REAPPLY_TRIGGER_CLASS_ID, null, null,
					DauntingDefenderReapplyReactionEffect.class, false, properties);
			saveOrUpdate(dauntingDefenderReapplyTriggerClass);

			// </Daunting Defender Reapply Trigger>

		} catch (Exception e) {
			logger.warn("GameTestData could not be created", e);
		}

	}

	// Getter & Setter

	protected final EntityManager getEntityManager() {
		return entityManager;
	}

	protected final void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
