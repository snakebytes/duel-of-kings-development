package dok.server.gameservice;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.jboss.logging.Logger;

import dok.commons.impl.CustomTimer.CustomTimerState;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.events.ConnectionClosedEvent;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.game.logic.statemachines.GameLogic_Master;
import dok.game.logic.statemachines.GameLogic_Master.GameLogicMasterState;
import dok.game.model.GameEvent;
import dok.game.model.GameResult;
import dok.game.model.GameStateModel;
import dok.game.model.GameType;
import dok.game.model.Player;
import dok.game.model.Position;
import dok.game.model.SwapRequest;
import dok.game.model.Trigger;
import dok.game.model.UserPlayer;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.model.api.ITargetFilter;
import dok.game.network.model.ActionPacket;
import dok.game.network.model.CharacterSelectionPacket;
import dok.game.network.model.GameResultPacket;
import dok.game.network.model.ModelPacket;
import dok.game.network.model.ReactionPacket;
import dok.game.network.model.StartGamePacket;
import dok.game.network.model.SurrenderPacket;
import dok.game.network.model.SwapRequestPacket;
import dok.game.network.model.WorkDonePacket;
import dok.game.services.util.WaitingManager;
import dok.server.gameservice.api.GameService;

/**
 * Class responsible for managing a one-versus-one game of 'Duel of Kings'.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public class DuelGame extends ServerGame {

	// Class Constants

	private static final Logger logger = Logger.getLogger(DuelGame.class);

	// Attributes

	private final WaitingManager waitingManager = new WaitingManager();

	// Constructor(s)

	/**
	 * Creates a new game with the given players.<br>
	 * The given character class and ability class services should not
	 * change when instantiating multiple objects of this class.<br>
	 * <br>
	 * <i>Description copied from {@linkplain ServerGame}.</i>
	 *
	 * @param gameService The service who created this game
	 * @param characterClassService The source of character data
	 * @param abilityClassService The source of ability data
	 * @param participants The players who are participating in this game
	 */
	public DuelGame(GameService gameService, CharacterDataService characterClassService, AbilityDataService abilityClassService, Map<UserPlayer, Connection> participants) {
		super(gameService, characterClassService, abilityClassService, participants);
		setGameLogicService(new GameLogic_Master(this, this, this, characterClassService, abilityClassService, this));
	}

	// <--- GameLogicWrapper --->

	@Override
	public void placeCharacters(UUID player) {
		try {
			getPlayerConnection(player).sendPacket(new SwapRequestPacket(null));
		} catch (IOException e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public void chooseCharacter(UUID player, ITargetFilter filter, Set<Position> availablePositions) {
		try {
			getPlayerConnection(player).sendPacket(new CharacterSelectionPacket(filter, availablePositions));
		} catch (IOException e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public void performCharacterAction(UUID player) {
		try {
			getPlayerConnection(player).sendPacket(new ActionPacket(null));
		} catch (IOException e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public void performReaction(UUID player, List<Trigger> list, List<GameEvent> gameEvents) {
		try {
			getPlayerConnection(player).sendPacket(new ReactionPacket(list, gameEvents));
		} catch (IOException e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public void handleModelUpdate(UUID player, GameStateModel gameStateModel) {
		try {
			getPlayerConnection(player).sendPacket(new ModelPacket(gameStateModel));
		} catch (Exception e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public boolean handleModelUpdate(GameStateModel gameStateModel) {

		logger.debug("handleModelUpdate: " + gameStateModel);

    	// Broadcast the model to all players
		logger.debug("Updating model of players: " + getPlayerIds());
    	for (UUID playerId : getPlayerIds()) {
    		logger.debug("Start waiting on: " + playerId);
    		getWaitingManager().startWaitingOn(playerId);

    		logger.debug("Updating model of player: " + playerId);
    		try {
				getPlayerConnection(playerId).sendPacket(new ModelPacket(gameStateModel));
			} catch (IOException e) {
				logger.debug("Packet could not be sent", e);
			}
    	}

		// Remember Timer States
    	logger.debug("Storing timer states");
		CustomTimerState turnTimerState = gameStateModel.getTurnTimer().getState();
		CustomTimerState chooseTimerState = gameStateModel.getChooseTimer().getState();

		// Pause Timers
		logger.debug("Pausing timers");
		if (turnTimerState == CustomTimerState.RUNNING) gameStateModel.getTurnTimer().pause();
		if (chooseTimerState == CustomTimerState.RUNNING) gameStateModel.getChooseTimer().pause();

		// Wait for everybody to finish overriding their models (& views)
		logger.debug("Start waiting for players to finish updating the model ...");
		boolean result = getWaitingManager().waitForAll();

		// Resume Timers
		logger.debug("Resuming timers");
		if (turnTimerState == CustomTimerState.RUNNING) gameStateModel.getTurnTimer().resume();
		if (chooseTimerState == CustomTimerState.RUNNING) gameStateModel.getChooseTimer().resume();

		return result;
	}

	@Override
	public void handleSwapRequest(UUID player, SwapRequest swapRequest) {
		try {
			getPlayerConnection(player).sendPacket(new SwapRequestPacket(swapRequest));
		} catch (IOException e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public void handleGameResult(UUID player, GameResult result) {
		try {
			getPlayerConnection(player).sendPacket(new GameResultPacket(result));
		} catch (IOException e) {
			logger.debug("Packet could not be sent", e);
		}
	}

	@Override
	public void onFinish(Object result) {
		logger.debug("A game has been finished with the following result: " + result);

		// Stop Waiting
		getWaitingManager().interrupt();

		// Remove the players from the game service
		for (Player p : getPlayers()) {
			try {
				if(!getGameService().removeUserGameMapping(((UserPlayer)p).getAccountId()))
					logger.warn("Player " + p + " could not be removed from game service.");
			} catch (Exception e) {
				logger.warn("Player " + p + " could not be removed from game service.", e);
			}
		}


	}

	// <--- Game --->

	@Override
	public void start() {
		((GameLogic_Master) getGameLogicService()).transition(GameLogicMasterState.GAMELOGIC_INITIALIZATION);
	}

	// <--- Runnable --->

	@Override
	public void run() {
		try {

			// Start the game on clients
			for (Connection connection : getConnections().values()) {
				connection.sendPacket(new StartGamePacket(GameType.Duel, getPlayers(), getConnectionPlayer(connection)));
				getWaitingManager().startWaitingOn(getConnectionPlayer(connection).getPlayerId());
			}

			// Start waiting for clients to start their game
			getWaitingManager().waitForAll();

			// Start the game on server
			start();

		} catch (Exception e) {

			logger.error("Game could not be started", e);
			// TODO Game could not be started

		}
	}

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		logger.debug("Handling GameConnectionEvent: " + connectionEvent);
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;
			if (packetReceivedEvent.getPacket() instanceof ActionPacket) {
				ActionPacket actionPacket = (ActionPacket) packetReceivedEvent.getPacket();
				getGameLogicService().handleAction(getConnectionPlayerId(connectionEvent.getSource()), actionPacket.getAction());
			} else if (packetReceivedEvent.getPacket() instanceof ReactionPacket) {
				ReactionPacket reactionPacket = (ReactionPacket) packetReceivedEvent.getPacket();
				getGameLogicService().handleReaction(getConnectionPlayerId(connectionEvent.getSource()), reactionPacket.getReaction());
			} else if (packetReceivedEvent.getPacket() instanceof SwapRequestPacket) {
				SwapRequestPacket swapRequestPacket = (SwapRequestPacket) packetReceivedEvent.getPacket();
				getGameLogicService().handleSwapRequest(getConnectionPlayerId(connectionEvent.getSource()), swapRequestPacket.getSwapRequest());
			} else if (packetReceivedEvent.getPacket() instanceof CharacterSelectionPacket) {
				CharacterSelectionPacket characterSelectionPacket = (CharacterSelectionPacket) packetReceivedEvent.getPacket();
				getGameLogicService().handleCharacterChoosen(getConnectionPlayerId(connectionEvent.getSource()), characterSelectionPacket.getSelectedPosition());
			} else if (packetReceivedEvent.getPacket() instanceof SurrenderPacket) {
				getGameLogicService().handleSurrenderRequest(getConnectionPlayerId(connectionEvent.getSource()));
			} else if (packetReceivedEvent.getPacket() instanceof WorkDonePacket) {
				logger.debug("Stop waiting on: " + getConnectionPlayerId(connectionEvent.getSource()));
				getWaitingManager().stopWaitingOn(getConnectionPlayerId(connectionEvent.getSource()));
				logger.debug("Still waiting on " + getWaitingManager().getWaitingCount() + " player(s).");
			}
		} else if (connectionEvent instanceof ConnectionClosedEvent) {
			getGameLogicService().handleSurrenderRequest(getConnectionPlayerId(connectionEvent.getSource()));
		}
	}

	// Getter & Setter

	protected final WaitingManager getWaitingManager() {
		return waitingManager;
	}

}
