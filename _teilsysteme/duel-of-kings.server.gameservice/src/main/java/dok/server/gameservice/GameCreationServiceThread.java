package dok.server.gameservice;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ExecutorService;

import org.apache.commons.lang3.tuple.Pair;
import org.jboss.logging.Logger;

import dok.commons.model.Lineup;
import dok.commons.network.api.Connection;
import dok.game.model.GameType;
import dok.game.model.UserPlayer;
import dok.server.gamedataservice.api.GameDataService;
import dok.server.gameservice.api.GameService;
import dok.server.loginservice.api.LoginService;
import dok.server.queueservice.api.QueueService;
import dok.server.userdataservice.api.UserDataService;

/**
 * Class responsible for performing the matchmaking of a single type of game.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 *
 */
public class GameCreationServiceThread implements Runnable {

	// Class Constants

	private static final Logger logger = Logger.getLogger(GameCreationServiceThread.class);

	// Constants

	private final GameType gameType;
	private final UserDataService userDataService;
	private final GameService gameService;
	private final LoginService loginService;
	private final GameDataService gameDataService;
	private final QueueService queueService;
	private final ExecutorService executorService;

	// Constructor(s)

	/**
	 * Creates a new game with the given parameters.
	 *
	 * @param executorService The concurrency service who will run the game
	 * @param queueService The service to get the users to matchmake from
	 * @param userDataService The services used to get the player names
	 * @param gameService The service who will be associated with the created games
	 * @param loginService The service to get the user connections from
	 * @param gameDataService The service to provide the game data needed (characters/abilities)
	 * @param gameType The type of game to do the matchmaking for
	 */
	public GameCreationServiceThread(ExecutorService executorService, QueueService queueService, UserDataService userDataService, GameService gameService, LoginService loginService, GameDataService gameDataService, GameType gameType) {
		this.gameType = gameType;
		this.userDataService = userDataService;
		this.gameService = gameService;
		this.loginService = loginService;
		this.gameDataService = gameDataService;
		this.queueService = queueService;
		this.executorService = executorService;
	}

	// Methods

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			try {

				// Fetch responsible services
				Queue<Pair<UUID, Lineup>> queue = getQueueService().getQueue(getGameType());

				// Every gameType has it's own way of coming together
				switch (getGameType()) {
					case Duel:
						// Continue with the next queue if there are not enough players to create a new game
						if (queue.size() < 2) continue;
						else {

							logger.info("Attempting to create game of type: " + getGameType());

							// Remove participants from queue
							Pair<UUID, Lineup> user1 = queue.poll();

							// Check if the polled user is already ingame, for whatever reason
							if (getGameService().getUserGame(user1.getKey()) != null) throw new IllegalStateException("User 1 is already in a game");

							Pair<UUID, Lineup> user2 = queue.poll();

							// Check if the polled user is already ingame, for whatever reason
							if (getGameService().getUserGame(user2.getKey()) != null) throw new IllegalStateException("User 2 is already in a game");

							Map<UserPlayer, Connection> participants = new HashMap<>();

							// Player 1
							participants.put(new UserPlayer(user1.getKey(),
													getUserDataService().getAccount(user1.getKey()).getName(),
													user1.getValue()),
											getLoginService().getUserConnection(user1.getKey()));

							// Player 2
							participants.put(new UserPlayer(user2.getKey(),
									getUserDataService().getAccount(user2.getKey()).getName(),
									user2.getValue()),
							getLoginService().getUserConnection(user2.getKey()));

							// Put the game together
							ServerGame game = new DuelGame(getGameService(), getGameDataService(), getGameDataService(), participants);

							// Mark the users as ingame
							getGameService().defineUserGame(user1.getKey(), game);
							getGameService().defineUserGame(user2.getKey(), game);

							// Start the game
							getExecutorService().execute(game);

							logger.info("New game has been successfully created: " + game);

						}
					break;
				}

			} catch (Exception e) {

				logger.error("New Game could not be created", e);
				break;

			}
		}
	}

	// Getter & Setter

	protected final GameType getGameType() {
		return gameType;
	}

	protected final UserDataService getUserDataService() {
		return userDataService;
	}

	protected final GameService getGameService() {
		return gameService;
	}

	protected final LoginService getLoginService() {
		return loginService;
	}

	protected final GameDataService getGameDataService() {
		return gameDataService;
	}

	protected final QueueService getQueueService() {
		return queueService;
	}

	protected final ExecutorService getExecutorService() {
		return executorService;
	}

}
