package dok.server.gameservice;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.jboss.logging.Logger;

import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionListener;
import dok.game.logic.Game;
import dok.game.model.Player;
import dok.game.model.UserPlayer;
import dok.game.model.api.AbilityDataService;
import dok.game.model.api.CharacterDataService;
import dok.game.services.GameLogicService;
import dok.game.services.GameLogicWrapper;
import dok.server.gameservice.api.GameService;

/**
 * Abstract super class of all server-side games.<br>
 * Fullfills the core functionalities like connection management.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public abstract class ServerGame extends Game implements GameLogicWrapper, Runnable, ConnectionListener {

	// Class Constants

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(ServerGame.class);

	// Attributes

	private final Map<Player, Connection> connections = new HashMap<>();
	private final GameService gameService;
	private GameLogicService gameLogicService;

	// Constructor(s)

	/**
	 * Creates a new game with the given players.<br>
	 * The given character class and ability class services should not
	 * change when instantiating multiple objects of this class.
	 *
	 * @param gameService The service who created this game
	 * @param characterClassService The source of character data
	 * @param abilityClassService The source of ability data
	 * @param participants The players who are participating in this game
	 */
	public ServerGame(GameService gameService, CharacterDataService characterClassService, AbilityDataService abilityClassService, Map<UserPlayer, Connection> participants) {
		super(null, characterClassService, abilityClassService, participants.keySet().toArray(new Player[participants.size()]));

		this.gameService = gameService;

		for (Entry<UserPlayer, Connection> entry : participants.entrySet()) {
			getConnections().put(entry.getKey(), entry.getValue());
		}
	}

	// Getter & Setter

	/**
	 * Returns the player who is representing the given account.
	 *
	 * @param accountId The account whose corressponding player to retrieve
	 * @return The player associated with the given account or null, if there is no such player
	 */
	public Player getUserPlayer(UUID accountId) {
		for (Player player : getPlayers()) {
			if (player instanceof UserPlayer) {
				UserPlayer userPlayer = (UserPlayer) player;
				if (userPlayer.getAccountId().equals(accountId)) return player;
			}
		}
		return null;
	}

	/**
	 * @param playerid The id of the player whose associated connection to retrieve
	 * @return The connection associated with the given player id or null, if there is no such connection
	 */
	public Connection getPlayerConnection(UUID playerid) {
		for (Entry<Player, Connection> entry : getConnections().entrySet()) {
			if (entry.getKey().getPlayerId().equals(playerid)) return entry.getValue();
		}
		return null;
	}

	/**
	 * @param conn The connection whose associated player to retrieve
	 * @return The id of the player associated with the given connection or null, if there is no such player
	 */
	public UUID getConnectionPlayerId(Connection conn) {
		for (Entry<Player, Connection> entry : getConnections().entrySet()) {
			if (entry.getValue().equals(conn)) return entry.getKey().getPlayerId();
		}
		return null;
	}

	/**
	 * @param conn The connection whose associated player to retrieve
	 * @return The player associated with the given connection or null, if there is no such player
	 */
	public Player getConnectionPlayer(Connection conn) {
		for (Entry<Player, Connection> entry : getConnections().entrySet()) {
			if (entry.getValue().equals(conn)) return entry.getKey();
		}
		return null;
	}

	/**
	 * @return The player connection mapping
	 */
	public Map<Player, Connection> getConnections() {
		return connections;
	}

	/**
	 * @return The service responsible for the game logic
	 */
	public final GameLogicService getGameLogicService() {
		return gameLogicService;
	}

	protected final void setGameLogicService(GameLogicService GameLogicService) {
		this.gameLogicService = GameLogicService;
	}

	/**
	 * @return The service which created this game
	 */
	public GameService getGameService() {
		return gameService;
	}

}
