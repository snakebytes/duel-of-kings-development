package dok.server.gameservice.api;

import java.util.UUID;

import javax.ejb.Local;

import dok.game.model.GameType;
import dok.server.gameservice.ServerGame;
import dok.server.queueservice.event.QueueEnteredEvent;

/**
 * Service responsible for the matchmaking system, creating games for matchmade players
 * and holding the game-state of all players/games. Requires an active queuing system to work.
 *
 * @author Konstantin Schaper
 * @since 0.2.61
 *
 */
@Local
public interface GameService {

	/**
	 * Attempts to start creating games (matchmaking) for the given game type.<br>
	 * Does not have any effect if the system already is creating games of the given type.
	 *
	 * @param gameType The game type for which to start the matchmaking system
	 */
	public void startCreatingGames(GameType gameType);

	/**
	 * @param accountId The account whose in-game status to check
	 * @return Whether or not the given user is currently playing
	 * @see #getUserGame(UUID)
	 */
	public boolean isUserInGame(UUID accountId);

	/**
	 * Marks the given user as not in-game.
	 *
	 * @param accountId The user to mark as not in-game
	 * @return Whether the user's game status has been successfully changed
	 */
	public boolean removeUserGameMapping(UUID accountId);

	/**
	 * @param accountId The account whose game status to check
	 * @return The game the given user is currently in or null, if the given user is not actually playing at the moment
	 */
	public ServerGame getUserGame(UUID accountId);

	/**
	 * Marks the given user as playing the given game.
	 *
	 * @param accountId The user whose game status to change
	 * @param game The game the given user is now playing in
	 */
	public void defineUserGame(UUID accountId, ServerGame game);

	/**
	 * Called on the implementing class when a queuing module on the server fires a
	 * queue entered event.
	 *
	 * @param event The event which has been fired
	 */
	public void onQueueEntered(QueueEnteredEvent event);

}
