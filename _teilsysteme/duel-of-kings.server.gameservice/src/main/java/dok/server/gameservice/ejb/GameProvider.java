package dok.server.gameservice.ejb;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import dok.game.model.GameType;
import dok.server.gamedataservice.api.GameDataService;
import dok.server.gameservice.GameCreationServiceThread;
import dok.server.gameservice.ServerGame;
import dok.server.gameservice.api.GameService;
import dok.server.loginservice.api.LoginService;
import dok.server.queueservice.api.QueueService;
import dok.server.queueservice.event.QueueEnteredEvent;
import dok.server.userdataservice.api.UserDataService;

/**
 * Singleton implementation of the {@linkplain GameService} business interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
public class GameProvider implements GameService {

	// Class Constants

	private static final Logger logger = Logger.getLogger(GameProvider.class);

	//Attributes

	private final Map<UUID, ServerGame> userGameMappings = new HashMap<>();
	private final Map<GameType, Thread> gameCreationThreads = new HashMap<>();

	// Injections

	@Resource
	private ManagedThreadFactory threadFactory;

	@Resource
	private ManagedExecutorService executorService;

	@Inject
	private LoginService loginService;

	@Inject
	private QueueService queueService;

	@Inject
	private UserDataService userDataService;

	@Inject
	private GameDataService gameDataService;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public GameProvider() {
		// Do nothing
	}

	// Events

	@Override
	public void onQueueEntered(@Observes QueueEnteredEvent event) {
		logger.info("Handling queue event: " + event);
		startCreatingGames(event.getGameType());
	}

	// <--- GameService --->

	@Override
	public boolean isUserInGame(UUID accountId) {
		return getUserGameMappings().get(accountId) != null;
	}

	@Override
	public ServerGame getUserGame(UUID accountId) {
		return getUserGameMappings().get(accountId);
	}

	@Override
	public void defineUserGame(UUID accountId, ServerGame game) {
		getUserGameMappings().put(accountId, game);
	}

	@Override
	public boolean removeUserGameMapping(UUID accountId) {
		if (getUserGameMappings().remove(accountId) == null) {
			logger.warn("The user " + accountId + " could not be removed from his current game");
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void startCreatingGames(GameType gameType) {
		// Start creating games
		if (getGameCreationThreads().get(gameType) == null || !getGameCreationThreads().get(gameType).isAlive()) {
			Thread t = getThreadFactory().newThread(new GameCreationServiceThread(getExecutorService(), getQueueService(), getUserDataService(),
																					this, getLoginService(), getGameDataService(), gameType));
			getGameCreationThreads().put(gameType, t);
			t.setName("GameCreationServiceThread(" + gameType + ")");
			t.setDaemon(true);
			t.start();

			logger.info("Starting to create games of type: " + gameType);
		}
	}

	public void stopCreatingGames(GameType gameType) {
		if (getGameCreationThreads().get(gameType) != null && getGameCreationThreads().get(gameType).isAlive())
			getGameCreationThreads().get(gameType).interrupt();
	}

	// Getter & Setter

	protected final Map<UUID, ServerGame> getUserGameMappings() {
		return userGameMappings;
	}

	protected final Map<GameType, Thread> getGameCreationThreads() {
		return gameCreationThreads;
	}

	protected final ManagedThreadFactory getThreadFactory() {
		return threadFactory;
	}

	protected final void setThreadFactory(ManagedThreadFactory threadFactory) {
		this.threadFactory = threadFactory;
	}

	protected final LoginService getLoginService() {
		return loginService;
	}

	protected final void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	protected final QueueService getQueueService() {
		return queueService;
	}

	protected final void setQueueService(QueueService queueService) {
		this.queueService = queueService;
	}

	protected final UserDataService getUserDataService() {
		return userDataService;
	}

	protected final void setUserDataService(UserDataService userDataService) {
		this.userDataService = userDataService;
	}

	protected final GameDataService getGameDataService() {
		return gameDataService;
	}

	protected final void setGameDataService(GameDataService gameDataService) {
		this.gameDataService = gameDataService;
	}

	protected final ManagedExecutorService getExecutorService() {
		return executorService;
	}

	protected final void setExecutorService(ManagedExecutorService executorService) {
		this.executorService = executorService;
	}

}
