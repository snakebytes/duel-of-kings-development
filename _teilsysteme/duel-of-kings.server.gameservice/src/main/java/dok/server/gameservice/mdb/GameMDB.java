package dok.server.gameservice.mdb;

import java.util.UUID;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import dok.commons.model.LoginSession;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.server.gameservice.ServerGame;
import dok.server.gameservice.api.GameService;
import dok.server.gameservice.ejb.GameProvider;
import dok.server.loginservice.api.LoginService;

/**
 * Message Driven Bean which asynchronously forwards all game related connection
 * events to the associated game.
 */
@MessageDriven(messageListenerInterface = ConnectionListener.class)
@ResourceAdapter(value = "duel-of-kings.server.ear#duel-of-kings.server.network.adapter.rar")
public class GameMDB implements ConnectionListener {

	// Class Constants

	private static final Logger logger = Logger.getLogger(GameMDB.class);

	// Injections

	@Inject
	private GameService gameService;

	@Inject
	private LoginService loginService;

	@Resource
	private ManagedExecutorService executorService;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public GameMDB() {
		// Do nothing
	}

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		try {
			LoginSession session = getLoginService().getConnectionSession(connectionEvent.getSource());
			if (session != null) {
				UUID accountId = session.getAccountId();
				if (accountId != null) {
					ServerGame userGame = getGameService().getUserGame(accountId);
					if (userGame != null) {
						getExecutorService().submit(() -> userGame.handle(connectionEvent));
					}
				}
			}
		} catch (Throwable e) {
			logger.error("An error occurred while process a game-related connection event", e );
		}
	}

	// Getter & Setter

	protected final GameService getGameService() {
		return gameService;
	}

	protected final void setGameService(GameService gameService) {
		this.gameService = gameService;
	}

	protected final LoginService getLoginService() {
		return loginService;
	}

	protected final void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	protected final ManagedExecutorService getExecutorService() {
		return executorService;
	}

	protected final void setExecutorService(ManagedExecutorService executorService) {
		this.executorService = executorService;
	}

}
