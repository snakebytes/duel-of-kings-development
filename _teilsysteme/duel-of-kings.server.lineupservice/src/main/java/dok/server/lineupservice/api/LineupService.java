package dok.server.lineupservice.api;

import java.util.List;
import java.util.UUID;

import javax.ejb.Local;

import dok.commons.model.Lineup;

/**
 * Service responsible for saving and retrieving user lineups.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Local
public interface LineupService {

	/**
	 * @param lineupId The unique identifier of the lineup to retrieve
	 * @return The lineup with the given id or null, if there is no such lineup
	 */
	public Lineup getLineup(long lineupId);

	/**
	 * @param accountId The user whose lineups to check for the given name
	 * @param lineupName The name of the lineup to retrieve
	 * @return A lineup of the given user with the given name or null,
	 * 			if the given user does not have a lineup with that name
	 */
	public Lineup getUserLineup(UUID accountId, String lineupName);

	/**
	 * @param accountId
	 * @return All lineups of the given user. Always returns at least an empty list
	 */
	public List<Lineup> getUserLineups(UUID accountId);

	/**
	 * Attempts to save the given lineup to the underlying data store.
	 *
	 * @param lineup The lineup to persist
	 * @return Whether or not the lineup has been successfully saved
	 */
	public boolean saveLineup(Lineup lineup);

}
