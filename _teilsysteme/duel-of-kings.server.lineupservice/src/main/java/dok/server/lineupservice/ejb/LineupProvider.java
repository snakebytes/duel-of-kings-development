package dok.server.lineupservice.ejb;

import java.util.List;
import java.util.UUID;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import dok.commons.model.Lineup;
import dok.server.lineupservice.api.LineupService;
import dok.server.userdataservice.api.UserDataService;

/**
 * Sole implementation of the {@linkplain LineupService} business interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Stateless
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
public class LineupProvider implements LineupService {

	// Class Constants

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(LineupProvider.class);

	// Injection

	@Inject
	private UserDataService userDataService;

	// <--- ServerLineupService --->

	@Override
	public Lineup getLineup(long lineupId) throws IllegalArgumentException {
		try {
			return userDataService.getObjectById(Lineup.class, lineupId);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Lineup getUserLineup(UUID accountId, String lineupName) {
		return userDataService.getUserLineup(accountId, lineupName);
	}

	@Override
	public List<Lineup> getUserLineups(UUID accountId) throws IllegalArgumentException {
		return userDataService.getUserLineups(accountId);
	}

	@Override
	public boolean saveLineup(Lineup lineup) throws IllegalArgumentException {
		return userDataService.saveLineup(lineup);
	}

}
