package dok.server.lineupservice.mdb;

import java.io.IOException;
import java.util.List;

import javax.ejb.MessageDriven;
import javax.inject.Inject;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import dok.commons.model.Lineup;
import dok.commons.model.LoginSession;
import dok.commons.model.StatusCode;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.game.network.model.LineupUpdatePacket;
import dok.game.network.model.SaveLineupPacket;
import dok.game.network.model.SaveLineupResultPacket;
import dok.server.lineupservice.api.LineupService;
import dok.server.loginservice.api.LoginService;
import dok.server.userdataservice.api.UserDataService;

/**
 * Message driven bean which handles all lineup related requests.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@MessageDriven(messageListenerInterface=ConnectionListener.class)
@ResourceAdapter(value = "duel-of-kings.server.ear#duel-of-kings.server.network.adapter.rar")
public class LineupMDB implements ConnectionListener {

	// Class Constants

	private static final Logger logger = Logger.getLogger(LineupMDB.class);

	// Injection

	@Inject
	private UserDataService userDataService;

	@Inject
	private LineupService lineupService;

	@Inject
	private LoginService loginService;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public LineupMDB() {
		// Do nothing
	}

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;

			// Save-Lineup requests
			if (packetReceivedEvent.getPacket() instanceof SaveLineupPacket) {
				SaveLineupPacket saveLineupPacket = (SaveLineupPacket) packetReceivedEvent.getPacket();

				try {

					// Check if user is logged in
					LoginSession session = null;
					try {
						session = getLoginService().getConnectionSession(connectionEvent.getSource());
					} catch (Exception e) {
						logger.debug("User connection status could not be determined", e);
					}

					if (session != null) {
						// Verify user
						if (session.getAccountId().equals(saveLineupPacket.getLineup().getAccountId())) {

							// Attempt to save the lineup
							if(getLineupService().saveLineup(saveLineupPacket.getLineup()))
								connectionEvent.getSource().sendPacket(new SaveLineupResultPacket(StatusCode.OK, saveLineupPacket.getLineup()));
							else
								connectionEvent.getSource().sendPacket(new SaveLineupResultPacket(StatusCode.INTERNAL_SERVER_ERROR));

						} else {
							// The user who sent this packet is not the owner of the given lineup!
							connectionEvent.getSource().sendPacket(new SaveLineupResultPacket(StatusCode.FORBIDDEN));
						}

					} else {

						// The user is not logged in!
						connectionEvent.getSource().sendPacket(new SaveLineupResultPacket(StatusCode.UNAUTHORIZED));

					}

				} catch (IOException e) {

					logger.debug("Packet could not be sent", e);

				}

			} else if (packetReceivedEvent.getPacket() instanceof LineupUpdatePacket) {

				// Check if user is logged in
				LoginSession session = null;
				try {
					session = getLoginService().getConnectionSession(connectionEvent.getSource());
				} catch (Exception e) {
					logger.warn("User connection status could not be determined", e);
				}

				if (session != null) {

					List<Lineup> lineups = getLineupService().getUserLineups(session.getAccountId());
					try {
						packetReceivedEvent.getSource().sendPacket(new LineupUpdatePacket(StatusCode.OK, lineups));
					} catch (IOException e) {
						logger.debug("Packet could not be sent", e);
					}

				} else {

					try {
						packetReceivedEvent.getSource().sendPacket(new LineupUpdatePacket(StatusCode.UNAUTHORIZED, null));
					} catch (IOException e) {
						logger.debug("Packet could not be sent", e);
					}

				}
			}
		}
	}

	// Getter & Setter

	protected final UserDataService getUserDataService() {
		return userDataService;
	}

	protected final void setUserDataService(UserDataService userDataService) {
		this.userDataService = userDataService;
	}

	protected final LineupService getLineupService() {
		return lineupService;
	}

	protected final void setLineupService(LineupService lineupService) {
		this.lineupService = lineupService;
	}

	protected final LoginService getLoginService() {
		return loginService;
	}

	protected final void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

}
