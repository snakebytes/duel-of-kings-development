package dok.server.loginservice.api;

import java.util.Map.Entry;
import java.util.UUID;

import javax.ejb.Local;

import dok.commons.model.LoginMethod;
import dok.commons.model.LoginSession;
import dok.commons.network.api.Connection;
import dok.server.loginservice.event.LoginEvent;
import dok.server.loginservice.event.LogoutEvent;
import dok.server.loginservice.exception.LoginException;

/**
 * Service responsible for authentication and managing the login state of users.<br>
 * Only accessible in local context (i.e. the application server).
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Local
public interface LoginService {

	// General authentication methods

	/**
	 * Attempts to login the user by first verifying the password and
	 * afterwards starting to manage a freshly created {@linkplain LoginSession}.
	 * Fires {@linkplain LoginEvent}s on successful login.
	 *
	 * @param username The provided username
	 * @param password The provided password
	 * @param loginMethod The type of login chosen
	 * @return The session, by which the user has been marked as logged in
	 * @throws LoginException If the login was unsuccessful
	 */
	public LoginSession login(String username, String password, LoginMethod loginMethod) throws LoginException;

	/**
	 * Attempts to login the user by first verifying the password and
	 * afterwards starting to manage a freshly created {@linkplain LoginSession}.
	 * The given connection becomes associated with the returned session.
	 * Fires {@linkplain LoginEvent}s on successful login.
	 *
	 * @param connection The connection through which the login request has been received
	 * @param username The provided username
	 * @param password The provided password
	 * @return The session, by which the user has been marked as logged in
	 * @throws LoginException If the login was unsuccessful
	 * @see #login(String, String, LoginMethod)
	 */
	public LoginSession login(Connection connection, String username, String password) throws LoginException;

	/**
	 * Attempts to unmanage the given session.<br>
	 * Fires {@linkplain LogoutEvent}s on successful logout.
	 *
	 * @param session The session to log out
	 * @return Whether the session has been successfully logged out
	 */
	public boolean logout(LoginSession session);

	/**
	 * Attempts to unmanage the session associated with the given connection.<br>
	 * Fires {@linkplain LogoutEvent}s on successful logout.
	 *
	 * @param connection The connection whose session to log out
	 * @return Whether the associated session has been successfully logged out
	 * @see #logout(LoginSession)
	 */
	public boolean logout(Connection connection);

	/**
	 * @param accountId The unique identifier of the account whose login status to check
	 * @return Whether there is a managed session associated with the given account id
	 */
	public boolean isLoggedIn(UUID accountId);

	/**
	 * @param session The session whose management status to check
	 * @return Whether the given session is managed
	 */
	public boolean isLoggedIn(LoginSession session);

	// Connection-state methods

	/**
	 * @param accountId The unique identifier of the account whose managed connection to retrieve
	 * @return The managed connection of the given account or null, if the account is not logged in (via game client)
	 */
	public Connection getUserConnection(UUID accountId);

	/**
	 * @param session The session whose associated connection to retrieve
	 * @return The managed connection of the given session or null, if the session is not logged in (via game client)
	 */
	public Connection getSessionConnection(LoginSession session);

	/**
	 * @param conn The connection whose associated session to retrieve
	 * @return The managed session of the given connection or null, if the connection is not managed
	 */
	public LoginSession getConnectionSession(Connection conn);

	/**
	 *
	 * @param conn The connection whose associated session to retrieve
	 * @return A map entry containing both the given connection and its associated session or null, if the connection is not managed
	 */
	public Entry<LoginSession, Connection> getConnectionEntry(Connection conn);

}
