package dok.server.loginservice.ejb;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import dok.commons.model.Account;
import dok.commons.model.LoginMethod;
import dok.commons.model.LoginSession;
import dok.commons.model.StatusCode;
import dok.commons.network.api.Connection;
import dok.server.loginservice.api.LoginService;
import dok.server.loginservice.event.LoginEvent;
import dok.server.loginservice.event.LogoutEvent;
import dok.server.loginservice.event.impl.LoginEventImpl;
import dok.server.loginservice.event.impl.LogoutEventImpl;
import dok.server.loginservice.exception.LoginException;
import dok.server.passwordservice.api.PasswordService;
import dok.server.userdataservice.api.UserDataService;

/**
 * Singleton-implementation of the {@linkplain LoginService} interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
public class LoginProvider implements LoginService {

	// Class Constants

	private static final Logger logger = Logger.getLogger(LoginProvider.class);

	// Constants

	private final Map<LoginSession, Connection> managedSessions = new HashMap<>();

	// Dependency Injection

	@Inject
	private Event<LoginEvent> loginEvents;

	@Inject
	private Event<LogoutEvent> logoutEvents;

	@Inject
	private PasswordService passwordService;

	@Inject
	private UserDataService userDataService;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public LoginProvider() {
		// Do nothing
	}

	// <--- LoginService --->

	@Override
	public boolean isLoggedIn(UUID accountId) {
		Optional<Entry<LoginSession, Connection>> result = getManagedSessions().entrySet().stream()
				.filter((entry) -> entry.getKey().getAccountId().equals(accountId)).findAny();
		try {
			return result.orElse(null) != null;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public boolean isLoggedIn(LoginSession session) {
		Optional<Entry<LoginSession, Connection>> result = getManagedSessions().entrySet().stream()
				.filter((entry) -> entry.getKey().equals(session)).findAny();
		try {
			return result.orElse(null) != null;
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	@Lock(LockType.WRITE)
	public LoginSession login(Connection connection, String username, String password) throws LoginException {
		if (connection == null) throw new LoginException(StatusCode.BAD_REQUEST, "Connection must not be null");
		if (username == null || username.trim().equals("")) throw new LoginException(StatusCode.BAD_REQUEST, "Username must not be null or empty");
		if (password == null || password.trim().equals("")) throw new LoginException(StatusCode.BAD_REQUEST, "Password must not be null or empty");

		LoginSession session = login(username, password, LoginMethod.CLIENT);
		if (session != null && addManagedSession(session, connection)) {
			getLoginEvents().fire(new LoginEventImpl(session, connection));
			return session;
		} else throw new LoginException(StatusCode.INTERNAL_SERVER_ERROR);
	}

	@Override
	@Lock(LockType.WRITE)
	public LoginSession login(String username, String password, LoginMethod loginMethod) throws LoginException {
		logger.debug("Attempting to login user ...");
		if (getUserDataService() != null) {

			// Fetch desired account
			Account account = null;
			try {

				account = getUserDataService().getAccount(username);

			} catch (Exception e) {

				// Account could not be retrieved from data store
				logger.debug("Account could not be retrieved from data store", e);
				throw new LoginException(StatusCode.INTERNAL_SERVER_ERROR, e);

			}

			// Check if the desired account actually exists
			if (account != null) {

				// Check if the requested user is already logged in
				if (!isLoggedIn(account.getId())) {

					if (getPasswordService() != null) {

						// Verify password
						if (getPasswordService().validatePassword(password, account.getPasswordHash())) {

							// Create LoginSession
							LoginSession loginSession = new LoginSession(account.getId(), new Date(),
									LoginMethod.CLIENT);

							// Check if the session has been successfully
							// persisted
							if (getUserDataService().saveOrUpdate(loginSession)) {

								logger.debug(username + " has been successfully logged in");

								// Client login sessions are started to be
								// managed at another point
								if (loginMethod != LoginMethod.CLIENT) {
									addManagedSession(loginSession, null);
									getLoginEvents().fire(new LoginEventImpl(loginSession, null));
								}

								return loginSession;

							} else {

								logger.warn("LoginSession could not be stored");
								throw new LoginException(StatusCode.INTERNAL_SERVER_ERROR);

							}

						} else {

							logger.debug("Password was incorrect");
							throw new LoginException(StatusCode.BAD_REQUEST);

						}

					} else {

						logger.warn("Password Service could not be found");
						throw new LoginException(StatusCode.SERVICE_UNAVAILABLE);

					}

				} else {

					logger.debug("There already is an active session tied to this account");
					throw new LoginException(StatusCode.LOCKED);

				}

			} else {

				logger.debug("Account does not exist");
				throw new LoginException(StatusCode.BAD_REQUEST);

			}

		} else {

			logger.warn("Persistence service is not available");
			throw new LoginException(StatusCode.SERVICE_UNAVAILABLE);

		}
	}

	@Override
	@Lock(LockType.WRITE)
	public boolean logout(LoginSession session) {
		// Check if user is not even logged in
		if (session == null)
			return true;

		try {

			if (getUserDataService() != null) {

				// Save logout date
				session.setLoggedOut(new Date());

				try {

					getUserDataService().saveOrUpdate(session);

				} catch (Throwable e) {

					logger.warn("LoginSession could not be updated", e);

				}

				// Unmanage session
				Connection sessionConnection = removeManagedSession(session);

				// Fire Event
				getLogoutEvents().fire(new LogoutEventImpl(session, sessionConnection));

				logger.debug(session.getAccountId() + " has been successfully logged out");

				return true;

			} else {

				logger.warn("UserDataService is not available");
				return false;

			}

		} catch (Throwable e) {

			logger.warn("An unexpected error occurred", e);
			return false;

		}
	}

	@Override
	@Lock(LockType.WRITE)
	public boolean logout(Connection connection) {
		return logout(getConnectionSession(connection));
	}

	@Override
	public Connection getSessionConnection(LoginSession session) {
		return getManagedSessions().get(session);
	}

	@Override
	public Connection getUserConnection(UUID accountId) {
		Optional<Entry<LoginSession, Connection>> result = getManagedSessions().entrySet().stream()
				.filter((entry) -> entry.getKey().getAccountId().equals(accountId)).findAny();

		if (result == null)
			return null;
		else {
			try {
				return result.get().getValue();
			} catch (Exception e) {
				return null;
			}
		}
	}

	@Override
	public LoginSession getConnectionSession(final Connection conn) {
		try {
			return getConnectionEntry(conn).getKey();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Entry<LoginSession, Connection> getConnectionEntry(final Connection conn) {
		try {
			return getManagedSessions().entrySet().stream().filter((entry) -> entry.getValue().equals(conn))
					.findFirst().orElse(null);
		} catch (Exception e) {
			return null;
		}
	}

	// Getter & Setter

	/**
	 * Adds the given session to the set of managed sessions, effectively
	 * logging in the user associated with the given session. The given
	 * connection may be null, if the {@linkplain LoginMethod} used was not
	 * {@linkplain LoginMethod#CLIENT}.
	 *
	 * @param session
	 *            The session to associate with the given connection
	 * @param connection
	 *            The connection to associate with the given session
	 * @return Whether the given combination has been stored successfully
	 */
	@Lock(LockType.WRITE)
	protected final boolean addManagedSession(LoginSession session, Connection connection) {
		if (session == null)
			return false;
		try {
			if (getManagedSessions().putIfAbsent(session, connection) != null)
				return false;
			else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Effectively logs out the client associated with the given session.
	 *
	 * @param session
	 *            The session to log out
	 * @return The connection previously associated with the given session or
	 *         null, if no user was connected with the given session or if the
	 *         user has not logged in via the game client
	 */
	@Lock(LockType.WRITE)
	protected final Connection removeManagedSession(LoginSession session) {
		if (session == null)
			return null;
		else {
			return getManagedSessions().remove(session);
		}
	}

	// Getter & Setter

	protected final Entry<LoginSession, Connection> getConnectionEntry(final LoginSession session) {
		Optional<Entry<LoginSession, Connection>> result = getManagedSessions().entrySet().stream()
				.filter((entry) -> entry.getKey().equals(session)).findFirst();
		try {
			return result.orElse(null);
		} catch (Exception e) {
			return null;
		}
	}

	protected final PasswordService getPasswordService() {
		return passwordService;
	}

	protected final void setPasswordService(PasswordService passwordService) {
		this.passwordService = passwordService;
	}

	protected final UserDataService getUserDataService() {
		return userDataService;
	}

	protected final void setUserDataService(UserDataService userDataService) {
		this.userDataService = userDataService;
	}

	protected final Event<LoginEvent> getLoginEvents() {
		return loginEvents;
	}

	protected final void setLoginEvents(Event<LoginEvent> loginEvents) {
		this.loginEvents = loginEvents;
	}

	protected final Event<LogoutEvent> getLogoutEvents() {
		return logoutEvents;
	}

	protected final void setLogoutEvents(Event<LogoutEvent> logoutEvents) {
		this.logoutEvents = logoutEvents;
	}

	protected final Map<LoginSession, Connection> getManagedSessions() {
		return managedSessions;
	}

}
