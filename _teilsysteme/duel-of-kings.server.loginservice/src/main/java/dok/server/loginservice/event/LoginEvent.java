package dok.server.loginservice.event;

import dok.commons.model.LoginMethod;
import dok.commons.model.LoginSession;
import dok.commons.network.api.Connection;
import dok.server.loginservice.api.LoginService;

/**
 * A login event is fired whenever a {@linkplain LoginSession} has been successfully created and managed by a {@linkplain LoginService}.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public interface LoginEvent {

	/**
	 * @return The newly created session
	 */
	public LoginSession getSession();

	/**
	 * @return The connection associated with the session or null,
	 * 			if the sessions login method was not {@linkplain LoginMethod#CLIENT}.
	 */
	public Connection getConnection();

}
