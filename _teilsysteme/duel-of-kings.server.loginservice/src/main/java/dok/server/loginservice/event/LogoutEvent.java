package dok.server.loginservice.event;

import dok.commons.model.LoginMethod;
import dok.commons.model.LoginSession;
import dok.commons.network.api.Connection;
import dok.server.loginservice.api.LoginService;

/**
 * A logout event is fired whenever a {@linkplain LoginSession} is invalidated by a {@linkplain LoginService}.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public interface LogoutEvent {

	/**
	 * @return The session which has just been invalidated
	 */
	public LoginSession getSession();

	/**
	 * @return The connection previously associated with the session or null,
	 * 			if the session's login method was not {@linkplain LoginMethod#CLIENT}.
	 */
	public Connection getConnection();

}
