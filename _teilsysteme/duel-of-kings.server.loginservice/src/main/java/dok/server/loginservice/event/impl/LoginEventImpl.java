package dok.server.loginservice.event.impl;

import dok.commons.model.LoginSession;
import dok.commons.network.api.Connection;
import dok.server.loginservice.event.LoginEvent;

/**
 * Sole implementation of the {@linkplain LoginEvent} interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public class LoginEventImpl implements LoginEvent {

	// Attributes

	private final LoginSession loginSession;
	private final Connection connection;

	// Constructor(s)

	/**
	 * Initializes the event with the given parameters.
	 *
	 * @param loginSession The session which has been managed
	 * @param connection The connection which is associated with the given session, may be null
	 */
	public LoginEventImpl(LoginSession loginSession, Connection connection) {
		super();
		this.loginSession = loginSession;
		this.connection = connection;
	}

	// Methods

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{"
				+ "loginSession: " + getSession()
				+ ", connection: " + getConnection()
				+ "}";
	}

	// Getter & Setter

	@Override
	public LoginSession getSession() {
		return loginSession;
	}

	@Override
	public Connection getConnection() {
		return connection;
	}

}
