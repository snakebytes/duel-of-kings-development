package dok.server.loginservice.exception;

import dok.commons.model.StatusCode;

/**
 * Signals an error occurred on a login attempt.<br>
 * Contains an error code, specifying the cause.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public class LoginException extends Exception {

	// Class Constants

	private static final long serialVersionUID = -2534417403718240467L;

	// Attributes

	private final StatusCode errorCode;

	// Constructor(s)

	/**
	 * Initializes the exception with the given error code.
	 *
	 * @param errorCode The type of error which caused the exception
	 */
	public LoginException(StatusCode errorCode) {
		super();
		this.errorCode = errorCode;
	}

	/**
	 * Initializes the exception with the given error code and message.
	 *
	 * @param errorCode The type of error which caused the exception
	 * @param message The error message
	 */
	public LoginException(StatusCode errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * Initializes the exception with the given error code and message.
	 * Also defines the cause of the exception, if it originated from another error.
	 *
	 * @param errorCode The type of error which caused the exception
	 * @param message The error message
	 * @param cause The error which lead to this exception
	 */
	public LoginException(StatusCode errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	/**
	 * Initializes the exception with the given error code and cause.
	 *
	 * @param errorCode The type of error which caused the exception
	 * @param cause The error which lead to this exception
	 */
	public LoginException(StatusCode errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}

	// Getter & Setter

	/**
	 * @return The type of error which caused the exception
	 */
	public StatusCode getErrorCode() {
		return errorCode;
	}

}
