package dok.server.loginservice.mdb;

import java.io.IOException;

import javax.ejb.MessageDriven;
import javax.inject.Inject;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import dok.commons.model.LoginSession;
import dok.commons.model.StatusCode;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.events.ConnectionClosedEvent;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.commons.network.api.packets.LoginPacket;
import dok.commons.network.api.packets.LoginResultPacket;
import dok.commons.network.core.packets.LoginResultPacketImpl;
import dok.server.loginservice.api.LoginService;
import dok.server.loginservice.exception.LoginException;

/**
 * Message Driven Bean which handles login and logout requests.
 */
@MessageDriven(messageListenerInterface=ConnectionListener.class)
@ResourceAdapter(value = "duel-of-kings.server.ear#duel-of-kings.server.network.adapter.rar")
public class LoginMDB implements ConnectionListener {

	// Class Constants

	private static final Logger logger = Logger.getLogger(LoginMDB.class);

	// Injections

	@Inject
	private LoginService loginService;

	// Constructor(s)

    /**
     * Default constructor for java bean specification.
     */
    public LoginMDB() {
        // Do nothing
    }

    // <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		Connection connection = connectionEvent.getSource();

		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;

			if (packetReceivedEvent.getPacket() instanceof LoginPacket) {
				LoginPacket loginPacket = (LoginPacket) packetReceivedEvent.getPacket();

				// Unpack the packet
				String providedAccountName = loginPacket.getAccountName();
				String providedPassword = loginPacket.getAccountPassword();

				// Catch any form of IOExceptions (Response-packets which could not
				// be sent)
				try {

					// Check if login service is available
					if (getLoginService() != null) {

						try {

							// Attempt to login user with provided credentials
							LoginSession loginSession = getLoginService().login(connection, providedAccountName, providedPassword);

							// Send successful-login response
							connection.sendPacket(new LoginResultPacketImpl(StatusCode.OK, loginSession));

						} catch (LoginException e) {

							if (e.getErrorCode() == null) {

								logger.warn("LoginException error code is null");
								connection.sendPacket(new LoginResultPacketImpl(StatusCode.INTERNAL_SERVER_ERROR));

							} else {

								connection.sendPacket(new LoginResultPacketImpl(e.getErrorCode()));

							}

						} catch (Exception e) {

							// Unexpected error
							logger.warn("An unexpected error occurred on user login", e);
							connection.sendPacket(new LoginResultPacketImpl(StatusCode.INTERNAL_SERVER_ERROR));

						}

					} else {

						// Login Service is not available
						connection.sendPacket(new LoginResultPacketImpl(StatusCode.SERVICE_UNAVAILABLE));

					}

				} catch (IOException e) {

					logger.warn(LoginResultPacket.class.getSimpleName() + " could not be sent", e);

				}
			}
		} else if (connectionEvent instanceof ConnectionClosedEvent) {

			ConnectionClosedEvent connectionClosedEvent = (ConnectionClosedEvent) connectionEvent;

			if (getLoginService() != null) {

				getLoginService().logout(connectionClosedEvent.getSource());

			} else {

				logger.warn("LoginService is not available");

			}

		}
	}

    // Getter & Setter

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

}
