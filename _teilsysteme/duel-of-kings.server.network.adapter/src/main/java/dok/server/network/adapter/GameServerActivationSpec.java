package dok.server.network.adapter;

import java.io.Serializable;

import javax.resource.ResourceException;
import javax.resource.spi.Activation;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.InvalidPropertyException;
import javax.resource.spi.ResourceAdapter;

import dok.commons.network.api.ConnectionListener;

/**
 * This activation spec is required by all message endpoints who attempt
 * to connection to the {@linkplain GameServerResourceAdapter}.<br>
 * <br>
 * Holds an instance to the resource adapter the calling message endpoint
 * is connected to.<br>
 * <br>
 * Does not expose or require any additional configuration.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 * @see ActivationSpec
 */
@Activation(messageListeners={ConnectionListener.class})
public class GameServerActivationSpec implements ActivationSpec, Serializable {

	// Class Constants

	private static final long serialVersionUID = -648221942279072745L;

	// Attributes

	private GameServerResourceAdapter adapter = null;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public GameServerActivationSpec() {
		// Do nothing
	}

	// Methods

	@Override
	public void validate() throws InvalidPropertyException {
		// Do nothing, as there is no configuration required
	}

	// Getter & Setter

	@Override
	public ResourceAdapter getResourceAdapter() {
		return adapter;
	}

	@Override
	public void setResourceAdapter(ResourceAdapter resourceAdapter) throws ResourceException {
		try {
			this.adapter = (GameServerResourceAdapter) resourceAdapter;
		} catch (ClassCastException e) {
			throw new ResourceException(e);
		}
	}

}
