package dok.server.network.adapter;

import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

import javax.resource.NotSupportedException;
import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.Connector;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.UnavailableException;
import javax.resource.spi.endpoint.MessageEndpoint;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.resource.spi.work.Work;
import javax.resource.spi.work.WorkManager;
import javax.transaction.xa.XAResource;

import org.jboss.logging.Logger;

import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.core.NetworkConstants;
import dok.commons.network.core.util.SSLContextFactory;
import dok.server.network.api.Server;
import dok.server.network.core.SSLServer;

/**
 * Exposes {@linkplain ServerSocket}-like functionality to the rest
 * of the enterprise application.<br>
 * <br>
 * Message Driven Beans can connect to this Resource Adapter to
 * receive any {@linkplain ConnectionEvent}s fired from the underlying
 * {@linkplain Server} instance.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Connector(displayName="GameServer",
			eisType="GAME",
			licenseRequired=false,
			vendorName="SnakeBytes",
			version="0.1.0")
public class GameServerResourceAdapter implements ResourceAdapter, Work, ConnectionListener {

	// Class Constants

	private static final Logger logger = Logger.getLogger(GameServerResourceAdapter.class);

	// Inner Classes

	/**
	 * Implementation of the {@linkplain Executor} interface which
	 * delegates all asynchronous execution requests to the work manager
	 * of the resource adapter and thereby to the application server.
	 */
	private class WorkManagerExecutor implements Executor {

		@Override
		public void execute(Runnable command) {
			try {
				getWorkManager().scheduleWork(new ConnectionWork((Connection) command));
			} catch (Exception e) {
				logger.error("Work could not be scheduled", e);
			}
		}

	}

	/**
	 * Reads packets from a single connection.<br>
	 * Implements the {@linkplain Work} interface and is thus
	 * supposed to be executed asynchronously by the application server.
	 */
	private class ConnectionWork implements Work {

		// Attributes

		/**
		 * The connection to read from.
		 */
		private final Connection connection;

		/**
		 * Holds the internal work state of the work.
		 */
		private boolean running = false;

		// Constructor(s)

		/**
		 * Initializes the freshly created {@linkplain Work} with
		 * a given connection to read from when the work is executed.
		 *
		 * @param connection The connection to read from
		 */
		public ConnectionWork(Connection connection) {
			this.connection = connection;
		}

		// <--- Work --->

		@Override
		public void run() {
			setRunning(true);
			while (!Thread.interrupted() && isRunning())
				try {
					getConnection().readPackets();
				} catch (Exception e) {
					logger.debug("Catching connection error", e);
					getConnection().close();
					break;
				}
		}

		@Override
		public void release() {
			setRunning(false);
		}

		// Getter & Setter

		private final Connection getConnection() {
			return connection;
		}

		private final boolean isRunning() {
			return running;
		}

		private final void setRunning(boolean running) {
			this.running = running;
		}

	}

	// Attributes

	private int port = NetworkConstants.SERVER_PORT;

	/**
	 * The injected WorkManager provides container-managed concurrency support.
	 */
	private WorkManager workManager = null;

	/**
	 * The established server instance.
	 */
	private Server server = null;

	/**
	 * Function pointer which provides a reference to the listener method
	 * of potential message driven beans. Initialized on startup.
	 * There is no need to override this field.
	 */
	private Method messageListenerMethod;

	/**
	 * Contains all connected message endpoints (i.e. Message Driven Beans).
	 */
	private final Map<GameServerActivationSpec, MessageEndpointFactory> factories =
		    new HashMap<GameServerActivationSpec, MessageEndpointFactory>();

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public GameServerResourceAdapter() {
		// Do nothing
	}

	// Methods

	// The JCA specification requires this method to be explicitly implemented.
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	// The JCA specification requires this method to be explicitly implemented.
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * Attempts to forward a given connection event fired by the server to all connected message endpoints.<br>
	 * When an event is consumed, it will not be forwarded to any other listeners.
	 *
	 * @param event The event to forward
	 */
	protected void forwardConnectionEvent(ConnectionEvent event) {
		synchronized(factories) {
			for (GameServerActivationSpec activationSpec : getFactories().keySet()) {
				if (event.isConsumed()) break;
				MessageEndpointFactory factory = factories.get(activationSpec);
		        MessageEndpoint endpoint = null;
		        try {
					endpoint = factory.createEndpoint(null);
						try {
							endpoint.beforeDelivery(getMessageListenerMethod());
							((ConnectionListener) endpoint).handle(event);
						} catch (NoSuchMethodException | ResourceException e) {
							logger.error("Message Endpoint could not be prepared", e);
						} finally {
							try {
								endpoint.afterDelivery();
							} catch (ResourceException e) {
								logger.error("Message Endpoint could not be cleaned up", e);
							}
						}
		        } catch (UnavailableException e) {
		        	logger.error("Message Endpoint is not available", e);
				} finally {
					if (endpoint != null) {
						endpoint.release();
					}
				}
			}
		}
	}

	// <-- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		logger.debug("Handling connection event:" + connectionEvent);
		try {
			forwardConnectionEvent(connectionEvent);
		} catch (Throwable e) {
			logger.warn("Catching connection event error", e);
		}
	}

	// <--- Work --->

	@Override
	public void run() {
		while (getServer().isRunning())
			try {
				getServer().acceptConnections();
			} catch (Exception e) {
				logger.error("Catching server error", e);
			}
	}

	@Override
	public void release() {
		logger.info("Attempting to shutdown underlying Server instance ...");
		getServer().shutdown();
		logger.info("Underlying Server instance has been shut down");
	}

	// <--- ResourceAdapter --->

	@Override
	public void start(BootstrapContext initialContext) throws ResourceAdapterInternalException {

		try {

			logger.info("Attempting to start ServerNetworkAdapter ...");

			if (SSLContextFactory.getServerContext() == null) throw new IllegalStateException("SSLContext could not be initialized");

			// Initialize Message Listener Method
			setMessageListenerMethod(ConnectionListener.class.getMethod("handle", new Class[]{ConnectionEvent.class}));

			// Initialize Server
			setServer(new SSLServer(SSLContextFactory.getServerContext(), getPort(), new WorkManagerExecutor()));

			// Start to listen to all events fired on the server
			getServer().addListener(this);

			// Define work manager
			setWorkManager(initialContext.getWorkManager());

			// Run Server
			getWorkManager().scheduleWork(this);

			logger.info("ServerNetworkAdapter has been successfully started");

		} catch (Throwable e) {

			throw new ResourceAdapterInternalException(e);

		}
	}

	@Override
	public void stop() {
		logger.info("Attempting to stop ServerNetworkAdapter ...");
		release();
		logger.info("ServerNetworkAdapter has been successfully stopped");
	}

	@Override
	public void endpointActivation(MessageEndpointFactory messageEndpointFactory, ActivationSpec activationSpec) throws ResourceException {
		if (!(activationSpec instanceof GameServerActivationSpec)) {
			throw new NotSupportedException("Invalid activationSpec");
		}
		GameServerActivationSpec spec = (GameServerActivationSpec) activationSpec;
		spec.validate();
		synchronized(this.factories) {
			this.getFactories().put(spec, messageEndpointFactory);
		}
	}

	@Override
	public void endpointDeactivation(MessageEndpointFactory messageEndpointFactory, ActivationSpec activationSpec) {
		synchronized(this.factories) {
			this.getFactories().remove(activationSpec);
		}
	}

	@Override
	public XAResource[] getXAResources(ActivationSpec[] activationSpecs) throws ResourceException {
		return null;
	}

	// Getter & Setter

	/**
	 * @return The port to which the underlying server socket is bound.
	 */
	public final int getPort() {
		return port;
	}

	protected final void setPort(int port) {
		this.port = port;
	}

	protected final WorkManager getWorkManager() {
		return workManager;
	}

	protected final void setWorkManager(WorkManager workManager) {
		this.workManager = workManager;
	}

	protected final Server getServer() {
		return server;
	}

	protected final void setServer(Server server) {
		this.server = server;
	}

	protected final Map<GameServerActivationSpec, MessageEndpointFactory> getFactories() {
		return factories;
	}

	protected final Method getMessageListenerMethod() {
		return messageListenerMethod;
	}

	protected final void setMessageListenerMethod(Method messageListenerMethod) {
		this.messageListenerMethod = messageListenerMethod;
	}

}
