package dok.server.network.api;

import java.io.IOException;
import java.util.List;

import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.Packet;

/**
 * Network service interface for handling incoming connections
 * and forwarding events to registered listeners.
 *
 * @author Konstantin Schaper
 */
public interface Server extends Runnable {

	/**
	 * Starts waiting for incoming connection attempts.<br>
	 * Does block the calling thread until the server is
	 * shutdown or any exception occurs.
	 *
	 * @throws IOException
	 */
	public void acceptConnections() throws IOException;

	/**
	 * Attempts to close all registered {@linkplain Connection}s and stop
	 * accepting connections.<br>
	 *
	 * @return Whether or not the server has been shut down correctly.
	 */
	public boolean shutdown();

	/**
	 * Forwards the given packet to all connections.
	 *
	 * @param packet The packet to be sent
	 * @throws IllegalArgumentException When packet is null
	 * @throws IOException If the packet could not be sent
	 */
	public void broadcastPacket(Packet packet) throws IllegalArgumentException, IOException;

	/**
	 * @return All currently registered connections
	 */
	public List<Connection> getConnections();

	/**
	 * @return The port on which the server listens to incoming connections
	 */
	public String getPort();

	/**
	 * @return The network interface to which this server is connected
	 */
	public String getIP();

	/**
	 * @return Whether the network component of the server is still ready to accept connections
	 */
	public boolean isRunning();

	/**
	 * Registers a listener for all upcoming {@linkplain ServerEvent}s
	 * fired by this Server.
	 *
	 * @param listener The listener to add
	 * @return Whether the listener has been added
	 */
	public boolean addListener(ServerListener listener);

	/**
	 * Registers a listener for all upcoming {@linkplain ConnectionEvent}s
	 * that occur on any connection maintained by this server instance
	 *
	 * @param listener to add
	 * @return whether the listener has been successfully added
	 */
	public boolean addListener(ConnectionListener listener);

	/**
	 * Unregisters a previously registered listener.
	 *
	 * @param listener The listener to remove
	 * @return Whether the listener was registered and has been successfully unregistered
	 */
	public boolean removeListener(ServerListener listener);

	/**
	 * Unregisters a previously registered listener.
	 *
	 * @param listener The listener to remove
	 * @return Whether the listener was registered and has been successfully unregistered
	 */
	public boolean removeListener(ConnectionListener listener);
}
