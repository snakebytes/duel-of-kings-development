package dok.server.network.api;

import dok.commons.CustomEvent;

/**
 * Super interface for every type of event being fired by {@linkplain Server}s.
 *
 * @author Konstantin Schaper
 */
public interface ServerEvent extends CustomEvent {

	/**
	 * @return The server this event has been fired on
	 */
	public Server getSource();

}
