package dok.server.network.api;

/**
 * Super interface for all entities interested in
 * events fired from {@linkplain Server}s.
 *
 * @author Konstantin Schaper
 */
public interface ServerListener {

	/**
	 * Called on the implementing class when the specified
	 * type of event is fired on the server to which
	 * this listener has been added.
	 *
	 * @param event The event which has been fired
	 */
	public void handle(ServerEvent event);

}
