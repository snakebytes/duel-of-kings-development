package dok.server.network.api.events;

import dok.server.network.api.ServerEvent;

/**
 * Generated when a server has been shut down.
 *
 * @author Konstantin Schaper
 */
public interface ServerShutdownEvent extends ServerEvent {

}
