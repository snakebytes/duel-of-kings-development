package dok.server.network.api.events;

import dok.server.network.api.ServerEvent;

/**
 * Generated when a server has been successfully started.
 *
 * @author Konstantin Schaper
 */
public interface ServerStartedEvent extends ServerEvent {

}
