package dok.server.network.core;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.ObservableConnection;
import dok.commons.network.api.Packet;
import dok.commons.network.api.events.ConnectionClosedEvent;
import dok.commons.network.core.SocketConnection;
import dok.server.network.api.Server;
import dok.server.network.api.ServerEvent;
import dok.server.network.api.ServerListener;
import dok.server.network.core.events.ServerShutdownEventImpl;
import dok.server.network.core.events.ServerStartedEventImpl;

/**
 * @author Konstantin Schaper
 */
public class SSLServer implements Server, ConnectionListener {

	// Constants

	private static final Logger logger = LogManager.getLogger();

	// Attributes

	private final SSLServerSocket serverSocket;
	private final Set<ServerListener> listeners = new HashSet<ServerListener>();
	private final Set<ConnectionListener> connectionListeners = new HashSet<>();
	private final List<ObservableConnection> connectionList = new ArrayList<>();
	private final Executor clientThreadExecutor = null;

	// Constructor(s)

	public SSLServer(int port) throws IOException {
		logger.entry(port);

		// Initialize Server Socket
		serverSocket = (SSLServerSocket) SSLServerSocketFactory.getDefault().createServerSocket(port);

		// Fire Event
		fireEvent(new ServerStartedEventImpl(this));

		logger.info("Server has been successfully started.");
		logger.exit(this);
	}

	public SSLServer(SSLContext context, int port, ServerListener... listeners) throws IOException {
		logger.entry(port, listeners);

		// Initialize Server Socket
		serverSocket = (SSLServerSocket) context.getServerSocketFactory().createServerSocket(port);

		// Initialize Listeners
		Arrays.asList(listeners).forEach((listener) -> addListener(listener));

		// Fire Event
		fireEvent(new ServerStartedEventImpl(this));

		logger.info("Server has been successfully started.");
		logger.exit(this);
	}

	public SSLServer(SSLContext context, int port, Executor clientThreadExecutor, ServerListener... listeners)
			throws IOException {
		logger.entry(port, listeners);

		// Initialize Server Socket
		serverSocket = (SSLServerSocket) context.getServerSocketFactory().createServerSocket(port);
		serverSocket.setEnabledCipherSuites(serverSocket.getEnabledCipherSuites());

		// Initialize Listeners
		Arrays.asList(listeners).forEach((listener) -> addListener(listener));

		// Fire Event
		fireEvent(new ServerStartedEventImpl(this));

		logger.info("Server has been successfully started.");
		logger.exit(this);
	}

	public SSLServer(int port, Executor clientThreadExecutor) throws IOException {
		logger.entry(port, listeners);

		// Initialize Server Socket
		serverSocket = (SSLServerSocket) ServerSocketFactory.getDefault().createServerSocket(port);

		// Fire Event
		fireEvent(new ServerStartedEventImpl(this));

		logger.info("Server has been successfully started.");
		logger.exit(this);
	}

	// Methods

	@Override
	public void run() {
		while (!Thread.interrupted())
			try {
				acceptConnections();
			} catch (SocketException e) {
				shutdown();
				break;
			} catch (Exception e) {
				logger.catching(Level.DEBUG, e);
			}
	}

	@Override
	public void acceptConnections() throws IOException {
		SSLSocket clientSocket;
		while ((clientSocket = (SSLSocket) getServerSocket().accept()) != null) {
			SocketConnection conn = new SocketConnection(clientSocket);

			if (getClientThreadExecutor() != null) {
				getClientThreadExecutor().execute(conn);
			} else {
				Thread t = new Thread(conn);
				t.setName("ConnectionThread");
				t.setDaemon(true);
				t.start();
			}

			conn.addListener(this);
			getConnectionList().add(conn);
			logger.debug("A connection has been accepted");
		}
	}

	@Override
	public void broadcastPacket(Packet packet) throws IOException {
		for (Connection connection : new ArrayList<>(getConnectionList())) {
			connection.sendPacket(packet);
		}
	}

	@Override
	public boolean shutdown() {
		try {
			if (getServerSocket().isClosed())
				return true;
			getServerSocket().close();
			fireEvent(new ServerShutdownEventImpl(this));
			logger.info("The Server has been successfully shut down");
			return true;
		} catch (IOException e) {
			logger.log(Level.ERROR, "An exception occurred while closing the server socket", e);
			return false;
		}
	}

	@Override
	public void handle(ConnectionEvent event) {
		logger.debug("A connection event occured: " + event);
		if (event instanceof ConnectionClosedEvent) {
			getConnectionList().remove(event.getSource());
		}
		fireEvent(event);
	}

	// Event Handling

	protected final void fireEvent(ServerEvent event) {
		logger.debug("The server event " + event + " is being fired on " + getListeners().size() + " listeners.");
		for (ServerListener serverListener : new HashSet<>(getListeners())) {
			try {
				serverListener.handle(event);
			} catch (Exception e) {
				logger.warn("An error occurred while a listener processed the event", e);
			}
			if (event.isConsumed())
				break;
		}
	}

	protected final void fireEvent(ConnectionEvent event) {
		logger.debug("The connection event " + event + " is being forwarded to " + getConnectionListeners().size()
				+ " listeners.");
		for (ConnectionListener connectionListener : new HashSet<>(getConnectionListeners())) {
			try {
				connectionListener.handle(event);
			} catch (Exception e) {
				logger.warn("An error occurred while a listener processed the event", e);
			}
			if (event.isConsumed())
				break;
		}
	}

	@Override
	public boolean addListener(ServerListener listener) {
		logger.debug("ServerListener added");
		return getListeners().add(listener);
	}

	@Override
	public boolean addListener(ConnectionListener listener) {
		logger.debug("ConnectionListener added");
		return getConnectionListeners().add(listener);
	}

	@Override
	public boolean removeListener(ServerListener listener) {
		logger.debug("ServerListener removed");
		return getListeners().remove(listener);
	}

	@Override
	public boolean removeListener(ConnectionListener listener) {
		logger.debug("ConnectionListener removed");
		return getConnectionListeners().remove(listener);
	}

	// Getter & Setter

	public List<ObservableConnection> getConnectionList() {
		return connectionList;
	}

	/**
	 * @return read only list containing all connections
	 */
	@Override
	public List<Connection> getConnections() {
		return Collections.unmodifiableList(getConnectionList());
	}

	public SSLServerSocket getServerSocket() {
		return serverSocket;
	}

	public Set<ServerListener> getListeners() {
		return listeners;
	}

	public Set<ConnectionListener> getConnectionListeners() {
		return connectionListeners;
	}

	@Override
	public String getPort() {
		return String.valueOf(getServerSocket().getLocalPort());
	}

	@Override
	public String getIP() {
		return String.valueOf(getServerSocket().getInetAddress());
	}

	public Executor getClientThreadExecutor() {
		return clientThreadExecutor;
	}

	@Override
	public boolean isRunning() {
		return !getServerSocket().isClosed();
	}

}
