package dok.server.network.core;

import dok.commons.impl.CustomEventImpl;
import dok.server.network.api.Server;
import dok.server.network.api.ServerEvent;
/**
 * @author Konstantin Schaper
 * @version 0.1.0
 */
public class ServerEventImpl extends CustomEventImpl implements ServerEvent {

	protected Server server;

	public ServerEventImpl(Server server)
	{
		this.server = server;
	}

	@Override
	public Server getSource() {
		return server;
	}

}