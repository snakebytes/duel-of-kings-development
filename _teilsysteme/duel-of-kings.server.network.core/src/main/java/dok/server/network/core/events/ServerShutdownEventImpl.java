package dok.server.network.core.events;

import dok.server.network.api.Server;
import dok.server.network.api.events.ServerShutdownEvent;
import dok.server.network.api.events.ServerStartedEvent;
import dok.server.network.core.ServerEventImpl;

/**
 * Implementation of the {@linkplain ServerShutdownEvent} interface.
 *
 * @author Konstantin Schaper
 */
public class ServerShutdownEventImpl extends ServerEventImpl implements ServerStartedEvent {

	public ServerShutdownEventImpl(Server server) {
		super(server);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
