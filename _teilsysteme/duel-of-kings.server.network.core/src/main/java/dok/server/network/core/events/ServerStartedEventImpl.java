package dok.server.network.core.events;

import dok.server.network.api.Server;
import dok.server.network.api.events.ServerStartedEvent;
import dok.server.network.core.ServerEventImpl;

/**
 * Implementation of the {@linkplain ServerStartedEvent} interface.
 *
 * @author Konstantin Schaper
 */
public class ServerStartedEventImpl extends ServerEventImpl implements ServerStartedEvent {

	public ServerStartedEventImpl(Server server) {
		super(server);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
