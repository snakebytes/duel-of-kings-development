package dok.server.web.registration.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import dok.commons.model.Account;
import dok.commons.model.AccountRole;
import dok.commons.model.AccountStatus;
import dok.commons.model.Coordinate;
import dok.commons.model.EnhancementPlan;
import dok.commons.model.EnhancementSlot;
import dok.commons.model.EnhancementType;
import dok.commons.model.Lineup;
import dok.game.model.constants.CharacterClassIds;
import dok.game.model.constants.EnhancementClassIds;
import dok.server.gamedataservice.api.GameDataService;
import dok.server.passwordservice.api.PasswordService;
import dok.server.userdataservice.api.UserDataService;

/**
 * Controller class for <code>registration.xhtml</code>.
 * Also holds the state of this view.
 * Objects of this class are automatically instantiated on client request
 * and live until the registration form is committed.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Named
@RequestScoped
public class RegistrationController {

	// Logging

	private static final Logger logger = Logger.getLogger(RegistrationController.class);

	// Injections

	@Inject
	private UserDataService userDataService;

	@Inject
	private GameDataService gameDataService;

	@Inject
	private PasswordService passwordService;

	// Attributes

	/**
	 * Represents the state of the registration form's username input textfield.
	 */
	private String username;

	/**
	 * Represents the state of the registration form's password input textfield.
	 */
	private String password;

	// Constructor(s)

    /**
     * Default constructor.
     */
    public RegistrationController() {
        // Do nothing
    }

    // Methods

    /**
     * Called when the registration form is submitted.<br>
     * Attempts to register the user with the given information.
     *
     * @return Either "success" or "failure", depending on the result of the registration.
     */
    public String register() {

    	logger.info("Attempting to register user: " + getUsername());

    	// Both required services have to be available for the registration to function
    	if (getUserDataService() == null) {

    		logger.error("UserDataService is not available");
    		return "failure";

    	} else if (getPasswordService() == null) {

    		logger.error("PasswordService is not available");
    		return "failure";

    	} else {
    		try {
				if (getUserDataService().getAccount(getUsername()) != null) {

					logger.error("A user with the requested username already exists");
					return "failure";

				} else {

					// Initialize account object with given and default values
					Account account = new Account();
					account.setName(getUsername());
					account.setStatus(AccountStatus.CONFIRMED);
					account.setPasswordHash(getPasswordService().generatePasswordString(getPassword()));
					account.setRole(AccountRole.PLAYER);

					if (getUserDataService().saveOrUpdate(account)) {
						logger.info("New account successfully created: " + account);

						// START DefaultLineup

						try {

							// Create Coordinates

							Coordinate c_0_0 = new Coordinate(0, 0);
							Coordinate c_0_1 = new Coordinate(0, 1);
							Coordinate c_0_2 = new Coordinate(0, 2);
							Coordinate c_1_0 = new Coordinate(1, 0);
							Coordinate c_1_1 = new Coordinate(1, 1);
							Coordinate c_1_2 = new Coordinate(1, 2);
							Coordinate c_2_0 = new Coordinate(2, 0);
							Coordinate c_2_1 = new Coordinate(2, 1);
							Coordinate c_2_2 = new Coordinate(2, 2);

							getUserDataService().save(c_0_0);
							getUserDataService().save(c_0_1);
							getUserDataService().save(c_0_2);
							getUserDataService().save(c_1_0);
							getUserDataService().save(c_1_1);
							getUserDataService().save(c_1_2);
							getUserDataService().save(c_2_0);
							getUserDataService().save(c_2_1);
							getUserDataService().save(c_2_2);

							logger.info("Attempting to create test lineup ...");

							// Create Lineup

							String lineupTitle = "Offensiv";

							Lineup lineup = new Lineup();

							// Get EnhancementSlots

							EnhancementSlot crystal1 = getUserDataService().getEnhancementSlot(EnhancementType.CRYSTAL, 1);
							if (crystal1 == null) {
								crystal1 = new EnhancementSlot(EnhancementType.CRYSTAL, 1);
								getUserDataService().saveOrUpdate(crystal1);
							}

							EnhancementSlot rune1 = getUserDataService().getEnhancementSlot(EnhancementType.RUNE, 1);
							if (rune1 == null) {
								rune1 = new EnhancementSlot(EnhancementType.RUNE, 1);
								getUserDataService().saveOrUpdate(rune1);
							}

							EnhancementSlot generic2 = getUserDataService().getEnhancementSlot(EnhancementType.GENERIC, 2);
							if (generic2 == null) {
								generic2 = new EnhancementSlot(EnhancementType.GENERIC, 2);
								getUserDataService().saveOrUpdate(generic2);
							}

							EnhancementSlot generic3 = getUserDataService().getEnhancementSlot(EnhancementType.GENERIC, 3);
							if (generic3 == null) {
								generic3 = new EnhancementSlot(EnhancementType.GENERIC, 3);
								getUserDataService().saveOrUpdate(generic3);
							}

							// Create Enhancement Plans

							EnhancementPlan ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.WARDENS_CODEX_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.VETERAN_OF_BATTLES_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.ENCOURAGE_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_0_0, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.AXE_BASH_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.CALLING_THE_PACK_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_0_1, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.WARDENS_CODEX_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.VETERAN_OF_BATTLES_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.ENCOURAGE_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_0_2, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.CURSE_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.SILENCE_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.BREATHTAKING_SILENCE_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_1_0, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.AROUSE_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.POWER_TRANSFER_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.MAGIC_HUNT_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_1_1, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.CURSE_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.SILENCE_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.BREATHTAKING_SILENCE_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_1_2, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.HEAL_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.TRANQUILITY_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.ADV_TRANQUILITY_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_2_0, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.BOMBARDMENT_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.ADAPTIVE_BOMB_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.THROW_BOMBS_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_2_1, ep);

							ep = new EnhancementPlan();
							ep.getEnhancementMap().put(crystal1, EnhancementClassIds.HEAL_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic2, EnhancementClassIds.TRANQUILITY_CRYSTAL_ID);
							ep.getEnhancementMap().put(generic3, EnhancementClassIds.ADV_TRANQUILITY_CRYSTAL_ID);
							getUserDataService().saveOrUpdate(ep);
							lineup.getEnhancementPlans().put(c_2_2, ep);

							// Define Lineup

							lineup.setAccountId(account.getId());
							lineup.setTitle(lineupTitle);
							lineup.setImageKey("Offensive");
							lineup.setKingPosition(c_1_1);
							lineup.getCharacterClassIds().put(c_0_0, CharacterClassIds.SWORD_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_0_1, CharacterClassIds.AXE_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_0_2, CharacterClassIds.SWORD_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_1_0, CharacterClassIds.BOW_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_1_1, CharacterClassIds.CROSSBOW_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_1_2, CharacterClassIds.BOW_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_2_0, CharacterClassIds.STAFF_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_2_1, CharacterClassIds.HANDCANNON_CHARACTER_CLASS_ID);
							lineup.getCharacterClassIds().put(c_2_2, CharacterClassIds.STAFF_CHARACTER_CLASS_ID);

							try {
								getUserDataService().saveLineup(lineup);
								logger.info("Test lineup has been successfully saved");
							} catch (Exception e) {
								logger.error("Testlineup could not be saved", e);
							}

						} catch (Throwable e) {

							logger.error("Test lineup could not be created", e);
							return "failure";

						}

						// END DefaultLineup

						return "success";
					} else {
						logger.error("Account could not be saved");
						return "failure";
					}

				}
			} catch (Exception e) {
				logger.error("Possible existing accounts could not be retrieved from data store", e);
				return "failure";
			}
    	}
    }

    // Getter & Setter

	public UserDataService getUserDataService() {
		return userDataService;
	}

	public void setUserDataService(UserDataService userDataService) {
		this.userDataService = userDataService;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PasswordService getPasswordService() {
		return passwordService;
	}

	public void setPasswordService(PasswordService passwordService) {
		this.passwordService = passwordService;
	}

	public GameDataService getGameDataService() {
		return gameDataService;
	}

	public void setGameDataService(GameDataService gameDataService) {
		this.gameDataService = gameDataService;
	}

}
