package dok.server.web.registration.validator;

import java.util.ResourceBundle;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

import dok.server.userdataservice.api.UserDataService;

/**
 *	Validates the username input field of the registration form.
 *
 * @author Konstantin Schaper
 * @since 0.2.7.1
 */
@Named
@RequestScoped
public class UsernameValidator implements Validator {

	// Injections

	@Inject
	private UserDataService userDataService;

	// <--- Validator --->

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {


		if ((context == null) || (component == null)) {
			throw new NullPointerException();
		}

		if (!(component instanceof UIInput)) {
			return;
		}

		if (value == null) {
			return;
		}

		String stringValue = value.toString().trim();
		ResourceBundle msg = ResourceBundle.getBundle("dok.server.web.registration.localization.locale");

		if (stringValue.length() < 4) {
			FacesMessage fm = new FacesMessage(msg.getString("registration.form.username.tooShortError"));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(fm);
		} else if (stringValue.length() > 24) {
			FacesMessage fm = new FacesMessage(msg.getString("registration.form.username.tooLongError"));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(fm);
		} else if (getUserDataService() != null && getUserDataService().getAccount(stringValue) != null) {
			FacesMessage fm = new FacesMessage(msg.getString("registration.form.username.alreadyTaken"));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(fm);
		}
	}

	// Getter & Setter

	protected UserDataService getUserDataService() {
		return userDataService;
	}

	protected void setUserDataService(UserDataService userDataService) {
		this.userDataService = userDataService;
	}

}
