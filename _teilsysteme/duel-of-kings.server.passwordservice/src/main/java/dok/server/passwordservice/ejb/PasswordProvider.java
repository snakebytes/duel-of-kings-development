package dok.server.passwordservice.ejb;

import java.io.Serializable;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import com.lambdaworks.crypto.SCryptUtil;

import dok.server.passwordservice.api.PasswordService;

/**
 * Singleton implementation of the {@linkplain PasswordService} business interface.<br>
 * Makes use of the lambdaworks scrypt implementation.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 * @see <a href="https://github.com/wg/scrypt">Lambdaworks Scrypt on Github</a>
 */
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
public class PasswordProvider implements PasswordService, Serializable {

	// Class Constants

	protected static final long serialVersionUID 	= -6284829848534056170L;

	/**
	 * Factor for CPU cost of the algorithm.
	 */
	protected static final int SOFTWARE_COST		= 16;

	/**
	 * Factor for memory cost of the algorithm.
	 */
	protected static final int HARDWARE_COST		= 1024;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public PasswordProvider() {
		// Do nothing
	}

	// <--- PasswordService --->

	@Override
	public String generatePasswordString(String password) {
		return SCryptUtil.scrypt(password, HARDWARE_COST, SOFTWARE_COST, 1);
	}

	@Override
	public boolean validatePassword(String providedPassword, String storedPassword) {
		return SCryptUtil.check(providedPassword, storedPassword);
	}

}
