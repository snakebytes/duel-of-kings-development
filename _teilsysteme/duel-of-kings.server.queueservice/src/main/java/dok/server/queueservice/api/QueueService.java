package dok.server.queueservice.api;

import java.util.Queue;
import java.util.UUID;

import javax.ejb.Local;

import org.apache.commons.lang3.tuple.Pair;

import dok.commons.model.Lineup;
import dok.game.model.GameType;
import dok.server.queueservice.exception.QueueDisabledException;
import dok.server.queueservice.model.QueueStatus;

/**
 * Holds the queuing state of all players who are currently part
 * of the matchmaking system. This service does not do the matchmaking
 * itself or create any games.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.6
 */
@Local
public interface QueueService {

	/**
	 * @param gameType The game type whose matchmaking queue to retrieve
	 * @return The matchmaking queue of the given game type
	 */
	public Queue<Pair<UUID, Lineup>> getQueue(GameType gameType);

	/**
	 * @param accountId The user whose queuing status to check
	 * @return The type of matchmaking queue the user is in or null, if the user is not queued up
	 */
	public GameType getQueueStatus(UUID accountId);

	/**
	 * @param gameType The game type of the queue which's status to receive
	 * @return The status of the queue for the given game type
	 */
	public QueueStatus getQueueStatus(GameType gameType);

	/**
	 * Attempts to add the given account to the matchmaking queue
	 * of the given game type. The given lineup will be used when a game
	 * has been found.
	 *
	 * @param gameType The type of matchmaking queue to join
	 * @param accountId The account to manage
	 * @param lineup The lineup to use
	 * @return Whether or not the account has been successfully queued up
	 * @throws QueueDisabledException If the desired queue is disabled
	 * @throws UnsupportedOperationException If the desired queue's status is not handled properly
	 */
	public boolean queueUp(GameType gameType, UUID accountId, Lineup lineup) throws Exception;

	/**
	 * Attempts to remove the given account from all matchmaking queues.
	 *
	 * @param accountId The account to remove from the queueing system.
	 * @return Whether or not the given account has successfully left all queues
	 * @throws IllegalStateException If the given user is not queued up
	 */
	public boolean leaveQueue(UUID accountId);

}
