package dok.server.queueservice.ejb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jboss.logging.Logger;

import dok.commons.model.Lineup;
import dok.game.model.GameType;
import dok.server.queueservice.api.QueueService;
import dok.server.queueservice.event.QueueEnteredEvent;
import dok.server.queueservice.event.QueueLeftEvent;
import dok.server.queueservice.event.impl.QueueEnteredEventImpl;
import dok.server.queueservice.event.impl.QueueLeftEventImpl;
import dok.server.queueservice.exception.QueueDisabledException;
import dok.server.queueservice.model.QueueStatus;

/**
 * Singleton ejb implementation of the {@linkplain QueueService} business interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
public class QueueProvider implements QueueService {

	// Class Constants

	private static final Logger logger = Logger.getLogger(QueueProvider.class);

	// Constants

	private final Map<GameType, Queue<Pair<UUID, Lineup>>> queues = new HashMap<>();
	private final Map<GameType, QueueStatus> queueStatusValues = new HashMap<>();

	// Injections

	@Inject
	private Event<QueueEnteredEvent> queueEnteredEventListener;

	@Inject
	private Event<QueueLeftEvent> queueLeftEventListener;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public QueueProvider() {
		// Do nothing
	}

	// Methods

	/**
	 * Initializes the default queue status values on service startup.
	 */
	@PostConstruct
	public void initialize() {
		getQueues().put(GameType.Duel, new LinkedBlockingQueue<>());
		getQueueStatusValues().put(GameType.Duel, QueueStatus.ENABLED);
	}

	// <--- QueueService --->

	@Override
	public boolean queueUp(GameType gameType, UUID accountId, Lineup lineup) throws QueueDisabledException, UnsupportedOperationException, IllegalArgumentException {

		if (gameType == null) throw new IllegalArgumentException("GameType must not be null");
		if (accountId == null) throw new IllegalArgumentException("AccountId must not be null");
		if (lineup == null) throw new IllegalArgumentException("Lineup must not be null");

		switch (getQueueStatus(gameType)) {
			case DISABLED:
				throw new QueueDisabledException(gameType);
			case ENABLED:
				if (getQueues().get(gameType).add(new MutablePair<>(accountId, lineup))) {
					getQueueEnteredEventListener().fire(new QueueEnteredEventImpl(accountId, gameType, lineup));
					logger.info(accountId + " has entered Queue.");
					return true;
				} else return false;
			default:
				throw new UnsupportedOperationException("The desired queue's status is not handled properly.");
		}

	}

	@Override
	public boolean leaveQueue(UUID accountId) throws IllegalStateException {
		GameType queueStatus = getQueueStatus(accountId);
		if (queueStatus != null) {
			Queue<Pair<UUID, Lineup>> queue = getQueue(queueStatus);
			if (queue != null) {
				Iterator<Pair<UUID, Lineup>> iterator = queue.iterator();
				while (iterator.hasNext()) {
					Pair<UUID, Lineup> entry = iterator.next();
					if (entry.getKey().equals(accountId) && queue.remove(entry)) {
						getQueueLeftEventListener().fire(new QueueLeftEventImpl(accountId, queueStatus, entry.getValue()));
						logger.info(accountId + " has left Queue.");
						return true;
					}
				}
				return false;
			} else throw new IllegalStateException("The queue, the user is in, is not existant. lol");
		} else {
			throw new IllegalStateException("User is not in queue");
		}
	}

	@Override
	public Queue<Pair<UUID, Lineup>> getQueue(GameType gameType) {
		return getQueues().get(gameType);
	}

	@Override
	public QueueStatus getQueueStatus(GameType gameType) {
		return getQueueStatusValues().get(gameType);
	}

	@Override
	public GameType getQueueStatus(UUID accountId) {
		for (Entry<GameType, Queue<Pair<UUID, Lineup>>> queuePair : getQueues().entrySet())
			for (Pair<UUID, Lineup> pair : queuePair.getValue())
				if (pair.getKey().equals(accountId)) return queuePair.getKey();
		return null;
	}

	protected final Map<GameType, Queue<Pair<UUID, Lineup>>> getQueues() {
		return queues;
	}

	protected final Map<GameType, QueueStatus> getQueueStatusValues() {
		return queueStatusValues;
	}

	protected final Event<QueueEnteredEvent> getQueueEnteredEventListener() {
		return queueEnteredEventListener;
	}

	protected final void setQueueEnteredEventListener(Event<QueueEnteredEvent> queueEnteredEvent) {
		this.queueEnteredEventListener = queueEnteredEvent;
	}

	protected final Event<QueueLeftEvent> getQueueLeftEventListener() {
		return queueLeftEventListener;
	}

	protected final void setQueueLeftEventListener(Event<QueueLeftEvent> queueLeftEvent) {
		this.queueLeftEventListener = queueLeftEvent;
	}

}
