package dok.server.queueservice.event;

import java.util.UUID;

import dok.commons.model.Lineup;
import dok.game.model.GameType;

/**
 * Fired whenever a user has successfully entered a matchmaking queue.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public interface QueueEnteredEvent {

	/**
	 * @return The unique identifier of the user who just entered a queue
	 */
	public UUID getAccountId();

	/**
	 * @return The lineup with which the user queued up
	 */
	public Lineup getLineup();

	/**
	 * @return The type of queue the user has entered
	 */
	public GameType getGameType();

}
