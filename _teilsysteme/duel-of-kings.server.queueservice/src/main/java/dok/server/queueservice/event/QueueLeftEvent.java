package dok.server.queueservice.event;

import java.util.UUID;

import dok.commons.model.Lineup;
import dok.game.model.GameType;

/**
 * Fired whenever a user has been successfully removed from a matchmaking queue.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public interface QueueLeftEvent {

	/**
	 * @return The unique identifier of the user who was removed
	 */
	public UUID getAccountId();

	/**
	 * @return The lineup with which the user initially queued up
	 */
	public Lineup getLineup();

	/**
	 * @return The type of queue the user has been removed from
	 */
	public GameType getGameType();

}
