package dok.server.queueservice.event.impl;

import java.util.UUID;

import dok.commons.Immutable;
import dok.commons.model.Lineup;
import dok.game.model.GameType;
import dok.server.queueservice.event.QueueLeftEvent;

/**
 * Implementation of the {@linkplain QueueLeftEvent} interface.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 *
 */
@Immutable
public class QueueLeftEventImpl implements QueueLeftEvent {

	// Attributes

	private final UUID accountId;
	private final Lineup lineup;
	private final GameType gameType;

	// Constructor(s)

	/**
	 * Initializes the event's fields with the given parameters.
	 *
	 * @param accountId See {@linkplain #getAccountId()}
	 * @param gameType See {@linkplain #getGameType()}
	 * @param lineup See {@linkplain #getLineup()}
	 */
	public QueueLeftEventImpl(UUID accountId, GameType gameType, Lineup lineup) {
		super();
		this.accountId = accountId;
		this.lineup = lineup;
		this.gameType = gameType;
	}

	// Getter & Setter

	@Override
	public UUID getAccountId() {
		return accountId;
	}

	@Override
	public Lineup getLineup() {
		return lineup;
	}

	@Override
	public GameType getGameType() {
		return gameType;
	}

}
