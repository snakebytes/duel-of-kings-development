package dok.server.queueservice.exception;

import dok.game.model.GameType;

/**
 * Thrown when a user attempts to join a matchmaking queue which is currently disabled.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
public final class QueueDisabledException extends Exception {

	// Class Constants

	private static final long serialVersionUID = 416404990202535614L;

	// Attributes

	private final GameType gameType;

	// Constructor(s)

	/**
	 * Initializes the exception with the given values.
	 *
	 * @param gameType The type of matchmaking queue the user attempted to join
	 */
	public QueueDisabledException(GameType gameType) {
		super();
		this.gameType = gameType;
	}

	// Getter & Setter

	/**
	 * @return The type of matchmaking queue the user attempted to join
	 */
	public GameType getGameType() {
		return gameType;
	}

}