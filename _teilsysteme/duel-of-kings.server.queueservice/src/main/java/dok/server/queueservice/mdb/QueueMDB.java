package dok.server.queueservice.mdb;

import java.io.IOException;
import java.util.Map.Entry;

import javax.ejb.MessageDriven;
import javax.inject.Inject;

import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import dok.commons.model.Lineup;
import dok.commons.model.LoginSession;
import dok.commons.network.api.Connection;
import dok.commons.network.api.ConnectionEvent;
import dok.commons.network.api.ConnectionListener;
import dok.commons.network.api.events.PacketReceivedEvent;
import dok.game.model.GameType;
import dok.game.network.model.QueuingPacket;
import dok.game.network.model.QueuingPacket.QueuingPacketType;
import dok.game.network.model.QueuingPacket.QueuingResult;
import dok.server.lineupservice.api.LineupService;
import dok.server.loginservice.api.LoginService;
import dok.server.queueservice.api.QueueService;
import dok.server.queueservice.exception.QueueDisabledException;

/**
 * Message Driven Bean which handles matchmaking queue-related requests.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@MessageDriven(messageListenerInterface=ConnectionListener.class)
@ResourceAdapter(value = "duel-of-kings.server.ear#duel-of-kings.server.network.adapter.rar")
public class QueueMDB implements ConnectionListener {

	// Class Constants

	private static final Logger logger = Logger.getLogger(QueueMDB.class);

	// Injections

	@Inject
	private QueueService queueService;

	@Inject
	private LoginService loginService;

	@Inject
	private LineupService lineupService;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification
	 */
	public QueueMDB() {
		// Do nothing
	}

	// <--- ConnectionListener --->

	@Override
	public void handle(ConnectionEvent connectionEvent) {
		Connection conn = connectionEvent.getSource();
		// Analyze incoming event
		if (connectionEvent instanceof PacketReceivedEvent) {
			PacketReceivedEvent packetReceivedEvent = (PacketReceivedEvent) connectionEvent;

			// Analyze incoming packet
			if (packetReceivedEvent.getPacket() instanceof QueuingPacket) {
				QueuingPacket queuingPacket = (QueuingPacket) packetReceivedEvent.getPacket();

				// Initialize needed variables
				Entry<LoginSession, Connection> userEntry = getLoginService().getConnectionEntry(conn);

				if (queuingPacket.getPacketType() == QueuingPacketType.JOIN) {

					GameType gameType = queuingPacket.getGameType();

					// Try queuing up the user
					if (userEntry != null) {

						try {

							Lineup lineup = getLineupService().getLineup(queuingPacket.getLineupId());

							if (getQueueService().queueUp(gameType, userEntry.getKey().getAccountId(), lineup))
								conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, gameType, QueuingResult.SUCCESSFULLY_ENTERED));
							else
								conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, gameType, QueuingResult.ERROR));

						} catch (IOException e) {

							logger.warn("Packet could not be sent", e);

						} catch (QueueDisabledException e) {

							try {
								conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, gameType, QueuingResult.DISABLED));

							} catch (IOException e2) {

								logger.warn("Packet could not be sent", e2);

							}

						} catch (IllegalStateException e) {

							try {

								conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, gameType, QueuingResult.FULL));

							} catch (IOException e2) {

								logger.warn("Packet could not be sent", e2);

							}

						} catch (Exception e) {

							logger.debug("An error occured while processing a queue request", e);

							try {

								conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, gameType, QueuingResult.ERROR));

							} catch (IOException e2) {

								logger.warn("Packet could not be sent", e2);

							}

						}

					} else {

						try {

							conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, gameType, QueuingResult.UNAUTHORIZED));

						} catch (IOException e) {

							logger.warn("Packet could not be sent", e);

						}

					}

				} else if (queuingPacket.getPacketType() == QueuingPacketType.LEAVE) {

					try {

						if (getQueueService().leaveQueue(userEntry.getKey().getAccountId()))
							conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, QueuingResult.SUCCESSFULLY_LEFT));
						else
							conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, QueuingResult.ERROR));

					} catch (IllegalStateException e) {

						try {

							conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, QueuingResult.REJECTED));

						} catch (IOException e2) {

							logger.warn("Packet could not be sent", e2);

						}

					} catch (Exception e) {

						logger.debug("An error occured while processing a queue request", e);

						try {

							conn.sendPacket(new QueuingPacket(QueuingPacketType.RESULT, QueuingResult.ERROR));

						} catch (IOException e2) {

							logger.warn("Packet could not be sent", e2);

						}

					}

				}
			}

		}
	}

	// Getter & Setter

	protected final QueueService getQueueService() {
		return queueService;
	}

	protected final void setQueueService(QueueService queueService) {
		this.queueService = queueService;
	}

	protected final LoginService getLoginService() {
		return loginService;
	}

	protected final void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	protected final LineupService getLineupService() {
		return lineupService;
	}

	protected final void setLineupService(LineupService lineupService) {
		this.lineupService = lineupService;
	}

}
