package dok.server.queueservice.model;

import dok.server.queueservice.exception.QueueDisabledException;

/**
 * Describes the state of a single type of matchmaking queue.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 *
 */
public enum QueueStatus {

	/**
	 * The queue is enabled and may be joined.
	 */
	ENABLED,

	/**
	 * The queue is disabled and any attempt to join it throws a {@linkplain QueueDisabledException}.
	 */
	DISABLED

}