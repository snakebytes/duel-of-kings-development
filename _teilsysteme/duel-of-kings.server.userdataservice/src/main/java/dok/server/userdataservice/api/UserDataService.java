package dok.server.userdataservice.api;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.ejb.Local;

import dok.commons.model.Account;
import dok.commons.model.EnhancementSlot;
import dok.commons.model.EnhancementType;
import dok.commons.model.Lineup;
import dok.commons.services.DataPersistenceService;

/**
 * Main DAO (Data Access Object) service for the
 * <i>com.duel-of-kings.userdata</i> persistence unit.<br>
 * <br>
 * Only accessible in local context (i.e. the application server).<br>
 * <br>
 * <code>Note:</code> The persistence context is only present in the same enterprise application,
 * where the corresponding persistence archive is placed. In this case this is <i>duel-of-kings.commons.jar</i>.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 */
@Local
public interface UserDataService extends DataPersistenceService {

	/**
	 * Convenience method for retrieving accounts from the data store.
	 *
	 * @param name The unique name of the account
	 * @return The retrieved account or null, if there is no account with the given name
	 */
	public Account getAccount(String name);

	/**
	 * Convenience method for retrieving an account by its id from the data store.
	 *
	 * @param accountId The unique identifier of the account
	 * @return The retrieved account or null, if there is no account with the given id or if an error occurred on retrieval
	 * @see #getObjectById(Class, Serializable)
	 */
	public Account getAccount(UUID accountId);

	/**
	 * Convenience method for retrieving a single lineup by its id from the data store.
	 *
	 * @param lineupId The id of the requested lineup
	 * @return The retrieved lineup or null, if there is no lineup with the given id or if an error occurred on retrieval
	 * @see #getObjectById(Class, Serializable)
	 */
	public Lineup getLineup(long lineupId);

	/**
	 * Convenience method for retrieving a single lineup of a user by its name from the data store.
	 *
	 * @param accountId The unique identifier of the owner of the requested lineup
	 * @param lineupName The unique name of the lineup to retrieve (a lineup's name is not immutable though)
	 * @return The retrieved lineup or null, if the given account does not own a lineup with the given name or if an error occurred on retrieval
	 */
	public Lineup getUserLineup(UUID accountId, String lineupName);

	/**
	 * Convenience method for retrieving all lineups of a single user from the data store.<br>
	 * Always returns an instance of {@linkplain java.util.List}.
	 *
	 * @param accountId The user whose lineups to retrieve
	 * @return A list of all lineups of the user with the given id or an empty list, if an error occurred on retrieval (of course the user could just not own any lineups)
	 */
	public List<Lineup> getUserLineups(UUID accountId);

	/**
	 * Convenience method for saving a lineup to the data store.
	 *
	 * @param lineup The lineup to save
	 * @return Whether the lineup has been successfully saved
	 * @see #saveOrUpdate(Serializable)
	 */
	public boolean saveLineup(Lineup lineup);

	public EnhancementSlot getEnhancementSlot(EnhancementType type, int rank);

}
