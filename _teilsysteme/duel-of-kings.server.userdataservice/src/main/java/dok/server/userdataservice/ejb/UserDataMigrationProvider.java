package dok.server.userdataservice.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.jboss.logging.Logger;

/**
 * Singleton local enterprise bean which migrates the database schema
 * of the <i>com.duel-of-kings.userdata</i> persistence unit on startup.
 * This bean is started on deployment.
 *
 * @author Konstantin Schaper
 * @since 0.2.6.1
 */
@Singleton
@LocalBean
@Startup
@Lock(LockType.READ)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@TransactionManagement(TransactionManagementType.BEAN)
public class UserDataMigrationProvider {

	// Class Constants

	private static final Logger logger = Logger.getLogger(UserDataMigrationProvider.class);

	/**
	 * The JNDI(Java Naming and Directory Interface)-Name of the sql data source to migrate.
	 */
	private static final String DATA_SOURCE = "java:/dok/datasources/userData";

	// Injection

	@Resource(lookup = DATA_SOURCE)
	private DataSource dataSource;

	// Constructor(s)

    /**
     * Default constructor for java bean specification.
     */
    public UserDataMigrationProvider() {
        // Do nothing
    }

    // Methods

    @PostConstruct
    protected void onStartup() {
    	logger.info("Attempting to migrate Data Source: " + DATA_SOURCE);
    	if (dataSource == null) {
    		logger.error("no datasource found to execute the db migrations!");
			throw new EJBException("no datasource found to execute the db migrations!");
		}

		Flyway flyway = new Flyway();
		flyway.setValidateOnMigrate(true);
		flyway.setLocations("/migration/user_data");
		flyway.setDataSource(dataSource);
		for (MigrationInfo i : flyway.info().all()) {
			logger.info("migrate task: " + i.getVersion() + " : " + i.getDescription() + " from file: " + i.getScript());
		}
		flyway.migrate();
		logger.info(DATA_SOURCE + " has been successfully migrated");
    }

}
