package dok.server.userdataservice.ejb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.DependsOn;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.jboss.logging.Logger;

import dok.commons.model.Account;
import dok.commons.model.Coordinate;
import dok.commons.model.EnhancementPlan;
import dok.commons.model.EnhancementSlot;
import dok.commons.model.EnhancementType;
import dok.commons.model.Lineup;
import dok.commons.model.constants.ModelConstants;
import dok.game.model.constants.CharacterClassIds;
import dok.server.passwordservice.api.PasswordService;
import dok.server.userdataservice.api.UserDataService;

/**
 * Singleton-implementation of the {@linkplain UserDataService} interface.
 * Requires the database migration to take place beforehand.
 *
 * @author Konstantin Schaper
 * @since 0.2.1.2
 * @see UserDataMigrationProvider
 */
@Singleton
@Startup
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Lock(LockType.READ)
@DependsOn(value = { "UserDataMigrationProvider" })
public class UserDataProvider implements UserDataService {

	// Class Constants

	private static final Logger logger = Logger.getLogger(UserDataProvider.class);

	// Injection

	@Inject
	private PasswordService passwordService;

	@PersistenceContext(unitName="com.duel-of-kings.userdata")
	private EntityManager entityManager;

	// Constructor(s)

	/**
	 * Default constructor for java bean specification.
	 */
	public UserDataProvider() {
		// Do nothing
	}

	// <-- DataPersistenceService --->

	@Override
	public Object save(Serializable object) {
		try {

			getEntityManager().persist(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be persisted", e);
			return false;

		}
	}

	@Override
	public boolean update(Serializable object) {
		try {

			getEntityManager().merge(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be updated", e);
			return false;

		}
	}

	@Override
	public boolean delete(Serializable object) {
		try {

			getEntityManager().remove(object);
			return true;

		} catch (Exception e) {

			logger.debug("Given object could not be deleted", e);
			return false;

		}
	}

	@Override
	public <T> List<T> getAll(Class<T> type) {
		try {

			if (type == null) throw new IllegalArgumentException("type must not be null");

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<T> query = cb.createQuery(type);
			Root<T> from = query.from(type);
			CriteriaQuery<T> all = query.select(from);

			// Execute the query

			return getEntityManager()
					.createQuery(all)
					.getResultList();

		} catch (Exception e) {

			logger.debug("An error occurred while retrieving the list of objects of the given type", e);
			return new ArrayList<>();

		}
	}

	@Override
	public <T> T getObjectById(Class<T> type, Serializable id) {
		try {

			return getEntityManager().find(type, id);

		} catch (Exception e) {

			logger.debug("Requested entity could not be retrieved", e);
			return null;

		}
	}

	@Lock(LockType.WRITE)
	public boolean saveOrUpdate(Serializable object) {
		try {

			Object objectId = null;

			try {
				objectId = getEntityManager().getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(object);
			} catch (NullPointerException e) {
				logger.debug("Given object " + object + " does currently have an id");
			}

			if (getEntityManager().contains(object)
					|| (objectId != null && getEntityManager().find(object.getClass(), objectId) != null)) {

				logger.debug("Merging: " + object);
				getEntityManager().merge(object);

			} else {

				logger.debug("Persisting: " + object);
				getEntityManager().persist(object);
			}

			return true;

		} catch (Throwable e) {

			logger.debug("Given object could not be saved", e);
			return false;

		}
	}

	// <--- UserDataService --->

	@Override
	public EnhancementSlot getEnhancementSlot(EnhancementType type, int rank) {
		try {

			if (type == null)
				throw new IllegalArgumentException("type must not be null");

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<EnhancementSlot> query = cb.createQuery(EnhancementSlot.class);
			Root<EnhancementSlot> from = query.from(EnhancementSlot.class);

			ParameterExpression<EnhancementType> typeParamter = cb.parameter(EnhancementType.class);
			Expression<EnhancementType> slotType = from.get("type");

			ParameterExpression<Integer> rankParameter = cb.parameter(Integer.class);
			Expression<Integer> slotRank = from.get("rank");

			query
			.select(from)
			.where(cb.and(
				cb.equal(slotType, typeParamter),
				cb.equal(slotRank, rankParameter)
			));

			// Execute the query

			return getEntityManager()
					.createQuery(query)
					.setParameter(typeParamter, type)
					.setParameter(rankParameter, rank)
					.getResultList().get(0);

		} catch (Exception e) {

			logger.debug("An error occurred while retrieving the list of objects of the given type", e);
			return null;

		}
	}

	@Override
	public Lineup getLineup(long lineupId) {
		try {

			if (lineupId == ModelConstants.UNDEFINED_ID) throw new IllegalArgumentException("lineupid must not be undefined");

			return getObjectById(Lineup.class, lineupId);

		} catch (Throwable e) {

			logger.debug("Requested lineup could not be retrieved", e);
			return null;

		}
	}

	@Override
	public Lineup getUserLineup(UUID accountId, String lineupName) {
		try {

			if (accountId == null) throw new IllegalArgumentException("accountId must not be null");
			if (lineupName == null) throw new IllegalArgumentException("lineupName must not be null");

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<Lineup> query = cb.createQuery(Lineup.class);
			Root<Lineup> from = query.from(Lineup.class);

			ParameterExpression<UUID> idParamter = cb.parameter(UUID.class);
			Expression<UUID> lineupAccountId = from.get("accountId");

			ParameterExpression<String> titleParameter = cb.parameter(String.class);
			Expression<String> lineupLineupName = from.get("title");

			query
			.select(from)
			.where(cb.and(
				cb.equal(lineupAccountId, idParamter),
				cb.equal(titleParameter, lineupLineupName)
			));

			// Execute the query

			return getEntityManager()
					.createQuery(query)
					.setParameter(idParamter, accountId)
					.setParameter(titleParameter, lineupName)
					.getSingleResult();

		} catch (NoResultException e) {

			return null;

		} catch (Throwable e) {

			logger.debug("An error occurred while retrieving user lineup", e);
			return null;

		}
	}

	@Override
	public List<Lineup> getUserLineups(UUID accountId) {
		try {

			if (accountId == null) throw new IllegalArgumentException("accountId must not be null");

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<Lineup> query = cb.createQuery(Lineup.class);
			Root<Lineup> from = query.from(Lineup.class);

			ParameterExpression<UUID> idParamter = cb.parameter(UUID.class);
			Expression<UUID> lineupAccountId = from.get("accountId");

			query
			.select(from)
			.where(cb.equal(lineupAccountId, idParamter));

			// Execute the query

			return getEntityManager()
					.createQuery(query)
					.setParameter(idParamter, accountId)
					.getResultList();

		} catch (Throwable e) {

			logger.debug("An error occurred while retrieving a users lineups", e);
			return new ArrayList<>();

		}
	}

	@Lock(LockType.WRITE)
	@Override
	public boolean saveLineup(Lineup lineup) {

		if (lineup == null) {
			logger.debug("Lineup must not be null");
			return false;
		}

		for (Coordinate c : lineup.getCharacterClassIds().keySet()) {
			if (!saveOrUpdate(c)) {
				logger.debug("Coordinate " + c + " of lineup " + lineup + " could not be persisted");
				return false;
			}
		}

		for (Coordinate c : lineup.getEnhancementPlans().keySet()) {
			if (!saveOrUpdate(c)) {
				logger.debug("Coordinate " + c + " of lineup " + lineup + " could not be persisted");
				return false;
			}
		}

		for (EnhancementPlan ep : lineup.getEnhancementPlans().values()) {
			if (ep == null) continue;

			for (EnhancementSlot es : ep.getEnhancementMap().keySet()) {

				if (getEntityManager().find(es.getClass(), es.getId()) == null) es.setId(0);

				if (!saveOrUpdate(es)) {
					logger.debug("EnhancementSlot " + es + " of EnhancementPlan " + ep + " of lineup " + lineup + " could not be persisted");
					return false;
				}
			}

			if (!saveOrUpdate(ep)) {
				logger.debug("EnhancementPlan " + ep + " of lineup " + lineup + " could not be persisted");
				return false;
			}
		}

		if (!saveOrUpdate(lineup.getKingPosition())) {
			logger.debug("KingPosition " + lineup.getKingPosition() + " of lineup " + lineup + " could not be persisted");
			return false;
		}

		return saveOrUpdate(lineup);

	}

	@Override
	public Account getAccount(String name) {
		try {

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

			// Build the query

			CriteriaQuery<Account> query = cb.createQuery(Account.class);
			Root<Account> from = query.from(Account.class);

			ParameterExpression<String> nameParameter = cb.parameter(String.class);
			Expression<String> accountName = from.get("name");

			query
			.select(from)
			.where(cb.equal(cb.lower(accountName), cb.lower(nameParameter)));

			// Execute the query

			return getEntityManager()
					.createQuery(query)
					.setParameter(nameParameter, name)
					.getSingleResult();

		} catch (NoResultException e) {

			return null;

		} catch (Throwable e) {

			logger.debug("Requested account could not be retrieved", e);
			return null;

		}
	}

	@Override
	public Account getAccount(UUID accountId) {
		try {

			return getEntityManager().find(Account.class, accountId);

		} catch (Throwable e) {

			logger.debug("Requested account could not be retrieved", e);
			return null;

		}
	}

	// Test Data

//	@PostConstruct
//	protected void onStartup() {
//		createGuestAccounts();
//		createTestLineups();
//	}

//	/**
//	 * Creates two basic guest accounts for testing purposes.<br>
//	 * Remove this method when a default registration mechanism is implemented.
//	 */
//	protected void createGuestAccounts() {
//
//		Account account = null;
//
//		try {
//
//			// Guest Account no. 1
//
//			String name = "Guest";
//			String email = "Guest@gangs-of-gathakar.com";
//			AccountRole accountRole = AccountRole.GUEST;
//			AccountStatus accountStatus = AccountStatus.CONFIRMED;
//			String password = "password";
//
//			if ((account = getAccount(name)) == null) account = new Account();
//
//			account.setName(name);
//			account.setEmail(email);
//			account.setRole(accountRole);
//			account.setStatus(accountStatus);
//			account.setPasswordHash(getPasswordService().generatePasswordString(password));
//
//			getEntityManager().persist(account);
//
//		} catch (Exception e) {
//
//			logger.debug("Guest accounts could not be created", e);
//
//		}
//
//		try {
//
//			logger.debug(getEntityManager().createQuery("SELECT e FROM Account e").getResultList());
//
//			// Guest Account no. 2
//
//			String name = "Guest2";
//			String email = "Guest@gangs-of-gathakar.com";
//			AccountRole accountRole = AccountRole.GUEST;
//			AccountStatus accountStatus = AccountStatus.CONFIRMED;
//			String password = "password";
//
//			if ((account = getAccount(name)) == null) account = new Account();
//
//			account.setName(name);
//			account.setEmail(email);
//			account.setRole(accountRole);
//			account.setStatus(accountStatus);
//			account.setPasswordHash(getPasswordService().generatePasswordString(password));
//
//			getEntityManager().persist(account);
//
//		} catch (Exception e) {
//
//			logger.debug("Guest accounts could not be created", e);
//
//		}
//
//	}

	/**
	 * Creates one basic lineup for each test account.<br>
	 * Remove this method when a default registration mechanism is implemented.
	 */
	protected void createTestLineups() {

		try {

			Lineup lineup = null;
			Account account = null;

			// Create Coordinates

			Coordinate c_0_0 = new Coordinate(0, 0);
			Coordinate c_0_1 = new Coordinate(0, 1);
			Coordinate c_0_2 = new Coordinate(0, 2);
			Coordinate c_1_0 = new Coordinate(1, 0);
			Coordinate c_1_1 = new Coordinate(1, 1);
			Coordinate c_1_2 = new Coordinate(1, 2);
			Coordinate c_2_0 = new Coordinate(2, 0);
			Coordinate c_2_1 = new Coordinate(2, 1);
			Coordinate c_2_2 = new Coordinate(2, 2);

			getEntityManager().persist(c_0_0);
			getEntityManager().persist(c_0_1);
			getEntityManager().persist(c_0_2);
			getEntityManager().persist(c_1_0);
			getEntityManager().persist(c_1_1);
			getEntityManager().persist(c_1_2);
			getEntityManager().persist(c_2_0);
			getEntityManager().persist(c_2_1);
			getEntityManager().persist(c_2_2);

			logger.info("Attempting to create test lineup ...");

			// Get Account

			try {
				account = getAccount("Guest");
			} catch (Exception e) {
				logger.error("Guest-Account could not be retrieved", e);
			}

			if (account != null) {

				// Create Lineup

				String lineupTitle = "Offensiv";

				if ((lineup = getUserLineup(account.getId(), lineupTitle)) == null) lineup = new Lineup();

				// Create Enhancement Plans

//				EnhancementPlan ep = new EnhancementPlan();
//				ep.getEnhancementMap().put(getGameDataService().getEnhancementSlot(EnhancementType.CRYSTAL, 1), 1);
//				saveOrUpdate(ep);
//				lineup.getEnhancementPlans().put(c_0_0, ep);

				// Define Lineup

				lineup.setAccountId(account.getId());
				lineup.setTitle(lineupTitle);
				lineup.setImageKey("Offensive");
				lineup.setKingPosition(c_1_1);
				lineup.getCharacterClassIds().put(c_0_0, CharacterClassIds.SWORD_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_0_1, CharacterClassIds.AXE_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_0_2, CharacterClassIds.SWORD_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_1_0, CharacterClassIds.BOW_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_1_1, CharacterClassIds.CROSSBOW_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_1_2, CharacterClassIds.BOW_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_2_0, CharacterClassIds.STAFF_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_2_1, CharacterClassIds.HANDCANNON_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_2_2, CharacterClassIds.STAFF_CHARACTER_CLASS_ID);

				try {
					saveLineup(lineup);
					logger.info("Test lineup has been successfully saved");
				} catch (Exception e) {
					logger.error("Testlineup could not be saved", e);
				}

			} else logger.warn("Test lineup could not be created, because there is no test account");

			// Get Account2

			try {
				account = getAccount("Guest2");
			} catch (Exception e) {
				logger.error("Guest-Account could not be retrieved", e);
			}

			if (account != null) {

				// Create Lineup

				String lineupTitle = "Offensiv";

				if ((lineup = getUserLineup(account.getId(), lineupTitle)) == null) lineup = new Lineup();

				lineup.setAccountId(account.getId());
				lineup.setTitle(lineupTitle);
				lineup.setImageKey("Offensive");
				lineup.setKingPosition(c_1_1);
				lineup.getCharacterClassIds().put(c_0_0, CharacterClassIds.SWORD_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_0_1, CharacterClassIds.AXE_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_0_2, CharacterClassIds.SWORD_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_1_0, CharacterClassIds.BOW_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_1_1, CharacterClassIds.CROSSBOW_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_1_2, CharacterClassIds.BOW_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_2_0, CharacterClassIds.STAFF_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_2_1, CharacterClassIds.HANDCANNON_CHARACTER_CLASS_ID);
				lineup.getCharacterClassIds().put(c_2_2, CharacterClassIds.STAFF_CHARACTER_CLASS_ID);

				try {
					saveLineup(lineup);
					logger.info("Test lineup has been successfully saved");
				} catch (Exception e) {
					logger.error("Testlineup could not be saved", e);
				}

			} else logger.warn("Test lineup could not be created, because there is no test account");

		} catch (Throwable e) {

			logger.error("Test lineup could not be created", e);

		}
	}

	// Getter & Setter

	protected final PasswordService getPasswordService() {
		return passwordService;
	}

	protected final void setPasswordService(PasswordService passwordService) {
		this.passwordService = passwordService;
	}

	protected final EntityManager getEntityManager() {
		return entityManager;
	}

}
