Setting up a new package:
1. create new folder
2. enter folder
3. run npm init
	3.1 Add types to package.json
		"types": "dist/index.d.ts",
4. run tsc --init
	4.1 Add declarations to compilation
		"declaration": true,
		
		
Sources:
https://www.tsmean.com/articles/how-to-write-a-typescript-library/