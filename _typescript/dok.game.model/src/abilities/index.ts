import { ImpactMap, ImpactFilter } from "../impact-filters";
import { Effect } from "../effects";
import { Ability } from '../ability.model';
import { Character } from "../character.model";
import { GameState } from '../game-state.model';
import { TargetFilter } from "../target-filter.model";

export interface AbilityType {

    resolve(args: {
        state: GameState,
        ability: Ability,
        targets: ImpactMap
    }): Array<Effect>

    canCast(args: {
        state: GameState,
        Character: Character,
        ability: Ability
    }): boolean;

    getTargetFilter(args: {
        state: GameState,
        character: Character,
        ability: Ability
    }): TargetFilter

    getImpactFilter(args: {
        state: GameState,
        character: Character,
        ability: Ability
    }): ImpactFilter

}

export enum AbilityTypes {
    
}

export const AbilityTypeMap: 
{readonly [abilityTypeName in AbilityTypes]?: AbilityType} = {

}
