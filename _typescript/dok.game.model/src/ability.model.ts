import { AbilityType } from './abilities/index';

export interface Ability {
    readonly type: AbilityType;
    readonly ownerId: string;
    readonly timeCounters: number;
}