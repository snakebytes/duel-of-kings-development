import { Target } from "./playfield.model";
import { AbilityTypes } from './abilities/index';

export interface Action {
    readonly sourceCharacterId: string;
    readonly ability: AbilityTypes;
    readonly target: Target;
}