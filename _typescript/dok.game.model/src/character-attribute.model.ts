export enum CharacterAttribute {
    ATTACK_DAMAGE = "attack_damage",
    HEALTH = "health",
    ARMOR = "armor",
    MAGIC_RESISTANCE = "magic_resistance"
}

export type CharacterAttributeMap = {
    readonly [attributeName in CharacterAttribute]: number;
}