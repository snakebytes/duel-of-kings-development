import { CharacterClasses } from "./characters";
import { Ability } from "./ability.model";
import { PlayerId } from './player.model';
import { CharacterClassMap } from './characters/index';
import { CharacterAttribute } from './character-attribute.model';

export interface Character {
    readonly type: CharacterClasses;
    readonly id: string;
    readonly ownerId: PlayerId;
    readonly abilities: Array<Ability>;
    readonly timeCounters: number;
    readonly damageTaken: number;
}

export function getCharacterClass(character: Character) {
    const characterClass = CharacterClassMap[character.type];
    if (characterClass) {
        return characterClass;
    } else {
        throw new Error(`Unkown character class ${character.type}`);
    }
}

export function getCharacterBaseAttribute(character: Character, attribute: CharacterAttribute) {
    return getCharacterClass(character).getBaseAttributes()[attribute];
}

export function getCharacterHealth(character: Character) {
    const health = getCharacterBaseAttribute(character, CharacterAttribute.HEALTH);
    return Math.max(0, health - character.damageTaken);
}

export function isCharacterDead(character: Character) {
    return getCharacterHealth(character) === 0;
}

export function isCharacterAlive(character: Character) {
    return !isCharacterDead(character);
}