import { CharacterAttributeMap } from "../character-attribute.model";
import { AbilityType } from "../abilities";

export interface CharacterClass {
    getBaseAttributes(): CharacterAttributeMap;
    getAbilityTypes(): Array<AbilityType>;
}

export enum CharacterClasses {
    AXE = "axe",
    SWORD = "sword",
    SPEAR = "spear",
    CROSSBOW = "crossbow",
    BOW = "bow",
    CANON = "canon"
}

export const CharacterClassMap: 
{readonly [characterClassName in CharacterClasses]?: CharacterClass} = {

}