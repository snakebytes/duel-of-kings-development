export enum DamageType {
    PHYSICAL = "physical",
    MAGICAL = "magical"
}