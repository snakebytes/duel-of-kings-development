import { ImpactMap } from "../impact-filters";
import { Ability } from "../ability.model";
import { GameState } from '../game-state.model';

export interface Effect {

    resolve(args: {
        state: GameState;
        source: EffectSource;
        targets: ImpactMap;
    }): Array<Effect> | null;
}

export type EffectSource = Ability | Effect;

export enum Effects {
    
}

export const EffectMap: 
{readonly [effectName in Effects]?: Effect} = {

}