import { Playfield } from "./playfield.model";
import { Player, PlayerId } from './player.model';
import { Position } from './position.model';
import { Character } from "./character.model";

export interface GameState {
    readonly players: [Player, Player];
    readonly playfield: Playfield;
    readonly playerOnTurn: Player;
    readonly playerOnPriority: Player;
    readonly roundCounter: number;
    readonly turnTimer: number;
    readonly chooseTimer: number;
}

export function getStartingPlayer(state: GameState) {
    return state.players[0];
}

export function getPlayfieldSide(state: GameState, playerId: PlayerId) {
    return state.playfield[playerId];
}

export function getPlayfieldSideCharacters(state: GameState, playerId: PlayerId) {
    const playfieldSide = getPlayfieldSide(state, playerId);
    const result: Character[] = [];
    for (const position in playfieldSide) {
        const character = playfieldSide[<Position> position];
        result.push(character);
    }
    return result;
}

export function getCharacters(state: GameState) {
    return [
        ...getPlayfieldSideCharacters(state, PlayerId.ONE),
        ...getPlayfieldSideCharacters(state, PlayerId.TWO)
    ]
}

export function getCharacterAt(state: GameState, playerId: PlayerId, position: Position) {
    return getPlayfieldSide(state,playerId)[position];
}

export function getCharacterById(state: GameState, characterId: string, playerId?: PlayerId) {
    let toSearch: Character[];
    if (playerId) {
        toSearch = getPlayfieldSideCharacters(state, playerId);
    } else {
        toSearch = getCharacters(state);
    }
    const found = toSearch.filter(character => character.id === characterId);
    if (Array.isArray(found)) {
        switch (found.length) {
            case 1:
                return found[0];
            case 0:
                throw new Error(`No character found for id ${characterId}.`);
            default:
                throw new Error(`More than one character with id ${characterId} found.`);
        }
    } else {
        throw new Error(`No character found for id ${characterId}.`);
    }
}