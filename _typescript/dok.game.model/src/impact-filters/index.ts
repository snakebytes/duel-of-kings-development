import { ImpactType } from "../impact-type.model";
import { Position } from '../position.model';

export interface ImpactFilter {

    generateFrom(args: {
        targets: Array<Position>
    }): ImpactMap;

}

export type ImpactMap = { readonly [position in Position]?: ImpactType }

export enum ImpactFilters {
    PRIMARY = "primary",
    SECONRARY = "secondary",
    TERTIARY = "tertiary",
    SELF = "self",
    ROW = "row",
    ADJACENT_IN_ROW = "adjacent_in_row",
    COLUMN = "column",
    ADJACENT_IN_COLUMN = "adjacent_in_column",
    PIERCING_SINGLE = "piercing_single",
    PIERCING_DOUBLE = "piercing_double"
}

export const ImpactFilterMap: 
{readonly [targetFilterName in ImpactFilters]?: ImpactFilter} = {

}