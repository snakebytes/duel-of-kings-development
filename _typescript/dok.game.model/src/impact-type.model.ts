export enum ImpactType {
    PRIMARY,
    SECONDARY,
    TERTIARY
}