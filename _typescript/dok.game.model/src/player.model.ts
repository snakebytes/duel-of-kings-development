export interface Player {
    readonly id: PlayerId;
}

export enum PlayerId {
    ONE,
    TWO
}