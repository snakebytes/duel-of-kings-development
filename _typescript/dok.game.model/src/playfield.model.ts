import { Position } from './position.model';
import { Character } from './character.model';
import { PlayerId } from './player.model';

export type PlayfieldSide = { readonly [position in Position]: Character };
export type Playfield = { readonly [playerId in PlayerId]: PlayfieldSide };
export type Target = [PlayerId, Position];