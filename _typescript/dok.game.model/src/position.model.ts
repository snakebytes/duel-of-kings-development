/**
 * 0 = Front
 * 1 = Middle
 * 3 = Back
 * 
 * Landscape:
 * Column x Row
 * 
 * Bottom to Top
 * 
 * Portrait:
 * Row x Column
 * 
 * Left to Right
 */
export enum Position {
    _0X0 = "0x0",
    _0X1 = "0x1",
    _0X2 = "0x2",
    _1X0 = "1x0",
    _1X1 = "1x1",
    _1X2 = "1x2",
    _2X0 = "2x0",
    _2X1 = "2x1",
    _2X2 = "2x2",
}

export function isInFront(position: Position) {
    return position === Position._0X0 ||
            position === Position._0X1 ||
            position === Position._0X2;
}

export function isInBack(position: Position) {
    return position === Position._2X0 ||
            position === Position._2X1 ||
            position === Position._2X2;
}