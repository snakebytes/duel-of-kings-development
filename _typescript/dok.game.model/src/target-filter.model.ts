import { Ability } from "./ability.model";
import { GameState } from "./game-state.model";
import { Player, PlayerId } from './player.model';
import { Position } from './position.model';

/**
 * A target filter describes legal targets for a given action.
 */
export interface TargetFilter {

    test(args: TargetFilterTestArgs): boolean;

}

export interface TargetFilterTestArgs {
    readonly state: GameState;
    readonly source: Ability;
    readonly targetPlayerId: PlayerId;
    readonly position: Position;
}