import { TargetFilter } from "../target-filter.model";
import { getCharacterAt } from '../game-state.model';
import { isCharacterAlive } from "../character.model";

export const AliveTargetFilter: TargetFilter = {

    test({
        state,
        targetPlayerId,
        position
    }) {
        return isCharacterAlive(getCharacterAt(state, targetPlayerId, position));
    }

};