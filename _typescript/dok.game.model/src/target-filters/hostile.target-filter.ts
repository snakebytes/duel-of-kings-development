import { TargetFilter } from "../target-filter.model";
import { getCharacterById } from "../game-state.model";

export const HostileTargetFilter: TargetFilter = {

    test({
        state,
        source,
        targetPlayerId
    }) {
        const caster = getCharacterById(state, source.ownerId);
        return caster.ownerId !== targetPlayerId;
    }

};