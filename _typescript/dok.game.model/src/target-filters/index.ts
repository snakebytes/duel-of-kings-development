import { TargetFilter, TargetFilterTestArgs } from '../target-filter.model';
import { FriendlyTargetFilter } from './friendly.target-filter';
import { HostileTargetFilter } from './hostile.target-filter';
import { AliveTargetFilter } from './alive.target-filter';
import { SemiRangedTargetFilter } from './semi-ranged.target-filter';
import { MeeleTargetFilter } from './meele.target-filter';
import { NotSelfTargetFilter } from './not-self.target-filter';

export enum TargetFilters {
    FRIENDLY = "friendly",
    HOSTILE = "hostile",
    MEELE = "meele",
    SEMI_RANGED = "semi_ranged",
    NOT_SELF = "not_self",
    ALIVE = "alive"
}

export const TargetFilterMap: 
{
    readonly [targetFilterName in TargetFilters]?: TargetFilter
} = {
    friendly: FriendlyTargetFilter,
    hostile: HostileTargetFilter,
    alive: AliveTargetFilter,
    semi_ranged: SemiRangedTargetFilter,
    meele: MeeleTargetFilter,
    not_self: NotSelfTargetFilter
}

export function targetFilterAnd(args: TargetFilterTestArgs, ...targetFilters: TargetFilter[]) {
    for (const targetFilter of targetFilters) {
        if (!targetFilter.test(args)) {
            return false;
        }
    }
    return true;
}