import { TargetFilter } from "../target-filter.model";
import { isInFront } from '../position.model';

export const MeeleTargetFilter: TargetFilter = {

    test({
        position
    }) {
        return isInFront(position)
    }

};