import { TargetFilter } from "../target-filter.model";
import { getCharacterAt, getCharacterById } from '../game-state.model';
import { isCharacterAlive } from "../character.model";

export const NotSelfTargetFilter: TargetFilter = {

    test({
        state,
        source,
        targetPlayerId,
        position
    }) {
        const target = getCharacterAt(state, targetPlayerId, position);
        return source.ownerId !== target.id;
    }

};