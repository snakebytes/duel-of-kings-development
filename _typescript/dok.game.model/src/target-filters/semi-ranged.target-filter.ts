import { TargetFilter } from "../target-filter.model";
import { isInBack } from '../position.model';

export const SemiRangedTargetFilter: TargetFilter = {

    test({
        position
    }) {
        return !isInBack(position);
    }

};